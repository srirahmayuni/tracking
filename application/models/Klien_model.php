<?php

class Klien_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'klien';
    }
    public function getSql2()
	{
		
			return "select 
			    a.*
		        from klien a
				join users s on s.users_id = a.created_by
                where 1=1 			
		      ";
		
		
	}
   
    public function getSql()
	{
		//$id=0;
		$id = $this->session->userdata('users_id');
		if($this->session->userdata('group_id')==1)
		{
			
			return "select 
			    a.*
		        from klien a
				join users s on s.users_id = a.created_by
                where 1=1 			
		      ";
			
		}
        else
        {
			return "select a.*
					from klien a
					join users s on s.users_id = a.created_by
					where 1=1  and a.created_by=$id		 
		      ";
		}			
		
	}
	public function GetByID($id)
	{
       $r = $this->db->query( "select a.*
		        from klien a
				where a.id_klien='$id'")->row(); 			
		return $r;
	}

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('id_klien', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('id_klien', $id);
        $this->db->delete($this->tableName);
    }


    

}
