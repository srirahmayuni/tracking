<?php

class Groups_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'groups';
    }

    public function get_dropdown_list() {
        // ambil data dari db
        $this->db->order_by('name', 'asc');
        $result = $this->db->get($this->tableName);

        $return = $result->result();

        return $return;
    }

    /* public function get_dropdown_listR() {
        // ambil data dari db
        // untuk sekarang group hanya ADMIN
        $reseller_id = NULL;
        $coordinator_id = NULL;
        $group_id = NULL;
        if ($this->session->userdata('reseller_id'))
            $reseller_id = $this->session->userdata('reseller_id');

        if ($this->session->userdata('coordinator_id'))
            $coordinator_id = $this->session->userdata('coordinator_id');

        if ($this->session->userdata('group_id'))
            $group_id = $this->session->userdata('group_id');

        $arr = NULL;
        if ($group_id == 2) {
            if ($reseller_id)
                $arr = array(2, 8);
        }
        else if ($group_id == 3) {
            if ($coordinator_id)
                $arr = array(3, 7);
        }
        $this->db->order_by('name', 'asc');
        $this->db->where_in('group_id', $arr);
        $result = $this->db->get($this->tableName);

        $return = $result->result();

        return $return;
    } */
	
	public function get_dropdown_listR() {
        // ambil data dari db
        // untuk sekarang group hanya ADMIN
        // $reseller_id = NULL;
        // $coordinator_id = NULL;
        // $group_id = NULL;
        // if ($this->session->userdata('reseller_id'))
            // $reseller_id = $this->session->userdata('reseller_id');

        // if ($this->session->userdata('coordinator_id'))
            // $coordinator_id = $this->session->userdata('coordinator_id');

        // if ($this->session->userdata('group_id'))
            // $group_id = $this->session->userdata('group_id');

        // $arr = NULL;
        // if ($group_id == 2) {
            // if ($reseller_id)
                // $arr = array(2, 8);
        // }
        // else if ($group_id == 3) {
            // if ($coordinator_id)
                // $arr = array(3, 7);
        // }
        $this->db->order_by('name', 'asc');
        //$this->db->where_in('group_id', $arr);
        $result = $this->db->get($this->tableName);

        $return = $result->result();

        return $return;
    }

    public function getGroupName($group_id) {
        $return = null;
        // ambil data dari db
        $this->db->select('name');
        $this->db->where('group_id', $group_id);

        $result = $this->db->get($this->tableName);
        // $query = $this->db->last_query();
        // echo "q = ".$query;exit;

        $ret = $result->row();
        if (isset($ret)) {
            $return = $ret->name;
        }

        return $return;
    }
    public function getGroupNameNote($group_id) {
        $return = null;
        // ambil data dari db
        $this->db->select('note');
        $this->db->where('group_id', $group_id);

        $result = $this->db->get($this->tableName);
        // $query = $this->db->last_query();
        // echo "q = ".$query;exit;

        $ret = $result->row();
        if (isset($ret)) {
            $return = $ret->name;
        }

        return $return;
    }
    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $group_id) {
        $this->db->where('group_id', $group_id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($group_id) {
        $this->db->where('group_id', $group_id);
        $this->db->delete($this->tableName);
    }

}
