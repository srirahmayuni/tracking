<?php

class Page_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'pages';
    }

    public function getName($_id) {
        $return = null;
        // ambil data dari db
        $this->db->select('controller');
        $this->db->where('page_id', $_id);

        $result = $this->db->get($this->tableName);
        // $query = $this->db->last_query();
        // echo "q = ".$query;exit;

        $ret = $result->row();
        if (isset($ret)) {
            $return = $ret->controller;
        }

        return $return;
    }

    public function ComboHalaman() {

        $result = $this->db->query('select * from pages');
        return $result->result_array();
    }

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('page_id', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('page_id', $id);
        $this->db->delete($this->tableName);
    }
	
	public function getSql($req)
	{
		
		$sq = '';
		if($req['gr_id'])
		{	
			//$sq="and b.group_id = ".$req['gr_id']." or b.group_id is null";
			//$sq="and b.group_id = ".$req['gr_id']."";
			$sq="where a.group_id = ".$req['gr_id']."";
        }
		//else
		//	$sq="where b.group_id= '' ";
		
		
		 $sql = "
		 
		     select * from 
			 (
		        select * from (    
				Select 
				c.controller,a.allow,b.name,a.privilege_page_id,c.page_id,b.group_id
				from 
				privilege_page a
				left join groups b on b.group_id=a.group_id
				left join pages c on c.page_id=a.page_id
				 $sq
				union all
				
				select controller, '' as allow,'' as name,'',page_id,''
				from pages
				
				) as zz group by zz.controller
              )	as zz where 1=1 
				
				"; 
		/* $sql = "Select 
				a.controller,b.allow,c.name,b.privilege_page_id,a.page_id
				from  
				pages a
				left join privilege_page b on b.page_id=a.page_id
				left join groups c on c.group_id=b.group_id
				
				where 1=1 $sq
				";		 */
		return $sql;    //$sq
	}

}
