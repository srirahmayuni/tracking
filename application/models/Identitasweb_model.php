<?php

class Identitasweb_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'identitas';
    }

    public function getData($_id) {
        $return = null;
        // ambil data dari db
        $this->db->select('*');
        $this->db->where('id_identitas', $_id);

        $result = $this->db->get($this->tableName);
        

        $ret = $result->row();
        if (isset($ret)) {
            $return = $ret;//->action;
        }

        return $return;
    }

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('id_identitas', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('id_identitas', $id);
        $this->db->delete($this->tableName);
    }

    

}
