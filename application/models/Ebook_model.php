<?php

class Ebook_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'tbl_ebook';
    }
   
    public function getSql()
	{
		$id = $this->session->userdata('users_id');
		if($this->session->userdata('group_id')==1)
		{
			
			return "select 
			    a.*
		        from tbl_ebook a
				join users s on s.users_id = a.created_by
                where 1=1 			
		      ";
			
		}
        else
        {
			return "select a.*
					from tbl_ebook a
					join users s on s.users_id = a.created_by
					where 1=1  and a.created_by=$id		 
		      ";
		}			
		
	}
	 public function getSql2()
	{
		
			return "select 
			    a.*
		        from tbl_ebook a
				join users s on s.users_id = a.created_by
                where 1=1 			
		       ";
			
		
	}
	public function GetByID($id)
	{
       $r = $this->db->query( "select a.*
		        from tbl_ebook a
				where a.ebook_id='$id'")->row(); 			
		return $r;
	}

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('ebook_id', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('ebook_id', $id);
        $this->db->delete($this->tableName);
    }


    function get_file_byid($id){
		$hsl=$this->db->query("SELECT ebook_id,file_judul,file_deskripsi,DATE_FORMAT(created_date,'%d/%m/%Y') AS tanggal,file_download,file_data FROM tbl_ebook WHERE ebook_id='$id'");
		return $hsl;
	}

}
