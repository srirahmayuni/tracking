<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends MY_Model {

    /**
     * @vars
     */
    private $tableName;

    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();

        // define primary table
        $this->tableName = 'users';
    }

    /**
     * @param null $username
     * @param null $password
     * @return mixed
     * Created BY irwan
     */
    /* function login_customer($username = NULL, $password = NULL) {
        $query = $this->db->query("select * from users where id='" . $username . "' or email='" . $username . "'");
        if ($query->num_rows()) {
            $results = $query->row_array();
            $salted_password = hash('sha512', $password . $results['salt']);

            if ($results['password'] == $salted_password) {
                unset($results['password']);
                unset($results['salt']);

                return $results;
            }
        }
    } */

    
   /*  public function checkCustomerEmail($email) {
        // cek customer berdasarkan email
        $return = null;
        $query = $this->db->query("SELECT * FROM users
							WHERE email = '" . $email . "'
							AND group_id = 4
							");
        $ret = $query->row();
        if (isset($ret)) {
            $return = $ret;
        }
        return $return;
    } */

    /**
     * @param $token
     * @return null
     */
    public function checkToken($token) {
        $return = null;
        $query = $this->db->query("SELECT * FROM users
							WHERE token = '" . $token . "'
							AND token_status = 0
							");
        $ret = $query->row();
        if (isset($ret)) {
            $return = $ret;
        }
        return $return;
    }

    function login($username = NULL, $password = NULL) {
        // login ini tdk support group customer (group_id = 4)
        if ($username && $password) {
            $sql = "
                SELECT
                    users.users_id 
					, users.group_id
					, users.id
					, users.password
					, users.salt
					, users.first_name
					, users.last_name
					, users.email
					, users.active
				FROM {$this->tableName}
				WHERE (users.id = " . $this->db->escape($username) . "
                        OR users.email = " . $this->db->escape($username) . ")
                    AND users.active = '1' 
                    AND users.flag_delete = 0 
					
                LIMIT 1
            ";

            $query = $this->db->query($sql);
            // $q = $this->db->last_query();
            // echo "q = ".$q;exit;
            if ($query->num_rows()) {
                $results = $query->row_array();
                $salted_password = hash('sha512', $password . $results['salt']);
                //echo $results['password']."<br>";
                //echo $salted_password."<br>";
				//exit;
				/*
				$salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                $password = hash('sha512', $adminUsersPassword . $salt);
                $dataUsers = array(
                    'group_id' => $adminUsersGroup,
                    'id' => $adminUsersMobile,
                    'password' => $password,
                    'salt' => $salt,
                    'first_name' => strtoupper($adminUsersFirstName),
                    'email' => $adminUsersEmail,
                    'active' => $adminUsersActive
                );
				
				
				*/
				
				
				
                if ($results['password'] == $salted_password) {
                    unset($results['password']);
                    unset($results['salt']);

                    return $results;
                }
            }
        }

        return FALSE;
    }
    public function checkUserRegEmail($regEmail) {
     
        $return = null;
        $query = $this->db->query("SELECT email FROM users 
							WHERE email = '" . $regEmail . "' 
							AND group_id <> 4 
							");
        $ret = $query->row();
        if (isset($ret))
            $return = $ret->email;
        //echo $this->db->last_query();exit;					
        return $return;
    }
	
   public function checkUserRegTlp($regTlp) {
        // cek no tlp untuk group selain customer (4)
        $return = null;
        $query = $this->db->query("SELECT id FROM users 
							WHERE id = '" . $regTlp . "' 
							AND flag_delete<>1 

						");

        $ret = $query->row();

        if (isset($ret))
            $return = $ret->id;
        return $return;
    }
	
   
    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function getDataUsers($users_id) {
        // ambil data dari db
        //$this->db->order_by('name', 'asc');
        $this->db->where('users_id', $users_id);
        $result = $this->db->get($this->tableName);

        $return = $result->row();

        return $return;
    }
     public function checkUserEmailCoordinator($regEmail, $users_id) {
        // cek no tlp untuk group selain customer (4)
        $return = null;
        $query = $this->db->query("SELECT email FROM users 
							WHERE email = '" . $regEmail . "' 
							AND group_id <> 4
							AND users_id <> '" . $users_id . "'"
        );
        $ret = $query->row();
        if (isset($ret))
            $return = $ret->email;
        //echo $this->db->last_query();exit;					
        return $return;
    }
	 public function checkUserTlpCoordinator($regTlp, $users_id) {
        // cek no tlp untuk group selain customer (4)
        $return = null;
        $query = $this->db->query("SELECT id FROM users 
							WHERE id = '" . $regTlp . "'
							AND group_id <> 4 and flag_delete=0 
							AND users_id <> '" . $users_id . "'");

        $ret = $query->row();
        if (isset($ret))
            $return = $ret->id;
        return $return;
    }
    /*   public function checkUserRegTlp2($regTlp,$is,$id) {
        // cek no tlp untuk group selain customer (4)
        $return = null;
		if($is)
		{
			    $query = $this->db->query("SELECT id FROM users 
							WHERE id = '" . $regTlp . "' 
							AND flag_delete<>1 

						");

				$ret = $query->row();

				if (isset($ret))
				{	
					//$return = $ret->id;
				    //return $return;
				    return false;
			    }
				else
				{
					return true;
				}	
		}
        else
        {
			   $query = $this->db->query("SELECT id FROM users 
							WHERE id = '" . $regTlp . "' AND users_id<>$id
							AND flag_delete<>1 

						");

				$ret = $query->row();

				if (isset($ret))
				{	
					//$return = $ret->id;
				    //return $return;
				    return false;
			    }
				else
				{
					return true;
				}	
		}			
		
		
       
    }
   */
   
   /*	
   public function checkUserRegEmail2($regEmail) {
       
        $return = null;
        $query = $this->db->query("SELECT email FROM users 
							WHERE email = '" . $regEmail . "' 
							");
        $ret = $query->row();
        if (isset($ret))
            $return = $ret->email;
        //echo $this->db->last_query();exit;					
        return $return;
    }


   

     public function getDataUsers2($users_id) {
        $reseller_id = NULL;
        if ($this->session->userdata('reseller_id'))
            $reseller_id = $this->session->userdata('reseller_id');

        $r1 = array('users.users_id' => $users_id);
        if ($reseller_id)
            $r1 = array('users.users_id' => $users_id, 'userreseller.reseller_id' => $reseller_id);
        else
            $r1 = array('users.users_id' => $users_id);

        // $array = array('name' => $name, 'title' => $title, 'status' => $status);
        // ambil data dari db
        //$this->db->order_by('name', 'asc');
        $this->db->select('users.*, userreseller.reseller_id ');
        $this->db->from($this->tableName);
        $this->db->join('userreseller', 'userreseller.users_id = users.users_id');
        $this->db->where($r1);
        $result = $this->db->get();
        // $q = $this->db->last_query();
        // echo $q;exit;
        $return = $result->row();

        return $return;
    }

    public function checkUserProfEmail($regEmail) {
        
        $return = null;
        $users_id = $this->session->userdata('users_id');
        $query = $this->db->query("SELECT email FROM users 
							WHERE email = '" . $regEmail . "' 
							AND group_id <> 4 
							AND users_id <> '" . $users_id . "'
						");
        $ret = $query->row();
        if (isset($ret))
            $return = $ret->email;
        //echo $this->db->last_query();exit;					
        return $return;
    } */

   /* 

    public function checkUserProfID($id) {
        // cek id untuk group selain customer (4)
        $return = null;
        $users_id = $this->session->userdata('users_id');
        $query = $this->db->query("SELECT id FROM users 
							WHERE id = '" . $id . "'
							AND group_id <> 4 
							AND users_id <> '" . $users_id . "'");

        $ret = $query->row();
        if (isset($ret))
            $return = $ret->id;
        return $return;
    }

   
 */
    public function update($data, $users_id) {
        $this->db->where('users_id', $users_id);

        return parent::_update($this->tableName, $data);

    }
    public function delete($users_id) {
        $this->db->where('users_id', $users_id);
        $this->db->delete($this->tableName);
    }

    public function getAll() {
     
									
		 $query = "
									SELECT users.users_id
										   , users.id	
										   , groups.name
										   , users.first_name
										   , users.last_name
										   , users.email
										   , users.active
									FROM users
									JOIN groups ON groups.group_id = users.group_id
									where 1=1";							
       
        

        return $query;
    }
    /* 
    function all_dataGetAll() {
        $reseller_id = NULL;
        if ($this->session->userdata('reseller_id'))
            $reseller_id = $this->session->userdata('reseller_id');

        $r1 = '';
        if ($reseller_id)
            $r1 = ' AND userreseller.reseller_id =' . $reseller_id . ' ';

        $query = $this->db->query("
									SELECT users.users_id
										   , users.id	
										   , groups.name
										   , users.first_name
										   , users.last_name
										   , users.email
										   , users.active
									FROM users
									JOIN groups ON groups.group_id = users.group_id
									JOIN userreseller ON userreseller.users_id = users.users_id
									WHERE users.group_id NOT IN (1, 3, 4, 5)
									AND users.users_id NOT IN (20, 22)
									" . $r1 . "
									");
        return $query->num_rows();
    }

    public function updateByEmail($data, $email) {
        $this->db->where('email', $email);

        if ($this->db->update($this->tableName, $data))
            return true;
        else
            return false;
    }

    public function deletes($users_ids) {
        $this->db->where_in('users_id', $users_ids);
        $this->db->delete($this->tableName);
    }

    public function getGroupIdByUserId($users_id) {
        $return = null;
        // ambil data dari db
        $this->db->select('group_id');
        $this->db->where('users_id', $users_id);

        $result = $this->db->get($this->tableName);
        // $query = $this->db->last_query();
        // echo "q = ".$query;exit;

        $ret = $result->row();
        if (isset($ret)) {
            $return = $ret->group_id;
        }

        return $return;
    } */

    /* method for mobile */
   /*  public function getUserIdByMobile($id) {

        $return = null;
        // ambil data dari db
        $this->db->select('users_id');
        $this->db->where('id', $id);

        $result = $this->db->get($this->tableName);
        // $query = $this->db->last_query();
        // echo "q = ".$query;exit;

        $ret = $result->row();
        if (isset($ret)) {
            $return = $ret->users_id;
        }

        return $return;
    }


     public function updateFlag($id)
	 {
		 $this->db->where('users_id', $id);
         $data = array(
		          'active'=>0,
		          'flag_delete'=>1
		         ); 
         return parent::_update($this->tableName, $data);
		 
	 } */
}
