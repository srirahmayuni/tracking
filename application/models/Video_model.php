<?php

class Video_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'video';
    }
    
    public function getSql()
	{
		$id = $this->session->userdata('users_id');
		if($this->session->userdata('group_id')==1)
		{
			
			return "select 
			    a.*,b.jdl_playlist
		        from video a
		        left join  playlist b on b.id_playlist=a.id_playlist
				left join users s on s.users_id = a.created_by
                where 1=1 			
		      ";
			
		}
        else
        {
			return "select
         			a.*,b.jdl_playlist
					from video a
					left join  playlist b on b.id_playlist=a.id_playlist
					left join users s on s.users_id = a.created_by
					where 1=1  and a.created_by=$id		 
		      ";
		}			
		
	}
	public function getById($id)
	{
       $r = $this->db->query( "select a.*
		        from video a
				where a.id_video='$id'")->row(); 			
		return $r;
	}
    public function playlist()
	{
       $r = $this->db->query( "select *
		        from playlist 
				")->result(); 			
		return $r;
	}
	
	
	
    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('id_video', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('id_video', $id);
        $this->db->delete($this->tableName);
    }


    public function GetCheckboxes($table, $key, $Label, $Nilai='')
	{
	  $s = "select * from $table order by nama_tag";
	  
	  
	  $r =$this->db->query($s)->result_array(); 
	  $_arrNilai = explode(',', $Nilai);
	  $str = '';
	  foreach($r as $w)
	  {
		  $_ck = (array_search($w[$key], $_arrNilai) === false)? '' : 'checked';
		  $str .= "<input class='minimal' type=checkbox name='".$key."[]' value='$w[$key]' $_ck> $w[$Label] ";
	  }
	  
	  return $str;
	} 
    
	public function hapusvideo($id)
	{
		
		$sss = $this->db->query("SELECT video FROM video WHERE id_video='$id'");
		$d   	= $sss->result_array();	
		
		unlink('./assets/images/img_video/'.$d[0]['video']);
    }
	
	public function hapusgambar($id)
	{
		$cccc = $this->db->query("SELECT gbr_video FROM video WHERE id_video='$id'");
		$d   	=  $cccc->result_array();
		unlink('./assets/images/img_video/'.$d[0]['gbr_video']);
		unlink('./assets/images/img_video/kecil_'.$d[0]['gbr_video']);
    }
}
