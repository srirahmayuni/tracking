<?php

class Dashboard_model extends MY_Model {

   private $tableName;

   public function __construct() {
        parent::__construct();
       
   }
    
   function target_up_to_week_summary($group) 
   {
	    
		if($group=="NETWORK")
		{
			$waktu_sekarang = date("Y-m-d");
			$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
			$tr = $dat["week"];
			$dat2 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr' AND regional <> '[HQ]'
									  ")->result_array()[0];
			return 'Target UpTo Week '.$tr.' :<br> '.$dat2["jumlah"];
			
		}	
	    else
		{	
	   
			$waktu_sekarang = date("Y-m-d");
			$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
			$tr = $dat["week"];
			$dat2 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr' AND `owner_program`='$group' 
									  ")->result_array()[0];
			return 'Target UpTo Week '.$tr.' :<br> '.$dat2["jumlah"];
        }
   }
   
   function target_up_to_week_summary_ach($group)
   {
	    
		if($group=="NETWORK")
		{
			$waktu_sekarang = date("Y-m-d");
			$dat_ach = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1,1")->result_array()[0];
			$tr_ach = $dat_ach["week"];
			
			$dat2 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr_ach' AND regional <> '[HQ]'
									  ")->result_array()[0];
			$dat3 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr_ach' AND regional <> '[HQ]'
									  and ( progress='closed' or progress='CLOSED' or progress='Closed' ) 
									  ")->result_array()[0];						  
			
			$Pa = $dat3["jumlah"];
			$Pb = ($dat2["jumlah"]==0) ? 1 : $dat2["jumlah"];
			
			$h = ($Pa/$Pb)*100;
			$per = round($h,2);
			return "Ach UpTo $tr_ach : $per%";
		
		}
		else
		{	
		$waktu_sekarang = date("Y-m-d");
		$dat_ach = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1,1")->result_array()[0];
		$tr_ach = $dat_ach["week"];
		
		$dat2 = $this->db->query("select count(*) AS jumlah  
		                          from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program`='$group' 
								  ")->result_array()[0];
		$dat3 = $this->db->query("select count(*) AS jumlah  
		                          from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program`='$group' 
								  and ( progress='closed' or progress='CLOSED' or progress='Closed' ) 
								  ")->result_array()[0];						  
		
		$Pa = $dat3["jumlah"];
		$Pb = ($dat2["jumlah"]==0) ? 1 : $dat2["jumlah"];
		
		$h = ($Pa/$Pb)*100;
		$per = round($h,2);
		return "Ach UpTo $tr_ach : $per%";
		
		}
   }
   //------------------------------------- NEP tot
   function _summary_NEP_total() 
   {
	  
	    $wher =  "owner_program like '%nep%'"; 
			
								
		 $dat = $this->db->query("
							    select 
								sum(s.jumlah_activity) AS target_total,
						        sum(s.jumlah_activity_done) as jumlah_activity_done,
								(sum(s.jumlah_activity_done) / sum(s.jumlah_activity)) * 100   as per
								
								from (							
									   select DISTINCT  a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
									   COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
									   from activity_progress a
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress   where ".$wher." 
												  group by activity) 
																	b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and $wher
												  group by activity) c on c.activity=a.activity
																	
							    )s		")->result_array();
							   
		return 		$dat[0];			   
   }
   //------------------------- total sum comb
   function _summary_COMBAT_total() 
   {
	  
			$wher =  "`group` like '%combat%'"; 
			
								
		 $dat = $this->db->query("
							    select 
								sum(s.jumlah_activity) AS target_total,
						        sum(s.jumlah_activity_done) as jumlah_activity_done,
							    (sum(s.jumlah_activity_done) / sum(s.jumlah_activity)) * 100   as per
								from (							
									   select DISTINCT  a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
									   COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
									   from activity_progress a
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress   where ".$wher." 
												  group by activity) 
																	b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and $wher
												  group by activity) c on c.activity=a.activity
																	
							    )s		")->result_array();
							   
		return 		$dat[0];			   
   }
   
   function _summary_regional_total($owner) 
   {
	    /* $sr = strtoupper(substr($owner,0,3));
		if( $sr ==  "NEP" )
		{
			$wher =  "owner_program='$owner'"; 
		}
        else
        { */
			$wher =  "regional='$owner'"; 
		//}			
		//COALESCE(( COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0) ),0) * 100 As per
									   						
		 $dat = $this->db->query("
							    select 
								sum(s.jumlah_activity) AS target_total,
						        sum(s.jumlah_activity_done) as jumlah_activity_done,
							    COALESCE(( COALESCE(sum(s.jumlah_activity_done), 0)/COALESCE(sum(s.jumlah_activity), 0) ),0) * 100 As per
								from (							
									   select DISTINCT a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done
									   from activity_progress a
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress where ".$wher."
												  group by activity 
												  ) b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and ".$wher."
												  group by activity
												  ) c on c.activity=a.activity														
							    )s")->result_array(); 			
		return $dat[0];			   
   }
   
   function target_up_to_week_summary_NEP()
   {
	   
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"];
		
		
		    $dat2 = $this->db->query("select count(*) AS jumlah  
		                          from activity_progress 
								  where 
								  target_week <= '$tr' AND owner_program like '%nep%' 
								  ")->result_array()[0];	
		
		return 'Target UpTo Week '.$tr.' :<br> '.$dat2["jumlah"];
   } 
   //combat%
   function target_up_to_week_summary_COMBAT() //alay
   {
	   
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"];
		
		
		    $dat2 = $this->db->query("select count(*) AS jumlah  
		                          from activity_progress 
								  where 
								  target_week <= '$tr' AND owner_program like '%combat%' 
								  ")->result_array()[0];	
		
		return 'Target UpTo Week '.$tr.' :<br> '.$dat2["jumlah"];
   } 
   
   //REGIONAL
   //plan
   function target_week_plan_regional($group)
   {
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 
		
		$total_plan_per_regional = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN ('".$tr."') AND regional = '$group'")->result_array()[0];  

		$total_plan = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional = '$group'")->result_array()[0]; 

		$total_plans = $total_plan["total"]==0?1:$total_plan["total"];  

		return 'Target '.$tr.' : '.$total_plan_per_regional["total"].' ('.round(($total_plan_per_regional["total"]/$total_plans) * 100,2).'%)';
   }
   
   function target_total_up_week_plan_regional($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data_weeks = $this->db->query("select distinct week from date_week where stardate <= '".$waktu_sekarang."' order by week DESC")->result_array();

		$data_week = [];
		foreach($data_weeks as $item) {
			$item_week = $item["week"];
			array_push($data_week, "'".$item_week."'"); 
		}
		
		$array_week = implode(',',$data_week);

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN (".$array_week.") AND regional = '$group'")->result_array()[0];  

		$total_plan = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional = '$group'")->result_array()[0]; 
		$total_plans = $total_plan["total"]==0?1:$total_plan["total"];

		//global delta
		global $total_delta_plan; 
		global $persentase_total_delta_plan; 

		$total_delta_plan = $data["total"]; 
		$persentase_total_delta_plan = ($data["total"]/$total_plans)*100; 

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_plans)*100,2).'%)';
   }

   //actual
   function target_week_actual_regional($group)
   { 
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"];
				
		$total_actual_per_regional = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN ('".$tr."') AND regional = '$group'
		AND progress = 'Closed'")->result_array()[0];  

		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress where regional = '$group'")->result_array()[0];  
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		return 'Target '.$tr.' : '.$total_actual_per_regional["total"].' ('.round(($total_actual_per_regional["total"]/$total_actuals) * 100,2).'%)';
   }
   
   function target_total_up_week_actual_regional($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE regional = '$group' AND progress = 'Closed'")->result_array()[0]; 
		// echo json_encode($data); die();
	 
		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional = '$group'")->result_array()[0]; 
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		//global delta
		global $total_delta_actual;
		global $persentase_total_delta_actual;
 
		$total_delta_actual = $data["total"]; 
		$persentase_total_delta_actual = ($data["total"]/$total_actuals)*100;

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_actuals) * 100,2).'%)';
   }

   function target_total_up_week_actual_regional_existing($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data_weeks = $this->db->query("select distinct week from date_week where stardate <= '".$waktu_sekarang."' order by week DESC")->result_array();

		$data_week = [];
		foreach($data_weeks as $item) {
			$item_week = $item["week"];
			array_push($data_week, "'".$item_week."'"); 
		}
		
		$array_week = implode(',',$data_week);

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN (".$array_week.") AND regional = '$group'  
		AND progress = 'Closed'")->result_array()[0];

		// echo json_encode($data); die();
	 
		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional = '$group'")->result_array()[0]; 
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		//global delta
		global $total_delta_actual;
		global $persentase_total_delta_actual;
 
		$total_delta_actual = $data["total"]; 
		$persentase_total_delta_actual = ($data["total"]/$total_actuals)*100;

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_actuals) * 100,2).'%)';
   }

   //Delta
   function total_delta()
   { 
		global $total_delta_actual;   
		global $persentase_total_delta_actual;
		
		global $total_delta_plan;
		global $persentase_total_delta_plan;
	 		
		$total_delta = $total_delta_actual - $total_delta_plan;
		$total_deltas = str_replace('-', '', $total_delta);

		// var_dump($persentase_total_delta_plan); die();

		$total_persentase = round($persentase_total_delta_actual - $persentase_total_delta_plan, 2); 

		if (strpos($total_persentase, '-') !== false) {  	 
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#fca5a5" style = "font-size: 14px"> ('.$total_persentase.'%) / '.$total_deltas.' Delayed</font></center>';     
		}else if ($total_persentase == 0) {
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#fff" style = "font-size: 14px"> ('.$total_persentase.'%) / '.$total_deltas.' On The Track</font></center></font></center>';			
		}else { 
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#78DE5C" style = "font-size: 14px"> ('.$total_persentase.'%) / '.$total_deltas.' Accelerate</font></center>';  
		}
   }
   
   //Total Activity
   function total_activity($group)
   {
		$total_activity = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional = '$group'")->result_array()[0]; 

		return 'Total '.$total_activity["total"].' Activity';
   }


	//REGIONAL NOQM
   //plan
   function target_week_plan_regional_noqm($group)
   {
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 
		
		$total_plan_per_regional = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN ('".$tr."') AND owner_program = '$group'")->result_array()[0];  

		$total_plan = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE owner_program = '$group'")->result_array()[0]; 

		$total_plans = $total_plan["total"]==0?1:$total_plan["total"];  

		return 'Target '.$tr.' : '.$total_plan_per_regional["total"].' ('.round(($total_plan_per_regional["total"]/$total_plans) * 100,2).'%)';
   }
   
   function target_total_up_week_plan_regional_noqm($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data_weeks = $this->db->query("select distinct week from date_week where stardate <= '".$waktu_sekarang."' order by week DESC")->result_array();

		$data_week = [];
		foreach($data_weeks as $item) {
			$item_week = $item["week"];
			array_push($data_week, "'".$item_week."'"); 
		}
		
		$array_week = implode(',',$data_week);

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN (".$array_week.") AND owner_program = '$group'")->result_array()[0];  

		$total_plan = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE owner_program = '$group'")->result_array()[0]; 
		$total_plans = $total_plan["total"]==0?1:$total_plan["total"];

		//global delta 
		global $total_delta_plan; 
		global $persentase_total_delta_plan; 

		$total_delta_plan = $data["total"]; 
		$persentase_total_delta_plan = ($data["total"]/$total_plans)*100; 

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_plans)*100,2).'%)';
   }

   //actual
   function target_week_actual_regional_noqm($group)
   { 
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"];
				
		$total_actual_per_regional = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN ('".$tr."') AND owner_program = '$group'
		AND progress = 'Closed'")->result_array()[0];  

		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress where owner_program = '$group'")->result_array()[0];  
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		return 'Target '.$tr.' : '.$total_actual_per_regional["total"].' ('.round(($total_actual_per_regional["total"]/$total_actuals) * 100,2).'%)';
   }
   
   function target_total_up_week_actual_regional_noqm($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE owner_program = '$group'  
		AND progress = 'Closed'")->result_array()[0];

		// echo json_encode($data); die();
	 
		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE owner_program = '$group'")->result_array()[0]; 
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		//global delta
		global $total_delta_actual;
		global $persentase_total_delta_actual;
 
		$total_delta_actual = $data["total"]; 
		$persentase_total_delta_actual = ($data["total"]/$total_actuals)*100;

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_actuals) * 100,2).'%)';
   }

   function target_total_up_week_actual_regional_noqm_existing($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data_weeks = $this->db->query("select distinct week from date_week where stardate <= '".$waktu_sekarang."' order by week DESC")->result_array();

		$data_week = [];
		foreach($data_weeks as $item) {
			$item_week = $item["week"];
			array_push($data_week, "'".$item_week."'"); 
		}
		
		$array_week = implode(',',$data_week);

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN (".$array_week.") AND owner_program = '$group'  
		AND progress = 'Closed'")->result_array()[0];

		// echo json_encode($data); die();
	 
		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE owner_program = '$group'")->result_array()[0]; 
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		//global delta
		global $total_delta_actual;
		global $persentase_total_delta_actual;
 
		$total_delta_actual = $data["total"]; 
		$persentase_total_delta_actual = ($data["total"]/$total_actuals)*100;

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_actuals) * 100,2).'%)';
   }

   //Delta
   function total_delta_noqm()
   { 
		global $total_delta_actual;   
		global $persentase_total_delta_actual;
		
		global $total_delta_plan;
		global $persentase_total_delta_plan;
	 		
		$total_delta = $total_delta_actual - $total_delta_plan;
		$total_deltas = str_replace('-', '', $total_delta);

		// var_dump($persentase_total_delta_plan); die();

		$total_persentase = round($persentase_total_delta_actual - $persentase_total_delta_plan, 2); 

		if (strpos($total_persentase, '-') !== false) {  	 
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#fca5a5" style = "font-size: 14px"> ('.$total_persentase.'%) / '.$total_deltas.' Delayed</font></center>';     
		}else if ($total_persentase == 0) {
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#fff" style = "font-size: 14px"> ('.$total_persentase.'%) / '.$total_deltas.' On The Track</font></center></font></center>';			
		}else { 
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#78DE5C" style = "font-size: 14px"> ('.$total_persentase.'%) / '.$total_deltas.' Accelerate</font></center>';  
		}
   }
   
   //Total Activity
   function total_activity_noqm($group)
   {
		$total_activity = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE owner_program = '$group'")->result_array()[0]; 

		return 'Total '.$total_activity["total"].' Activity';
   }

   //AREA
   //plan
   function target_week_plan_area($group)
   {
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];

		$tr = $dat["week"]; 
		
		$total_plan_per_regional = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN ('".$tr."') AND regional IN (".$group.")")->result_array()[0];  

		$total_plan = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional IN (".$group.")")->result_array()[0]; 

		$total_plans = $total_plan["total"]==0?1:$total_plan["total"];  

		return 'Target '.$tr.' : '.$total_plan_per_regional["total"].' ('.round(($total_plan_per_regional["total"]/$total_plans) * 100,2).'%)';
   }
   
   function target_total_up_week_plan_area($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data_weeks = $this->db->query("select distinct week from date_week where stardate <= '".$waktu_sekarang."' order by week DESC")->result_array();

		$data_week = [];
		foreach($data_weeks as $item) {
			$item_week = $item["week"];
			array_push($data_week, "'".$item_week."'"); 
		}
		
		$array_week = implode(',',$data_week);

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN (".$array_week.") AND regional IN (".$group.")")->result_array()[0];  

		$total_plan = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional IN (".$group.")")->result_array()[0]; 
		$total_plans = $total_plan["total"]==0?1:$total_plan["total"];

		//global delta
		global $total_delta_area_plan;
		global $persentase_area_total_delta_plan;

		$total_delta_area_plan = $data["total"];

		$persentase_area_total_delta_plan = ($data["total"]/$total_plans)*100; 

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_plans)*100,2).'%)';
   }

   //actual
   function target_week_actual_area($group)
   { 
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"];
				
		$total_actual_per_regional = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN ('".$tr."') AND regional IN (".$group.") 
		AND progress = 'Closed'")->result_array()[0];  

		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress where regional IN (".$group.")")->result_array()[0];  
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		return 'Target '.$tr.' : '.$total_actual_per_regional["total"].' ('.round(($total_actual_per_regional["total"]/$total_actuals) * 100,2).'%)';
   }
   
   function target_total_up_week_actual_area($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE regional IN (".$group.")  
		AND progress = 'Closed'")->result_array()[0];
	 
		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional IN (".$group.") ")->result_array()[0]; 
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		//global delta 
		global $total_delta_area_actual;
		global $persentase_area_total_delta_actual;

		$total_delta_area_actual = $data["total"];  
		$persentase_area_total_delta_actual = ($data["total"]/$total_actuals)*100;

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_actuals) * 100,2).'%)';
   }

   function target_total_up_week_actual_area_existing($group)
   {
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 

		$data_weeks = $this->db->query("select distinct week from date_week where stardate <= '".$waktu_sekarang."' order by week DESC")->result_array();
		
		$data_week = [];
		foreach($data_weeks as $item) {
			$item_week = $item["week"];
			array_push($data_week, "'".$item_week."'"); 
		}
		
		$array_week = implode(',',$data_week);

		$data = $this->db->query("SELECT COUNT(*) as total FROM activity_progress
		WHERE target_week IN (".$array_week.") AND regional IN (".$group.")  
		AND progress = 'Closed'")->result_array()[0];
	 
		$total_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional IN (".$group.") ")->result_array()[0]; 
		$total_actuals = $total_actual["total"]==0?1:$total_actual["total"];

		//global delta
		global $total_delta_area_actual;
		global $persentase_area_total_delta_actual;

		$total_delta_area_actual = $data["total"];  
		$persentase_area_total_delta_actual = ($data["total"]/$total_actuals)*100;

		return 'Target Up To '.$tr.' : '.$data["total"].' ('.round(($data["total"]/$total_actuals) * 100,2).'%)';
   }

   //Delta
   function total_delta_area()
   {

	global $total_delta_area_actual;   
	global $persentase_area_total_delta_actual;
	
	global $total_delta_area_plan;
	global $persentase_area_total_delta_plan;
		 
	$total_area_delta = $total_delta_area_actual - $total_delta_area_plan;
	$total_area_deltas = str_replace('-', '', $total_area_delta);

	$total_area_persentase = round($persentase_area_total_delta_actual - $persentase_area_total_delta_plan, 2); 

		if (strpos($total_area_persentase, '-') !== false) {  
			return '<center><i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#fca5a5" style = "font-size: 14px"> ('.$total_area_persentase.'%) / '.$total_area_deltas.' Delayed</font></center></font></center>';
		} 
		else if ($total_area_persentase == 0) {
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#fff" style = "font-size: 14px"> ('.$total_area_persentase.'%) / '.$total_area_deltas.' On The Track</font></center></font></center>';			
		}
		else {
			return '<center> <i class="fa fa-toggle-up" style="color:white; font-size:15px"></i> <font color="#78DE5C" style = "font-size: 14px"> ('.$total_area_persentase.'%) / '.$total_area_deltas.' Accelerate</font></center></font></center>';
		} 
		
   }
   
   //Total Activity
   function total_activity_area($group)
   {
		$total_activity = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE regional IN (".$group.")")->result_array()[0]; 

		return 'Total '.$total_activity["total"].' Activity';
   }

   //PROGRESS TRACKING
   //Total Plan
	function total_plan_week_nasional($week) 
   {
	    $date			= date('Y-m-d');
		$date_last_week	= date('Y-m-d', strtotime("-1 week"));
		$last_week		= $this->db->query("select week from date_week where stardate<='$date_last_week' and enddate>='$date_last_week' order by week asc")->result_array()[0]; 

		$total_target_up_to_plan   = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE target_week <= '".$week."'")->result_array()[0];
		$total_target_up_to_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE progress = 'Closed'")->result_array()[0];
 
		$count_last = $this->db->query("select target_actual_up_to from activity_progress_speed_week where updated_at < '$date' AND week = '$week' order by updated_at desc limit 1")->result_array()[0];

		$totals_target_up_to_actual = 0;
		if($week == $last_week["week"]) {
			$totals_target_up_to_actual = $total_target_up_to_actual["total"];  
		} else if ($week < $last_week["week"]) {
			$totals_target_up_to_actual = $count_last["target_actual_up_to"];
		} else {
			$totals_target_up_to_actual = 0;
		}
 
		$array = [ 
			'week' => $week,
			'target_up_to_plan' => $total_target_up_to_plan["total"], 
			'target_up_to_actual' => $totals_target_up_to_actual
		];

		return $array;
   }

   	function total_plan_week_regional($week, $group) 
   {
		$date			= date('Y-m-d');
		$date_last_week	= date('Y-m-d', strtotime("-1 week"));
		$last_week		= $this->db->query("select week from date_week where stardate<='$date_last_week' and enddate>='$date_last_week' order by week asc")->result_array()[0];
	
		$total_target_up_to_plan   = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE target_week <= '".$week."' AND owner_program = '$group'")->result_array()[0];
		$total_target_up_to_actual = $this->db->query("SELECT COUNT(*) as total FROM activity_progress WHERE progress = 'Closed' AND owner_program = '$group'")->result_array()[0];

		$count_last = $this->db->query("select target_actual_up_to from activity_progress_speed_week_owner where owner_program = '$group' and updated_at < '$date' and week = '$week' order by updated_at desc limit 1")->result_array();

		if(empty($count_last)) {
			array_push($count_last, ['target_actual_up_to'=> 0]);
		}

		$totals_target_up_to_actual = 0;
		if($week == $last_week["week"]) {
			$totals_target_up_to_actual = $total_target_up_to_actual["total"];  
		} else if ($week < $last_week["week"]) {
			$totals_target_up_to_actual = $count_last[0]['target_actual_up_to'];
		} else { 
			$totals_target_up_to_actual = 0;
		} 

		$array = [ 
			'week' => $week,
			'owner_program' => $group,
			'target_up_to_plan' => $total_target_up_to_plan["total"], 
			'target_up_to_actual' => $totals_target_up_to_actual
		];

		return $array;
   }


   function target_up_to_week_summary_regional($group)
   {
	   
	    $waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"]; 
		
		$sr = strtoupper(substr($group,0,3));
		if( $sr ==  "NEP" )
		{
		    $dat2 = $this->db->query("select count(*) AS jumlah  
		                          from activity_progress 
								  where 
								  target_week <= '$tr' AND `owner_program`='$group' 
								  ")->result_array()[0];	
		}	
		else
		{	
		$dat2 = $this->db->query("select count(*) AS jumlah  
		                          from activity_progress 
								  where 
								  target_week <= '$tr' AND `regional`='$group'  and owner_program not like '%nep%' and `group` not like '%combat%'
								  ")->result_array()[0];
	    }
		return 'Target UpTo Week '.$tr.' :<br> '.$dat2["jumlah"];
   }

   function target_up_to_week_summary_regional_ach($group)
   {
	    $waktu_sekarang = date("Y-m-d");
		$dat_ach = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1,1")->result_array()[0];
		$tr_ach = $dat_ach["week"];
		
		$sr = strtoupper(substr($group,0,3));
		if( $sr ==  "NEP" )
		{
		
			$dat2 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr_ach' AND `owner_program`='$group' 
									  ")->result_array()[0];
			$dat3 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr_ach' AND `owner_program`='$group' 
									  and ( progress='closed' or progress='Closed' or progress='CLOSED' ) 
									  ")->result_array()[0];						  
		
		}
		
		else
		{
			
			$dat2 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr_ach' AND `regional`='$group' 
									  ")->result_array()[0];
			$dat3 = $this->db->query("select count(*) AS jumlah  
									  from activity_progress 
									  where 
									  target_week <= '$tr_ach' AND `regional`='$group' 
									  and ( progress='Closed' or progress='closed' or progress='CLOSED' ) 
									  ")->result_array()[0];						  
		
		}	
		$Pa = $dat3["jumlah"]; // ditanyain lagi
		$Pb = ($dat2["jumlah"]==0) ? 1 : $dat2["jumlah"];
		
		$h = ($Pa/$Pb)*100;
		$per = round($h,2);
		return "Ach UpTo $tr_ach : $per%";
   }
   // NEP
    function target_up_to_week_summary_NEP_ach()
   {
	    $waktu_sekarang = date("Y-m-d");
		$dat_ach = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1,1")->result_array()[0];
		$tr_ach = $dat_ach["week"];
		
		
		$dat2 = $this->db->query("select count(*) AS jumlah  
								  from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program` like '%nep%' 
								  ")->result_array()[0];
		$dat3 = $this->db->query("select count(*) AS jumlah  
								  from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program` like '%nep%' 
								  and ( progress='closed' or progress='Closed' or progress='CLOSED' ) 
								  ")->result_array()[0];						  
		
		
		
		$Pa = $dat3["jumlah"]; // ditanyain lagi
		$Pb = ($dat2["jumlah"]==0) ? 1 : $dat2["jumlah"];
		
		$h = ($Pa/$Pb)*100;
		$per = round($h,2);
		return "Ach UpTo $tr_ach : $per%";
   }
   // COMBAT
    function target_up_to_week_summary_COMBAT_ach()
   {
	    $waktu_sekarang = date("Y-m-d");
		$dat_ach = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1,1")->result_array()[0];
		$tr_ach = $dat_ach["week"];
		
		
		$dat2 = $this->db->query("select count(*) AS jumlah  
								  from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program` like '%combat%' 
								  ")->result_array()[0];
		$dat3 = $this->db->query("select count(*) AS jumlah  
								  from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program` like '%combat%' 
								  and ( progress='closed' or progress='Closed' or progress='CLOSED' ) 
								  ")->result_array()[0];						  
		
		
		
		$Pa = $dat3["jumlah"]; // ditanyain lagi
		$Pb = ($dat2["jumlah"]==0) ? 1 : $dat2["jumlah"];
		
		$h = ($Pa/$Pb)*100;
		$per = round($h,2);
		return "Ach UpTo $tr_ach : $per%";
   }
   
   function target_up_to_week_combat_tr()
   {
	     $waktu_sekarang = date("Y-m-d");
		 $dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
		 $tr = $dat["week"];
		
		 $dat2 = $this->db->query("select count(*) AS jumlah  
		                           from activity_progress 
								   where 
								   target_week <= '$tr' AND `owner_program` like '%combat%' 
								   ")->result_array()[0];	
		 $h1 = 'Target UpTo Week '.$tr.' : '.$dat2["jumlah"];
		 //=====================================================================================
		 $dat = $this->db->query(" select 
                          sum(s.jumlah_activity) AS target_total,
						        sum(s.jumlah_activity_done) as jumlah_activity_done,
								s.per
								from (							
									   select DISTINCT  a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
									   COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
									   from activity_progress a
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress   where owner_program like '%combat%' 
												  group by activity) 
																	b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='closed' or progress='CLOSED' ) 
												  and  owner_program like '%combat%' 
												  group by activity) c on c.activity=a.activity
																	
							   )s		")->result_array();
							   
		
		$total_done   =		$dat[0]['jumlah_activity_done'];	
		$total_target =		$dat[0]['target_total'];	
		$total_per    =		$dat[0]['per'];	
        //=================================================================================
		$waktu_sekarang = date("Y-m-d");
		$dat_ach = $this->db->query("select week from date_week 
									 where stardate<='$waktu_sekarang' 
									 order by id DESC limit 1,1")->result_array()[0];
		$tr_ach = $dat_ach["week"];
		  
		$dat2 = $this->db->query("select count(*) AS jumlah  
								  from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program` like '%combat%' 
								  ")->result_array()[0];
		$dat3 = $this->db->query("select count(*) AS jumlah  
								  from activity_progress 
								  where 
								  target_week <= '$tr_ach' AND `owner_program` like '%combat%' 
								  and ( progress='Closed' or progress='CLOSED' or progress='closed' ) 
								  ")->result_array()[0];

        $Pa = $dat3["jumlah"]; // ditanyain lagi
		$Pb = ($dat2["jumlah"]==0) ? 1 : $dat2["jumlah"];
		
		$h = ($Pa/$Pb)*100;
		$per = round($h,2);
		$pesen_ach = "Ach UpTo $tr_ach : $per%";
        //================================================				  
			
		$g = '<tr>
						  <td colspan="2" bgcolor="#d9d9d9">
							<div align="center">'.$h1.'</div>
							<div align="center">Total Done : <strong> '.$total_done.'</strong></div>  
							<div align="center">Target Total : <strong> '.$total_target .'</strong></div>
						  </td>
						  <td colspan="2" bgcolor="#bebebe">
							<div align="center">'.$pesen_ach.'</div>
							<div align="center"> Ach Overall : <strong> '.$total_per.' %</strong></div>
						  </td>
					  </tr>';
		return $g;	
  
   }
   function progres_tracking_regional_query()
   {
			  $dat3 = $this->db->query("
		        select a.owner_program,count('a.owner_program') As jumlah_target,COALESCE(b.jumlah_done,0) as jumlah_done  from 
				activity_progress a
					left join (
					select count('owner_program') As jumlah_done,owner_program from activity_progress 
					where
					progress in ('Closed','CLOSED','closed') AND owner_program like '%Region%'
					group by owner_program
					) as b on b.owner_program=a.owner_program
					 
				where a.owner_program like '%Region%' and a.regional  <> '[HQ]'
				group by a.owner_program ")->result_array();
				return $dat3;
	   
   }	//".filterBygroup2."
   
   function progres_tracking_nasional_query()
   {
		$dat3 = $this->db->query("
							     
					    select 
						   a.section, 
						  	COALESCE(c.jumlah_done,0) as jumlah_done,
							COALESCE(b.remaining,0) as remaining,
							(COALESCE(c.jumlah_done,0) / (COALESCE(b.remaining,0) + COALESCE(c.jumlah_done,0)) ) * 100 as on_progress
							from activity_progress a
								left join (
										select count(*) As remaining,section from activity_progress 
										where  
										 progress in ('Closed','CLOSED','closed') 
										group by section
								) as b on b.section=a.section

								left join (
										select count(*) As jumlah_done,section from activity_progress 
										where  
										 progress in ('Closed','CLOSED','closed') 
										group by section
								) as c on c.section=a.section
							  where a.regional <> '[HQ]'	
							
							group by a.section
								")->result_array(); //".filterBygroup122()." 
		return $dat3;
   }	   
   function progres_tracking_nasional_query_week()
   {
		$dat3 = $this->db->query("select distinct target_week from activity_progress where target_week like 'W%' order by target_week asc")->result_array();
		return $dat3;						  
   }
  
   function progres_tracking_nasional_query_section()
   {
		$dat3 = $this->db->query("select owner_program as section
								  from activity_progress
							      group by owner_program")->result_array(); // ".filterBygroup12()." 
		return $dat3;						  
   }

   function progress_tracking_nasional_qiery_ach_target($group)
   {
		 $dat3 = $this->db->query("SELECT updated_at, owner_program, week, target_plan_up_to AS target, target_actual_up_to AS target_done
		 FROM activity_progress_speed_week_owner
		 WHERE owner_program = '$group'
		 AND updated_at = (SELECT MAX(updated_at) FROM activity_progress_speed_week_owner)")->result_array();
		return $dat3;	
   }
   
   function progress_tracking_nasional_qiery_ach_target_existing($group)
   {
		 $dat3 = $this->db->query("
									 select a.target_week,
									 COALESCE(b.jum_target_week,0) as target,
									 COALESCE(c.jum_target_done,0) as target_done
									 from activity_progress a
									 left join (
											 select count('target_week') as jum_target_week,target_week from activity_progress
											 where owner_program='$group' 
											 group by target_week
											 order by target_week asc
									  ) b on b.target_week= a.target_week
									 left join (
											 select count('target_week') as jum_target_done,
													  target_week from activity_progress
											 where owner_program='$group' AND progress in ('Closed','CLOSED','closed')
											 group by target_week
											 order by target_week asc
									  ) c on c.target_week= a.target_week
									  WHERE a.target_week LIKE 'W%'
									  group by a.target_week
									  order by  CAST(SUBSTRING(a.target_week, 2, 3) as unsigned)
		  
									")->result_array();
		  return $dat3;	
   }
  
  function progres_tracking_regional_query_admin()
  {
	         //$rs = $this->progres_tracking_regional_query();
	         //$rs = $this->progress_tracking_data_net_nas();
			 $rs = $this->db->query("select * from
								   (
									  select DISTINCT a.regional
									  from activity_progress a
									  where regional
									  like '%[R%'
									 
									  group by a.regional
								   ) ac 
								  ")->result_array();
			 $r =array();
			 foreach($rs as $row)
			 {
			  //$per = ( $row['jumlah_done'] / $row['jumlah_target'] ) * 100;
			  //$per2 = round($per,2);
			  $per2 = round($this->_summary_regional_total($row["regional"])['per'],2);
			  //array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	


			  //$regi = $row["regional"]." ".array_regional()[$row["regional"]];
			  $regi = $row["regional"];
			  array_push($r,array("owner_program"=>$regi, 
			  "target"=> $this->_summary_regional_total($row["regional"])['target_total'],
			  "persen"=>$per2,
			  "done"=> $this->_summary_regional_total($row["regional"])['jumlah_activity_done'] ));	
			 } 

	   $ret = [
			"chart"=> [
			"caption"=> "Progress Tracking",
			"subcaption"=> "( Per Regional )",
			//"xaxisname"=> "Years",
			//"yaxisname"=> "Total",
			//"pyaxisname"=> "Units Sold",
			//"formatnumberscale"=> "1",
			"plottooltext" => '<b>$dataValue</b>  activity on <b>$seriesName</b> in $label',
			//"syaxisname"=> "% of total ",
			"labelDisplay" => "Auto",
            "useEllipsesWhenOverflow"  => "1",
		    "snumbersuffix"=> "%",
			"showvalues"=> "0",
		    "syaxismaxvalue"=> "100",
			"theme"=>"fusion",
			"drawcrossline"=> "1",
			"divlinealpha"=> "20"
		  ],
		  "categories"=> [
			[
			  "category"=> [
				[
				  "label"=> "Sumbagut"
				],
				[
				  "label"=> "Sumbagsel"
				],
				[
				  "label"=> "Jabotabek"
				],
				[
				  "label"=> "Jabar"
				],
				[
				  "label"=> "Jateng"
				],
				[
				  "label"=> "Jatim"
				],
				[
				  "label"=> "Bali Nusra"
				],
				[
				  "label"=> "Kalimantan"
				],
				[
				  "label"=> "Sulawesi"
				],
				[
				  "label"=> "Sumbagteng"
				],
				[
				  "label"=> "PUMA"
				]
			  ]
			]
		  ],
		    "dataset"=> [
    [
      "dataset"=> [
        [
          "seriesname"=> "Target",
          "data"=> [
						[
						  "value"=> $r[array_search('[R01]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R02]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R03]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R04]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R05]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R06]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R07]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R08]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R09]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R10]', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('[R11]', array_column($r, 'owner_program'))]['target']
						]
          ]
        ],
        
      ]
    ],
    [
      "dataset"=> [
        [
          "seriesname"=> "Done",
          "data"=> [
                [
				  "value"=> $r[array_search('[R01]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R02]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R03]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R04]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R05]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R06]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R07]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R08]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R09]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R10]', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('[R11]', array_column($r, 'owner_program'))]['done']
				]
          ]
        ],
        
      ]
    ]
  ],
		
		    "lineset"=> [
			[
			  "seriesname"=> "on progress %",
			  "plottooltext"=> 'Total $label is <b>$dataValue</b> ',
			  "showvalues"=> "1",
			  "data"=> [
					[
					  "value"=> $r[array_search('[R01]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R02]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R03]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R04]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R05]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R06]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R07]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R08]', array_column($r, 'owner_program'))]['persen']
					],
					
					[
					  "value"=> $r[array_search('[R09]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R10]', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('[R11]', array_column($r, 'owner_program'))]['persen']
					]
			  ]
			]
		  ] 
		 
		];

		return $ret;
		
  }  
  function progres_tracking_regional_query_region()
  {
	$waktu_sekarang = date("Y-m-d");
	$cat = $this->db->query("select week from date_week  where stardate<='$waktu_sekarang' order by id asc limit 0,10")->result_array();
    $categori =array();
    foreach($cat as $wa)
    {
		array_push($categori,array("label"=>$wa['week']));	

	}	
	//-- order by  CAST(SUBSTRING(a.target_week, 2, 3) as unsigned)
            
	$dat3 = $this->db->query("
							select 
							a.week2 as minggu,
							COALESCE(b.jumlah_target,0) as target,
							COALESCE(b.jumlah_done,0) as target_done
							from date_week a 
							left join (
								 select a.target_week,
								 COALESCE(b.jum_target_week,0) as jumlah_target,
								 COALESCE(c.jum_target_done,0) as jumlah_done
								
								 from activity_progress a
								 left join (
										 select count('target_week') as jum_target_week,target_week 
										 from activity_progress
										 
										 group by target_week
										 order by target_week asc
								  ) b on b.target_week= a.target_week
								 left join (
										 select count('target_week') as jum_target_done,
												  target_week from activity_progress
										 where progress in ('CLOSED','Closed','closed')
										
										 group by target_week
										 order by target_week asc
								  ) c on c.target_week= a.target_week
								  group by a.target_week
								 
							) b on a.week2=b.target_week
							where a.stardate<='$waktu_sekarang' order by a.id asc limit 0,10
							
							")->result_array();
			 //$rs = $this->progres_tracking_regional_query();
			 $r1 =array();//target
			 $r2 =array();//done
			 $r3 =array();//persen
			 foreach($dat3 as $row)
			 {
				  $per = ( $row['target_done'] / (($row['target']==0) ? 1 : $row['target']) ) * 100;
				  $per2 = round($per,2);
				  //array_push($r1,array("value"=>$row['target'],"color" => "#9b59b6"));
				  array_push($r1,array("value"=>$row['target']));
				  array_push($r2,array("value"=>$row['target_done']));
				  array_push($r3,array("value"=>$per2));
			 }
	   $ret = [
			"chart"=> [
			"caption"=> "Regional Progress Tracking",
			"subcaption"=> "2019",
			 "paletteColors" => '#0075c2,#1aaf5d,#f2c500',
			//"xaxisname"=> "Years",
			//"yaxisname"=> "Total",
			//"pyaxisname"=> "Units Sold",
			//"formatnumberscale"=> "1",
			//"plottooltext" => '<b>$dataValue</b> apps were available on <b>$seriesName</b> in $label',
			//"syaxisname"=> "% of total ",
		    "snumbersuffix"=> "%",
			"showvalues"=> "0",
		    "syaxismaxvalue"=> "110",
			"theme"=>"fusion",
			"drawcrossline"=> "1",
			"divlinealpha"=> "20"
		  ],
		  "categories"=> [
			[
			  "category"=> $categori
			]
		  ],
		    "dataset"=> [
    [
      "dataset"=> [
        [
          "seriesname"=> "Target",
          "data"=> $r1 
        ],
        
      ]
    ],
    [
      "dataset"=> [
        [
          "seriesname"=> "Done",
          "data"=> $r2 
        ],
        
      ]
    ]
  ],
		
		    "lineset"=> [
			[
			  "seriesname"=> "on progress %",
			  "plottooltext"=> 'Total $label is <b>$dataValue</b> ',
			  "showvalues"=> "1",
			  "data"=> $r3 
			]
		  ] 
		 
		];

		return $ret;
  }  
  function progres_tracking_nep_admin()
  {
	   
								// $rs = $this->db->query("
								// select a.owner_program,count('a.owner_program') As jumlah_target,COALESCE(b.jumlah_done,0) as jumlah_done  from 
								// activity_progress a
									// left join (
									// select count('owner_program') As jumlah_done,owner_program from activity_progress 
									// where
									// progress in ('Closed','CLOSED','closed') AND owner_program like '%nep%'
									// group by owner_program
									// ) as b on b.owner_program=a.owner_program
								// where a.owner_program like '%nep%' 
								// group by a.owner_program ")->result_array();
		
			$rs = $this->db->query("  select 
			  distinct a.owner_program, 
			  COALESCE(b.jumlah_activity,0)  as jumlah_target,
			  COALESCE(c.jumlah_activity_done,0)  as jumlah_done 
			  from activity_progress a
			  left join ( select count(*) as jumlah_activity,owner_program 
						  from activity_progress  
						  group by owner_program ) b on b.owner_program=a.owner_program
			  left join ( select count(*) as jumlah_activity_done,owner_program
									from activity_progress where (progress='Closed' or progress='CLOSED' or progress='closed' ) 
									 group by owner_program) c on c.owner_program=a.owner_program
									 
			  where a.regional  <> '[HQ]' and 		a.owner_program like '%nep%'				 
			  order by a.owner_program ")->result_array();
		
	 
			 $r =array();
			 foreach($rs as $row)
			 {
			  $per = ( $row['jumlah_done'] / $row['jumlah_target'] ) * 100;
			  $per2 = round($per,2);
			  array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
			 }
	   
	   $ret = [
			"chart"=> [
			"caption"=> "Progress NEP Tracking",
			"subcaption"=> "2019",
			//"xaxisname"=> "Years",
			//"yaxisname"=> "Total",
			//"pyaxisname"=> "Units Sold",
			//"formatnumberscale"=> "1",
			"plottooltext" => '<b>$dataValue</b>  were available on <b>$seriesName</b> in $label',
			//"syaxisname"=> "% of total ",
		    "snumbersuffix"=> "%",
			"showvalues"=> "0",
		    "syaxismaxvalue"=> "100",
			"theme"=>"fusion",
			"drawcrossline"=> "1",
			"divlinealpha"=> "20"
		  ],
		  "categories"=> [
			[
			  "category"=> [
				[
				  "label"=> "NEP Area 1"
				],
				[
				  "label"=> "NEP Area 2"
				],
				[
				  "label"=> "NEP Area 3"
				],
				[
				  "label"=> "NEP Area 4"
				],
			  ]
			]
		  ],
		    "dataset"=> [
    [
      "dataset"=> [
        [
          "seriesname"=> "Target",
          "data"=> [
						[
						  "value"=> $r[array_search('NEP Area 01', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('NEP Area 02', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('NEP Area 03', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('NEP Area 04', array_column($r, 'owner_program'))]['target']
						],
						
          ]
        ],
        
      ]
    ],
    [
      "dataset"=> [
        [
          "seriesname"=> "Done",
          "data"=> [
                [
				  "value"=> $r[array_search('NEP Area 01', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('NEP Area 02', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('NEP Area 03', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('NEP Area 04', array_column($r, 'owner_program'))]['done']
				],
				
          ]
        ],
        
      ]
    ]
  ],
		
		    "lineset"=> [
			[
			  "seriesname"=> "on progress %",
			  "plottooltext"=> 'Total $label is <b>$dataValue</b> ',
			  "showvalues"=> "1",
			  "data"=> [
					[
					  "value"=> $r[array_search('NEP Area 01', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('NEP Area 02', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('NEP Area 03', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('NEP Area 04', array_column($r, 'owner_program'))]['persen']
					],
				]
			]
		  ] 
		 
		];

		return $ret;
		
  }  
  
  function combat_query_sum()
  {
	  $has = $this->db->query("	select 
					sum(asd.jumlah_activity) as jum1,
					sum(asd.jumlah_activity_done) as jum2
					from
					(				
						  select a.regional,
						  count('a.regional') As jumlah_target,
						  COALESCE(b.jumlah_done,0) as jumlah_done  
						  from 
						  activity_progress a
						  left join ( select count('regional') As jumlah_done,regional
											from activity_progress 
											where
											 progress in ('Closed','CLOSED','closed') AND 
											(owner_program like '%combat%' or 
											`group` like '%combat%') 
											group by regional
							) as b on b.regional=a.regional
							where (a.owner_program like '%combat%' or  a.group like '%combat%') 
							group by a.regional
								  				 
					)asd
                 		
					")->result_array(); 	
		return $has;
  }
  
  function combat_query()
  {
	  $rs = $this->db->query("  
			 select re.name_regional,b.regional,
			 COALESCE(b.jumlah_target,0) as jumlah_target,
			 COALESCE(b.jumlah_done,0) as jumlah_done   
			 from
		     (  
			 select 'Sumbagut' as name_regional,  '[R01]' as regional
			 union
			 select 'Sumbagsel' as name_regional, '[R02]' as regional
			 union
			 select 'Jabotabek' as name_regional, '[R03]' as regional
			 union
			 select 'Jabar' as name_regional, '[R04]' as regional
			 union
			 select 'Jateng' as name_regional, '[R05]' as regional
			 union
			 select 'Jatim' as name_regional, '[R06]' as regional
			 union
			 select 'Bali Nusra' as name_regional, '[R07]' as regional
			 union
			 select 'Kalimantan' as name_regional, '[R08]' as regional
			 union
			 select 'Sulawesi' as name_regional, '[R09]' as regional
			 union
			 select 'Sumbagteng' as name_regional, '[R10]' as regional
			 union
			 select 'PUMA' as name_regional, '[R11]' as regional 
          )re
          left join
          (
			  
							  select a.regional,
		                          count('a.regional') As jumlah_target,
								  COALESCE(b.jumlah_done,0) as jumlah_done  
								  from 
								  activity_progress a
								  left join ( select count('regional') As jumlah_done,regional
													from activity_progress 
													where
													 progress in ('Closed','CLOSED','closed') AND 
													(owner_program like '%combat%' or 
													`group` like '%combat%') 
													group by regional
									) as b on b.regional=a.regional
									where (a.owner_program like '%combat%' or  a.group like '%combat%') 
								 group by a.regional
			 )b on b.regional=re.regional
							  ")->result_array();
							  
	  return $rs;						  
  }
  function progres_tracking_combat_admin()
  {
	   
		$rs = $this->combat_query();
	 
			 $r =array();
			 $ct =array();
			 $vl_r =array();
			 $vl_d =array();
			 $v_per =array();
			 foreach($rs as $row)
			 {
			  $per = ( $row['jumlah_done'] / ($row['jumlah_target']==0 ? 1 : $row['jumlah_target']) ) * 100;
			  $per2 = round($per,2);
			  array_push($ct, array("label"=>$row['name_regional']));
			  array_push($vl_r, array("value"=>$row['jumlah_target']));
			  array_push($vl_d, array("value"=>$row['jumlah_done']));
			  array_push($v_per, array("value"=>$per2));
			  //array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
			 }
	        $ret = [
			"chart"=> [
			"caption"=> "Progress Tracking Combat",
			"subcaption"=> "2019",
			//"xaxisname"=> "Years",
			//"yaxisname"=> "Total",
			//"pyaxisname"=> "Units Sold",
			//"formatnumberscale"=> "1",
			"plottooltext" => '<b>$dataValue</b>  were available on <b>$seriesName</b> in $label',
			//"syaxisname"=> "% of total ",
			"labelDisplay" => "rotate",
			"slantLabel" => "1",
		    "snumbersuffix"=> "%",
			"showvalues"=> "0",
		    "syaxismaxvalue"=> "100",
			"theme"=>"fusion",
			"drawcrossline"=> "1",
			"divlinealpha"=> "20"
		  ],
		  "categories"=> [
			[
			  "category"=> $ct
			 
			]
		  ],
		    "dataset"=> [
    [
      "dataset"=> [
        [
          "seriesname"=> "Target",
          "data"=> $vl_r
			
        ],
        
      ]
    ],
    [
      "dataset"=> [
        [
          "seriesname"=> "Done",
          "data"=>  $vl_d
			
        ],
        
      ]
    ]
  ],
		
		    "lineset"=> [
			[
			  "seriesname"=> "on progress %",
			  "plottooltext"=> 'Total $label is <b>$dataValue</b> ',
			  "showvalues"=> "1",
			  "data"=>  $v_per
			]
		  ] 
		 
		];

		return $ret;
		
  }  

  function nilaiMaxSummaryregional()
  {
	  $regional = $this->db->query("
		  select DISTINCT regional
		  from activity_progress
		  where regional
		  like '%[R%'		 
		  group by regional
		  ")->result_array();		   

	$max = []; 
	foreach($regional as $item) {

		$get_count_regional = $this->db->query("
		SELECT DISTINCT activity FROM activity_progress
		WHERE regional = '".$item['regional']."' ")->result_array();	

		array_push($max, count($get_count_regional));
	} 
	 return max($max);	  
  }

  function nilaiMaxSummaryregionalNOQM()
  {
	$regional = $this->db->query("SELECT DISTINCT owner_program
	FROM activity_progress
	WHERE owner_program LIKE 'NOQM%'")->result_array();		   

	$max = [];  
	foreach($regional as $item) {
	$get_count_regional = $this->db->query("
	SELECT DISTINCT activity FROM activity_progress
	WHERE owner_program = '".$item['owner_program']."' ")->result_array();	

	array_push($max, count($get_count_regional));
	} 
	 return max($max);	  
  }

  function nilaiMaxSummaryArea()
  {
	  $regional = $this->db->query("
		  select DISTINCT regional
		  from activity_progress
		  where regional
		  like '%[R%'		 
		  group by regional
		  ")->result_array();		   

	$max = []; 
	foreach($regional as $item) {

		$get_count_regional = $this->db->query("
		SELECT DISTINCT activity FROM activity_progress
		WHERE regional = '".$item['regional']."' ")->result_array();	

		array_push($max, count($get_count_regional));
	} 
	 return max($max);	  
  }

  function nilaiMaxRowSummary()
  {
	  $arr = array();
	  $hasil = $this->db->query("select 
										 DISTINCT a.owner_program,
										 COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
										 b.jumlah_activity as target_total,
										 (COALESCE(c.jumlah_activity_done, 0)/b.jumlah_activity) * 100 As per 
										 from activity_progress a 
										 left join ( select count('owner_program') as jumlah_activity,owner_program 
																  from activity_progress  
																  group by `owner_program` ) b on b.owner_program=a.owner_program
										 left join ( select count('owner_program') as jumlah_activity_done,owner_program
														from activity_progress where progress in ('closed','Closed','CLOSED') 
													  group by `owner_program`) c on c.owner_program=a.owner_program
										 where a.regional in ('[HQ]')
										 
										 union all
										 
										 select
										 
										 'NETWORK' as owner_program,
										 (select count(*) as jumlah_activity_done from 
										 				activity_progress where progress in ('closed','Closed','CLOSED') ) 
														 as jumlah_activity_done, 
									    
										 (select count(*) as jumlah_activity  from activity_progress  ) as target_total,
										 
										  ( (select count(*) as jumlah_activity_done from 
										 				activity_progress where progress in ('closed','Closed','CLOSED') ) /
										 				(select count(*) as jumlah_activity  from activity_progress  ) ) AS per
								 ")->result_array(); //".filterBygroup1()."		

		
		foreach($hasil As $row)
		{
	         
			  
			         if( strtoupper($row['owner_program'])=="NETWORK")
					 {	 
			  
								  $has = $this->db->query("
											   	select DISTINCT  a.group as nama,
											    COALESCE(b.jumlah_activity, 0) As jumlah_target,
											    COALESCE(c.jumlah_activity_done, 0) As jumlah_target_done,
											    COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
													from activity_progress a
											   left join (select count('group') as jumlah_activity,`group` 
														  from activity_progress 
														  group by `group`) b on b.group=a.group 
											    left join (select count('group') as jumlah_activity_done,`group` 
														  from activity_progress 
														  where (progress='closed' or progress='CLOSED' or progress='Closed') 
														  group by `group`) c on c.group=a.group
											   where b.jumlah_activity != '0'	and   a.regional <> '[HQ]'			
										")->result_array(); 
					 }
					 else
                     {
						 
								  $has = $this->db->query("
											  
                                              
											  
											  select DISTINCT  a.group as nama,
											   COALESCE(b.jumlah_section, 0) As jumlah_target,
											   COALESCE(c.jumlah_section_done, 0) As jumlah_target_done,
											   COALESCE((COALESCE(c.jumlah_section_done, 0)/COALESCE(b.jumlah_section, 0)),0) * 100 As per
													from activity_progress a
											   left join (select count('group') as jumlah_section,`group` 
														  from activity_progress 
														   where `owner_program`='".$row['owner_program']."' 
														  group by `group`) b on b.group=a.group 
											   left join (select count('group') as jumlah_section_done,`group` 
														  from activity_progress where  (
														  progress='Closed' or progress='closed' or progress='CLOSED' ) 
														  and `owner_program`='".$row['owner_program']."'  
														  group by `group`)c on c.group=a.group
												where b.jumlah_section != '0'	 	


												
										")->result_array();
						 
					 }						 
			   $c = 0;			  
			   foreach($has as $row)
			   {
				$c++;			
			   }
			   $arr[$row["nama"]] = $c;	
		}	   
		
     return max($arr);		
  }  
  function cariJumlahrowsummaryExceGroup()
  {
	$group = $this->db->query("
	SELECT DISTINCT `group` FROM activity_progress
	")->result_array();		   

	$max = []; 
	foreach($group as $item) {

	$get_count_group = $this->db->query("
	SELECT activity FROM activity_progress
	WHERE `group` = '".$item['group']."' GROUP BY activity")->result_array();	
	
	array_push($max, count($get_count_group));
	}
	 	
	return max($max);
  }
}
