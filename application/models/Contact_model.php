<?php

class Contact_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'hubungi';
    }

    
   /*  public function getSql()
	{
		return "
				Select
 				b.name as provinsi,
				a.city_tarif,
				a.name as region,
				a.id as id_kota,
				b.id as id_provinsi
				
				FROm regencies2 a
				left join provinsi b on b.id=a.id
				where 1=1
				";
		
	}
	function getById($id)
	{
		$sql= $this->getSql();
		$sql.=" and a.id=$id";
		return $this->db->query($sql)->row();		
	}
    
	function ProvinsiAuto($searchTerm)
	{
		return $this->db->query("SELECT id,name FROM provinsi WHERE name LIKE '%".$searchTerm."%'  ORDER BY name ASC")->result();
	
	} */
	
    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('id', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->tableName);
    }

}
