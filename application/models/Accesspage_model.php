<?php

class Accesspage_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'privilege_page';
    }

    public function getSql() {
        
      $sql =  "
      			select 
      			pa.privilege_page_id AS id,
      			g.name as groupname,
      			pg.controller as pagename,
      			pa.allow as allow,
      			pa.group_id,
      			pa.page_id
      			
      			from privilege_page pa
                left join groups g on g.group_id=pa.group_id
      			left join pages pg on pg.page_id=pa.page_id
				where 1=1
      			

      			";

      return $sql;
    }
	
	function GetAccesspageByID($id)
	{
		$s = "select * from privilege_page where privilege_page_id='".$id."'";
		$data = $this->db->query($s)->row();
		return $data;
	}

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('privilege_page_id', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('privilege_page_id', $id);
        $this->db->delete($this->tableName);
    }
    
	 public function cekDelete($id) {
		
        $q = "select * from privilege_page where privilege_page_id=$id";
        $da = $this->db->query($q)->row();
		
		$p_id = $da->page_id;
		
		$q2 = "select * from privilege_action where page_id=$p_id";
        $da2 = $this->db->query($q);
		
        if($da2->num_rows()>0)
        {
			//tak bisa didelete
			return true;
		}
        else
        {
			return false;
		}			
        
    }
  

    

}
