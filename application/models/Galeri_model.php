<?php

class Galeri_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'gallery';
    }
   
    public function getSql()
	{
		$id = $this->session->userdata('users_id');
		if($this->session->userdata('group_id')==1)
		{
			
			return "select 
			    a.* 
		        from gallery a
		        
				left join users s on s.users_id = a.created_by
                where 1=1 			
		      ";
			
		}
        else
        {
			return "select
         			a.*
					from gallery a
					
					left join users s on s.users_id = a.created_by
					where 1=1  and a.created_by=$id		 
		      ";
		}			
		
	}
	public function getDataFront()
	{
		 $sql= "select 
			    a.*
		        from gallery a
		       
				left join users s on s.users_id = a.created_by
                where 1=1 	AND a.publish='YA'		
		      ";
		return $this->db->query($sql)->result();	  
	}
	public function getById($id)
	{
       $r = $this->db->query( "select a.*
		        from gallery a
				where a.id_gallery='$id'")->row(); 			
		return $r;
	}
    public function namaalbum()
	{
        //$r = $this->db->query( "SELECT * FROM album ORDER BY jdl_album")->result(); 			
		//return $r;
	}
	
	
	
    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('id_gallery', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('id_gallery', $id);
        $this->db->delete($this->tableName);
    }


    

}
