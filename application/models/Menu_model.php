<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends MY_Model {

    /**
     * @vars
     */
    private $_db;

    function __construct() {
        parent::__construct();

        // define primary table
        $this->_db = 'menu';
    }

    function get_menu($username = NULL, $password = NULL) {
        if ($username && $password) {
            $sql = "
                SELECT
                    users_id,
					group_id,
                    id, 
                    password,
                    salt,
                    first_name,
                    last_name,
                    email,
                    active
                FROM {$this->_db}
                WHERE (id = " . $this->db->escape($username) . "
                        OR email = " . $this->db->escape($username) . ")
                    AND active = '1'
                LIMIT 1
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows()) {
                $results = $query->row_array();
                $salted_password = hash('sha512', $password . $results['salt']);

                if ($results['password'] == $salted_password) {
                    unset($results['password']);
                    unset($results['salt']);

                    return $results;
                }
            }
        }

        return FALSE;
    }

    function getMenuByID($id) {
        $result = $this->db->query('select * from menu where menu_id=' . $id . '');
        return $result->row();
    }

    function ComboMenu() {
        $result = $this->db->query('select * from menu');
        return $result->result();
    }

    public function insert($data) {
        return parent::_insert($this->_db, $data);
    }

    public function update($data, $id) {
        $this->db->where('menu_id', $id);
        return parent::_update($this->_db, $data);
    }

    public function delete($id) {
        $this->db->where('menu_id', $id);
        $this->db->delete($this->_db);
    }

}
