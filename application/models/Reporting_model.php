<?php

class Reporting_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'activity_progress';
    }

    public function getSql($req)
	{   
		$sq="";
		if($req['columns'][0]['search']['value']) 
	    {
			$sq.="and a.owner_program like '%".$req['columns'][0]['search']['value']."%' ";
		} 
        if($req['columns'][1]['search']['value'])
	    {
			$sq.="and a.detail_activity like '%".$req['columns'][1]['search']['value']."%' ";
		}
		if($req['columns'][2]['search']['value'])
	    {
			$sq.="and a.progress like '%".$req['columns'][2]['search']['value']."%' ";
		}
		if($req['columns'][3]['search']['value'])
	    {
			$sq.="and a.target_week like '%".$req['columns'][3]['search']['value']."%' ";
		}
		

		if( $this->session->userdata('group_id')==1 or $this->session->userdata('group_id')==2)
		{
			$date = date('Y-m-d');
			return "select 
			    a.*
		        from activity_progress a
				where target_week = (SELECT week from date_week WHERE stardate<='$date' and enddate>='$date' ORDER BY stardate DESC LIMIT 1)
				AND 1=1 	$sq     				
		      ";
		}  
		else if ( $this->session->userdata('groupName')== 'IT HQ' ) {
			$date = date('Y-m-d');			
			return "select 
			    a.*
		        from activity_progress a
				where target_week = (SELECT week from date_week WHERE stardate<='$date' and enddate>='$date' ORDER BY stardate DESC LIMIT 1)
				AND 1=1 $sq and a.owner_program in ('IT CHARGING SERVICES','IT FULFILLMENT SERVICES','IT INFRASTRUCTURE MANAGEMENT','IT NEW BUSINESS SERVICES', 'IT SQM')			
		        ";
		}  
        else 
        {
			$date = date('Y-m-d');
			return "select 
			    a.*
		        from activity_progress a
				where target_week = (SELECT week from date_week WHERE stardate<='$date' and enddate>='$date' ORDER BY stardate DESC LIMIT 1)
				AND 1=1 $sq and a.owner_program='".$this->session->userdata('groupName')."'			
		      "; 
		}			
			
   }
   
   public function getExcel($owner_program)
   {
		$waktu_sekarang = date("Y-m-d");
		$target_weeks = $this->db->query("select week from date_week where stardate<='$waktu_sekarang' and enddate>='$waktu_sekarang' order by id DESC limit 1")->result_array()[0];
		$target_week = $target_weeks["week"]; 

		if($owner_program == 'admin' || $owner_program == 'SEKRE NARU') {
			$query = "SELECT * FROM activity_progress 
			WHERE target_week = '$target_week'";			
		} else if($owner_program == 'IT HQ') {
			$query = "SELECT * FROM activity_progress
			WHERE owner_program IN ('IT CHARGING SERVICES','IT FULFILLMENT SERVICES','IT INFRASTRUCTURE MANAGEMENT','IT NEW BUSINESS SERVICES', 'IT SQM')
			AND target_week = '$target_week'";			
		} else {
			$query = "SELECT * FROM activity_progress
			WHERE owner_program = '$owner_program'
			AND target_week = '$target_week'";
		} 
		$data = $this->db->query($query);
		return $data->result(); 
   }
   
   public function GetByID($id)
   {
       $r = $this->db->query( "select a.*
		        from activity_progress a
				where a.id_activity_progress='$id'")->row(); 			
		return $r;
	}

    public function ins($data) {
        return parent::_insert2($this->tableName, $data);
    }

    public function upd($data, $id) {
        $this->db->where('id_activity_progress', $id);
        return parent::_update3($this->tableName, $data);
    }
	
	public function insLog($dataLog)
	{
		//$this->db->insert('activity_progress', $dataLog);
		return parent::_insert2("log_activity_progress", $dataLog);
	}

	function _summary_regional_total($owner) 
	{
		 /* $sr = strtoupper(substr($owner,0,3));
		 if( $sr ==  "NEP" )
		 {
			 $wher =  "owner_program='$owner'"; 
		 }
		 else
		 { */
			 $wher =  "regional='$owner'"; 
		 //}			
		 //COALESCE(( COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0) ),0) * 100 As per
																
		  $dat = $this->db->query("
								 select 
								 sum(s.jumlah_activity) AS target_total,
								 sum(s.jumlah_activity_done) as jumlah_activity_done,
								 COALESCE(( COALESCE(sum(s.jumlah_activity_done), 0)/COALESCE(sum(s.jumlah_activity), 0) ),0) * 100 As per
								 from (							
										select DISTINCT a.activity,
										COALESCE(b.jumlah_activity, 0) As jumlah_activity,
										COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done
										from activity_progress a
										left join ( select count('activity') as jumlah_activity,activity 
												   from activity_progress where ".$wher."
												   group by activity 
												   ) b on b.activity=a.activity
										left join ( select count('activity') as jumlah_activity_done,activity 
												   from activity_progress where (
												   progress='Closed' or progress='CLOSED' or progress='closed' ) 
												   and ".$wher."
												   group by activity
												   ) c on c.activity=a.activity														
								 )s")->result_array(); 			
		 return $dat[0];			   
	}

	function progres_tracking_regional_query_admin()
	{
			   //$rs = $this->progres_tracking_regional_query();
			   //$rs = $this->progress_tracking_data_net_nas();
			   $rs = $this->db->query("select * from
									 (
										select DISTINCT a.regional
										from activity_progress a
										where regional
										like '%[R%'
									   
										group by a.regional
									 ) ac 
									")->result_array();
			   $r =array();
			   foreach($rs as $row)
			   { 
				//$per = ( $row['jumlah_done'] / $row['jumlah_target'] ) * 100;
				//$per2 = round($per,2);
				$per2 = round($this->_summary_regional_total($row["regional"])['per'],2);
				//array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
  
  
				//$regi = $row["regional"]." ".array_regional()[$row["regional"]];
				$regi = $row["regional"];
				array_push($r,array("owner_program"=>$regi, 
				"target"=> $this->_summary_regional_total($row["regional"])['target_total'],
				"persen"=>$per2,
				"done"=> $this->_summary_regional_total($row["regional"])['jumlah_activity_done'] ));	
			   }  
  
		 $ret = [
			  "chart"=> [
			  "caption"=> "Progress Tracking",
			  "subcaption"=> "( Per Regional )",
			  //"xaxisname"=> "Years",
			  //"yaxisname"=> "Total",
			  //"pyaxisname"=> "Units Sold",
			  //"formatnumberscale"=> "1",
			  "plottooltext" => '<b>$dataValue</b>  activity on <b>$seriesName</b> in $label',
			  //"syaxisname"=> "% of total ",
			  "labelDisplay" => "Auto",
			  "useEllipsesWhenOverflow"  => "1",
			  "snumbersuffix"=> "%",
			  "showvalues"=> "0",
			  "syaxismaxvalue"=> "100",
			  "theme"=>"fusion",
			  "drawcrossline"=> "1",
			  "divlinealpha"=> "20"
			],
			"categories"=> [
			  [
				"category"=> [
				  [
					"label"=> "R1"
				  ],
				  [
					"label"=> "R2"
				  ],
				  [
					"label"=> "R3"
				  ],
				  [
					"label"=> "R4"
				  ],
				  [
					"label"=> "R5"
				  ],
				  [
					"label"=> "R6"
				  ],
				  [
					"label"=> "R7"
				  ],
				  [
					"label"=> "R8"
				  ],
				  [
					"label"=> "R9"
				  ],
				  [
					"label"=> "R10"
				  ],
				  [
					"label"=> "R11"
				  ]
				]
			  ]
			],
			  "dataset"=> [
	  [
		"dataset"=> [
		  [
			"seriesname"=> "Target",
			"data"=> [
						  [
							"value"=> $r[array_search('[R01]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R02]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R03]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R04]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R05]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R06]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R07]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R08]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R09]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R10]', array_column($r, 'owner_program'))]['target']
						  ],
						  [
							"value"=> $r[array_search('[R11]', array_column($r, 'owner_program'))]['target']
						  ]
			]
		  ],
		  
		]
	  ],
	  [
		"dataset"=> [
		  [
			"seriesname"=> "Done",
			"data"=> [
				  [
					"value"=> $r[array_search('[R01]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R02]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R03]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R04]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R05]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R06]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R07]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R08]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R09]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R10]', array_column($r, 'owner_program'))]['done']
				  ],
				  [
					"value"=> $r[array_search('[R11]', array_column($r, 'owner_program'))]['done']
				  ]
			]
		  ],
		  
		]
	  ]
	],
		  
			  "lineset"=> [
			  [
				"seriesname"=> "on progress %",
				"plottooltext"=> 'Total $label is <b>$dataValue</b> ',
				"showvalues"=> "1",
				"data"=> [
					  [
						"value"=> $r[array_search('[R01]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R02]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R03]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R04]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R05]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R06]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R07]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R08]', array_column($r, 'owner_program'))]['persen']
					  ],
					  
					  [
						"value"=> $r[array_search('[R09]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R10]', array_column($r, 'owner_program'))]['persen']
					  ],
					  [
						"value"=> $r[array_search('[R11]', array_column($r, 'owner_program'))]['persen']
					  ]
				]
			  ]
			] 
		   
		  ];
  
		  return $ret;
		  
	}  

	//network
	function _summary_regional_total_network($owner) 
	{
		 /* $sr = strtoupper(substr($owner,0,3));
		 if( $sr ==  "NEP" )
		 {
			 $wher =  "owner_program='$owner'"; 
		 }
		 else
		 { */
			 $wher =  "regional='$owner'"; 
		 //}			
		 //COALESCE(( COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0) ),0) * 100 As per
																
		  $dat = $this->db->query("
								 select 
								 sum(s.jumlah_activity) AS target_total,
								 sum(s.jumlah_activity_done) as jumlah_activity_done,
								 COALESCE(( COALESCE(sum(s.jumlah_activity_done), 0)/COALESCE(sum(s.jumlah_activity), 0) ),0) * 100 As per
								 from (							
										select DISTINCT a.activity,
										COALESCE(b.jumlah_activity, 0) As jumlah_activity,
										COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done
										from activity_progress a
										left join ( select count('activity') as jumlah_activity,activity 
												   from activity_progress where ".$wher."
												   AND directorate = 'Network'
												   group by activity 
												   ) b on b.activity=a.activity
										left join ( select count('activity') as jumlah_activity_done,activity 
												   from activity_progress where (
												   progress='Closed' or progress='CLOSED' or progress='closed' ) 
												   and ".$wher."
												   AND directorate = 'Network'
												   group by activity
												   ) c on c.activity=a.activity														
								 )s")->result_array(); 			
		 return $dat[0];			   
	}


	function progres_tracking_regional_query_admin_network()
	{
			   //$rs = $this->progres_tracking_regional_query();
			   //$rs = $this->progress_tracking_data_net_nas();
			   $rs = $this->db->query("select * from
									 (
										select DISTINCT a.regional
										from activity_progress a
										where regional
										like '%[R%'
										and directorate = 'Network'
										group by a.regional
									 ) ac 
									")->result_array();
			   $r =array();
			   foreach($rs as $row)
			   { 
				//$per = ( $row['jumlah_done'] / $row['jumlah_target'] ) * 100;
				//$per2 = round($per,2);
				$per2 = round($this->_summary_regional_total_network($row["regional"])['per'],2);
				//array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
  
  
				//$regi = $row["regional"]." ".array_regional()[$row["regional"]];
				$regi = $row["regional"];
				array_push($r,array("owner_program"=>$regi, 
				"target"=> $this->_summary_regional_total_network($row["regional"])['target_total'],
				"persen"=>$per2,
				"done"=> $this->_summary_regional_total_network($row["regional"])['jumlah_activity_done'] ));	
			   }
			   
			   $label_reg_network = [];
			   $value_done_network = [];
			   $value_target_network = [];
			   $value_persen_network = [];
			   foreach($rs as $item) { 
				   array_push($label_reg_network, ["label"=> str_replace(array("[","]"),"",$item['regional'])]);
			   }  
   
			   foreach($r as $item) {
				   array_Push($value_done_network, ["value"=> $item['done'] ]);
				   array_Push($value_target_network, ["value"=> $item['target']]);
				   array_Push($value_persen_network, ["value"=> $item['persen']]);
			   }
  
		 $ret = [
			  "chart"=> [
			  "caption"=> "Progress Tracking Network",
			  "subcaption"=> "( Per Regional )",
			  //"xaxisname"=> "Years",
			  //"yaxisname"=> "Total",
			  //"pyaxisname"=> "Units Sold",
			  //"formatnumberscale"=> "1",
			  "plottooltext" => '<b>$dataValue</b>  activity on <b>$seriesName</b> in $label',
			  //"syaxisname"=> "% of total ",
			  "labelDisplay" => "Auto",
			  "useEllipsesWhenOverflow"  => "1",
			  "snumbersuffix"=> "%",
			  "showvalues"=> "0",
			  "syaxismaxvalue"=> "100",
			  "theme"=>"fusion",
			  "drawcrossline"=> "1",
			  "divlinealpha"=> "20"
			],
			"categories"=> [
			  [
				"category"=> $label_reg_network
			  ]
			],
			  "dataset"=> [
	  [
		"dataset"=> [
		  [
			"seriesname"=> "Target",
			"data"=> $value_target_network
		  ],
		  
		]
	  ],
	  [
		"dataset"=> [
		  [
			"seriesname"=> "Done",
			"data"=> $value_done_network
		  ],
		  
		]
	  ]
	],
		  
			  "lineset"=> [
			  [
				"seriesname"=> "on progress %",
				"plottooltext"=> 'Total $label is <b>$dataValue</b> ',
				"showvalues"=> "1",
				"data"=> $value_persen_network
			  ]
			] 
		   
		  ];
  
		  return $ret;
		  
	} 
	//network

   //sales
   function _summary_regional_total_sales($owner) 
   {
		/* $sr = strtoupper(substr($owner,0,3));
		if( $sr ==  "NEP" )
		{
			$wher =  "owner_program='$owner'"; 
		}
		else
		{ */
			$wher =  "regional='$owner'"; 
		//}			
		//COALESCE(( COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0) ),0) * 100 As per
															   
		 $dat = $this->db->query("
								select 
								sum(s.jumlah_activity) AS target_total,
								sum(s.jumlah_activity_done) as jumlah_activity_done,
								COALESCE(( COALESCE(sum(s.jumlah_activity_done), 0)/COALESCE(sum(s.jumlah_activity), 0) ),0) * 100 As per
								from (							
									   select DISTINCT a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done
									   from activity_progress a
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress where ".$wher."
												  AND directorate = 'Sales'
												  group by activity 
												  ) b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and ".$wher."
												  AND directorate = 'Sales'
												  group by activity
												  ) c on c.activity=a.activity														
								)s")->result_array(); 			
		return $dat[0];			   
   }


   function progres_tracking_regional_query_admin_sales()
   {
			  //$rs = $this->progres_tracking_regional_query();
			  //$rs = $this->progress_tracking_data_net_nas();
			  $rs = $this->db->query("select * from
									(
									   select DISTINCT a.regional as regional
									   from activity_progress a
									   where regional
									   like '%[R%'
									   AND directorate = 'Sales'
									   group by a.regional
									) ac 
								   ")->result_array();
			  $r =array();
			  foreach($rs as $row)
			  { 
			   //$per = ( $row['jumlah_done'] / $row['jumlah_target'] ) * 100;
			   //$per2 = round($per,2);
			   $per2 = round($this->_summary_regional_total_sales($row["regional"])['per'],2);
			   //array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
 
 
			   //$regi = $row["regional"]." ".array_regional()[$row["regional"]];
			   $regi = $row["regional"]; 
			   array_push($r,array("owner_program"=>$regi, 
			   "target"=> $this->_summary_regional_total_sales($row["regional"])['target_total'],
			   "persen"=>$per2,
			   "done"=> $this->_summary_regional_total_sales($row["regional"])['jumlah_activity_done'] ));	
			  }

			$label_reg_sales = [];
			$value_done_sales = [];
			$value_target_sales = [];
			$value_persen_sales = [];
			foreach($rs as $item) { 
				array_push($label_reg_sales, ["label"=> str_replace(array("[","]"),"",$item['regional'])]);
			}  

			foreach($r as $item) {
				array_Push($value_done_sales, ["value"=> $item['done'] ]);
				array_Push($value_target_sales, ["value"=> $item['target']]);
				array_Push($value_persen_sales, ["value"=> $item['persen']]);
			}

			// echo json_encode($value_done_sales); die();
 
		$ret = [
			 "chart"=> [
			 "caption"=> "Progress Tracking Sales",
			 "subcaption"=> "( Per Regional )",
			 //"xaxisname"=> "Years",
			 //"yaxisname"=> "Total",
			 //"pyaxisname"=> "Units Sold",
			 //"formatnumberscale"=> "1",
			 "plottooltext" => '<b>$dataValue</b>  activity on <b>$seriesName</b> in $label',
			 //"syaxisname"=> "% of total ",
			 "labelDisplay" => "Auto",
			 "useEllipsesWhenOverflow"  => "1",
			 "snumbersuffix"=> "%",
			 "showvalues"=> "0",
			 "syaxismaxvalue"=> "100",
			 "theme"=>"fusion",
			 "drawcrossline"=> "1",
			 "divlinealpha"=> "20"
		   ],
		   "categories"=> [
			 [
			   "category"=> $label_reg_sales
			 ]
		   ],
			 "dataset"=> [
	 [
	   "dataset"=> [
		 [
		   "seriesname"=> "Target",
		   "data"=> $value_target_sales
		 ],
		 
	   ]
	 ],
	 [
	   "dataset"=> [
		 [
		   "seriesname"=> "Done",
		   "data"=> $value_done_sales
		 ],
		 
	   ]
	 ]
   ],
		 
			 "lineset"=> [
			 [
			   "seriesname"=> "on progress %",
			   "plottooltext"=> 'Total $label is <b>$dataValue</b> ',
			   "showvalues"=> "1",
			   "data"=> $value_persen_sales
			 ]
		   ] 
		  
		 ];
 
		 return $ret;
		 
   } 
   //sales

   //it
   function _summary_regional_total_it($owner) 
   {
		/* $sr = strtoupper(substr($owner,0,3));
		if( $sr ==  "NEP" )
		{
			$wher =  "owner_program='$owner'"; 
		}
		else
		{ */
			$wher =  "regional='$owner'"; 
		//}			
		//COALESCE(( COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0) ),0) * 100 As per
															   
		 $dat = $this->db->query("
								select 
								sum(s.jumlah_activity) AS target_total,
								sum(s.jumlah_activity_done) as jumlah_activity_done,
								COALESCE(( COALESCE(sum(s.jumlah_activity_done), 0)/COALESCE(sum(s.jumlah_activity), 0) ),0) * 100 As per
								from (							
									   select DISTINCT a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done
									   from activity_progress a
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress where ".$wher." 
												  AND directorate = 'Information Technology'
												  group by activity 
												  ) b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and ".$wher."
												  AND directorate = 'Information Technology'
												  group by activity
												  ) c on c.activity=a.activity														
								)s")->result_array(); 			
		return $dat[0];			   
   } 


   function progres_tracking_regional_query_admin_it()
   {
			  //$rs = $this->progres_tracking_regional_query();
			  //$rs = $this->progress_tracking_data_net_nas();
			  $rs = $this->db->query("select * from
									(
									   select DISTINCT a.regional 
									   from activity_progress a 
									   where regional 
									   like '%[R%'
									   and directorate = 'Information Technology'
									   group by a.regional
									) ac 
								   ")->result_array();
			  $r =array();
			  foreach($rs as $row)
			  { 
			   //$per = ( $row['jumlah_done'] / $row['jumlah_target'] ) * 100;
			   //$per2 = round($per,2);
			   $per2 = round($this->_summary_regional_total_it($row["regional"])['per'],2);
			   //array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
 
  
			   //$regi = $row["regional"]." ".array_regional()[$row["regional"]]; 
			   $regi = $row["regional"];  
			   array_push($r,array("owner_program"=>$regi,  
			   "target"=> $this->_summary_regional_total_it($row["regional"])['target_total'],
			   "persen"=>$per2,
			   "done"=> $this->_summary_regional_total_it($row["regional"])['jumlah_activity_done'] ));	
			  }  

			  $label_reg_it = [];
			  $value_done_it = [];
			  $value_target_it = [];
			  $value_persen_it = [];
			  foreach($rs as $item) { 
				  array_push($label_reg_it, ["label"=> str_replace(array("[","]"),"",$item['regional'])]);
			  }   
  
			  foreach($r as $item) {
				  array_Push($value_done_it, ["value"=> $item['done'] ]);
				  array_Push($value_target_it, ["value"=> $item['target']]);
				  array_Push($value_persen_it, ["value"=> $item['persen']]);
			  }
 
		$ret = [
			 "chart"=> [
			 "caption"=> "Progress Tracking Information Technology",
			 "subcaption"=> "( Per Regional )",
			 //"xaxisname"=> "Years",
			 //"yaxisname"=> "Total",
			 //"pyaxisname"=> "Units Sold",
			 //"formatnumberscale"=> "1",
			 "plottooltext" => '<b>$dataValue</b>  activity on <b>$seriesName</b> in $label',
			 //"syaxisname"=> "% of total ",
			 "labelDisplay" => "Auto",
			 "useEllipsesWhenOverflow"  => "1",
			 "snumbersuffix"=> "%", 
			 "showvalues"=> "0",
			 "syaxismaxvalue"=> "100",
			 "theme"=>"fusion",
			 "drawcrossline"=> "1",
			 "divlinealpha"=> "20"
		   ],
		   "categories"=> [
			 [
			   "category"=> $label_reg_it
			 ]
		   ],
			 "dataset"=> [
	 [
	   "dataset"=> [
		 [
		   "seriesname"=> "Target",
		   "data"=> $value_target_it
		 ],
		 
	   ]
	 ],
	 [
	   "dataset"=> [
		 [
		   "seriesname"=> "Done",
		   "data"=> $value_done_it
		 ],
		 
	   ]
	 ]
   ],
		 
			 "lineset"=> [
			 [
			   "seriesname"=> "on progress %",
			   "plottooltext"=> 'Total $label is <b>$dataValue</b> ',
			   "showvalues"=> "1",
			   "data"=> $value_persen_it 
			 ]
		   ]   
		  
		 ];
 
		 return $ret;
		 
   } 
   //it

	//chart owner program
	function progres_tracking_owner_program_query($owner_program)
	{	

		if($owner_program == 'IT HQ') {
			$datas = $this->db->query("SELECT a.owner_program, a.section, 
			COALESCE(COUNT(*), 0) AS jumlah_activity,
			COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
			FROM activity_progress a
				LEFT JOIN 
				( 
					SELECT owner_program, section, COUNT(*) AS jumlah_activity_done FROM activity_progress
					WHERE progress = 'Closed'
					AND owner_program IN ('IT CHARGING SERVICES','IT FULFILLMENT SERVICES','IT INFRASTRUCTURE MANAGEMENT','IT NEW BUSINESS SERVICES', 'IT SQM')
					GROUP BY section 
				) b ON a.section = b.section	
			WHERE a.owner_program IN ('IT CHARGING SERVICES','IT FULFILLMENT SERVICES','IT INFRASTRUCTURE MANAGEMENT','IT NEW BUSINESS SERVICES', 'IT SQM')
			GROUP BY a.section")->result_array();		 	
		
		$section = [];
		$jumlah_activity = [];
		$jumlah_activity_done = [];
		$average = [];

		foreach($datas as $item) {
			array_push($section,['label'=>$item['section']]);
			array_push($jumlah_activity,['value'=>$item['jumlah_activity']]);
			array_push($jumlah_activity_done,['value'=>$item['jumlah_activity_done']]);
			array_push($average,['value'=> (($item['jumlah_activity_done']/$item['jumlah_activity'])*100)]);
		}
	
	} else {

		$datas = $this->db->query("SELECT a.owner_program, a.section, 
		COALESCE(COUNT(*), 0) AS jumlah_activity,
		COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
		FROM activity_progress a
			LEFT JOIN 
			( 
				SELECT owner_program, section, COUNT(*) AS jumlah_activity_done FROM activity_progress
				WHERE progress = 'Closed'
				AND owner_program = '$owner_program'
				GROUP BY section 
			) b ON a.section = b.section	
		WHERE a.owner_program = '$owner_program'
		GROUP BY a.section")->result_array(); 

		$section = [];
		$jumlah_activity = [];
		$jumlah_activity_done = [];
		$average = [];

		foreach($datas as $item) {
			array_push($section,['label'=>$item['section']]);
			array_push($jumlah_activity,['value'=>$item['jumlah_activity']]);
			array_push($jumlah_activity_done,['value'=>$item['jumlah_activity_done']]);
			array_push($average,['value'=> (($item['jumlah_activity_done']/$item['jumlah_activity'])*100)]);
		}

		}

		// echo json_encode($datas); die();

		// echo json_encode($section); die(); 
		 $ret = [
			  "chart"=> [
			  "caption"=> "Progress Tracking <br> $owner_program",
			  "subcaption"=> "( Per Type )",
			  //"xaxisname"=> "Years",
			  //"yaxisname"=> "Total",
			  //"pyaxisname"=> "Units Sold",
			  //"formatnumberscale"=> "1",
			  "plottooltext" => '<b>$dataValue</b>  activity on <b>$seriesName</b> in $label',
			  //"syaxisname"=> "% of total ",
			  "labelDisplay" => "Auto",
			  "useEllipsesWhenOverflow"  => "1",
			  "snumbersuffix"=> "%",
			  "showvalues"=> "0",
			  "syaxismaxvalue"=> "100",
			  "theme"=>"fusion",
			  "drawcrossline"=> "1",
			  "divlinealpha"=> "20"
			],
			"categories"=> [
			  [
				"category"=> $section
			  ]
			],
			  "dataset"=> [
	  [
		"dataset"=> [
		  [
			"seriesname"=> "Target",
			"data"=> $jumlah_activity
		  ],
		  
		]
	  ],
	  [
		"dataset"=> [
		  [
			"seriesname"=> "Done",
			"data"=> $jumlah_activity_done
		  ],
		  
		]
	  ]
	],
		  
			  "lineset"=> [
			  [
				"seriesname"=> "on progress %",
				"plottooltext"=> 'Total $label is <b>$dataValue</b> ',
				"showvalues"=> "1",
				"data"=> $average
			  ]
			] 
		   
		  ];
  
		  return $ret;
		  
	}

    

}
