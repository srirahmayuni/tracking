<?php

class Album_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'album';
    }
   
    public function getSql()
	{
		//$id=0;
		$id = $this->session->userdata('users_id');
		if($this->session->userdata('group_id')==1)
		{
			
			return "select 
			    a.*
		        from album a
				join users s on s.users_id = a.created_by
                where 1=1 			
		      ";
			
		}
        else
        {
			return "select a.*
					from album a
					join users s on s.users_id = a.created_by
					where 1=1  and a.created_by=$id		 
		      ";
		}			
		
	}
	public function GetByID($id)
	{
       $r = $this->db->query( "select a.*
		        from album a
				where a.id_album='$id'")->row(); 			
		return $r;
	}

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('id_album', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('id_album', $id);
        $this->db->delete($this->tableName);
    }


    

}
