<?php

class Activity_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'activity_progress';
    }

    public function getSql($req)
	{   
		$sq="";
		if($req['columns'][1]['search']['value'])
	    {
			$sq.="and a.progress like '%".$req['columns'][1]['search']['value']."%' ";
		}
		if(!empty($req['columns'][2]['search']['value']))
	    {
			$sq.="and a.target_week like '%".$req['columns'][2]['search']['value']."%' ";
		}
		if($req['columns'][3]['search']['value'])
	    {
			$sq.="and a.activity like '%".$req['columns'][3]['search']['value']."%' ";
		}
		if($req['columns'][4]['search']['value'])
	    {
			$sq.="and a.detail_activity like '%".$req['columns'][4]['search']['value']."%' ";
		}	
        if($req['columns'][5]['search']['value'])
	    {
			$sq.="and a.directorate like '%".$req['columns'][5]['search']['value']."%' ";
		}
		if($req['columns'][6]['search']['value'])
	    {
			$sq.="and a.group like '%".$req['columns'][6]['search']['value']."%' ";
		}
		if($req['columns'][7]['search']['value'])
	    {
			$sq.="and a.owner_program like '%".$req['columns'][7]['search']['value']."%' ";
		}
		if($req['columns'][8]['search']['value'])
	    {
			$sq.="and a.section like '%".$req['columns'][8]['search']['value']."%' ";
		}
		if($req['columns'][9]['search']['value'])
	    {
			$sq.="and a.regional like '%".$req['columns'][9]['search']['value']."%' ";
		}
		if($req['columns'][10]['search']['value'])
	    {
			$sq.="and a.supporting_program like '%".$req['columns'][10]['search']['value']."%' ";
		}
		if($req['columns'][11]['search']['value'])
	    {
			$sq.="and a.nama_poi like '%".$req['columns'][11]['search']['value']."%' ";
		}
        if($req['columns'][12]['search']['value'])
	    {
			$sq.="and a.divisi_in_charge like '%".$req['columns'][12]['search']['value']."%' ";
		}
		if($req['columns'][13]['search']['value'])
	    {
			$sq.="and a.pic like '%".$req['columns'][13]['search']['value']."%' ";
		}	
        if($req['columns'][14]['search']['value'])
	    {
			$sq.="and a.pic_support like '%".$req['columns'][14]['search']['value']."%' ";
		}
 

		if( $this->session->userdata('group_id')==1 or $this->session->userdata('group_id')==2)
		{
			return "select 
			    a.*
		        from activity_progress a
				
                where 1=1 	$sq	
		      ";
		}  
		else if ( $this->session->userdata('groupName')== 'IT HQ' ) {
			return "select 
			    a.*
		        from activity_progress a
                where 1=1 $sq and a.owner_program in ('IT CHARGING SERVICES','IT FULFILLMENT SERVICES','IT INFRASTRUCTURE MANAGEMENT','IT NEW BUSINESS SERVICES', 'IT SQM', 'IT UX Enablement')			
		        ";
		} 
        else 
        {
			return "select 
			    a.*
		        from activity_progress a
				
                where 1=1 $sq and a.owner_program='".$this->session->userdata('groupName')."'			
		      ";
		}			
			
   }
   public function getExcel2()
   {
	    //$r = $this->getSql();
		$r = "select 
			    b.detail_activity,a.created_date,a.status,c.id as user
		        from log_activity_progress a
				left join activity_progress b on b.id_activity_progress=a.id_activity_progress
				left join users c on c.users_id=a.created_by
                where 1=1 	
		      ";
	    $ss = $this->db->query($r);
		return $ss->result();
	   
   }
   
   public function getExcel()
   {
	   $r = "select 
			    a.*
		        from activity_progress a
				
                where 1=1 	
		      ";
	    $ss = $this->db->query($r);
		return $ss->result();
	   
   }
   
   public function GetByID($id)
   {
       $r = $this->db->query( "select a.*
		        from activity_progress a
				where a.id_activity_progress='$id'")->row(); 			
		return $r;
	}

    public function ins($data) {
        return parent::_insert2($this->tableName, $data);
    }

    public function upd($data, $id) {
        $this->db->where('id_activity_progress', $id);
        return parent::_update3($this->tableName, $data);
    }
	
	public function insLog($dataLog)
	{
		//$this->db->insert('activity_progress', $dataLog);
		return parent::_insert2("log_activity_progress", $dataLog);
	}

   


    

}
