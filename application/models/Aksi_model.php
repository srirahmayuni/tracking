<?php

class Aksi_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'actions';
    }

    public function getName($_id) {
        $return = null;
        // ambil data dari db
        $this->db->select('action');
        $this->db->where('action_id', $_id);

        $result = $this->db->get($this->tableName);
        // $query = $this->db->last_query();
        // echo "q = ".$query;exit;

        $ret = $result->row();
        if (isset($ret)) {
            $return = $ret->action;
        }

        return $return;
    }

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('action_id', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('action_id', $id);
        $this->db->delete($this->tableName);
    }

    function ComboAction() {
        $result = $this->db->query('select * from actions');
        return $result->result_array();
    }

    public function getSql($req)
	{
		
		$sq = '';
		if($req['gr_id'] and $req['id_page'])
		//if($req['gr_id'])
		{	
			$sq="where a.group_id = ".$req['gr_id']." and a.page_id=".$req['id_page']."";
        }
		 $sql = "
		       select * from 
			   (
					select * from (    
								Select 
									c.action,a.allow,a.privilege_action_id,d.page_id,b.group_id,c.action_id
								from 
								privilege_action a
									left join groups b on b.group_id=a.group_id
									left join actions c on c.action_id=a.action_id
									left join pages d on d.page_id=a.page_id
								$sq
								union all
								select action, '' as allow,'','','',action_id
								from actions
							) as zz group by zz.action_id
               )	as zz where 1=1 
				"; 
		return $sql;    
	}
    

}
