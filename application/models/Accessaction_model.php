<?php

class Accessaction_model extends MY_Model {

    private $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'privilege_action';
    }

    public function getSql() {
        
      $sql =  "
      			select 
      			pa.privilege_action_id AS id,
      			g.name as groupname,
      			pg.controller as pagename,
      			ac.action as actionname,
      			pa.allow as allow,
      			pa.group_id,
      			pa.page_id,
      			pa.action_id
      			
      			from privilege_action pa
                left join groups g on g.group_id=pa.group_id
      			left join pages pg on pg.page_id=pa.page_id
      			left join actions ac on ac.action_id=pa.action_id
				where 1=1

      			";

      return $sql;
    }
	
	function GetAccessactionByID($id)
	{
		$s = "select * from privilege_action where privilege_action_id='".$id."'";
		$data = $this->db->query($s)->row();
		return $data;
	}

    public function insert($data) {
        return parent::_insert($this->tableName, $data);
    }

    public function update($data, $id) {
        $this->db->where('privilege_action_id', $id);
        return parent::_update($this->tableName, $data);
    }

    public function delete($id) {
        $this->db->where('privilege_action_id', $id);
        $this->db->delete($this->tableName);
    }

    function ComboAction() {
        $result = $this->db->query('select * from actions');
        return $result->result_array();
    }

    public function cekDelete($id) {
		
        $q = "select * from privilege_action where privilege_action_id=$id";
        $da = $this->db->query($q)->row();
		
		$p_id = $da->page_id;
		
		$q2 = "select * from menu where page_id=$p_id";
        $da2 = $this->db->query($q);
		
        if($da2->num_rows()>0)
        {
			//tak bisa didelete
			return true;
		}
        else
        {
			return false;
		}			
        
    }
    

}
