<?php
class MY_Loader extends CI_Loader {

function mymodel($model, $folder = '',$vars = array(), $return = FALSE) {

    array_push($this->_ci_model_paths, ""); //replace "" with any other directory
    parent::model($model);
}


function myview($folder, $view, $vars = array(), $return = FALSE) {
        $this->_ci_view_paths = array_merge($this->_ci_view_paths, array(APPPATH . '../' . $folder . '/' => TRUE));
        return $this->_ci_load(array(
                '_ci_view' => $view,
                '_ci_vars' => $this->_ci_object_to_array($vars),
                '_ci_return' => $return
        ));
}

 public function base_view($view, $vars = array(), $get = FALSE) {
            //  ensures leading /
            if ($view[0] != '/') $view = '/' . $view;
            //  ensures extension   
            $view .= ((strpos($view, ".", strlen($view)-5) === FALSE) ? '.php' : '');
            //  replaces \'s with /'s
            $view = str_replace('\\', '/', $view);

            if (!is_file($view)) if (is_file($_SERVER['DOCUMENT_ROOT'].$view)) $view = ($_SERVER['DOCUMENT_ROOT'].$view);
           //if (!is_file($view)) if (is_file(base_url().$view)) $view = (base_url().$view);

            if (is_file($view)) {
                if (!empty($vars)) extract($vars);
                ob_start();
                include($view);
                $return = ob_get_clean();
                if (!$get) echo($return);
                return $return;
            }

            //return show_404($view);
            return show_404($_SERVER['DOCUMENT_ROOT'].$view);
           // return $_SERVER['DOCUMENT_ROOT'].$view;
             // return $this->view($_SERVER['DOCUMENT_ROOT'].$view, $vars, $return);
             //return APPPATH;
        }

   public function load_ext_view($view, $vars = array(), $return = FALSE)
  {
    //$view_file = '/full/path/to/'.$view.'.php';
    $view_file = base_url().$view.'.php';

    if(file_exists($view_file)){
       $view_to_load = array('_ci_path'   => $view_file,
                             '_ci_view'   => $view,
                             '_ci_vars'   => $this->_ci_object_to_array($vars),
                             '_ci_return' => $return
                             );

       return $this->_ci_load($view_to_load);
    }

       return $this->view($view, $vars, $return);
    }     

}

?>