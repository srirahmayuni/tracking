<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $_fields;

    public function __construct() {
        parent::__construct();

		
        //$isAdmin = $this->uri->segment(1);

        //if ($isAdmin == folderBack()) {
           if ($this->session->userdata('logged_in') !== TRUE)
                need_login();
        //}
        $siteLang = $this->session->userdata('site_lang');
        if ($siteLang == NULL) {
            $siteLang = 'bahasa_indonesia';
        }
        $this->lang->load('template', $siteLang);

        $this->config->set_item('language', $siteLang);

        $this->backButtonHandle();
        //$this->checkBaseUrl();
    }

    protected function _load_layout($content, $data) {
		$data['menus'] = $this->get_menu();
        $data['authorization'] = $this->get_authorization();
        $this->template->load('admin', $content, $data);
        //$this->template->load('admin', $content, $data);
    }
    
   
    protected function get_menu() {
        return $this->session->userdata('menus');
    }

    protected function get_authorization() {
        return $this->session->userdata('authorization');
    }

    protected function result($response) {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response)
        );
    }

    function backButtonHandle() {
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', FALSE);
        header('Pragma: no-cache');
    }

    public function checkBaseUrl() {
        $c = current_url();
        $s = site_url('admin');
        if ($this->session->userdata('logged_in') == TRUE && ($c == $s))
            //redirect(folderBack().'/home/index');
            redirect('/home/index');
    }
	
	/* Front */
	protected function _load_layout_cetak($data_masuk) {
		
		 $this->template->load('base_cetak',$data_masuk['view'], $data_masuk);
	}	
	protected function _load_layout_front($data_masuk) {
	    //menu depan top
		$data = array();
        // $mn='';
        // $menu= $this->db->query("SELECT * FROM menu_depan where position='Top' AND aktif='Ya' ORDER BY urutan ASC")->result_array();
        // foreach($menu as $me)
        // {
           // $mn.= "<li><a href='$me[link]'>$me[nama_menu]</a></li>";
        // } 
		
		 $link= $this->db->query("SELECT id_identitas,nama_website,
		                          logo_website,
								  email,
								  url,
								  facebook,
								  no_telp,
								  alamat,
		                          meta_deskripsi,
								  meta_keyword,
                                  footer,
                                  favicon 								  
								  
								  FROM identitas")->result_array()[0];
         $data['identitas'] =  $link;
		// $data['menu_top'] = $mn;
		$data = array_merge($data,$data_masuk);
        $this->template->load('base_view',$data_masuk['view'], $data);
	
	}

    
	
	/* protected function _load_layout_frontXX($data_masuk) {
		//$data['menus'] = $this->get_menu();
    	$dfg = $this->home->template();
		$data['f'] = $dfg;
		$data['c'] = $this->home->background();
		// data header /
		$data['berita_runing_text'] =  $this->home->berita_runing_teks_atas();
       
        $dh='';
        $link= $this->db->query("SELECT * FROM identitas")->result_array()[0];
        $data['identitas'] =  $link;
        $logo= $this->db->query("SELECT * FROM logo ORDER BY id_logo DESC LIMIT 1")->result_array();
        foreach($logo as $b)
        {
           $dh.= "<a href='$link[url]'><img src='".base_url().$dfg['folder']."/logo/$b[gambar]'/></a>";
        }  
        $data['logo'] = $dh;

        //menu depan top
        $mn='';
        $menu= $this->db->query("SELECT * FROM menu_depan where position='Top' AND aktif='Ya' ORDER BY urutan ASC")->result_array();
        foreach($menu as $me)
        {
           $mn.= "<li><a href='$me[link]'>$me[nama_menu]</a></li>";
        } 
		
        $data['menu_top'] = $mn;

        $data['linkk']="<p>$link[meta_deskripsi] hubungi kami di : $link[no_telp] - $link[email]</p>";
        //menu depan
        $data['menu_depan'] = $this->home->menu_depan();
		 //tag
        $tg='';
        $tag = $this->db->query("SELECT * FROM tag order by RAND() DESC LIMIT 14")->result_array();
        foreach ($tag as $t){
            $tg.= "<li><a href='tag-$t[tag_seo].html'>$t[nama_tag]</a></li>";
        }
        $data['tag'] = $tg;
		// data header end /
		
		$data = array_merge($data,$data_masuk);
        
		$this->load->view('templates/'.$dfg['folder'].'/template',$data);
    } */
    

}
