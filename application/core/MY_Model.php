<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function _created() {
        //$id = NULL;
        $id = 0; // 0 jika default jika tidak login biasanya dari front end
        if ($this->session->userdata('users_id'))
            $id = $this->session->userdata('users_id');

        return array(
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $id
        );
    }

    public function _modified() {
        //$id = NULL;
        $id = 0; // 0 jika default jika tidak login biasanya dari front end
        if ($this->session->userdata('users_id'))
            $id = $this->session->userdata('users_id');

        return array(
            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' => $id
        );
    }

    public function _insert($tabel, $data) {
        $data = array_merge($data, $this->_created());
        $this->db->insert($tabel, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

	public function _insert2($tabel, $data) {
        $data = array_merge($data, $this->_created());
        $this->db->insert($tabel, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }
	
    public function _update($tabel, $data) {
        $data = array_merge($data, $this->_modified());
        return $this->db->update($tabel, $data);
    }

    //addali
    public function _update2($tabel, $data, $arrID) {
        $data = array_merge($data, $this->_modified());
        return $this->db->update($tabel, $data, $arrID);
    }
	
	public function _update3($tabel, $data) {
        $data = array_merge($data, $this->_modified());
        return $this->db->update($tabel, $data);
    }

}
