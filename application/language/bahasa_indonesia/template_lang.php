<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* Indonesia */
$lang['menu_cara_kerja'] = 'Cara Kerja';
$lang['menu_mengapa'] = 'Mengapa';
$lang['menu_armada'] = 'Armada';
$lang['menu_partner'] = 'Transporter';
$lang['menu_pemesanan'] = 'Pemesanan';
$lang['footer_sitemap'] = 'Sitemap';
$lang['footer_sitemap_utama'] = 'Utama';
$lang['footer_sitemap_mengapa'] = 'Mengapa';
$lang['footer_sitemap_armada'] = 'Armada';
$lang['footer_sitemap_partner'] = 'Transporter';
$lang['footer_sitemap_pemesanan'] = 'Pemesanan';
$lang['footer_sitemap_syarat'] = 'Syarat & ketentuan';
$lang['footer_hak_cipta'] = ' Hak Cipta Dilindungi Undang-Undang.';
$lang['footer_terhubung_medsos'] = 'Terhubung dengan Kami';
