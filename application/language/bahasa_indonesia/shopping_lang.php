<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#indonesia
$lang['pembayaran_pemesanan'] = 'PEMESANAN';
$lang['pembayaran_pemesanan_order'] = 'Pesan';
$lang['pembayaran_pemesanan_bayar'] = 'Bayar';
$lang['pembayaran_pemesanan_konfirmasi'] = 'Konfirmasi';
$lang['pembayaran_pemesanan_pesanan_anda'] = 'Pesanan anda';
$lang['pembayaran_pemesanan_tanggal_pickup'] = 'Tanggal Pickup';
$lang['pembayaran_pemesanan_jumlah'] = 'Jumlah';
$lang['pembayaran_pemesanan_harga'] = 'Harga';
$lang['pembayaran_pemesanan_tabel_kolom_lokasi'] = 'Lokasi';
$lang['pembayaran_pemesanan_tabel_kolom_lokasi_asal'] = 'Asal';
$lang['pembayaran_pemesanan_tabel_kolom_lokasi_tujuan'] = 'Tujuan';
$lang['pembayaran_pemesanan_tabel_kolom_alamat'] = 'Alamat';
$lang['pembayaran_pemesanan_tabel_kolom_nama'] = 'Nama';
$lang['pembayaran_pemesanan_tabel_kolom_telepon'] = 'Telepon';
$lang['pembayaran_pemesanan_grand_total'] = 'Grand total Rp';
$lang['pembayaran_pemesanan_button_pembayaran'] = 'Pembayaran';
$lang['pembayaran_pemesanan_informasi_detail_pemesanan'] = 'Informasi detail pemesanan';
$lang['pembayaran_pemesanan_nama_lengkap'] = 'Nama lengkap';
$lang['pembayaran_pemesanan_email'] = 'Email';
$lang['pembayaran_pemesanan_telepon'] = 'Telepon';
$lang['pembayaran_pemesanan_sudah_memiliki_akun'] = 'Sudah Memiliki Akun ? Login Disini';
$lang['pembayaran_pemesanan_button_login'] = 'Login';
$lang['pembayaran_pemesanan_informasi_pemesanan'] = 'Informasi pemesanan';
$lang['pembayaran_pemesanan_peringantan'] = "<p>Harap lakukan pembayaran transfer pada rekening dibawah ini <strong>TIDAK LEBIH DARI 45 MENIT</strong>:</p>
										 <p>Pemegang Rekening: <strong>PT. Cari Truk Sehat</strong><br />
											 Nomor Rekening: <strong>450-822-3399</strong><br />
											 Nama Bank: <strong>BCA Cabang Bidakara Jakarta</strong></p>
										 <p>Mohon mencantumkan nomor pengiriman anda pada berita transfer. Pesanan anda akan di batalkan <strong>TANPA</strong> konfirmasi jika pembayaran <strong>TIDAK</strong> dilakukan dalam waktu 45 menit.</p>";
$lang['pembayaran_pemesanan_button_proses_pembayaran'] = 'Proses pembayaran';
$lang['pembayaran_pemesanan_terima_kasih'] = 'Terima kasih atas Pesanan yang telah Anda lakukan';
$lang['pembayaran_pemesanan_kode_invoice'] = 'Kode Invoice Anda';
$lang['pembayaran_pemesanan_total_dibayar'] = 'Total yang harus di bayar';
$lang['pembayaran_pemesanan_button_konfirmasi_pembayaran'] = 'Konfirmasi pembayaran';
$lang['pembayaran_konfirmasi'] = 'KONFIRMASI';
$lang['pembayaran_konfirmasi_informasi_konfirmasi'] = 'Informasi Konfirmasi Pembayaran';
$lang['pembayaran_konfirmasi_no_invoice'] = 'No invoice';
$lang['pembayaran_konfirmasi_no_rekening'] = 'No rekening';
$lang['pembayaran_konfirmasi_nama_pemilik_rekening'] = 'Nama pemilik rekening';
$lang['pembayaran_konfirmasi_bank'] = 'Bank';
$lang['pembayaran_konfirmasi_bank_pilih'] = 'Pilih';
$lang['pembayaran_konfirmasi_bank_selainnya'] = 'Selainnya';
$lang['pembayaran_konfirmasi_nominal'] = 'Nominal Transfer *';
$lang['pembayaran_konfirmasi_tangal_transfer'] = 'Tanggal transfer';
$lang['pembayaran_konfirmasi_melalui_bank'] = 'Melalui bank';
$lang['pembayaran_konfirmasi_informasi_tambahan'] = 'Informasi Tambahan (optional)';
$lang['pembayaran_konfirmasi_tulis_informasi_tambahan'] = 'Tulis informasi tambahan yang ingin di sampaikan';
$lang['pembayaran_konfirmasi_button_kirim_konfirmasi'] = 'Kirim konfirmasi';
$lang['pembayaran_konfirmasi_jumlah_lebih'] = 'Konfirmasi ditolak nominal melebihi jumlah harga Order !';
$lang['pembayaran_hapus_pesanan'] = 'Hapus pesanan?';
