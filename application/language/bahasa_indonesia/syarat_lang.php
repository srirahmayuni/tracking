<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#bahasa indonesia
$lang['syarat_ketentuan'] = 'Syarat dan ketentuan Pengiriman';
$lang['syarat_ketentuan_konten'] = 'Sebelum menggunakan layanan <a href=' . site_url() . '> caritruk.com </a> , Anda diminta terlebih dahulu membaca syarat dan ketentuan di bawah ini. Dengan menggunakan CariTruk.com, Anda berarti setuju dengan syarat dan ketentuan dari <a href=' . site_url() . '> caritruk.com </a> .';
$lang['syarat_kewajiban'] = 'I. Kewajiban Dan Tanggung Jawab Pengirim';
$lang['syarat_kewajiban_1'] = 'Pengirim wajib memberitahukan isi kiriman dan nilai barang yang sebenarnya, serta memberikan hak kepada perusahaan <a href=' . site_url() . '> caritruk.com </a> untuk memeriksa isi kiriman bila diperlukan jika barang yang dikirim mencurigakan.';
$lang['syarat_kewajiban_2'] = 'Pengirim/Penerima bersedia menerima dan tidak keberatan kirimannya dibuka/diperiksa oleh yang berwajib.';
$lang['syarat_kewajiban_3'] = '<a href=' . site_url() . '> caritruk.com </a> hanya bertanggung jawab atas pengiriman dan penyampaian barang kiriman ke alamat barang tujuan (penerima). jika terjadi sesuatu dan lain hal barang tersebut di tahan oleh pihak berwajib, maka pemilik barang wajib bertanggung jawab dan menyelesaikan permasalahan tersebut.';
$lang['syarat_kewajiban_4'] = 'Barang-barang yang akan di kirim wajib dan diharuskan dibungkus/dipak secara sempurna. Pengepakan yang tidak sempurna dapat menimbulkan kerugian yang sepenuhnya menjadi tanggung jawab pihak pengirim.';
$lang['syarat_kewajiban_5'] = 'Pengirim bertanggung jawab terhadap semua biaya pengiriman sesuai perhitungan menurut tarif yang dikeluarkan oleh perusahaan <a href=' . site_url() . '> caritruk.com</a>.';
$lang['syarat_kewajiban_6'] = 'Pengirim wajib mengasuransikan barang kiriman selama dalam pengiriman.';
$lang['syarat_tanggung_jawab'] = 'II. Kewajiban Dan Tanggung Jawab Perusahaan';
$lang['syarat_tanggung_jawab_1'] = 'Perusahaan <a href=' . site_url() . '> caritruk.com </a> wajib mengirimkan dan  menyerahkan kiriman ke penerima tujuan dengan baik kondisi barangnya.';
$lang['syarat_tanggung_jawab_2'] = 'Perusahaan <a href=' . site_url() . '> caritruk.com </a> bertanggung jawab terhadap kiriman barang selama belum diserahkan kepada pihak penerima, kecuali jika terjadi sesuatu dan lain hal barang tersebut ditahan oleh pihak berwajib maka pihak pengirim/penerima bertanggung jawab dan menyelesaikan permasalahan tersebut.';
$lang['syarat_tanggung_jawab_3'] = 'Perusahaan <a href=' . site_url() . '> caritruk.com </a> berkewajiban membayar mengganti rugi sebesar 10 kali biaya pengiriman atau Maksimal Rp.1.000.000 (Satu Juta Rupiah) apabila terjadi kehilangan barang (Mana yang lebih besar).';
$lang['syarat_tanggung_jawab_4'] = 'Harga yang tercantum dan ditawarkan Perusahaan <a href=' . site_url() . '> caritruk.com </a> adalah harga belum termasuk Biaya Bongkar/Muat dengan menggunakan Tenaga Kerja Bongkar Muat (TKBM) ataupun forklift.';
$lang['syarat_resiko'] = 'III. Resiko Yang Dikecualikan Dan Tidak Dijamin';
$lang['syarat_resiko_1'] = 'Perusahaan <a href=' . site_url() . '> caritruk.com </a> tidak memberikan ganti rugi terhadap kerusakan/kehilangan akibat bencana alam, huru hara atau force majeure.';
$lang['syarat_resiko_2'] = 'Pengajuan claim setelah kendaraan <a href=' . site_url() . '> caritruk.com </a> keluar dari area pihak penerima barang tidak akan dilayani dan dianggap barang yang telah diterima dengan kondisi baik dan sempurna.';
$lang['syarat_larangan'] = 'IV. Larangan-Larangan';
$lang['syarat_larangan_1'] = 'Dilarang memasukkan wesel pos, cek, giro dan atau uang kedalam bungkusan kiriman dalam bentuk apapun.';
$lang['syarat_larangan_2'] = 'Dilarang mengirim barang-barang yang mudah meledak, menyala/terbakar sendiri dan barang-barang yang dapat membahayakan keselamatan umum lainnya.';
$lang['syarat_larangan_3'] = 'Dilarang mengirim barang/binatang yang dilindungi oleh pemerintah serta dilarang untuk diantar pulaukan.';
$lang['syarat_larangan_4'] = 'Dilarang mengirim barang-barang berupa narkotika dan sejenisnya serta obat terlarang lainnya.';
$lang['syarat_larangan_5'] = 'Dilarang mengirim barang-barang cetakan/rekaman yang isinya dapat mengganggu keamanan/ketertiban dan stabilitas nasional dan/atau menyinggung kesusilaan, serta barang-barang yang melanggar hukum lainnya yang berlaku di Negara Republik Indonesia.';
$lang['syarat_packing_kayu'] = 'V. Packing kayu';
$lang['syarat_packing_kayu_1'] = 'Packing kayu adalah untuk barang/kiriman yang mudah pecah/rusak/bocor.<br>
                                Setiap barang yang pengirimannya tidak mau di- Packing kayu, maka jika ada kerusakan barang adalah tanggungan pengirim. Hal ini harus dideklarasikan pada saat transaksi dan dituliskan di Delivery Order (DO).
                                <br><br>
                                CATATAN : Namun ada barang yang benar-benar wajib di-packing kayu adalah barang yang jika rusak merugikan barang lainnya atau merugikan armada angkutan. contoh : cairan.';
$lang['syarat_bahan_kimia'] = 'VI. Penanganan Zat Kimia/Dangerous Good';
$lang['syarat_bahan_kimia_1'] = 'Beberapa penanganan khusus diperuntukan untuk cairan/zat kimia/dangerous good.
                                Kiriman zat/cairan kimia harus menyertakan MSDS (Material Safety Data Sheet), yaitu surat keterangan yang berisi tentang kandungan, sifat dan cara penanganan zat. MSDS dikeluarkan oleh lembaga/perusahaan/laboratorium resmi.';
