<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Field above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (//ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (//bcit.ca/)
 * @license	//opensource.org/licenses/MIT	MIT License
 * @link	//codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required'] = 'Field {field} harus diisi.';
$lang['form_validation_isset'] = 'Field {field} harus memiliki nilai.';
$lang['form_validation_valid_email'] = 'Field {field} harus berisi format e-mail yang benar.';
$lang['form_validation_valid_emails'] = 'Field {field} harus berisi format e-mail yang benar.';
$lang['form_validation_valid_url'] = 'Field {field} harus berisi format URL yang benar.';
$lang['form_validation_valid_ip'] = 'Field {field} harus berisi format IP yang benar.';
$lang['form_validation_min_length'] = 'Field {field} minimal {param} karakter.';
$lang['form_validation_max_length'] = 'Field {field} maksimal {param} karakter.';
$lang['form_validation_exact_length'] = 'Field {field} harus tepat {param} karakter.';
$lang['form_validation_alpha'] = 'Field {field} harus berisi alphabet.';
$lang['form_validation_alpha_numeric'] = 'Field {field} harus berisi alpha-numeric.';
$lang['form_validation_alpha_numeric_spaces'] = 'Field {field} harus berisi alpha-numeric dan spasi.';
$lang['form_validation_alpha_dash'] = 'Field {field} harus berisi alpha-numeric, underscores, dashes.';
$lang['form_validation_numeric'] = 'Field {field} hanya boleh berisi bilangan.';
$lang['form_validation_is_numeric'] = 'Field {field} hanya boleh berisi karakter angka.';
$lang['form_validation_integer'] = 'Field {field} harus berisi integer.';
$lang['form_validation_regex_match'] = 'Field {field} harus berisi format yang benar.';
$lang['form_validation_matches'] = 'Field {field} tidak sesuai dengan field {param}.';
$lang['form_validation_differs'] = 'Field {field} field must differ from the {param} field.';
$lang['form_validation_is_unique'] = 'Field {field} sudah digunakan';
$lang['form_validation_is_natural'] = 'Field {field} harus berisi bilangan positif.';
$lang['form_validation_is_natural_no_zero'] = 'Field {field} harus berisi bilangan bukan 0 (nol).';
$lang['form_validation_decimal'] = 'Field {field} harus berisi bilangan desimal.';
$lang['form_validation_less_than'] = 'Field {field} harus berisi bilangan < {param}.';
$lang['form_validation_less_than_equal_to'] = 'Field {field} harus berisi bilangan <= {param}.';
$lang['form_validation_greater_than'] = 'Field {field} harus berisi bilangan > {param}.';
$lang['form_validation_greater_than_equal_to'] = 'Field {field} harus berisi bilangan >= {param}.';
$lang['form_validation_error_message_not_set'] = 'Unable to access an error message corresponding to your field name {field}.';
$lang['form_validation_in_list'] = 'Field {field} harus bagian dari: {param}.';
