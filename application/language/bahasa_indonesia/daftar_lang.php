<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#bahasa indonesia
$lang['daftar_partner'] = 'Daftar Sebagai Mitra Transporter';
$lang['daftar_partner_konten'] = 'Ijinkan kami mengenal anda lebih dalam lagi. Kami akan segera menghubungi anda';
$lang['form_nama_perusahan'] = 'Nama Perusahan';
$lang['form_contact_person'] = 'Contact Person';
$lang['form_no_hp'] = 'No Hp';
$lang['form_email'] = 'Email';
$lang['form_catatan'] = 'Catatan';
$lang['form_ex'] = 'ex : Masukan pendapat/saran';
$lang['button_kirim'] = 'Send';
$lang['terimakasih_telah_daftar'] = 'Telah mendaftar menjadi mitra transporter CariTruk. Kami akan hubungi Anda';
