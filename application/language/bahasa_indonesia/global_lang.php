<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#bahasa indonesia
$lang['lokasi'] = 'Lokasi';
$lang['kota'] = 'Kota';
$lang['global_detail_alamat'] = 'Detail Alamat';
$lang['global_nama'] = 'Nama';
$lang['lokasi_asal'] = 'Lokasi Asal';
$lang['lokasi_tujuan'] = 'Lokasi Tujuan';
$lang['jenis_truk'] = 'Jenis truk';
$lang['global_pickup_bak'] = 'pickup bak';
$lang['global_dimensi_maks'] = 'Dimensi Maks';
$lang['global_volume_maks'] = 'Volume Maks';
$lang['global_beban_maks'] = 'Beban Maks';
$lang['global_harga'] = 'Harga';
$lang['global_pesan'] = 'pesan';
$lang['global_pickup_box'] = 'pickup box';
$lang['global_cd 4_bak'] = 'cd 4 bak';
$lang['global_mulai_dari_harga'] = 'Mulai dari';
$lang['global_cd_6_bak'] = 'cd 6 bak';
$lang['global_fuso_bak'] = 'fuso bak';
$lang['global_terima_kasih'] = 'Terima Kasih';
$lang['global_submit'] = 'Kirim';
$lang['global_jumlah'] = 'Jumlah';
$lang['global_lokasi'] = 'Lokasi';
$lang['global_alamat'] = 'Alamat';
$lang['global_telepon'] = 'Telepon';
$lang['global_pic'] = 'PIC';
$lang['global_email'] = 'Email';
$lang['global_password'] = 'Kata Sandi';
$lang['global_password_confirm'] = 'Konfirmasi Kata Sandi';
$lang['global_asal'] = 'Asal';
$lang['global_tujuan'] = 'Tujuan';
$lang['global_bahasa_kode'] = 'id';
$lang['global_bahasa'] = 'Bahasa';
$lang['home_waktu_operasional'] = 'Jam Operasional : Senin - Jumat 08:00 - 17:00 di luar hari libur Nasional';
$lang['home_waktu_operasional_detail'] = 'Pemesanan diluar jam operasional akan dilayani pada waktu operasional berikutnya';