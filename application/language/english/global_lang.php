<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#english
$lang['lokasi'] = 'Location';
$lang['kota'] = 'City';
$lang['global_detail_alamat'] = 'Address';
$lang['global_nama'] = 'Name';
$lang['lokasi_asal'] = 'Pick-up Location';
$lang['lokasi_tujuan'] = 'Drop-off Location';
$lang['jenis_truk'] = 'Truck Type';
$lang['global_pickup_bak'] = 'pick-up truck';
$lang['global_dimensi_maks'] = 'Maximum dimension';
$lang['global_volume_maks'] = 'Maximum volume';
$lang['global_beban_maks'] = 'Maximum weight';
$lang['global_harga'] = 'Rate';
$lang['global_pic'] = 'PIC';
$lang['global_pesan'] = 'Message/Request';
$lang['global_pickup_box'] = 'Pick-up Box/Small moving truck';
$lang['global_cd_4_bak'] = 'Dump truck (small)';
$lang['global_mulai_dari_harga'] = 'Starting from';
$lang['global_cd_6_bak'] = 'Dump truck (medium)';
$lang['global_fuso_bak'] = 'Fuso Dump Truck (large)';
$lang['global_terima_kasih'] = 'Thank You';
$lang['global_submit'] = 'Submit';
$lang['global_jumlah'] = 'Qty';
$lang['global_lokasi'] = 'Location';
$lang['global_alamat'] = 'Address';
$lang['global_telepon'] = 'Phone';
$lang['global_email'] = 'Email';
$lang['global_password'] = 'Password';
$lang['global_password_confirm'] = 'Confirm your password';
$lang['global_asal'] = 'Origin';
$lang['global_tujuan'] = 'Destination';
$lang['global_bahasa_kode'] = 'en';
$lang['global_bahasa'] = 'Language';
$lang['home_waktu_operasional'] = 'Operational Time: Monday - Friday 08:00 - 17:00 Outside of national holidays';
$lang['home_waktu_operasional_detail'] = 'Reservations outside of operational time will be served at the next operational time';