<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#english
$lang['syarat_ketentuan'] = 'Terms and Conditions of Service';
$lang['syarat_ketentuan_konten'] = 'Before using <a href=' . site_url() . '> CariTruk</a>, we request sender to read the terms and conditions of the agreement below. By using the website, sender are implying that sender agree with all the policies of <a href=' . site_url() . '> CariTruk </a>.';
$lang['syarat_kewajiban'] = "I. Sender Liability";
$lang['syarat_kewajiban_1'] = 'Sender have fully and adequately described the goods, including but not limited to providing a full and accurate description of the goods, weight, volume, number, of items and to declare the actual market value of the goods. Sender shall be deemed to authorize <a href=' . site_url() . '> CariTruk </a> to check the content if the goods are intended suspicious.';
$lang['syarat_kewajiban_2'] = 'Sender agree that <a href=' . site_url() . '> CariTruk </a> or any governmental authority including customs may open and inspect your shipment at any time and take any action considered necessary in relation to the goods if <a href=' . site_url() . '> CariTruk </a> or any governmental authorities consider the shipment may contain prohibited items or dangerous goods.';
$lang['syarat_kewajiban_3'] = "<a href='.site_url().'> CariTruk </a> is solely responsible for the consignment and arrival of sent goods to the receiver's adress. If something happens that cause the goods kept by the authorities, then the sender must be responsible to solve the issue.";
$lang['syarat_kewajiban_4'] = 'Before making shipment, goods have been prepared and packed safely and carefully by sender with the outer packaging to protect the goods against shock, vibration, compression. Sender will be liable for any possible damages caused by the incorrectly packaged goods.';
$lang['syarat_kewajiban_5'] = "Sender or customer is fully liable for all the shipment fee as displayed on <a href=" . site_url() . "> CariTruk </a>'s website.";
$lang['syarat_kewajiban_6'] = 'Sender must use insurance coverage during the shipment.';
$lang['syarat_tanggung_jawab'] = "II .<a href='.site_url().'> CariTruk's </a> Liability";
$lang['syarat_tanggung_jawab_1'] = "<a href='.site_url().'> CariTruk </a> is responsible to send the goods to the recipient's address in a good conditions";
$lang['syarat_tanggung_jawab_2'] = 'The conditions of goods that have not been delivered to the receiver will be sole responsibility of <a href=' . site_url() . '> CariTruk </a>. If something happens that cause the goods kept by the authorities, then the sender must be responsible to solve the issue.';
$lang['syarat_tanggung_jawab_3'] = '<a href=' . site_url() . '> CariTruk </a> will be liable for any cost caused by lost material or any damages and responsible to pay compensation with Rp 1.000.000 as the maximum value.';
$lang['syarat_tanggung_jawab_4'] = "Rate as displayed on <a href='.site_url().'> CariTruk </a>'s website is not included loading and unloading service with the help of man power or forklift.";
$lang['syarat_resiko'] = 'III. Unguaranteed Risks';
$lang['syarat_resiko_1'] = '<a href=' . site_url() . '> CariTruk </a> shall not liable for any loss or damage arising out of any circumstances beyond the control of <a href=' . site_url() . '> CariTruk </a>. These include but are not limited to "Act of God" i.e. earthquakes, floods, cyclone, storm, fog etc. or "Force Majeure" i.e. Riots, civil war; any act.';
$lang['syarat_resiko_2'] = "Sender's right to claim damages against <a href='.site_url().'> CariTruk </a> shall be objected when the transporter has been out from the receiver address area. In that condition, <a href='.site_url().'> CariTruk </a> will assume that your goods has been accepted by the receiver in a good condition.";
$lang['syarat_larangan'] = 'IV. Unacceptable Shipments';
$lang['syarat_larangan_1'] = 'It contains money order, cheque or cash in any form.';
$lang['syarat_larangan_2'] = 'It is classified as explosive, flammable and combustible goods and other stuff that can endanger public safety.';
$lang['syarat_larangan_3'] = 'Goods or animals that are protected by the government and restricted to be transferred.';
$lang['syarat_larangan_4'] = 'Illegal items, such as narcotics and other types of drugs.';
$lang['syarat_larangan_5'] = 'Printed material and/or recording that can be disrupted security, public order, national stability and/or offend decency or violate other laws of the Republic of Indonesia.';
$lang['syarat_packing_kayu'] = 'V. Timber Packing';
$lang['syarat_packing_kayu_1'] = 'Timber packing reserved for goods or items that are easily broken, damaged or leaking. If there is any damage to the goods that is not sent by timber packing (which is supposed to), the responsibility is on the sender. It must be declared at the transaction and written on Delivery Order (DO).
                                <br><br>
                                NOTE: Goods that if it is damaged can harm the fleets must use timber packing. Example: liquid.';
$lang['syarat_bahan_kimia'] = 'VI.  Handling of Chemical Substances or Dangerous Goods';
$lang['syarat_bahan_kimia_1'] = 'Some special treatments intended for liquids, chemicals, and/or dangerous goods. Substance or chemical solution must include MSDS (Material Safety Data Sheet), which is a certificate explaining the content, nature and the way of handling the substance. MSDS must be issued by the institution, company, and/or official laboratory.';
