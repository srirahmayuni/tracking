<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#english
$lang['daftar_partner'] = 'Register as Transporter';
$lang['daftar_partner_konten'] = "Allow us to know you better! We'll contact you right away.";
$lang['form_nama_perusahan'] = 'Company Name';
$lang['form_contact_person'] = 'Contact Person';
$lang['form_no_hp'] = 'Phone ';
$lang['form_email'] = 'Email';
$lang['form_catatan'] = 'Notes';
$lang['form_ex'] = 'Ex.: type your suggestion';
$lang['terimakasih_telah_daftar'] = 'Registration succeeded! We will be in touch soon.';
