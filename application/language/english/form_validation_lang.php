<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Field above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (//ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (//bcit.ca/)
 * @license	//opensource.org/licenses/MIT	MIT License
 * @link	//codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required'] = 'Field {field} must be filled.';
$lang['form_validation_isset'] = 'Field {field} must contain value.';
$lang['form_validation_valid_email'] = 'Field {field} not valid.';
$lang['form_validation_valid_emails'] = 'Field {field} not valid.';
$lang['form_validation_valid_url'] = 'Field {field} URL format not valid.';
$lang['form_validation_valid_ip'] = 'Field {field} IP format in valid.';
$lang['form_validation_min_length'] = 'Field {field} minimum {param} character.';
$lang['form_validation_max_length'] = 'Field {field} maximum {param} character.';
$lang['form_validation_exact_length'] = 'Field {field} must exact {param} character.';
$lang['form_validation_alpha'] = 'Field {field} must contain any alphabet.';
$lang['form_validation_alpha_numeric'] = 'Field {field} must contain any alpha-numeric.';
$lang['form_validation_alpha_numeric_spaces'] = 'Field {field} must contain any alpha-numeric and space.';
$lang['form_validation_alpha_dash'] = 'Field {field} must contain any alpha-numeric, underscores, dashes.';
$lang['form_validation_numeric'] = 'Field {field} number only.';
$lang['form_validation_is_numeric'] = 'Field {field} numeric only.';
$lang['form_validation_integer'] = 'Field {field} integer only.';
$lang['form_validation_regex_match'] = 'Field {field} wrong format.';
$lang['form_validation_matches'] = 'Field {field} mismatch with field {param}.';
$lang['form_validation_differs'] = 'Field {field} field must differ from the {param} field.';
$lang['form_validation_is_unique'] = 'Field {field} already use';
$lang['form_validation_is_natural'] = 'Field {field} must be positif number.';
$lang['form_validation_is_natural_no_zero'] = 'Field {field} must be not 0 (zero).';
$lang['form_validation_decimal'] = 'Field {field} decimal only.';
$lang['form_validation_less_than'] = 'Field {field} number must be < {param}.';
$lang['form_validation_less_than_equal_to'] = 'Field {field} number must be <= {param}.';
$lang['form_validation_greater_than'] = 'Field {field} number must be > {param}.';
$lang['form_validation_greater_than_equal_to'] = 'Field {field} number must be >= {param}.';
$lang['form_validation_error_message_not_set'] = 'Unable to access an error message corresponding to your field name {field}.';
$lang['form_validation_in_list'] = 'Field {field} must be part of : {param}.';
