<?php

defined('BASEPATH') OR exit('No direct script access allowed');
#english
$lang['pembayaran_pemesanan'] = 'Reservation';
$lang['pembayaran_pemesanan_order'] = 'Order';
$lang['pembayaran_pemesanan_bayar'] = 'Buy';
$lang['pembayaran_pemesanan_konfirmasi'] = 'Confirmation ';
$lang['pembayaran_pemesanan_pesanan_anda'] = 'Your order';
$lang['pembayaran_pemesanan_tanggal_pickup'] = 'Pickup Date';
$lang['pembayaran_pemesanan_jumlah'] = 'Total';
$lang['pembayaran_pemesanan_harga'] = 'Price';
$lang['pembayaran_pemesanan_tabel_kolom_lokasi'] = 'Location';
$lang['pembayaran_pemesanan_tabel_kolom_lokasi_asal'] = 'Origin';
$lang['pembayaran_pemesanan_tabel_kolom_lokasi_tujuan'] = 'Destination';
$lang['pembayaran_pemesanan_tabel_kolom_alamat'] = 'Address';
$lang['pembayaran_pemesanan_tabel_kolom_nama'] = 'Name';
$lang['pembayaran_pemesanan_tabel_kolom_telepon'] = 'Phone';
$lang['pembayaran_pemesanan_grand_total'] = 'Grand Total Rp';
$lang['pembayaran_pemesanan_button_pembayaran'] = 'Payment';
$lang['pembayaran_pemesanan_informasi_detail_pemesanan'] = 'Order details';
$lang['pembayaran_pemesanan_nama_lengkap'] = 'Full name';
$lang['pembayaran_pemesanan_email'] = 'Email';
$lang['pembayaran_pemesanan_telepon'] = 'Phone';
$lang['pembayaran_pemesanan_sudah_memiliki_akun'] = 'Already have an account? Login here.';
$lang['pembayaran_pemesanan_button_login'] = 'Login';
$lang['pembayaran_pemesanan_informasi_pemesanan'] = 'Order information';
$lang['pembayaran_pemesanan_peringantan'] = "<p>Please pay your invoice WITHIN 45 minutes.</p>
<p>Name: <strong>PT. Cari Truk Sehat</strong></br>
Account number: <strong>450-822-3399 </strong></br>
Bank: <strong>BCA Cabang Bidakara Jakarta </strong></p>
<p>Please include your delivery number on the transfer news. Your order will be canceled <strong>WITHOUT</strong> notification, if the payment is <strong>NOT</strong> made within 45 minutes.</p>";
$lang['pembayaran_pemesanan_button_proses_pembayaran'] = 'Payment ';
$lang['pembayaran_pemesanan_terima_kasih'] = 'Thank you for ordering.';
$lang['pembayaran_pemesanan_kode_invoice'] = 'Your invoice code';
$lang['pembayaran_pemesanan_total_dibayar'] = 'The total amount that you must pay are';
$lang['pembayaran_pemesanan_button_konfirmasi_pembayaran'] = 'Payment confirmation';
$lang['pembayaran_konfirmasi'] = 'CONFIRM';
$lang['pembayaran_konfirmasi_informasi_konfirmasi'] = 'Payment confirmation details';
$lang['pembayaran_konfirmasi_no_invoice'] = 'Invoice code';
$lang['pembayaran_konfirmasi_no_rekening'] = 'Account number';
$lang['pembayaran_konfirmasi_nama_pemilik_rekening'] = 'Name';
$lang['pembayaran_konfirmasi_bank'] = 'Bank';
$lang['pembayaran_konfirmasi_bank_pilih'] = 'Choose';
$lang['pembayaran_konfirmasi_bank_selainnya'] = 'Other';
$lang['pembayaran_konfirmasi_nominal'] = 'Transfer value';
$lang['pembayaran_konfirmasi_tangal_transfer'] = 'Transfer date';
$lang['pembayaran_konfirmasi_melalui_bank'] = 'Via bank';
$lang['pembayaran_konfirmasi_informasi_tambahan'] = 'Additional info';
$lang['pembayaran_konfirmasi_tulis_informasi_tambahan'] = 'Leave a message';
$lang['pembayaran_konfirmasi_button_kirim_konfirmasi'] = 'Confirm';
$lang['pembayaran_konfirmasi_jumlah_lebih'] = 'Confirmation is rejected. Rate exceeds the total price of order';
$lang['pembayaran_hapus_pesanan'] = 'Delete order?';
