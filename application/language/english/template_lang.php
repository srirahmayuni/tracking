<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* english */
$lang['menu_cara_kerja'] = 'How It Works';
$lang['menu_mengapa'] = 'Why Us?';
$lang['menu_armada'] = 'Fleets';
$lang['menu_partner'] = 'Transporter';
$lang['menu_pemesanan'] = 'Order';
$lang['footer_sitemap'] = 'Sitemap';
$lang['footer_sitemap_utama'] = 'Home';
$lang['footer_sitemap_mengapa'] = 'Why Us?';
$lang['footer_sitemap_armada'] = 'Fleets';
$lang['footer_sitemap_partner'] = 'Transporter';
$lang['footer_sitemap_pemesanan'] = 'Order';
$lang['footer_sitemap_syarat'] = 'Terms and Conditions';
$lang['footer_hak_cipta'] = 'All right reserved';
$lang['footer_terhubung_medsos'] = 'Connect with us';
