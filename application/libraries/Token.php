<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Fitrah Hidayat
 * Email : fitrah@caritruk.com
 * Description : 
 * ***************************************************************
 */

class Token extends CI_Controller {

    protected $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }

    public function checkToken($users_id, $token) {
        $this->ci->load->database();
        $_check = "SELECT * FROM users WHERE users_id = '" . $users_id . "' AND token = '" . $token . "' AND token_status = 1";
        $sql_check = $this->ci->db->query($_check);

        if ($sql_check->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }

}
