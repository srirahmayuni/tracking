<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Andy Caherudin.
 * Email : andychaerudin@gmail.com
 * Description : 
 * ***************************************************************
 */

/**
 * Description of Pushgcm
 *
 * @author by Andy
 * @modified by Rizky
 */
class Pushgcm {

    var $ci;

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->database();
    }

    public function pushing($method = NULL, $arrUserID, $msg, $activity, $orderdet_id) {
        $pushStatus = "";
        $pushStatusNew = "";
        $regDevices = array();
        $regDevices_new = array();
        $users_ids = array();

        if (!empty($method) && $method == "push") {
            //Get All Registered Devices
            foreach ($arrUserID as $row) {
                $users_ids[] = $row->ids;
            }

            $this->ci->db->select('gcm_token, users_id');
            $this->ci->db->from('users');
            $this->ci->db->where('group_id', '3');
            $this->ci->db->where_in('users_id', $users_ids);
            $this->ci->db->order_by("gcm_token", "desc");
            $sql_devices = $this->ci->db->get();

            $devices = $sql_devices->result();

            $pushMessage = $msg;
            foreach ($devices as $row) {
                if ($row->gcm_token != NULL) {
                    $regDevices[] = $row->gcm_token;
                    $messageOld = array("message" => $pushMessage, "activity" => $activity, "orderdet_id" => $orderdet_id, "users_id" => $row->users_id);

                    $pushStatus = $this->sendPushNotificationToGCM($regDevices, $messageOld);
                }
            }
        }
    }

    /* Funtion Push Notification (NEW API KEY) */

    public function sendPushNotificationToGCM($registatoin_ids, $message) {
        //Google cloud messaging GCM-API url
        //$url = 'https://android.googleapis.com/gcm/send';
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );

        // Replace this:
        // define('constant', 'value');
        // Google Cloud Messaging GCM API Key
        // with this:
        //if (!defined('GOOGLE_API_KEY')) define('GOOGLE_API_KEY', 'AIzaSyDEAxOl_eZZBm0_ixCB0NoZXvYozca6i3o'); (API KEY lama)
        // define("GOOGLE_API_KEY", "AIzaSyDEAxOl_eZZBm0_ixCB0NoZXvYozca6i3o"); 	
        // define("server_key", "AAAA301nUR8:APA91bF9_oX8F3vPcXyqKR36FTyytKEEXRUmFnojigwgD78Bs2JsLrtHYHE68Xe-zHbpvHseHFiRqyvSiyxIMxxFatXHMScWDkBEno5TCMflSc8ldqXUseW_i57Dy4OXtUE5r37EaeEo"); 	
        // define("legacy_server_key", "AIzaSyDnk5SSZwejo6Evu4Z7CqbX23KZGtddYJE"); 	
        if (!defined('GOOGLE_API_KEY'))
            define('GOOGLE_API_KEY', 'AIzaSyDnk5SSZwejo6Evu4Z7CqbX23KZGtddYJE');

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}
