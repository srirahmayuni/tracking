<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Fitrah Hidayat
 * Email : fitrah@caritruk.id   
 * Description : library for send sms using zenziva.id
 * ***************************************************************
 */

class Zenziva extends CI_Controller {

    protected $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }

    public function send($to, $message) {

        $return = false;

        if (defined('ENVIRONMENT') && ENVIRONMENT=='development') {
            $this->_send('087887754517', $to." - ".$message);
            // $this->_send('081260689371', $message);            
        }else{
            $this->_send($to,$message);
        }
        
        return $return;
    }



    private function _send($phone,$message){
        $url = "https://reguler.zenziva.net/apps/smsapi.php";
        $userkey = "hesyg4";
        $passkey = "Admin123**";

        $fields = array(
                'userkey' => $userkey,
                'passkey' => $passkey,
                'nohp'  => $phone,
                'pesan' =>$message
            );

        //?userkey=hesyg4&passkey=&nohp=081297610516&pesan=Ada kerjaan baru di Caritruk.com silahkan ajukan bid bit.ly/caritruk
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        var_dump($result);
        if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    
}