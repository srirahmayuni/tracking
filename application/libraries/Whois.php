<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter cek users
 * librari untuk cek apakah users sudah pernah create data di aplikasi.
 *
 * syarat penggunaannya adalah setiap table harus memiliki field created_by dan modified_by (atau sebangsanya)
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Andy Chaerudin
 */
class Whois extends CI_Controller {

    var $ci;
    var $db;

    function __construct() {
        $this->ci = & get_instance();
        $this->db = $this->ci->db->database;
    }

    public function loud($users_id) {
        //echo "server = ".$this->db->hostname;
        $tables = NULL;
        $flag = false;
        $tables = $this->getTables();

        foreach ($tables as $rows) {
            // echo "<br />table = ". $rows->TABLE_NAME;
            $return = null;
            $query = $this->ci->db->query("							
										SELECT   created_by, modified_by
										FROM " . $rows->TABLE_NAME . "
										WHERE (created_by = " . $users_id . " OR modified_by = " . $users_id . ")
									 ");

            $return = $query->result();
            if ($return) {
                $flag = true;
                break;
            }
        }

        return $flag;


        // echo "<pre>";
        // echo var_dump($tables);
        //exit;
        // return $this->ci->db->database;
    }

    public function getTables() {
        $return = null;
        $query = $this->ci->db->query("							
									SELECT DISTINCT TABLE_NAME 
									FROM INFORMATION_SCHEMA.COLUMNS
									WHERE COLUMN_NAME IN ('created_by','modified_by')
									AND TABLE_SCHEMA = '" . $this->db . "'
								 ");

        $return = $query->result();

        return $return;
    }

}
