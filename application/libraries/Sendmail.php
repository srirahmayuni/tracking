<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Fitrah Hidayat
 * Email : fitrah@caritruk.id   
 * Description : 
 * ***************************************************************
 */

class Sendmail extends CI_Controller {

    protected $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }

    public function send($subject, $to, $data, $view, $template) {

        $return = false;

        $this->ci->load->library('email'); //$config
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from('info@caritruk.com', 'CariTruk');
        
        
        if (defined('ENVIRONMENT') && ENVIRONMENT=='development') {
            $this->ci->email->to('devel@caritruk.id');
            if (isset($to['cc'])) {
                $this->ci->email->cc('devel@caritruk.id');
            }
            if (isset($to['bcc'])) {
                $this->ci->email->bcc('devel@caritruk.id');
            }
            $this->ci->email->subject($subject." to : ".$to['to']);
        }else{
            $this->ci->email->to($to['to']);
            if (isset($to['cc'])) {
                $this->ci->email->cc($to['cc']);
            }
            if (isset($to['bcc'])) {
                $this->ci->email->bcc($to['bcc']);
            }
            $this->ci->email->subject($subject);
        }
        

        if ($template){

            $body = $this->ci->load->view($view, $data, TRUE);

            if (is_null($data)) {
                $data = array('body' => $body);
            } else if (is_array($data)) {
                $data['body'] = $body;
            } else if (is_object($data)) {
                $data->body = $body;
            }
            
            $this->ci->email->message($this->ci->load->view('templates/email', $data, TRUE));
            // $this->ci->email->message($this->ci->template->load('email',$view, $data, TRUE));
            //$this->template->load('email', 'email/order_create_user', $dataEmail);
        }else{
            $this->ci->email->message($this->ci->load->view($view, $data, TRUE));
        }

        if ($this->ci->email->send()) {
            //echo 'Your email was sent';
            //echo $this->ci->email->print_debugger();
            $return = true;
        } else {
            echo $this->ci->email->print_debugger();
            show_error($this->ci->email->print_debugger());
        }

        return $return;
    }

}
