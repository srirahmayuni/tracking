<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Andy Caherudin.
 * Email : andychaerudin@gmail.com
 * Description : menyatukan build captcha 
 * ***************************************************************
 */

class Captchaku {
    
    var $ci;
    
    function __construct() {
        $this->ci = & get_instance();
        // Memanggil helper captcha.
        $this->ci->load->helper('captcha');
        $this->ci->load->helper('string');
    }

    public function buat_captcha() {
        // Random string untuk captcha.
        $word = strtoupper(random_string('alnum', 4));
        $this->ci->session->set_userdata('captcha', $word);

        // Konfigurasi captcha.
        $captcha = array(
            'word' => $word,
            'img_path' => './assets/captcha/',
            'img_url' => base_url() . 'assets/captcha/',
            'font_path' => './assets/font/monaco.ttf',
            'font_size' => '23',
            'img_width' => '150',
            'img_height' => '50',
            'expiration' => '1' // 1 detik
        );
        
        // Membuat gambar captcha.
        $img = create_captcha($captcha);
        // echo "<pre>";
        // echo var_dump($captcha);
        // echo "</pre>";
        // echo $img['image'];exit;
        // Mengembalikan link ke gambar captcha yang sudah dibuat.
        return $img['image'];
    }

}
