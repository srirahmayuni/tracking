<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends MY_Controller {

  
    public function __construct() {
        parent::__construct();
        $this->load->model('Identitasweb_model', 'identitas');
		$this->load->library(array('form_validation', 'pagination'));
		$this->load->helper('thumb');
       
    }

    public function index() {
       // if (!is_authorized('profile', 'index'))
       //     access_denied();
		
		$users_id = NULL;
        if ($this->session->userdata('users_id'))
            $users_id = $this->session->userdata('users_id');
        $data['title'] = title().' | Jasa-jasa Perusahaan';
        $data['breadcrumb'] = array('Beranda' => base_url() .'/layanan');
		$err = false;
		$messageProf = NULL;
        $messageProfSucc = NULL;

        $id_identitas = 1;
        $profile_perusahaan = null;
        //print_r($_POST); exit;
		$dataId =$this->identitas->getData($id_identitas);
		if ($this->input->post('Simpan')=='Simpan') {
            // hidden file
            $id_identitas = $this->input->get_post('id_identitas');
            
			$this->id_identitas =  $id_identitas ;
 
            $layanna = $this->input->get_post('layanan');
            
            //$this->form_validation->set_rules($this->myRules());
			
			//if (empty($_FILES['favicon']['name']))
			//{
			//	$this->form_validation->set_rules('favicon', 'Favicon', 'required');
			//}
			$this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() == FALSE) {
                //$this->session->set_flashdata('messageProf', validation_errors());
                //$messageProf = $this->session->flashdata('messageProf');
                 $err = true;
                 $data['pesan_error'] = $this->form_validation->error_array();
			} else {
				//$gambar =null;
				$fdfdfd1 = array(
					   'layanan' => $layanna
					 );
                // start transaction
                $this->db->trans_start();

                $this->identitas->update($fdfdfd1, $id_identitas);

                $this->db->trans_complete();
                // end transaction		
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                } else {

                    $this->session->set_flashdata('messageProfSucc', 'Layanan Jasa  berhasil disimpan!');
                    $messageProfSucc = $this->session->flashdata('messageProfSucc');

                    $dataId = $this->identitas->getData($id_identitas);
                }
            }
        } 

        $data['title'] = title().' | Jasa-jasa Perusahaan';
        $data['err'] = $err;

		$data['dataId'] = $dataId;
		$data['messageProf'] = $messageProf;
		$data['id_identitas'] = $id_identitas;
        $data['messageProfSucc'] = $messageProfSucc;
        $content = 'layanan/content';
		$this->_load_layout($content, $data);
          
    }
	
    public function myRules() {
       
        return $this->_fields = array(
            array('field' => 'layanan'
                , 'label' => 'Jasa-jasa'
                , 'rules' => 'trim|required'
            )
        );
    }
	

}
