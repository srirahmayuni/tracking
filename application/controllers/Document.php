<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends MY_Controller {

    protected $id_files;
   
    protected $_isCreate;
    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
	    $this->load->helper('thumb');
		//$this->load->model('m_files');
		//$this->load->model('m_pengguna');
		//$this->load->library('upload');
		$this->load->helper('download');
		
		
    }

  
	public function index() {
        //if (!is_authorized('download', 'index'))
        //    access_denied();

        $data['title'] = title().' | Download';
        $data['message'] = $this->session->flashdata('message');
		
		 
		$data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';
		$data['cls_reporting'] = '';
        $content = 'download/content';
        $this->_load_layout($content, $data);
    }
	public function data()
    {
        //if (!is_authorized('download', 'index'))
         //  access_denied();
	   
        $this->load->model('Download_model','download');
              
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            0 => 'a.file_judul',
            1 => 'a.file_data',
            //2 => 'a.publish',
            2 => 'a.created_by'
           
         ); 

        $defaultOrder =  'created_date desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->download->getSql();
    
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {
            $no = $requestData['start'] + 1;
        }           
     
        foreach($query[0] As $row)
        {
             $nestedData=array(); 
             $nestedData[] = 	 $no;
             $nestedData[] =  	 $row['file_judul'];
			 $si = '<a  class="btn btn-sm btn-info" href="'.base_url().'home/unduh/'.$row['file_id'].'">'.$row['file_data'].'</a>';
		     $nestedData[] =    $si;    
             //$nestedData[] =  	 $row['publish'];
			 $aksi = '<a  class="btn btn-sm btn-info" href="'.base_url().'document/update/'.$row['file_id'].'"> <i class="glyphicon glyphicon-pencil"></i>Edit</a>';
		     $aksi2 = '<a  class="btn btn-sm btn-danger"   href="#" onclick="deleted('.$row['file_id'].')"><i class="glyphicon glyphicon-trash"></i>Delete</a>';
             $nestedData[] = $aksi.' '.$aksi2;   
             $data[] = $nestedData;
             $no++;
        }
        $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
        
    }
	
	
	
	  public function create() {
       // if (!is_authorized('download', 'create'))
       //     access_denied();
        $this->load->model('Download_model','download');
        $file_id = null;
        $file_judul = null;
        $file_data = null;
        $aktif = null;
       
       
       
        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => base_url().'home',
            'Master' => '#',
            'Doc' => base_url().'document',
            'Tambah File' => base_url().'document/create'
			);
			
        $data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';		
		

        if ($this->input->post('Create')) {
            $this->_isCreate = true;
            $file_judul = $this->input->post('file_judul');
           
            //$aktif = $this->input->post('aktif');
            $this->form_validation->set_rules($this->myRules());
			if (empty($_FILES['file_data']['name']))
			{
				$this->form_validation->set_rules('file_data', 'File', 'required');
			}
			
			
			
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
                
				$arr = array();
                $lokasi_file    = $_FILES['file_data']['tmp_name'];
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['file_data']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadDownloadFile($nama_file_unik,$_FILES);
					$arr = array(
					   'file_data' => $nama_file_unik
					 );
					
				}	
				$ssss1 = array(
                     'file_judul' => $file_judul,
                     'publish' => 'YA'
                   
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->download->insert($ssss);
				$this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect('document');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Tambah File';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
        $data['file_id'] = $file_id;
        $data['file_data'] = $file_data;
        $data['file_judul'] = $file_judul;
        $data['aktif'] = $aktif;
        
		
		$content = 'download/create';
        $this->_load_layout($content, $data);
    }
	
	 public function update() {
      // if (!is_authorized('download', 'update'))
       //     access_denied();
        $this->load->model('Download_model','download');
        //$this->load->helper('thumb');   
        
        $file_id = null;
        $file_judul = null;
        $file_data = null;
        $aktif = null;
       
        
		$file_id=$this->uri->segment(3);
        $err = false;
        $this->_isCreate = false;
        $data['breadcrumb'] = array('Beranda' => base_url().'home',
            'Master' => '#',
            'Document' =>  base_url().'document',
            'Edit Files' => base_url().'document/update/'.$file_id 
			);

	    $data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';		
			
        if ($this->input->post('Create')) {
            $this->_isCreate = false;

            $file_id = $this->input->post('file_id');
            $file_judul = $this->input->post('file_judul');
            
			//$aktif = $this->input->post('aktif');
            
			
			
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
				
				$arr = array();
                $lokasi_file    = $_FILES['file_data']['tmp_name'];
				
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['file_data']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadDownloadFile($nama_file_unik,$_FILES);
					//UploadImage($nama_file_unik,'../../../foto_berita/',300,120);
					//$folder = './assets/images/img_album/';
					$i      = $file_id;
					$tabel  = 'tbl_files';
					$arr = array(
					   'file_data' => $nama_file_unik
					 );
					removeFile($i,$tabel);
				}	
				
				$ssss1 = array(
                     'file_judul' => $file_judul,
                     'publish' => 'YA'
                    
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->download->update($ssss,$file_id);
				
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect('document');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Edit File';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
		$ssssa = $this->download->getById($file_id==null ?  $this->input->post('file_id'):$file_id);
        $data['file_id'] =  $ssssa->file_id;
		
		
        $data['file_judul'] =  $ssssa->file_judul;
       
        $data['aktif'] =  $ssssa->publish;
        $data['file_data'] = $ssssa->file_data;
		$content = 'download/create';
		
        $this->_load_layout($content, $data);
    }
	
	public function delete() {
       //if (!is_authorized('download', 'delete'))
        //    access_denied();
		$this->load->model('Download_model','download');
		$ss = $this->uri->segment(3);
		
		//$sd = $this->db->query("select * from tbl_files where file_id='$ss'");
		//if($sd->num_rows()>0)
		//{
		//	$this->session->set_flashdata('message', 'Data download tidak bisa dihapus!');
		//	redirect(folderBack().'/download/index');
		//}
        //else
        //{
			removeFile($ss,"tbl_files");
			$r = $this->download->delete($ss);
			$this->session->set_flashdata('message', 'Data download berhasil dihapus!');
			redirect('document/index');
		//}			
	}
    public function myRules() {
        return $this->_fields = array(
            array(
                'field' => 'file_judul',
                'label' => 'Judul File',
                'rules' => 'required|trim' 
            )
			
        );
    }
}


