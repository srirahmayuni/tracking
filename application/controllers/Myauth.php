<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Myauth extends CI_Controller {

    protected $_fields; 
    public function __construct() {
        parent::__construct();
         $this->checkBaseUrl();
    }

    public function index() {
        $uri_return = '';
        $action = $this->uri->segment(4);
        $messageResetSuccess = NULL;
        if ($action == 'return') {
            $uri_array = $this->uri->uri_to_assoc(5);
            $uri_return = $this->uri->assoc_to_uri($uri_array);

            if ($this->session->userdata('logged_in') == TRUE) {
                redirect($uri_return);
            }
        }

        $data['title'] = title().'| Login';
        $data['uri_return'] = $uri_return;
        //$data['logo'] = $this->db->query("select logo_website from identitas where id_identitas='1'")->result_array()[0];
        $data['message'] = $this->session->flashdata('message');
        $data['messageResetSuccess'] = $this->session->flashdata('messageResetSuccess');
         //$this->template->load('login', folderBack().'/login/content', $data);
         $this->template->load('login', '/login/content', $data);

    }

    public function authentication() {
        $this->load->library('form_validation');

        $field_settings = array(
            array('field' => 'adminLoginEmail', 'label' => 'Email', 'rules' => 'trim|required'),
            array('field' => 'adminLoginPassword', 'label' => 'Password', 'rules' => 'trim|required')
        );
        $this->form_validation->set_rules($field_settings);
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('message', validation_errors());
            //redirect(folderBack());
            redirect('Myauth');
            return;
        }
        
        $email = $this->input->post('adminLoginEmail');
        $password = $this->input->post('adminLoginPassword');
        $is_success = $this->check_login($email, $password);
        //$return = $this->input->post('return');
        //$group_id = $this->session->userdata('group_id');
        //$arGroup = array("1", "2", "3", "7", "8");
                
        if (!$is_success) {
            $this->session->set_flashdata('message', 'Login gagal, email dan password tidak sesuai atau user tidak aktif!');
             redirect('Myauth');
            return;
        }
		else
		{
			redirect('/home/index');
		}	
        
        // if (in_array($group_id, $arGroup)) {
            // if (!empty($return)) {
                // redirect($return);
            // }
            // else {
                // redirect('/home/index');
            // }
        // }
        // else {
            // $this->session->set_flashdata('message', 'Anda tidak berhak masuk!');
            // $this->session->unset_userdata('logged_in');
            // redirect('myauth');
        // }
    }

    public function check_login($email, $password) {
        $this->load->model('Users_model', 'users');
        
        $this->load->model('Groups_model', 'groups');
        $login = $this->users->login($email, $password);

        if ($login) {
            $this->session->set_userdata('logged_in', $login);
            $authorized = $this->_build_authorization($login['users_id']);
            $userProfiles = null;
            $userProfiles1 = null;
            $userProfiles2 = null;
            $groupName = NULL;
            // getGroupName
            if ($this->groups->getGroupName($login['group_id']))
                $groupName = $this->groups->getGroupName($login['group_id']);
			// getGroupName
            if ($this->groups->getGroupName($login['group_id']))
                $getGroupNameNote = $this->groups->getGroupNameNote($login['group_id']);

            // sbg superadmin
            if ($login['group_id'] == 1) {
                // -- User Profile --
                $userProfiles = array(
                    'users_id' => $login['users_id'],
                    'group_id' => $login['group_id'],
                    'groupName' => $groupName,
                    'groupNameNote' => $getGroupNameNote,
                    'id' => $login['id'],
                    'first_name' => $login['first_name'],
                    'last_name' => $login['last_name'],
                    'email' => $login['email'],
                    'active' => $login['active'],
                    'logged_in' => TRUE,
                    'menus' => $this->_build_menu($authorized),
                    'authorization' => $authorized
                );
            } else {
               
			   

                $userProfiles2 = array(
                    'users_id' => $login['users_id'],
                    'group_id' => $login['group_id'],
                    'groupName' => $groupName,
					'groupNameNote' => $getGroupNameNote,
                    'id' => $login['id'],
                    'first_name' => $login['first_name'],
                    'last_name' => $login['last_name'],
                    'email' => $login['email'],
                    'active' => $login['active'],
                    'logged_in' => TRUE,
                    'menus' => $this->_build_menu($authorized),
                    'authorization' => $authorized
                );

                // $userProfiles = array_merge($userProfiles1, $userProfiles2);
                $userProfiles = $userProfiles2;
            }

          
            $this->session->set_userdata($userProfiles);

            return TRUE;
        }

        return FALSE;
    }

    private function _build_authorization($user_id) {
        $results = array();

        // -- Get authorization of page and action --
        $this->db->select("pages.controller AS page, actions.action AS action");
        $this->db->select("(CASE WHEN privilege_action.allow = 1 THEN 1 ELSE 0 END) AS allow", FALSE);
        $this->db->from('privilege_action');
        $this->db->join('users', "users.group_id = privilege_action.group_id");
        $this->db->join('pages', "pages.page_id = privilege_action.page_id");
        $this->db->join('actions', "actions.action_id = privilege_action.action_id");
        $this->db->where('users.users_id', $user_id);
        $this->db->group_by(array('pages.controller', 'actions.action'));
        $page_action_src = $this->db->get();
        //echo $this->db->last_query();exit;
        $page_actions = $page_action_src->result();
        foreach ($page_actions as $page_action) {
            $results[$page_action->page][$page_action->action] = $page_action->allow;
        }

        return $results;
    }

    private function _build_menu($authorized, $menu_id_parent = NULL) {
        $results = array();

        $this->db->order_by('menu.urutan', 'asc');
        if ($menu_id_parent != NULL)
            $this->db->where('menu.parent_menu_id', $menu_id_parent);
        else
            $this->db->where('menu.parent_menu_id IS NULL', NULL, FALSE);

        $this->db->select("menu.menu_id, menu.menu_name AS name, pages.controller AS page, actions.action AS action");
        $this->db->from('menu');
        $this->db->join('pages', "pages.page_id = menu.page_id", 'left');
        $this->db->join('actions', "actions.action_id = menu.action_id", 'left');
        $menu_resource = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($menu_resource->num_rows() > 0) {
            $menus = $menu_resource->result();
            foreach ($menus as $menu) {
                $menu->childs = $this->_build_menu($authorized, $menu->menu_id);

                $is_authorized = FALSE;
                if ($menu->page != NULL && $menu->action != NULL) {
                    if (isset($authorized[$menu->page][$menu->action]))
                        $is_authorized = $authorized[$menu->page][$menu->action] == TRUE;
                }
                elseif ($menu->page == NULL && $menu->action == NULL) {
                    if (count($menu->childs) > 0)
                        $is_authorized = TRUE;
                }

                if ($is_authorized == TRUE)
                    $results[] = $menu;
            }
        }

        return $results;
    }

    function logout() {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('myauth');
    }

    public function checkBaseUrl() {
        /*
          $c = current_url();
          $s = site_url('admin');
          if ($this->session->userdata('logged_in') == TRUE && ($c == $s))
          redirect('admin/home/index');
         */
        $c = current_url();
        //$s = site_url(folderBack());
        $s = site_url();
        if ($this->session->userdata('logged_in') == TRUE && ($c == $s)) {

            // if ($this->session->userdata('group_id') == '1' ||
                    // $this->session->userdata('group_id') == '2' ||
                    // $this->session->userdata('group_id') == '3' ||
                    // $this->session->userdata('group_id') == '7' ||
                    // $this->session->userdata('group_id') == '8') 
				if ($this->session->userdata('group_id'))	
				{
                $authorization = $this->session->userdata('authorization');
                if ($authorization) {
                    //redirect(folderBack().'/home/index');
                    redirect('/home/index');
                } else {
                    // echo "mau masuk ya";exit;
                    $this->session->set_flashdata('message', 'Anda baru saja logout silahkan login!');
                    $this->session->unset_userdata('logged_in');
                    redirect('Myauth');
                }
            } else {
                $this->session->set_flashdata('message', 'Anda tidak berhak masuk!');
                $this->session->unset_userdata('logged_in');
                redirect('Myauth');
            }
        }
    }

    public function forgot() {
        $this->load->model('Token_model', 'token'); // andy add	
        $this->load->library('form_validation');
        $this->load->helper('generatecode'); // andy add
        $data['title'] = 'CariTruk | Lupa Password';
        $messageForgotSucc = NULL;
        //$data['uri_return'] = $uri_return;

        if ($this->input->post('adminMyauthForgot')) {
            $adminMyauthForgotEmail = $this->input->get_post('adminMyauthForgotEmail');
            $field_settings = array(
                array('field' => 'adminMyauthForgotEmail', 'label' => 'Email', 'rules' => 'trim|required|valid_email|callback_checkEmail')
            );
            $this->form_validation->set_rules($field_settings);
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('message', validation_errors());
            } else {
                // email sdh diferifikasi terdaftar
                $token = generateCodeID();
                // insert data to token table
                $dataToken = array('email' => trim($adminMyauthForgotEmail),
                    'token' => $token);

                $token_id = $this->token->insertToken($dataToken);

                if ($token_id) {
                    // preparing send mail
                    $dataEmail['email'] = trim($adminMyauthForgotEmail);
                    $dataEmail['token'] = $token;
                    $emailto = trim($adminMyauthForgotEmail);

                    $returnEmail = $this->notifikasiEmailResetPassword('Reset Password', $emailto, $dataEmail);
                    $this->session->set_flashdata('messageForgotSucc', 'Berhasil mengirim link reset password ke Email Anda.');
                    $messageForgotSucc = $this->session->flashdata('messageForgotSucc');
                }
            }
        }
        $data['messageForgotSucc'] = $messageForgotSucc;
        $data['message'] = $this->session->flashdata('message');
        //$this->template->load('login', folderBack().'/login/forgot', $data);
        $this->template->load('login','/login/forgot', $data);
    }

    function checkEmail($string) {
        $this->load->model('Users_model', 'users'); // andy add	
        if ($this->users->checkUserRegEmail($string)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('checkEmail', '%s ' . $string . ' tidak ada, silahkan edit data Anda!');
            return FALSE;
        }
    }

    public function notifikasiEmailResetPassword($subject, $to, $data) {
        $return = false;

        $this->load->library('email'); //$config
        $this->email->set_newline("\r\n");
        $this->email->from('caritruk@gmail.com', 'CariTruk Reset Password');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($this->load->view('email/resetPassword', $data, TRUE));

        if ($this->email->send()) {
            //echo 'Your email was sent';
            $return = true;
        }
        // else
        // {
        // show_error($this->email->print_debugger());
        // }

        return $return;
    }

    public function resetPassword() {
        $data['title'] = 'CariTruk | Reset Password';
        $this->load->library('form_validation');
        $token = $this->uri->segment(4);
        $messageResetErr = NULL;

        if ($this->input->post('adminMyauthResetPasswordReset')) {
            $token = $this->input->get_post('token');
            $adminMyauthResetPassword = $this->input->get_post('adminMyauthResetPassword');
            $field_settings = array(
                array('field' => 'adminMyauthResetPassword', 'label' => 'Password', 'rules' => 'trim|required')
                ,
                array('field' => 'adminMyauthResetPassword2', 'label' => 'Password Konfirmasi', 'rules' => 'trim|required|matches[adminMyauthResetPassword]')
            );
            $this->form_validation->set_rules($field_settings);
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('message', validation_errors());
            } else {
                $this->load->model('Token_model', 'token'); // andy add	
                $this->load->model('Users_model', 'users'); // andy add	
                // isian password 1 dan 2 sdh sesuai
                // ambil email dari token terkait
                $email = $this->token->getEmail($token);
                if ($email) {
                    $this->db->trans_start();
                    // update password di users table
                    // secure password and create validation code
                    $salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                    $password = hash('sha512', $adminMyauthResetPassword . $salt);
                    $updateDataUsers = array(
                        'password' => $password,
                        'salt' => $salt
                    );

                    $updateUsers = $this->users->updateByEmail($updateDataUsers, $email);
                    if ($updateUsers) {
                        // update token
                        $updateDataToken = array(
                            'status' => 1
                        );

                        $updateUsers = $this->token->updateToken($updateDataToken, $token);
                    }

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('message', 'Gagal reset Password, silahkan coba kembali!');
                    } else {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('messageResetSuccess', 'Recovery Password berhasil, silahkan login!');
                        redirect('Myauth');
                    }
                } else {
                    $this->session->set_flashdata('messageResetErr', 'Invalid link atau Password sudah berubah!');
                    $messageResetErr = $this->session->flashdata('messageResetErr');
                }
            }
        }

        $data['message'] = $this->session->flashdata('message');
        $data['token'] = $token;
        $data['messageResetErr'] = $messageResetErr;
        $this->template->load('login','/login/resetPassword', $data);
    }

}
