<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reporting extends MY_Controller
{

    function __construct()
    {
		parent::__construct();
		
		$this->load->model("Reporting_model","reporting");
    }

    public function index()
    { 

        $data['title'] = 'Reporting | Telkomsel';
        $data['breadcrumb'] = array('Beranda' =>  base_url() .'/home',
            'Master' => '#', 
            'Activity Progress' => siteAdminUrl('activity_progress'));

        $data['message'] = $this->session->flashdata('message');
        $data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_log_activity'] = ''; 
		$data['cls_activity_progress'] = ''; 
		$data['cls_reporting'] = 'class="active"';

		$data_directorates = $this->db->query("SELECT a.directorate,
		COALESCE(COUNT(*), 0) AS jumlah_activity,
		COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
		FROM activity_progress a
			LEFT JOIN 
			( 
				SELECT directorate, COUNT(*) AS jumlah_activity_done FROM activity_progress
				WHERE progress = 'Closed'
				GROUP BY directorate 
			) b ON a.directorate = b.directorate 	
		GROUP BY a.directorate")->result_array();		
		$data['directorates'] = $data_directorates; 

		$data_owner = $this->db->query("SELECT a.owner_program,
		COALESCE(COUNT(*), 0) AS jumlah_activity,
		COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
		FROM activity_progress a
			LEFT JOIN 
			( 
				SELECT owner_program, COUNT(*) AS jumlah_activity_done FROM activity_progress
				WHERE progress = 'Closed'
				GROUP BY owner_program 
			) b ON a.owner_program = b.owner_program 	
		WHERE a.owner_program LIKE 'NEP%' OR a.owner_program LIKE 'NOQM%'
		GROUP BY a.owner_program
		ORDER BY a.OWNER_program ASC")->result_array();		
		$data['owners'] = $data_owner;

		$data_section = $this->db->query("SELECT a.section,
		COALESCE(COUNT(*), 0) AS jumlah_activity,
		COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
		FROM activity_progress a
			LEFT JOIN 
			( 
				SELECT section, COUNT(*) AS jumlah_activity_done FROM activity_progress
				WHERE progress = 'Closed'
				GROUP BY section 
			) b ON a.section = b.section 	
		GROUP BY a.section
		ORDER BY a.section ASC")->result_array();
		$data['sections'] = $data_section;
			
		if($this->session->group_id == 1) {
			$content = 'reporting/content-admin';
		} else {
			$content = 'reporting/content'; 
		}
		$this->_load_layout($content, $data);   
	}

	public function progress_tracking_regional()
	{
		$adm = $this->reporting->progres_tracking_regional_query_admin();  
		echo json_encode($adm); die(); 
	}

	public function progress_tracking_regional_network()
	{
		$adm = $this->reporting->progres_tracking_regional_query_admin_network();  
		echo json_encode($adm); die(); 
	}

	public function progress_tracking_regional_sales()
	{
		$adm = $this->reporting->progres_tracking_regional_query_admin_sales();  
		echo json_encode($adm); die(); 
	}

	public function progress_tracking_regional_it()
	{
		$adm = $this->reporting->progres_tracking_regional_query_admin_it();  
		echo json_encode($adm); die(); 
	}

	public function progress_tracking_owner_program()
	{
		$owner_program = $this->session->groupName;
		$adm = $this->reporting->progres_tracking_owner_program_query($owner_program);  
		echo json_encode($adm); die(); 
	}
    
	public function data()
    {
	    $this->load->model('Reporting_model','activity');
        $this->load->library('querydata');
        $requestData= $_REQUEST;

        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            0 => 'a.id_activity_progress',
            1 => 'a.owner_program',
			2 => 'a.detail_activity', 
			3 => 'a.progress',
			4 => 'a.target_week'
         ); 
   
        $defaultOrder =  'a.id_activity_progress asc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->activity->getSql($requestData);
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder); 

        $data = array();
        if($requestData['start']==0)
        {
            $no =1; 
        }
        else
        {
            $no = $requestData['start'] + 1;
        }
		foreach($query[0] As $row)
        {
             $nestedData=array(); 

            //  $nestedData[] = $row['id_activity_progress'];
            
			 $pr = strtolower($row['progress']);
    
			 $prg = "";
			    //$this->session->userdata('group_id')==2
				if($pr=="open")
				{
					$prg = "<div style='color:#A52A2A;'>".$pr."</div >"; 
				}
				else if($pr=="done")
				{
					$prg = "<div  style='color:#008B8B;'>".$pr."</div >"; 
				}
				else if($pr=="close" or $pr=="closed" or $pr=="Closed" )
				{
					$prg = "<div  style='color:#008B8B;'>".$pr."</div >"; 
				}
				else
				{
					$prg = "<div style='color:#A52A2A;'>".$pr."</div >"; 
                }	 

			 $nestedData[] = $row['owner_program'];
			 $nestedData[] = $row['detail_activity'];
             $nestedData[] = $prg;
			 $nestedData[] = $row['target_week'];

             $data[] = $nestedData;

             $no++;
 
            }

	    $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data); 		
	}
    
   public function detail_approv() {
	   
	   
        $this->load->model('Reporting_model','activity');
        $result = array();
        $result['res'] = '0';
		
		
        $request_id = $this->input->post('id');
        $status = $this->input->post('status');
        $mille = $this->input->post('mille');
        
		$this->db->trans_start();
        
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"];	
		
		if($status=="Open" or $status=="open")
		{
			$data2 = array(
			   'progress' =>  ucfirst($status),
			   'millestone_progress' =>  $mille,
			   'finish_week' => "",
			   'finish_date' => ""
		   
			);
		}
        else
        {
			$data2 = array(
			   'progress' =>  ucfirst($status),
			   'millestone_progress' =>  $mille,
			   'finish_week' => $tr,
			   'finish_date' => date("Y-m-d h:i:s")
		);
		}			
		
		
		
		$this->activity->upd($data2,$request_id);
		
		$data= array(
				'id_activity_progress' => $request_id,
				'status' => ucfirst($status)
		  	);
		
		$this->activity->insLog($data);
        
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $result;
           
		}
		echo json_encode($result); 
	}
	
    function export_reporting()
	{
		$owner_program = $this->session->first_name;

		$this->load->model('Reporting_model','reporting');
		$data['data'] = $this->reporting->getExcel($owner_program);
		// print_r($data['data']);
		$this->load->view('excel/report', $data);
	}

	public function editData()
	{  
		$this->load->model('Reporting_model','activity');
        $result = array();
        $result['res'] = '0';
        $id = $this->input->post('id');
        
        $this->db->trans_start();
        $ss =  $this->activity->GetByID($id);
		
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $ss;
           
		}
		echo json_encode($result); 
	}
	
	public function update() {
	   
	   
        $this->load->model('Reporting_model','activity');
        $result = array();
        $result['res'] = '0';
		
		
        $id = $this->input->post('id');
        //$status = $this->input->post('status');
        //$mille = $this->input->post('mille');
        //echo "<pre>";
		//var_dump($_POST['data']);
		//echo "</pre>";
		
		$ar = array();
		foreach($_POST['data'] as $key=>$val)
		{
				//echo $val['name'];			
				//echo $val['value'];	

				if(	$val['name'] == 'finish_week' or $val['name'] == 'target_week' ) 
				{
					$waa = strtoupper($val['value']);
					//array_push($ar,array( $val['name'] => $waa));
				    $ar[$val['name']]=$waa;
				}
				else
				{
					//array_push($ar,array( $val['name'] => $val['value'] ));
					$ar[$val['name']]= $val['value'];
				}				
		}
		
		
		$this->db->trans_start();
        
		$this->activity->upd($ar,$id);
		
        
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $result;
           
		}
		echo json_encode($result); 
    }
}
