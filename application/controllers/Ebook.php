<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ebook extends MY_Controller {

    protected $id_files;
   
    protected $_isCreate;
    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
	    $this->load->helper('thumb');
		//$this->load->model('m_files');
		//$this->load->model('m_pengguna');
		//$this->load->library('upload');
		$this->load->helper('download');
		
		
    }

  
	public function index() {
        //if (!is_authorized('ebook', 'index'))
        //    access_denied();

        $data['title'] = title().' | ebook';
        $data['message'] = $this->session->flashdata('message');
		
		 
		$data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';
		$data['cls_reporting'] = '';
        $content = 'ebook/content';
        $this->_load_layout($content, $data);
    }
	public function data()
    {
        //if (!is_authorized('ebook', 'index'))
         //  access_denied();
	   
        $this->load->model('Ebook_model','ebook');
              
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            0 => 'a.file_judul',
            1 => 'a.file_data',
            //2 => 'a.publish',
            2 => 'a.created_by'
           
         ); 

        $defaultOrder =  'created_date desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->ebook->getSql();
    
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {
            $no = $requestData['start'] + 1;
        }           
     
        foreach($query[0] As $row)
        {
             $nestedData=array(); 
             $nestedData[] = 	 $no;
             $nestedData[] =  	 $row['file_judul'];
			 $si = '<a  class="btn btn-sm btn-info" href="'.base_url().'home/unduh2/'.$row['ebook_id'].'">'.$row['file_data'].'</a>';
		     $nestedData[] =    $si;    
             //$nestedData[] =  	 $row['publish'];
			 $aksi = '<a  class="btn btn-sm btn-info" href="'.base_url().'ebook/update/'.$row['ebook_id'].'"> <i class="glyphicon glyphicon-pencil"></i>Edit</a>';
		     $aksi2 = '<a  class="btn btn-sm btn-danger"   href="#" onclick="deleted('.$row['ebook_id'].')"><i class="glyphicon glyphicon-trash"></i>Delete</a>';
             $nestedData[] = $aksi.' '.$aksi2;   
             $data[] = $nestedData;
             $no++;
        }
        $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
        
    }
	
	
	
	  public function create() {
       // if (!is_authorized('ebook', 'create'))
       //     access_denied();
        $this->load->model('Ebook_model','ebook');
        $ebook_id = null;
        $file_judul = null;
        $file_data = null;
        $aktif = null;
       
       
       
        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => base_url().'home',
            'Master' => '#',
            'Doc' => base_url().'ebook',
            'Tambah File' => base_url().'ebook/create'
			);
			
        $data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';		
		

        if ($this->input->post('Create')) {
            $this->_isCreate = true;
            $file_judul = $this->input->post('file_judul');
           
            //$aktif = $this->input->post('aktif');
            $this->form_validation->set_rules($this->myRules());
			if (empty($_FILES['file_data']['name']))
			{
				$this->form_validation->set_rules('file_data', 'File', 'required');
			}
			
			
			
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
                
				$arr = array();
                $lokasi_file    = $_FILES['file_data']['tmp_name'];
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['file_data']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadebookFile($nama_file_unik,$_FILES);
					$arr = array(
					   'file_data' => $nama_file_unik
					 );
					
				}	
				$ssss1 = array(
                     'file_judul' => $file_judul,
                     'publish' => 'YA'
                   
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->ebook->insert($ssss);
				$this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect('ebook');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Tambah File';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
        $data['ebook_id'] = $ebook_id;
        $data['file_data'] = $file_data;
        $data['file_judul'] = $file_judul;
        $data['aktif'] = $aktif;
        
		
		$content = 'ebook/create';
        $this->_load_layout($content, $data);
    }
	
	 public function update() {
      // if (!is_authorized('ebook', 'update'))
       //     access_denied();
        $this->load->model('Ebook_model','ebook');
        //$this->load->helper('thumb');   
        
        $ebook_id = null;
        $file_judul = null;
        $file_data = null;
        $aktif = null;
       
        
		$ebook_id=$this->uri->segment(3);
        $err = false;
        $this->_isCreate = false;
        $data['breadcrumb'] = array('Beranda' => base_url().'home',
            'Master' => '#',
            'Ebook' =>  base_url().'ebook',
            'Edit Files' => base_url().'ebook/update/'.$ebook_id 
			);

	    $data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';		
			
        if ($this->input->post('Create')) {
            $this->_isCreate = false;

            $ebook_id = $this->input->post('ebook_id');
            $file_judul = $this->input->post('file_judul');
            
			//$aktif = $this->input->post('aktif');
            
			
			
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
				
				$arr = array();
                $lokasi_file    = $_FILES['file_data']['tmp_name'];
				
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['file_data']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadebookFile($nama_file_unik,$_FILES);
					//UploadImage($nama_file_unik,'../../../foto_berita/',300,120);
					//$folder = './assets/images/img_album/';
					$i      = $ebook_id;
					$tabel  = 'tbl_ebook';
					$arr = array(
					   'file_data' => $nama_file_unik
					 );
					removeFileEbook($i,$tabel);
				}	
				
				$ssss1 = array(
                     'file_judul' => $file_judul,
                     'publish' => 'YA'
                    
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->ebook->update($ssss,$ebook_id);
				
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect('ebook');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Edit File';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
		$ssssa = $this->ebook->getById($ebook_id==null ?  $this->input->post('ebook_id'):$ebook_id);
        $data['ebook_id'] =  $ssssa->ebook_id;
		
		
        $data['file_judul'] =  $ssssa->file_judul;
       
        $data['aktif'] =  $ssssa->publish;
        $data['file_data'] = $ssssa->file_data;
		$content = 'ebook/create';
		
        $this->_load_layout($content, $data);
    }
	
	public function delete() {
       //if (!is_authorized('ebook', 'delete'))
        //    access_denied();
		$this->load->model('Ebook_model','ebook');
		$ss = $this->uri->segment(3);
		
		//$sd = $this->db->query("select * from tbl_ebook where ebook_id='$ss'");
		//if($sd->num_rows()>0)
		//{
		//	$this->session->set_flashdata('message', 'Data ebook tidak bisa dihapus!');
		//	redirect(folderBack().'/ebook/index');
		//}
        //else
        //{
			removeFileEbook($ss,"tbl_ebook");
			$r = $this->ebook->delete($ss);
			$this->session->set_flashdata('message', 'Data ebook berhasil dihapus!');
			redirect('ebook/index');
		//}			
	}
    public function myRules() {
        return $this->_fields = array(
            array(
                'field' => 'file_judul',
                'label' => 'Judul File',
                'rules' => 'required|trim' 
            )
			
        );
    }
}


