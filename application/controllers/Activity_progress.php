<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_progress extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->session->userdata('userid');
        //$this->session->userdata('name');
        //$this->session->userdata('level');

        //$this->_is_logged_in();
    }

    public function index()
    {
      
		 //if (!is_authorized('activity_progress', 'index'))
        //    access_denied();

        $data['title'] = 'Activity Progress | Telkomsel';
        $data['breadcrumb'] = array('Beranda' =>  base_url() .'/home',
            'Master' => '#',
            'Activity Progress' => siteAdminUrl('activity_progress'));

        $data['message'] = $this->session->flashdata('message');
        $data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_log_activity'] = ''; 
		$data['cls_activity_progress'] = 'class="active"';
		$data['cls_reporting'] = '';

        $content = 'activity_progress/content';
        $this->_load_layout($content, $data); 
		
        
    }
    
	public function data()
    {
       
	    $this->load->model('Activity_model','activity');
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
			0 => 'a.progress',
			1 => 'a.target_week',
			2 => 'a.activity',
			3 => 'a.detail_activity',
			4=> 'a.directorate',
			5=> 'a.group',			
			6=> 'a.owner_program',
			7 => 'a.section',
			8 => 'a.regional',
			9 => 'a.supporting_program',
			10 => 'a.nama_poi',
			11 => 'a.divisi_in_charge',
			12 => 'a.pic',
			13 => 'a.pic_support', 
         ); 
   
        $defaultOrder =  'a.id_activity_progress asc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->activity->getSql($requestData);
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {
            $no = $requestData['start'] + 1;
        }
		foreach($query[0] As $row)
        {
             $nestedData=array(); 
             //$nestedData[] = $no;
            //  $nestedData[] = $row['id_activity_progress'];
			 
            
			 $pr = strtolower($row['progress']);
			 $aksi ="";
			 if($this->session->userdata('group_id') != 2 )
			 {
				  if($this->session->userdata('group_id') == 1 )
				  {
				  
						$aksi.= '<div class="controls">
						  <div class="btn-group">
						  <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
						  <ul class="dropdown-menu">
							<li><a href="#" onclick="edit(\''.$row['id_activity_progress'].'\')"><i class="icon-pencil"></i> Edit Row</a></li>
							<li><a href="#" onclick="approv(\''.$row['id_activity_progress'].'\',\''.$pr.'\')"><i class="icon-ban-circle"></i> Change Progress</a></li>
						  </ul>
						</div>
						  </div>';
				  
				  }
				  else if($this->session->userdata('group_id') == 36 )
				  {
					   $aksi.="";
				  }	  
				  else
				  {
					   $aksi.= '<a  class="btn btn-sm btn-info"  onclick="approv(\''.$row['id_activity_progress'].'\',\''.$pr.'\')"> <i class="glyphicon"></i>Change</a>';
				 
				  }	  
			 }
			 $nestedData[] = $aksi;
			 $prg = "";
			    //$this->session->userdata('group_id')==2
				if($pr=="open")
				{
					$prg = "<div style='color:#A52A2A;'>".$pr."</div >"; 
				}
				else if($pr=="done")
				{
					$prg = "<div  style='color:#008B8B;'>".$pr."</div >"; 
				}
				else if($pr=="close" or $pr=="closed" or $pr=="Closed" )
				{
					$prg = "<div  style='color:#008B8B;'>".$pr."</div >"; 
				}
				else
				{
					$prg = "<div style='color:#A52A2A;'>".$pr."</div >"; 
				}	
			$nestedData[] = $prg;			 				
			$nestedData[] = $row['target_week']; 
			$nestedData[] = $row['activity']; 
			$nestedData[] = $row['detail_activity'];			
			$nestedData[] = $row['directorate'];
			$nestedData[] = $row['group'];    
			$nestedData[] = $row['owner_program']; 
			$nestedData[] = $row['section'];
			$nestedData[] = "<span style='text-align:center;'>".$row['regional']."</span>"; 
			$nestedData[] = $row['supporting_program']; 
			$nestedData[] = $row['nama_poi'];  
			$nestedData[] = $row['divisi_in_charge']; 
			$nestedData[] = $row['pic']; 
			$nestedData[] = $row['pic_support']; 			
			$data[] = $nestedData;
			$no++;
        }
	    $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
		
	}
    
   public function detail_approv() {
	   
	   
        $this->load->model('Activity_model','activity');
        $result = array();
        $result['res'] = '0';
		
		
        $request_id = $this->input->post('id');
        $status = $this->input->post('status');
        $mille = $this->input->post('mille');
        
		$this->db->trans_start();
        
		$waktu_sekarang = date("Y-m-d");
		$dat = $this->db->query("select week from date_week where stardate<='$waktu_sekarang'  order by id DESC limit 1")->result_array()[0];
		$tr = $dat["week"];	
		
		if($status=="Open" or $status=="open")
		{
			$data2 = array(
			   'progress' =>  ucfirst($status),
			   'millestone_progress' =>  $mille,
			   'finish_week' => "",
			   'finish_date' => ""
		   
			);
		}
        else
        {
			$data2 = array(
			   'progress' =>  ucfirst($status),
			   'millestone_progress' =>  $mille,
			   'finish_week' => $tr,
			   'finish_date' => date("Y-m-d h:i:s")
		);
		}			
		
		
		
		$this->activity->upd($data2,$request_id);
		
		$data= array(
				'id_activity_progress' => $request_id,
				'status' => ucfirst($status)
		  	);
		
		$this->activity->insLog($data);
        
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $result;
           
		}
		echo json_encode($result); 
    }
    function export_excel()
	{
	     $this->load->model('Activity_model','activity');
	     $data['data'] = $this->activity->getExcel();
		 // print_r($data['data']);
         $this->load->view('excel/excel', $data);
	}
	function export_excel2()
	{
	     $this->load->model('Activity_model','activity');
	     $data['data'] = $this->activity->getExcel2();
		 // print_r($data['data']);
         $this->load->view('excel/excel2', $data);
	}
	public function editData()
	{
		 
		 
		$this->load->model('Activity_model','activity');
        $result = array();
        $result['res'] = '0';
        $id = $this->input->post('id');
        
        $this->db->trans_start();
        $ss =  $this->activity->GetByID($id);
		
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $ss;
           
		}
		echo json_encode($result); 
	}
	
	public function update() {
	   
	   
        $this->load->model('Activity_model','activity');
        $result = array();
        $result['res'] = '0';
		
		
        $id = $this->input->post('id');
        //$status = $this->input->post('status');
        //$mille = $this->input->post('mille');
        //echo "<pre>";
		//var_dump($_POST['data']);
		//echo "</pre>";
		
		$ar = array();
		foreach($_POST['data'] as $key=>$val)
		{
				//echo $val['name'];			
				//echo $val['value'];	

				if(	$val['name'] == 'finish_week' or $val['name'] == 'target_week' ) 
				{
					$waa = strtoupper($val['value']);
					//array_push($ar,array( $val['name'] => $waa));
				    $ar[$val['name']]=$waa;
				}
				else
				{
					//array_push($ar,array( $val['name'] => $val['value'] ));
					$ar[$val['name']]= $val['value'];
				}				
		}
		
		
		$this->db->trans_start();
        
		$this->activity->upd($ar,$id);
		
        
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $result;
           
		}
		echo json_encode($result); 
	}
	
	function bc_activity() {

		$date_now = date('Y-m-d');
		$date_now_his = date('Y-m-d H:i:s');

		$query_week = $this->db->query("SELECT week from date_week where stardate <= '$date_now' and enddate >= '$date_now'")->result_array();

		$query_dir = $this->db->query("SELECT a.directorate,
					COALESCE(COUNT(*), 0) AS jumlah_activity,
					COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
					FROM activity_progress a
						LEFT JOIN 
						( 
							SELECT directorate, COUNT(*) AS jumlah_activity_done FROM activity_progress
							WHERE progress = 'Closed'
							GROUP BY directorate
						) b ON a.directorate = b.directorate 	 
					GROUP BY a.directorate ORDER BY a.directorate ASC")->result_array();	

		$query_network = $this->db->query("SELECT a.owner_program,
					COALESCE(COUNT(*), 0) AS jumlah_activity,
					COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
					FROM activity_progress a
						LEFT JOIN 
						( 
							SELECT owner_program, COUNT(*) AS jumlah_activity_done FROM activity_progress
							WHERE progress = 'Closed'
							AND owner_program LIKE 'NEP%' OR owner_program LIKE 'NOQM%'
							GROUP BY owner_program
						) b ON a.owner_program = b.owner_program 
					WHERE a.owner_program LIKE 'NEP%' OR a.owner_program LIKE 'NOQM%'
					GROUP BY a.owner_program")->result_array();

		$query_section = $this->db->query("SELECT a.section,
					COALESCE(COUNT(*), 0) AS jumlah_activity,
					COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
					FROM activity_progress a
						LEFT JOIN 
						( 
							SELECT section, COUNT(*) AS jumlah_activity_done FROM activity_progress
							WHERE progress = 'Closed'
							GROUP BY section
						) b ON a.section = b.section  
					GROUP BY a.section")->result_array();
		
		echo '<b>NARU 2019 Daily Activity Report</b><br>
		Week '.$query_week[0]['week'].'<br>
		Tanggal '.$date_now_his.'<br><br>
		
		<b>Format : [Divisi/Total Activity/Done/%Done]</b><br><br>
		';
 
		$total_activity = 0;
		$total_activity_done = 0;
		$average = 0;
		foreach($query_dir as $item) {
			$total_activity += $item['jumlah_activity'];
			$total_activity_done += $item['jumlah_activity_done'];
			$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
			echo $item['directorate'].' / '.$item['jumlah_activity'].' / '.$item['jumlah_activity_done'].' / '.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
		}	
		echo '<br>Overall / '.$total_activity.' / '.$total_activity_done.' / '.$average.'<br><br>';
		
		echo '<b>Network Activities</b><br><br>';
		$total_activity_network = 0;
		$total_activity_network_done = 0; 
		$average = 0; 
		foreach($query_network as $item) {
			$total_activity_network += $item['jumlah_activity'];
			$total_activity_network_done += $item['jumlah_activity_done'];
			$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
			echo $item['owner_program'].' / '.$item['jumlah_activity'].' / '.$item['jumlah_activity_done'].' / '.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
		}
  
		echo '<br><b>Activities Based On Domain</b><br><br>';
		$total_activity_section = 0;
		$total_activity_section_done = 0; 
		$average = 0; 
		foreach($query_section as $item) {
			$total_activity_section += $item['jumlah_activity'];
			$total_activity_section_done += $item['jumlah_activity_done'];
			$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
			echo $item['section'].' / '.$item['jumlah_activity'].' / '.$item['jumlah_activity_done'].' / '.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
		}

		}
}
