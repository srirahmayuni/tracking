<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class APIDashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
       
        //$this->load->model("Dashboard_model","dash"); 
       
    }

    public function index()
    {
     
		
        
    }
   
	
	/* public function summary()
	{
	      
		   $hasil = $this->db->query("
		   
										 select 
										 DISTINCT a.owner_program,
										 COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
										 b.jumlah_activity as target_total,
										 COALESCE(d.per, 0) As per
										 
										 from activity_progress a 
										 left join ( select count('owner_program') as jumlah_activity,owner_program 
																  from activity_progress  
																  group by `owner_program` ) b on b.owner_program=a.owner_program
										 left join ( select count('owner_program') as jumlah_activity_done,owner_program
														from activity_progress where progress in ('closed','Closed','CLOSED') 
													  group by `owner_program`) c on c.owner_program=a.owner_program
										 left join (
													   SELECT DISTINCT  a.owner_program,
													   COALESCE(d.rata, 0) As per
															from activity_progress a
															left join (  select avg(millestone_progress) as rata,`owner_program`
																		 from activity_progress 
																		 where millestone_progress is not null AND millestone_progress <> 0 
																		 group by  `owner_program`
																   )d on d.owner_program=a.owner_program		  
														  where a.regional = '[HQ]'
										           )d on d.owner_program = a.owner_program			  
										 where a.regional = '[HQ]'
										 
										 union all
										 
										 select
										 
										 'NETWORK' as owner_program,
										
									     
										 COALESCE( (select count(*) as jumlah_activity_done from 
										 				activity_progress where  regional <> '[HQ]' and progress in ('closed','Closed','CLOSED') )  , 0) As jumlah_activity_done,
									     
										 
										 COALESCE( (select count(*) as jumlah_activity  from activity_progress where regional  <> '[HQ]' ) , 0) As target_total,
										
										 
										 (COALESCE( (select count(*) as jumlah_activity_done from 
										 				activity_progress where  regional <> '[HQ]' and progress in ('closed','Closed','CLOSED') )  , 0)
														
										 /
										 
										 COALESCE( (select count(*) as jumlah_activity  from activity_progress where regional  <> '[HQ]' ))
										  )* 100 as per
									
										 ")->result_array(); 
			 
			$return=''; 
			$nilai_terbesar = $this->dash->nilaiMaxRowSummary();
			
			foreach($hasil As $row)
			{
				  //if( strtoupper($row['owner_program'])=="NETWORK")
				  //{
						$g = round($row["per"],2);  
                  //}					  
				 // else
				  //{
					//    $g   = "x";
				  //}	  
				  
				  //'.round($row["per"],2).'
				  $return .= '<div class="pricing-plans plans-2">
				  <div class="plan-container">
				  <table class="table table-condensed" width="100%" border="0">
				  <tbody>
				  <tr>
				  <td colspan="4" bgcolor="#000">
				  <center><font color="#FFFFFF" size="+1">'.$row["owner_program"].'</font></center>
				  </td>
				  </tr>
				  <tr>
				  <td colspan="2" bgcolor="#d9d9d9">
					<div align="center">'.$this->dash->target_up_to_week_summary($row["owner_program"]).'</div>
					<div align="center">Total Done : <strong>'.$row["jumlah_activity_done"].'</strong></div>  
					<div align="center">Target Total : <strong>'.$row["target_total"].'</strong></div>
				  </td>
				  <td colspan="2" bgcolor="#bebebe"> 
					<div align="center">'.$this->dash->target_up_to_week_summary_ach($row["owner_program"]).'</div> 
					<div align="center"> Ach Overall : <strong>'.$g.' %</strong></div>
				  </td>
				  </tr>';
				 		 if( strtoupper($row['owner_program'])=="NETWORK")
						 {	 
							 $has = $this->db->query("
										select DISTINCT  a.group as nama,
										COALESCE(b.jumlah_activity, 0) As jumlah_target,
										COALESCE(c.jumlah_activity_done, 0) As jumlah_target_done,
										COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
											from activity_progress a
									   left join (select count('group') as jumlah_activity,`group` 
												  from activity_progress 
												  group by `group`) b on b.group=a.group 
										left join (select count('group') as jumlah_activity_done,`group` 
												  from activity_progress 
												  where (progress='closed' or progress='CLOSED' or progress='Closed') 
												  group by `group`) c on c.group=a.group
									   where b.jumlah_activity != '0'	and   a.regional <> '[HQ]'			
								")->result_array(); 
						 }
						 else
						 {
							 				 $has = $this->db->query("   
											   select DISTINCT  a.group as nama,
											   COALESCE(b.jumlah_section, 0) As jumlah_target,
											   COALESCE(c.jumlah_section_done, 0) As jumlah_target_done,
											   COALESCE(d.rata, 0) As per
													from activity_progress a
											   left join (select count('group') as jumlah_section,`group` 
														  from activity_progress 
														   where `owner_program`='".$row['owner_program']."' 
														  group by `group`) b on b.group=a.group 
											   left join (select count('group') as jumlah_section_done,`group` 
														  from activity_progress where  (
														  progress='Closed' or progress='closed' or progress='CLOSED' ) 
														  and `owner_program`='".$row['owner_program']."' 
														  group by `group`)c on c.group=a.group
														  
												left join (  select avg(millestone_progress) as rata,`group`
																 from activity_progress 
																 where millestone_progress is not null AND millestone_progress <> 0 
																  and `owner_program`='".$row['owner_program']."'  
																 group by  `group`
												           )d on d.group=a.group		  
												 where b.jumlah_section != '0'	")->result_array();
										
										
										
										
										
							 
						 }			 		 
				   $c = 0;			  
				   foreach($has as $row)
				   {
					   $return .= '<tr>
											<td width="90" bgcolor="#d9d9d9">'.$row["jumlah_target_done"].' / '.$row["jumlah_target"].'</td>
											<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row["nama"].'</font></td>
											<td width="55" bgcolor="#A9D18E">'.round($row["per"],2).' % </td>
									   </tr>';		
						
					$c++;			
				   } 
				    if($c < $nilai_terbesar)
				   {
						   $g = $nilai_terbesar - $c;
						   for($i=0; $i<$g; $i++)
						   {
							   $return .= '<tr class="success"> 
												<td width="90" bgcolor="#d9d9d9">&nbsp;</td>
												<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">&nbsp;</font></td>
												<td width="55" bgcolor="#A9D18E">&nbsp;</td>
										   </tr>';	
						   }	   
				   } 
				   
				   $return .='</tbody>
				   </table>
				   </div> 
				   </div> 
				   ';
			}
			echo $return;		
				 
   } */
 	
   public function api_summary()
   {
									    $hasil = $this->db->query("
		     							 select 
										 DISTINCT a.owner_program,
										 COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
										 b.jumlah_activity as target_total,
										 COALESCE(d.per, 0) As per
										 
										 from activity_progress a 
										 left join ( select count('owner_program') as jumlah_activity,owner_program 
																  from activity_progress  
																  group by `owner_program` ) b on b.owner_program=a.owner_program
										 left join ( select count('owner_program') as jumlah_activity_done,owner_program
														from activity_progress where progress in ('closed','Closed','CLOSED') 
													  group by `owner_program`) c on c.owner_program=a.owner_program
										 left join (
													   SELECT DISTINCT  a.owner_program,
													   COALESCE(d.rata, 0) As per
															from activity_progress a
															left join (  select avg(millestone_progress) as rata,`owner_program`
																		 from activity_progress 
																		 where millestone_progress is not null AND millestone_progress <> 0 
																		 group by  `owner_program`
																   )d on d.owner_program=a.owner_program		  
														  where a.regional = '[HQ]'
										           )d on d.owner_program = a.owner_program			  
										 where a.regional = '[HQ]'
										 union all
										 select
										 'NETWORK' as owner_program,
										 COALESCE( (select count(*) as jumlah_activity_done from 
										 				activity_progress where  regional <> '[HQ]' and progress in ('closed','Closed','CLOSED') )  , 0) As jumlah_activity_done,
										 COALESCE( (select count(*) as jumlah_activity  from activity_progress where regional  <> '[HQ]' ) , 0) As target_total,
										 (COALESCE( (select count(*) as jumlah_activity_done from 
										 				activity_progress where  regional <> '[HQ]' and progress in ('closed','Closed','CLOSED') )  , 0)
										 /
										 COALESCE( (select count(*) as jumlah_activity  from activity_progress where regional  <> '[HQ]' ))
										  )* 100 as per
									
										 ")->result_array(); 
										 
										 $arr1 = array();
										 
										 foreach($hasil As $row)
										 {
												array_push($arr1,array('owner_program'=>$row["owner_program"]));
										 }	 
										 
										 
										 
		echo 	json_encode($arr1,true);							 
		//echo 	json_encode(array("hai"),true);//$arr1;							 
										 
   }   
	
	
   
}
