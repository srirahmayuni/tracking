<?php
class Artikel extends MY_Controller{
	function __construct(){
		parent::__construct();
		//if($this->session->userdata('masuk') !=TRUE){
        //    $url=base_url('administrator');
        //    redirect($url);
        //};
		//$this->load->model('m_kategori');
		$this->load->model('M_tulisan','m_tulisan');
		$this->load->helper('thumb');
		$this->load->library(array('form_validation', 'pagination'));
		
		//$this->load->model('m_pengguna');
		//$this->load->library('upload');
	}
	function index(){
		
		if (!is_authorized('artikel', 'index'))
           access_denied();
	
	    $data['message'] = $this->session->flashdata('message');
	
		$data['title'] = title().' | Artikel';
		$data['data']=$this->m_tulisan->get_all_tulisan();
		$content = 'artikel/v_tulisan';
		$this->_load_layout($content, $data);
	}
	
 	function create(){
	    if (!is_authorized('artikel', 'create'))
           access_denied();
	    
		$tulisan_id = null;
        $tulisan_judul = null;
        $tulisan_isi = null;
        $tulisan_tanggal = null;
        $tulisan_author = null;
        $publish = null;
		
        $gambar = null;
       
       
        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Artikel' => siteAdminUrl('artikel'),
            'Tambah Artikel' => siteAdminUrl('artikel/create'));

        if ($this->input->post('Create')) {
            $this->_isCreate = true;
            $tulisan_judul = $this->input->post('tulisan_judul');
            $tulisan_isi = $this->input->post('tulisan_isi');
            $tulisan_tanggal = $this->input->post('tulisan_tanggal');
            $tulisan_author = $this->input->post('tulisan_author');
            $publish = $this->input->post('aktif');
			
            $this->form_validation->set_rules($this->myRules());
			if (empty($_FILES['tulisan_gambar']['name']))
			{
				$this->form_validation->set_rules('tulisan_gambar', 'Gambar', 'required');
			}
			
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
                
				$arr = array();
                $lokasi_file    = $_FILES['tulisan_gambar']['tmp_name'];
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['tulisan_gambar']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadGalArtikel($nama_file_unik,$_FILES);
					$arr = array(
					   'tulisan_gambar' => $nama_file_unik
					 );
				}	
				$ssss1 = array(
                     //'tulisan_gambar' => $tulisan_gambar,
                     'tulisan_judul' => $tulisan_judul,
                     'tulisan_isi' => $tulisan_isi,
                     'tulisan_tanggal' => $tulisan_tanggal,
                     'tulisan_author' => $tulisan_author,
                     'publish' => $publish
                   
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->m_tulisan->insert($ssss);
				$this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect('artikel');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Tambah Artikel';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
        $data['tulisan_id'] = $tulisan_id;
        $data['gambar'] = $gambar;
        $data['tulisan_judul'] = $tulisan_judul;
        $data['tulisan_author'] = $tulisan_author;
        $data['tulisan_tanggal'] = $tulisan_tanggal;
        $data['aktif'] = $publish;
        $data['tulisan_isi'] = $tulisan_isi;
		$content = 'artikel/create';
        $this->_load_layout($content, $data);
		
		
	}
	
	public function update() {
       if (!is_authorized('artikel', 'update'))
            access_denied();
        
        $tulisan_id = null;
        $tulisan_judul = null;
        $tulisan_isi = null;
        $tulisan_tanggal = null;
        $tulisan_author = null;
        $publish = null;
		
        $gambar = null;
       
       
       
		$tulisan_id=$this->uri->segment(4);
        $err = false;
        $this->_isCreate = false;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Artikel' => siteAdminUrl('artikel'),
            'Edit Artikel' => siteAdminUrl('artikel/update/'.$tulisan_id));

        if ($this->input->post('Create')) {
            $this->_isCreate = false;

            $this->_isCreate = true;
            $tulisan_id = $this->input->post('tulisan_id');
            $tulisan_judul = $this->input->post('tulisan_judul');
            $tulisan_isi = $this->input->post('tulisan_isi');
            $tulisan_tanggal = $this->input->post('tulisan_tanggal');
            $tulisan_author = $this->input->post('tulisan_author');
            $publish = $this->input->post('aktif');
			
          
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
				
				$arr = array();
                $lokasi_file    = $_FILES['tulisan_gambar']['tmp_name'];
				
				
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['tulisan_gambar']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadGalArtikel($nama_file_unik,$_FILES);
					//UploadImage($nama_file_unik,'../../../foto_berita/',300,120);
					//$folder = './assets/images/img_album/';
					$i      = $tulisan_id;
					$tabel  = 'tbl_tulisan';
					$arr = array(
					   'tulisan_gambar' => $nama_file_unik
					 );
					removeGambarArtikel($i,$tabel);
				}	
				
				$ssss1 = array(
                     //'tulisan_gambar' => $tulisan_gambar,
                     'tulisan_judul' => $tulisan_judul,
                     'tulisan_isi' => $tulisan_isi,
                     'tulisan_tanggal' => $tulisan_tanggal,
                     'tulisan_author' => $tulisan_author,
                     'publish' => $publish
                   
                );
				
				
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->m_tulisan->update($ssss,$tulisan_id);
				
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect('artikel');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Edit Artikel';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
		$ssssa = $this->m_tulisan->getById($tulisan_id==null ?  $this->input->post('tulisan_id'):$tulisan_id);
        $data['tulisan_id'] =  $ssssa->tulisan_id;
		
		
		$data['gambar'] = $ssssa->tulisan_gambar; 
        $data['tulisan_judul'] = $ssssa->tulisan_judul;
        $data['tulisan_author'] = $ssssa->tulisan_author;
        $data['tulisan_tanggal'] = $ssssa->tulisan_tanggal;
        $data['aktif'] =  $ssssa->publish;
        $data['tulisan_isi'] =  $ssssa->tulisan_isi;
		
		
		
        $content = 'artikel/create';
        $this->_load_layout($content, $data);
    }
	
	public function delete() {
       if (!is_authorized('artikel', 'delete'))
            access_denied();
		//$this->load->model('Album_model','album');
		$ss = $this->uri->segment(4);
	    removeGambarArtikel($ss,"tbl_tulisan");
		
			$r = $this->m_tulisan->delete($ss);
			$this->session->set_flashdata('message', 'Data  berhasil dihapus!');
			redirect('artikel/index');
					
	} 
	
	


    public function myRules() {
       
        return $this->_fields = array(
            array('field' => 'tulisan_judul'
                , 'label' => 'Judul'
                , 'rules' => 'trim|required'
            ),
            array('field' => 'tulisan_isi'
                , 'label' => 'Isi'
                , 'rules' => 'required|trim'
            ),
           array(
                'field' => 'tulisan_author',
                'label' => 'Penulis',
                'rules' => 'required|trim'
            ),
			array(
                'field' => 'tulisan_tanggal',
                'label' => 'Tanggal',
                'rules' => 'required|trim'
            )
        );
    }
	 
	
}