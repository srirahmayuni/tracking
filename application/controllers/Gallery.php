<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->helper('thumb');   
		$this->load->model('Galeri_model','gal');   
    }

    public function index() {
       //if (!is_authorized('gallery', 'index'))
        //    access_denied();
        $data['title'] = title().'| Silde';
		$data['message'] = $this->session->flashdata('message');
		
		 $data['cls_home']      = '';
         $data['cls_dashboard'] = '';
		 $data['cls_activity_progress'] = '';
		 $data['cls_log_activity'] = '';
         $data['cls_reporting'] = '';
		
        $data['breadcrumb'] = array('Beranda' => base_url().'/home');
        $content = 'galeri/content';
		$this->_load_layout($content, $data);
    }
	
	public function data()
    {
       // if (!is_authorized('gallery', 'index'))
        //    access_denied();
        
        //$this->load->model('Galeri_model','galeri');      
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            0 => 'a.jdl_gallery', 
			1 => 'a.gbr_gallery',
			2 => 'a.keterangan',
			3 => 'a.publish'
         ); 
		
		
        $defaultOrder =  'a.created_date asc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->gal->getSql();
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {
            $no = $requestData['start'] + 1;
        }           
        
        foreach($query[0] As $row)
        {  
             $nestedData=array(); 
             $nestedData[] = $no;
             $nestedData[] = $row['jdl_gallery'];  
			 $nestedData[] = $row['keterangan'];
			 
             $nestedData[] =     '<center><img src='.base_url().'asset/images/img_galeri/'.$row["gbr_gallery"].' width=50></center>';  
			 $nestedData[] = $row['publish'];
			 $aksi = '<a  class="btn btn-sm btn-info" href="'.base_url().'gallery/update/'.$row['id_gallery'].'"> <i class="glyphicon glyphicon-pencil"></i>Edit</a>';
             $aksi2 = '<a class="btn btn-sm btn-danger" onClick="Delete(' . $row['id_gallery'] . ')"><i class="glyphicon glyphicon-trash">
												</i>Delete</a>';
             $nestedData[] = $aksi.' '.$aksi2;   
             $nestedData[] = $aksi;   
             $data[] = $nestedData;
             $no++;
        }
		
        $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
        
    }
	
	public function create() {
       //if (!is_authorized('gallery', 'create'))
       //     access_denied();
       // $this->load->model('Galeri_model','galeri');       
       
        $id_gallery = null;
        $jdl_gallery = null;
        $id_album = null;
        $keterangan = null;
        $gambar = null;
        $aktif = null;
       
		
        $err = false;
        $this->_isCreate = true;
		
		 $data['cls_home']      = '';
         $data['cls_dashboard'] = '';
		 $data['cls_activity_progress'] = '';
		 $data['cls_log_activity'] = '';
		
        $data['breadcrumb'] = array('Beranda' => base_url(),
            'Master' => '#',
            'Slide' => base_url().'gallery',
            'Tambah Slide' => base_url().'gallery/create');

        if ($this->input->post('Create')) {
            $this->_isCreate = true;

            $id_gallery = $this->input->post('id_gallery');
            $aktif = $this->input->post('aktif');
            
			
            $jdl_gallery = $this->input->post('judul_gallery');
            $keterangan = $this->input->post('keterangan');
            
            $this->form_validation->set_rules($this->myRules());
			if (empty($_FILES['gambar']['name']))
			{
				$this->form_validation->set_rules('gambar', 'Gambar', 'required');
			}
			
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
				
				$arr = array();
                $lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['gambar']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					
					UploadGallery($nama_file_unik,$_FILES);
					//watermark_image('./assets/images/img_galeri/'.$nama_file_unik);
					//watermark_image('./assets/images/img_galeri/kecil_'.$nama_file_unik);
					$arr = array(
					   'gbr_gallery' => $nama_file_unik
					  
					 );
					
				}	
				
				$ssss1 = array(
                   
                    'jdl_gallery' => $jdl_gallery,
					'publish' =>  $aktif,
                    'keterangan' => $keterangan
				
                   
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->gal->insert($ssss);
				
				
				
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Galeri gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Galeri baru berhasil di buat!');
                    redirect('gallery');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Tambah Slide';
        $data['err'] = $err;

        $data['action'] = $this->_isCreate;
        $data['id_gallery'] =  $id_gallery;
        $data['aktif'] =  $aktif;
       
        $data['judul_gallery'] =  $jdl_gallery;
        $data['keterangan'] =  $keterangan;
        $data['gambar'] = $gambar;
		
		$content = 'galeri/create';
        $this->_load_layout($content, $data);
    }
	
	 public function update() {
      // if (!is_authorized('gallery', 'update'))
       //     access_denied();
        //$this->load->model('Galeri_model','galeri');       
        
        $aktif = null;
        $id_gallery = null;
        $jdl_gallery = null;
        $id_album = null;
        $keterangan = null;
        $gambar = null;
        $this->_isCreate = false;
		$id_gallery=$this->uri->segment(3);
        $err = false;
        $this->_isCreate = false;
        $data['breadcrumb'] = array('Beranda' => base_url(),
            'Master' => '#',
            'Slide' => base_url().'gallery',
            'Edit Slide' => base_url().'gallery/update/'.$id_gallery);
        
		 $data['cls_home']      = '';
         $data['cls_dashboard'] = '';
		 $data['cls_activity_progress'] = '';
		 $data['cls_log_activity'] = '';
		
        if ($this->input->post('Create')) {
            $this->_isCreate = false;

         
            $id_gallery = $this->input->post('id_gallery');
            $aktif = $this->input->post('aktif');
            $id_album = $this->input->post('id_album');
            $jdl_gallery = $this->input->post('judul_gallery');
            $keterangan = $this->input->post('keterangan');
            $this->form_validation->set_rules($this->myRules());
			if (empty($_FILES['gambar']['name']))
			{
				$this->form_validation->set_rules('gambar', 'Gambar', 'required');
			}
			
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
				
				$arr = array();
                $lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['gambar']['name'];
					$acak           = rand(1,999999);
					$nama_file_unik = $acak.$nama_file; 
					UploadGallery($nama_file_unik,$_FILES);
					
					
					$i      = $id_gallery;
					$tabel  = 'gallery';
					$arr = array(
					   'gbr_gallery' => $nama_file_unik
					  
					 );
					removeGambarGallery($i,$tabel);
				}	
				
				$ssss1 = array(
                    
                    'jdl_gallery' => $jdl_gallery,
					'publish' => $aktif,
                    'keterangan' => $keterangan
                   
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->gal->update($ssss,$id_gallery);
				
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data  gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect('gallery');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Edit Slide';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
		$xxxx = $this->gal->getById($id_gallery==null ?  $this->input->post('id_gallery'):$id_gallery);
        $data['id_gallery'] =  $xxxx->id_gallery;
        $data['aktif'] =  $xxxx->publish;
        
        $data['judul_gallery'] =  $xxxx->jdl_gallery;
        $data['keterangan'] =  $xxxx->keterangan;
        $data['gambar'] =  $xxxx->gbr_gallery;
		
		$content = 'galeri/create';
        $this->_load_layout($content, $data);
    }
	public function myRules() {
       
	   return $this->_fields = array(
            array('field' => 'judul_gallery'
                , 'label' => 'Judul Galeri'
                , 'rules' => 'trim|required'
            ),
			
            array('field' => 'keterangan'
                , 'label' => 'keterangan'
                , 'rules' => 'required|trim'
            )
        );
    }
	
	public function delete() {
      // if (!is_authorized('gallery', 'delete'))
       //     access_denied();
		//$this->load->model('Galeri_model','galeri');
		
		$ss = $this->uri->segment(3);
		
		$i      = $ss;
		$tabel  = 'gallery';
		removeGambarGallery($i,$tabel);
		
		
		$r = $this->gal->delete($ss);
		
		$this->session->set_flashdata('message', 'Data Galeri berhasil dihapus!');
		redirect('gallery/index');
		
		
		
	}
	
}
