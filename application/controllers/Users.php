<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

    protected $_fields; 
    protected $_isCreate; 
    protected $_useridnya; 
    protected $_isNumber; 

    public function __construct() {
        parent::__construct();

        $this->load->model('Users_model', 'users'); 
        $this->load->model('Groups_model', 'groups'); 
       
        $this->load->library(array('form_validation'));
    }

    

    public function index() {
        if (!is_authorized('users', 'index'))
            access_denied();

        $data['title'] = 'Tracking Telkomesl Siaga| Pengguna';
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Pengguna' => siteAdminUrl('users'));

        $data['message'] = $this->session->flashdata('message');
        $data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
        $data['cls_log_activity'] = '';
        $data['cls_reporting'] = '';

        $content = 'users/content';
        $this->_load_layout($content, $data);
    }

    public function data() 
	{
		
		$this->load->library('querydata');
		$requestData= $_REQUEST;
		$columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
			0 => 'users.id',
			1 => 'groups.name',
			2 => 'users.first_name',
			3 => 'users.email',
			4 => 'users.active'
		 ); 
			
		//$defaultOrder =  ''; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
		$defaultOrder =  'users.first_name ASC'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
		$sql = $this->users->getAll();
    	
    	$query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
		$data = array();
		if($requestData['start']==0)
		{
			$no =1;
		}
        else
        {
			$no = $requestData['start'] + 1;
		}			
		
		foreach($query[0] As $row)
		{
			 $nestedData=array(); 
			 $nestedData[] = $no;
			 $nestedData[] = $row['id'];
			 $nestedData[] = $row['name'];	
			 $nestedData[] = $row['first_name'];	
			 $nestedData[] = $row['email'];	
			 $nestedData[] = $row['active'];	
			 $delete = "";
			 $update = "";
			 //if (is_authorized('users', 'delete')) 
			 //{
                                                   
                   $delete = '<a class="btn btn-sm btn-danger" 
                                                       href="javascript:void(0);" 
                                                       title="Hapus" 
                                                       onclick="adminUsersDelete(\''.$row['users_id'].'\', \''.$row['first_name'].'\');">
                                                        <i class="glyphicon glyphicon-trash"></i> Hapus
                              </a>';
			 
			 //}
			 //if (is_authorized('users', 'update'))
			 // {
                                                   
				   $update = '<a class="btn btn-sm btn-info" 
					   href="javascript:void(0);" 
					   title="Hapus" 
					   onclick="adminUsersUpdate(\''.$row['users_id'].'\');">
						<i class="glyphicon glyphicon-pencil"></i> Edit
					</a>';
			 // }
			
			$aksi = $delete." ".$update;
			$nestedData[] = $aksi;
			$data[] = $nestedData;
			$no++;
		}
		
		
		$json_data = array(
					"draw"            => intval( $requestData['draw'] ),   
					"recordsTotal"    => intval( $query[1] ),  
					"recordsFiltered" => intval( $query[2] ), 
					"data"            => $data
					);

		echo json_encode($json_data);  // send data as json format
		
	}
	public function create() {
        //if (!is_authorized('users', 'create'))
        //    access_denied();

        $adminUsersUsers_id = null;
        $adminUsersFirstName = null;
        $adminUsersEmail = null;
        $adminUsersMobile = null;
        $adminUsersGroup = null;
        $err = false;
        $this->_isCreate = true;
        $this->_isNumber = true;
        $dataGroups = $this->groups->get_dropdown_listR();

        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Pengguna' => siteAdminUrl('users'),
            'Tambah Pengguna' => siteAdminUrl('users/create'));

        if ($this->input->post('adminUsersCreate')) {
            // $reseller_id = NULL;
            // $coordinator_id = NULL;
            // if ($this->session->userdata('reseller_id'))
                // $reseller_id = $this->session->userdata('reseller_id');

            // if ($this->session->userdata('coordinator_id'))
                // $coordinator_id = $this->session->userdata('coordinator_id');

            // jika dia bukan superadmin, cek table keterkaitanya (reseller/ coordinator)
            $group_id = NULL;
            if ($this->session->userdata('group_id'))
                $group_id = $this->session->userdata('group_id');

            $this->_isCreate = true;

            $adminUsersFirstName = $this->input->get_post('adminUsersFirstName');
            $adminUsersEmail = $this->input->get_post('adminUsersEmail');
            $adminUsersMobile = $this->input->get_post('adminUsersMobile');
            $adminUsersPassword = $this->input->get_post('adminUsersPassword');
            $adminUsersGroup = $this->input->get_post('adminUsersGroup');
            $adminUsersActive = $this->input->get_post('adminUsersActive');

            if ($adminUsersActive)
                $adminUsersActive = 1;
            else
                $adminUsersActive = 0;

            if ($adminUsersMobile) {
                if (!is_numeric($adminUsersMobile))
                    $this->_isNumber = false;
            }


            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();

                // secure password and create validation code
                $salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                $password = hash('sha512', $adminUsersPassword . $salt);
                $dataUsers = array(
                    'group_id' => $adminUsersGroup,
                    'id' => $adminUsersMobile,
                    'password' => $password,
                    'salt' => $salt,
                    'first_name' => strtoupper($adminUsersFirstName),
                    'email' => $adminUsersEmail,
                    'active' => $adminUsersActive
                );

                $users_id = $this->users->insert($dataUsers);
                /*// adminadmin || admin
                if ($group_id == 2 || $group_id == 8) {
                    if ($reseller_id) {
                        // dengan catatan Group masih 1 (ADMIN/ reseller)
                        $dataUserreseller = array(
                            'users_id' => $users_id,
                            'reseller_id' => $reseller_id);

                        $userreseller_id = $this->userreseller->insert($dataUserreseller);
                    }
                }
                // coordinatoradmin atau coordinator
                else if ($group_id == 3 || $group_id == 7) {
                    if ($coordinator_id) {
                        // dengan catatan Group masih 1 (Coordinator)
                        // 1.	create coordusers
                        $dataCoordusers = array(
                            'users_id' => $users_id,
                            'coordinator_id' => $coordinator_id);
                        $coordinator_id = $this->coordusers->insert($dataCoordusers);
                    }
                }
                */
                // masukkan data ke userreseller

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Pengguna gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();

                    // preparing send mail
                    $dataEmail['namaCoordinator'] = strtoupper($adminUsersFirstName);
                    $dataEmail['id'] = $adminUsersMobile;
                    $dataEmail['email'] = $adminUsersEmail;
                    $dataEmail['password'] = $adminUsersPassword;
                    $dataEmail['status'] = $adminUsersActive;
                    $emailto = $adminUsersEmail;

                    //$returnEmail = $this->notifikasiEmailLogin('Konfirmasi Login', $emailto, $dataEmail);

                    $this->session->set_flashdata('message', 'Pengguna dengan nama ' . strtoupper($adminUsersFirstName) . ' berhasil di buat!');
                    redirect('users');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = 'Telkomsel | Tambah Pengguna';
        $data['err'] = $err;
        $data['isNumber'] = $this->_isNumber;
        $data['action'] = $this->_isCreate;
        $data['dropdownGroups'] = $dataGroups;
        $data['adminUsersUsers_id'] = $adminUsersUsers_id;
        $data['adminUsersFirstName'] = $adminUsersFirstName;
        $data['adminUsersEmail'] = $adminUsersEmail;
        $data['adminUsersMobile'] = $adminUsersMobile;
        $data['adminUsersGroup'] = $adminUsersGroup;
		
		$data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';

        $content = 'users/create';
        $this->_load_layout($content, $data);
    }

    public function update() {
        //if (!is_authorized('users', 'update'))
          //  access_denied();

        $err = false;
        $adminUsersUsers_id = null;
        $adminUsersFirstName = null;
        $adminUsersLastName = null;
        $adminUsersEmail = null;
        $adminUsersMobile = null;
        $adminUsersGroup = null;
        $adminUsersActive = null;
        $dataUsers = new stdClass();
        $this->_isCreate = false;
        $dataGroups = $this->groups->get_dropdown_listR();
        $users_id = $this->uri->segment(3);

        if ($this->input->post('adminUsersCreate')) 
		{
            //$reseller_id = NULL;
            //$coordinator_id = NULL;
            //if ($this->session->userdata('reseller_id'))
             //   $reseller_id = $this->session->userdata('reseller_id');

            //if ($this->session->userdata('coordinator_id'))
            //    $coordinator_id = $this->session->userdata('coordinator_id');

            // jika dia bukan superadmin, cek table keterkaitanya (reseller/ coordinator)
            $group_id = NULL;
            if ($this->session->userdata('group_id'))
                $group_id = $this->session->userdata('group_id');

            $this->_isCreate = false;
            $users_id = $this->input->get_post('adminUsersUsers_id');
            $dataUsers = $this->users->getDataUsers($users_id);
            $adminUsersFirstName = $this->input->get_post('adminUsersFirstName');
            $adminUsersEmail = $this->input->get_post('adminUsersEmail');
            $adminUsersMobile = $this->input->get_post('adminUsersMobile');
            $adminUsersPassword = $this->input->get_post('adminUsersPassword');
            $adminUsersGroup = $this->input->get_post('adminUsersGroup');
            $adminUsersActive = $this->input->get_post('adminUsersActive');

            if ($adminUsersActive)
                $adminUsersActive = 1;
            else
                $adminUsersActive = 0;

            if ($adminUsersMobile) {
                if (is_numeric($adminUsersMobile))
                    $this->_isNumber = true;
                else
                    $this->_isNumber = false;
            }
            else {
                if (is_numeric($dataUsers->id))
                    $this->_isNumber = true;
                else
                    $this->_isNumber = false;
            }

            $this->_useridnya = $users_id;

            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false)
			{
                // echo "siap update";exit;	
                $password = NULL;
                $salt = NULL;
                $updateDataUsers = NULL;
                $updateDataUsers1 = NULL;
                $updateDataUsers2 = NULL;

                if ($adminUsersPassword) {
                    // secure password and create validation code
                    $salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                    $password = hash('sha512', $adminUsersPassword . $salt);

                    $updateDataUsers1 = array(
                        'password' => $password,
                        'salt' => $salt
                    );
                }

                $updateDataUsers2 = array(
                    'first_name' => strtoupper($adminUsersFirstName),
                    'email' => $adminUsersEmail,
                    'id' => $adminUsersMobile,
                    'group_id' => $adminUsersGroup,
                    'active' => $adminUsersActive
                );

                if ($updateDataUsers1)
                    $updateDataUsers = array_merge($updateDataUsers1, $updateDataUsers2);
                else
                    $updateDataUsers = $updateDataUsers2;

                $this->db->trans_start();

                $updateUsers = $this->users->update($updateDataUsers, $users_id);
				
				//add ali
		
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
				{
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Pengguna gagal di update, silahkan coba kembali!');
                }
				else 
				{
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();

                    /* if ($adminUsersPassword) {
                        // preparing send mail
                        $dataEmail['namaCoordinator'] = strtoupper($adminUsersFirstName);
                        $dataEmail['id'] = $adminUsersMobile;
                        $dataEmail['email'] = $adminUsersEmail;
                        $dataEmail['password'] = $adminUsersPassword;
                        $dataEmail['status'] = $adminUsersActive;
                        $emailto = $adminUsersEmail;
                        $this->notifikasiEmailLogin('Konfirmasi Update Login', $emailto, $dataEmail);
                    } */

                    $this->session->set_flashdata('message', 'Pengguna dengan nama ' . strtoupper($adminUsersFirstName) . ' berhasil di update!');
                    redirect('users');
                }
            } 
			else
			{
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }
        // getData
        $dataUsers = $this->users->getDataUsers($users_id);
        // prepare data for update
        $adminUsersUsers_id = $users_id;
        $adminUsersFirstName = $dataUsers->first_name;
        $adminUsersEmail = $dataUsers->email;
        $adminUsersMobile = $dataUsers->id;
        $adminUsersGroup = $dataUsers->group_id;
        $adminUsersActive = $dataUsers->active;

        if ($adminUsersMobile) {
            if (is_numeric($adminUsersMobile))
                $this->_isNumber = true;
            else
                $this->_isNumber = false;
        }
        else {
            if (is_numeric($dataUsers->id))
                $this->_isNumber = true;
            else
                $this->_isNumber = false;
        }

        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Pengguna' => siteAdminUrl('users'),
            'Update Pengguna ' . $adminUsersFirstName => siteAdminUrl('users/update/' . $users_id));

        $data['dropdownGroups'] = $dataGroups;
        $data['adminUsersUsers_id'] = $adminUsersUsers_id;
        $data['adminUsersFirstName'] = $adminUsersFirstName;
        $data['adminUsersEmail'] = $adminUsersEmail;
        $data['adminUsersMobile'] = $adminUsersMobile;
        $data['adminUsersGroup'] = $adminUsersGroup;
        $data['adminUsersActive'] = $adminUsersActive;
        $data['action'] = $this->_isCreate;
        $data['err'] = $err;
        $data['isNumber'] = $this->_isNumber;
        $data['title'] = 'Telkomsel; | Update Pengguna';

		$data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = '';
		
        $content = 'users/create';
        $this->_load_layout($content, $data);
    }

    public function delete() {
        $this->load->library('whois');
        $result = new stdClass();
        $result->response = FALSE;
        $users_id = $this->input->post('users_id');
        $reseller_id = NULL;
        $coordinator_id = NULL;
        $group_id = NULL;
        
        if ($this->session->userdata('reseller_id')) {
            $reseller_id = $this->session->userdata('reseller_id');
        }
        if ($this->session->userdata('coordinator_id')) {
            $coordinator_id = $this->session->userdata('coordinator_id');
        }

        // getGroupIdByUserId
        
        if ($users_id) {
            $group_id = $this->users->getGroupIdByUserId($users_id);
        }
        
        $cekHaveData = $this->whois->loud($users_id);
        
        if (!$cekHaveData) {
            $this->db->trans_start();
            /*
              if($reseller_id)
              $this->userreseller->deleteUserreseller($users_id, $reseller_id);
              if($coordinator_id)
              $this->coordusers->delete($users_id, $coordinator_id);
             */
            // adminadmin || admin
            if ($group_id == 2 || $group_id == 8) {
                if ($reseller_id)
                    $this->userreseller->deleteUserreseller($users_id, $reseller_id);
            }
            // coordinatoradmin atau coordinator
            else if ($group_id == 3 || $group_id == 7) {
                if ($coordinator_id)
                    $this->coordusers->delete($users_id, $coordinator_id);
            }

            $this->users->delete($users_id);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                //if something went wrong, rollback everything
                $this->db->trans_rollback();
            } else {
                //if everything went right, commit the data to the database
                $this->db->trans_commit();

                $result->response = TRUE;
                $this->result($result);
            }
        } else
            $this->result($result);
    }

    public function checkEmail($string) {
        if ($this->users->checkUserRegEmail($string)) {
            $this->form_validation->set_message('checkEmail', '%s ' . $string . ' sudah ada, silahkan edit data Anda!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function checkEmail2($string) {
        if ($this->users->checkUserEmailCoordinator($string, $this->_useridnya)) {
            $this->form_validation->set_message('checkEmail', '%s ' . $string . ' sudah ada, silahkan edit data Anda!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function checkTlp($string) {
        if ($this->users->checkUserRegTlp($string)) {
            $this->form_validation->set_message('checkTlp', '%s ' . $string . ' sudah ada, silahkan edit data Anda!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function checkTlp2($string) {
        if ($this->users->checkUserTlpCoordinator($string, $this->_useridnya)) {
            $this->form_validation->set_message('checkTlp2', '%s ' . $string . ' sudah ada, silahkan edit data Anda!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function notifikasiEmailLogin($subject, $to, $data) {
        $return = false;

        $this->load->library('email'); //$config
        $this->email->set_newline("\r\n");
        $this->email->from('caritruk@gmail.com', 'CariTruk Konfirmasi Login ' . $data['namaCoordinator']);
        $this->email->to($to);
        $this->email->subject($subject . ' ' . $data['namaCoordinator']);
        $this->email->message($this->load->view('email/confirmLogin', $data, TRUE));

        if ($this->email->send()) {
            //echo 'Your email was sent';
            $return = true;
        }
        // else
        // {
        // show_error($this->email->print_debugger());
        // }

        return $return;
    }

    public function myRules() {
        $r = null;

        if ($this->_isNumber) {
            // No tlp
            if ($this->_isCreate)
                $r = 'trim|required|min_length[4]|integer|callback_checkTlp';
            else
                $r = 'trim|required|min_length[4]|integer|callback_checkTlp2';
        }
        else {
            // Username
            if ($this->_isCreate)
                $r = 'trim|required|callback_checkTlp';
            else
                $r = 'trim|required|callback_checkTlp2';
        }

        //buat aturan terhadap form input
        return $this->_fields = array(
            array(
                'field' => 'adminUsersFirstName',
                'label' => 'Nama',
                'rules' => 'required|trim'),
            array(
                'field' => 'adminUsersEmail',
                'label' => 'Email',
                'rules' => ($this->_isCreate) ? 'required|trim|valid_email|callback_checkEmail' : 'required|trim|valid_email|callback_checkEmail2'
            ),
            array(
                'field' => 'adminUsersMobile',
                'label' => ($this->_isNumber) ? 'No Telepon' : 'Username',
                'rules' => $r
            ),
            array(
                'field' => 'adminUsersPassword',
                'label' => 'Password',
                'rules' => ($this->_isCreate) ? 'trim|required|min_length[4]' : 'trim|min_length[4]|matches[adminUsersPassword2]'
            ),
            array(
                'field' => 'adminUsersPassword2',
                'label' => 'Password Konfirmasi',
                'rules' => ($this->_isCreate) ? 'trim|required|min_length[4]|matches[adminUsersPassword]' : 'trim|min_length[4]|matches[adminUsersPassword]'
            ),
            array(
                'field' => 'adminUsersGroup',
                'label' => 'Group',
                'rules' => 'trim|required'
            )
        );
    }

}
