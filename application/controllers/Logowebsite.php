<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logowebsite extends MY_Controller { 

    
    protected $_isCreate;
   
    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('thumb');
	   
    }

  
	public function index() {
        if (!is_authorized('logowebsite', 'index'))
            access_denied();

        $data['title'] = title().' | Logo';
        $data['message'] = $this->session->flashdata('message');
		
        $content = 'logo/content';
        $this->_load_layout($content, $data);
    }
	public function data()
    {
        if (!is_authorized('logowebsite', 'index'))
            access_denied();
        $this->load->model('Logo_model','logo');
              
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            0 => 'a.gambar'
           
         ); 
 
        $defaultOrder =  'a.id_logo desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->logo->getSql();
    
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {
            $no = $requestData['start'] + 1;
        }           
      
        foreach($query[0] As $row)
        {
             $nestedData=array(); 
             $nestedData[] = $no;
             $nestedData[] = "<center><img src=".base_url()."assets/images/logo/".$row['gambar']."></img></center>";
			 $aksi = '<a  class="btn btn-sm btn-info" href="'.base_url_admin().'/logowebsite/update/'.$row['id_logo'].'"> <i class="glyphicon glyphicon-pencil"></i>Edit</a>';
		     $nestedData[] = $aksi;   
             $data[] = $nestedData;
             $no++;
        } 
        $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
        
    }
	  public function update() {
        if (!is_authorized('logowebsite', 'update'))
            access_denied();
      
        $id_logo = $this->uri->segment(4);
       
        $this->load->model('Logo_model','logo');
        $err = false;
        $this->_isCreate = false;
		
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Logo ' => siteAdminUrl('logowebsite'),
            'Update Logo' => siteAdminUrl('logowebsite/update/'.$id_logo.''));

        if ($this->input->post('Create')) {
            $this->_isCreate = true;

            $id_logo = $this->input->post('id_logo'); 
            
            if (empty($_FILES['fupload']['name']))
			{
				$this->form_validation->set_rules('fupload', 'Gambar', 'required');
			}
         
            //$this->form_validation->set_rules($this->myRules());
			
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
               
			    $arr = array();
                $lokasi_file    = $_FILES['fupload']['tmp_name'];
				
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['fupload']['name'];
					$acak           = rand(1,9779);
					$nama_file_unik = $acak.$nama_file; 
					UploadLogo($nama_file,$_FILES);
					$arr = array(
					   'gambar' => $nama_file_unik
					 );
					
				}	
				
                $ss = $this->logo->update($arr,$id_logo);
				
				
				
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data  gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message','Data  berhasil di simpan!');
                    redirect(folderBack().'/logowebsite');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' |  Logo Website';
        $data['err'] = $err;
       
		 $fg = $this->logo->getById($id_logo=='' ? $this->input->post('id_logo') : $id_logo );
        $data['action'] = $this->_isCreate;
       
       
        $data['id_logo'] = $fg->id_logo;
        $data['gambar'] = $fg->gambar;
		$content = 'logo/create';
        $this->_load_layout($content, $data);
    }
	  
}	