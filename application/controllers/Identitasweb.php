<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Identitasweb extends MY_Controller {

  
    public function __construct() {
        parent::__construct();
        $this->load->model('Identitasweb_model', 'identitas');
		$this->load->library(array('form_validation', 'pagination'));
		$this->load->helper('thumb');
       
    }

    public function index() {
        if (!is_authorized('identitasweb', 'index'))
            access_denied();
		
		$users_id = NULL;
        if ($this->session->userdata('users_id'))
            $users_id = $this->session->userdata('users_id');
        $data['title'] = title().' | Identitas web';
        $data['breadcrumb'] = array('Beranda' => base_url() . folderBack().'/identitasweb');
		
		$messageProf = NULL;
        $messageProfSucc = NULL;

        $id_identitas = 1;
        $namawebsite = null;
        $email = null;
        $url = null;
        $facebook = null;
        $rekening = null;
        $no_telp = null;
        $meta_deskripsi	 = null;
        $meta_keyword	 = null;
        $alamat	 = null;
        $favicon	 = null;
        $maps	 = null;
        $atasnama	 = null;
        $foot	 = null;
        $bank	 = null;
		$dataId = $this->identitas->getData($id_identitas);
		if ($this->input->post('update')) {
            // hidden file
            $id_identitas = $this->input->post('id_identitas');
            
			$this->id_identitas =  $id_identitas ;

            $namawebsite = $this->input->post('namawebsite');
            $email = $this->input->post('email');
            $url = $this->input->post('url');
            $facebook = $this->input->post('facebook');
            $rekening = $this->input->post('rekening');
			$no_telp = $this->input->get_post('no_telp');
			$meta_deskripsi = $this->input->get_post('meta_deskripsi');
			$meta_keyword = $this->input->get_post('meta_keyword');
			$maps = $this->input->get_post('maps');
			$footer = $this->input->get_post('footer');
			$atasnama = $this->input->get_post('atasnama');
			$alamat = $this->input->get_post('alamat');
			$bank = $this->input->get_post('bank');
            $this->form_validation->set_rules($this->myRules());
			
			//if (empty($_FILES['favicon']['name']))
			//{
			//	$this->form_validation->set_rules('favicon', 'Favicon', 'required');
			//}
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('messageProf', validation_errors());
                $messageProf = $this->session->flashdata('messageProf');
            } else {
				//$gambar =null;
				$fdfdfd1 = array();
                $lokasi_file    = $_FILES['favicon']['tmp_name'];
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['favicon']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadFavicon($nama_file_unik,$_FILES);
					$fdfdfd1 = array(
					   'favicon' => $nama_file_unik
					 );
				}	
				$logobank = array();
                $lokasi_file_logobank    = $_FILES['logo_bank']['tmp_name'];
				if (!empty($lokasi_file_logobank)){
					$nama_file      = $_FILES['logo_bank']['name'];
					$acak           = rand(1,99);
					$nama_file_unik9 = $acak.$nama_file; 
					UploadBank($nama_file_unik,$_FILES);
					$logobank = array(
					   'logo_bank' => $nama_file_unik9
					 );
					
				}
				$logo_website = array();
                $lokasi_file_logo_webiste    = $_FILES['logo_website']['tmp_name'];
				if (!empty($lokasi_file_logo_webiste)){
					$nama_file      = $_FILES['logo_website']['name']; 
					$acak           = rand(1,929);
					$nama_file_unik = $acak.$nama_file; 
					UploadLogo($nama_file_unik,$_FILES);
					$logo_website = array(
					   'logo_website' => $nama_file_unik
					 );
					
				}
				//$dataResellers =array();
				$fdfdfd2 = array(
                    'nama_website' => $namawebsite,
                    'email' => $email,
                    'url' => $url,
                    'facebook' => $facebook,
                    'rekening' => $rekening,
                    'atasnama' => $atasnama,
                    'bank' => $bank,
					'no_telp' => $no_telp,
					'alamat' => $alamat,
					'meta_deskripsi' => $meta_deskripsi,
					'meta_keyword' => $meta_keyword,
					'maps' => $maps,
					'footer' => $footer
					);
				 $fdfdfd = array_merge($fdfdfd1,$fdfdfd2,$logobank,$logo_website);	
                // start transaction
                $this->db->trans_start();

                $this->identitas->update($fdfdfd, $id_identitas);

                $this->db->trans_complete();
                // end transaction		
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                } else {

                    $this->session->set_flashdata('messageProfSucc', 'Identitas Web ' . strtoupper($namawebsite) . ' berhasil di ubah!');
                    $messageProfSucc = $this->session->flashdata('messageProfSucc');

                    $dataId = $this->identitas->getData($id_identitas);
                }
            }
        }

       
		$data['dataId'] = $dataId;
		$data['messageProf'] = $messageProf;
        $data['messageProfSucc'] = $messageProfSucc;
        $content = 'identitasweb/content';
		$this->_load_layout($content, $data);
          
    }
	
	/*  public function checkTlpRes2($string) {
		
		$is = false; 
		$r  = $this->reseller_id;  
      
        if ($this->reseller->checkRegTlp2($string,$is,$r)) {
            return TRUE;
			
        } else {
         
			$this->form_validation->set_message('checkTlpRes2', '%s ' . $string . ' sudah ada, silahkan edit data Anda!');
            return FALSE;
        }
    } */
   		
    public function myRules() {
       
        return $this->_fields = array(
            array('field' => 'namawebsite'
                , 'label' => 'Nama website'
                , 'rules' => 'trim|required'
            ),
            array('field' => 'email'
                , 'label' => 'Email'
                , 'rules' => 'required|trim'
            ),
            array('field' => 'url'
                , 'label' => 'URL'
                , 'rules' => 'trim'
            ),
            array('field' => 'facebook'
                , 'label' => 'Facebook'
                , 'rules' => 'trim'
            ),
            array(
                'field' => 'rekening',
                'label' => 'Rekening',
                'rules' => 'trim'
            ),
			
            array(
                'field' => 'atasnama',
                'label' => 'atasnama',
                'rules' => 'trim'
            ),
			array(
                'field' => 'no_telp',
                'label' => 'No_telp',
                'rules' => 'trim|required'
            ),
			array(
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'trim|required'
            ),
			array(
                'field' => 'meta_deskripsi',
                'label' => 'meta_deskripsi',
                'rules' => 'trim'
            )
			,
			array(
                'field' => 'meta_keyword',
                'label' => 'meta_keyword',
                'rules' => 'trim'
            ),
			array(
                'field' => 'maps',
                'label' => 'maps',
                'rules' => 'trim'
            ),
			array(
                'field' => 'footer',
                'label' => 'footer',
                'rules' => 'trim'
            )
        );
    }
	
	
    /*  public function uploadGambar($data,$id_identitas) {
        $this->load->library('upload');
        
        $config['upload_path'] = './assets/ico/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = '5048';
        $config['max_width'] = '5048';
        $config['max_height'] = '5048';
        $config['file_name'] = "ico_" . $id_identitas . "_" . time();

        $this->upload->initialize($config);

        if ($data['favicon']['name']) {
            // cek di db
            $this->cekGambar($id_identitas);
            if ($this->upload->do_upload('favicon')) {
                $gbr = $this->upload->data();

                return $gbr['file_name'];
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
	 function cekGambar($id_identitas) {

        $this->db->where('id_identitas', $id_identitas);
        $result = $this->db->get('identitas');
            if ($result->row()->favicon != "" or $result->row()->favicon != null) {
                @unlink(base_url() . '/assets/ico/' . $result->row()->favicon);
            }
         
    } */

}
