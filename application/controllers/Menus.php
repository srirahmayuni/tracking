<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends MY_Controller {

    protected $_fields;
    protected $page_id;
    protected $menu_id;
    protected $action_id;
    protected $menu;

    public function __construct() {
        parent::__construct();

        $this->load->model('Menu_model', 'menus');
        $this->load->model('Page_model', 'pages');
        $this->load->model('Aksi_model', 'actions');
        $this->load->library(array('form_validation', 'pagination'));
    }

    public function index() {
        if (!is_authorized('menus', 'index'))
            access_denied();

        $data['title'] = title().'| Menu';
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Menu' => siteAdminUrl('menus'));

        $data['message'] = $this->session->flashdata('message');

        $content = 'menus/content';
        $this->_load_layout($content, $data);
    }

    public function getData() {
        if (!is_authorized('menus', 'index'))
            access_denied();

        $table = 'menu';
        $primaryKey = 'a.menu_id';
        $columns = array(
            array('db' => 'a.menu_id', 'dt' => 1, 'field' => 'menu_id'),
            array('db' => 'a.page_id', 'dt' => 2, 'field' => 'page_id'),
            array('db' => 'a.menu_name AS Menu', 'dt' => 3, 'field' => 'Menu'),
            //array( 'db' => 'a.menu_name',   'dt' => 3 ,  'field' =>'menu_name'),
            array('db' => 'menu.menu_name AS MenuParent', 'dt' => 4, 'field' => 'MenuParent'),
            array('db' => 'pages.controller', 'dt' => 5, 'field' => 'controller'),
            array('db' => 'a.action_id', 'dt' => 6, 'field' => 'action_id'),
            array('db' => 'actions.action', 'dt' => 7, 'field' => 'action'),
            array('db' => 'a.parent_menu_id', 'dt' => 8, 'field' => 'parent_menu_id'),
            array('db' => 'a.urutan', 'dt' => 9, 'field' => 'urutan')
        );

        $columnsTanpaAlias = array(
            array('db' => 'a.menu_id', 'dt' => 1, 'field' => 'menu_id'),
            array('db' => 'a.page_id', 'dt' => 2, 'field' => 'page_id'),
            array('db' => 'a.menu_name', 'dt' => 3, 'field' => 'menu_name'),
            //array( 'db' => 'a.menu_name',   'dt' => 3 ,  'field' =>'menu_name'),
            array('db' => 'menu.menu_name', 'dt' => 4, 'field' => 'menu_name'),
            array('db' => 'pages.controller', 'dt' => 5, 'field' => 'controller'),
            array('db' => 'a.action_id', 'dt' => 6, 'field' => 'action_id'),
            array('db' => 'actions.action', 'dt' => 7, 'field' => 'action'),
            array('db' => 'a.parent_menu_id', 'dt' => 8, 'field' => 'parent_menu_id'),
            array('db' => 'a.urutan', 'dt' => 9, 'field' => 'urutan')
        );
        $CekcolumnsTanpaAlias = 1;
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        require( 'ssp.php' );

        $joinQueryS = '';
        $joinQuery = "FROM menu a
						LEFT JOIN pages ON pages.page_id=a.page_id
						LEFT JOIN actions ON actions.action_id=a.action_id
						LEFT JOIN menu  ON a.parent_menu_id=menu.menu_id
						";
        $extraWhere = "";
        // static function simple ( $request, $sql_details, $table, $primaryKey, $columns, $joinQuery = NULL, $extraWhere = '', $groupBy = '', $columnsTanpaAlias, $cek)
        $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, '', $columnsTanpaAlias, $CekcolumnsTanpaAlias);
        $start = $_GET['start'];

        $res = array();
        $i = $start;
        //print_r($result['data']);
        foreach ($result['data'] as $sss) {
            $crd = '<a class="btn btn-sm btn-info" href=' . base_url() . folderBack().'/menus/update/' . $sss[1] . '>
			           <i class="glyphicon glyphicon-pencil"></i>Edit</a> ';
            $crd.= '<a class="btn btn-sm btn-danger" onClick="Delete(' . $sss[1] . ',\'' . $sss[3] . '\')"><i class="glyphicon glyphicon-trash">
												</i>Delete</a>';
            array_push($res, array('0' => $i + 1, '1' => $sss[3], '2' => $sss[4], '3' => $sss[7], '4' => $sss[5], '5' => $sss[9], '6' => $crd));
            $i++;
        }
        $result['data'] = $res;
        echo json_encode($result);
    }

    public function create() {
        if (!is_authorized('menus', 'create'))
            access_denied();

        $menu_id = null;
        $menu = null;
        $urutan = null;
        $page_id = null;
        $parent_id = null;
        $action_id = null;

        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Menu' => siteAdminUrl('menus'),
            'Tambah Menu' => siteAdminUrl('menus/create'));

        if ($this->input->post('Crate')) {
            $this->_isCreate = true;

            $menu = $this->input->post('menu');
            $urutan = $this->input->post('urutan');
            $page_id = $this->input->post('page_id');
            $parent_id = $this->input->post('parent_id');
            $action_id = $this->input->post('action_id');
            $this->page_id = $page_id;
            $this->action_id = $action_id;
            $this->menu = $menu;

            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();

                $ssss = array(
                    'menu_name' => $menu,
                    'page_id' => EMPTY($page_id) ? NULL : $page_id,
                    'action_id' => EMPTY($action_id) ? NULL : $action_id, 
                    'parent_menu_id' => EMPTY($parent_id) ? NULL : $parent_id, 
                    'urutan' => $urutan
                );
                $ss = $this->menus->insert($ssss);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Menu gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Menu baru dengan nama ' . $menu . ' berhasil di buat!');
                    redirect(folderBack().'/menus');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = 'CariTruk | Tambah Menu';
        $data['err'] = $err;

        $data['action'] = $this->_isCreate;
        $data['menu_id'] = $menu_id;
        $data['action_id'] = $action_id;
        $data['menu'] = $menu;
        $data['parent_id'] = $parent_id;
        $data['dropdownMenu'] = $this->menus->ComboMenu();
        $data['dropdownHalaman'] = $this->pages->ComboHalaman();
        $data['dropdownAction'] = $this->actions->ComboAction();
        $data['page_id'] = $page_id;
        $data['urutan'] = $urutan;
        $content = 'menus/create';
        $this->_load_layout($content, $data);
    }

    public function update() {
       if (!is_authorized('menus', 'update'))
            access_denied();
        $menu = null;
        $urutan = null;
        $page_id = null;
        $parent_id = null;
        $action_id = null;
        $err = false;
        $this->_isCreate = false;
        $menu_id = $this->uri->segment(4);

        if ($this->input->post('Crate')) {
            $this->_isCreate = false;
            $menu = $this->input->post('menu');
            $menu_id = $this->input->post('menu_id');
            $urutan = $this->input->post('urutan');
            $page_id = $this->input->post('page_id');
            $parent_id = $this->input->post('parent_id');
            $action_id = $this->input->post('action_id');
            $this->page_id = $page_id;
            $this->menu_id = $menu_id;
            $this->action_id = $action_id;
			$this->menu = $menu;

            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
                $ssss = array(
                    'menu_name' => $menu,
                    'page_id' => EMPTY($page_id) ? NULL : $page_id,
                    'action_id' => EMPTY($action_id) ? NULL : $action_id, 
                    'parent_menu_id' => EMPTY($parent_id) ? NULL : $parent_id, 
                    'urutan' => $urutan
                );
                $sss = $this->menus->update($ssss, $menu_id);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Menu gagal di update, silahkan coba kembali!');
                } else {
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Menu baru dengan nama ' . $menu . ' berhasil diupdate!');
                    redirect(folderBack().'/menus');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }
        $dt = $this->menus->getMenuByID($menu_id);
        $data['title'] = 'CariTruk | Update Menu';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
        $data['menu_id'] = $dt->menu_id;
        $data['action_id'] = $dt->action_id;
        $data['menu'] = $dt->menu_name;
        $data['parent_id'] = $dt->parent_menu_id;
        $data['dropdownMenu'] = $this->menus->ComboMenu();
        $data['dropdownHalaman'] = $this->pages->ComboHalaman();
        $data['dropdownAction'] = $this->actions->ComboAction();
        $data['page_id'] = $dt->page_id;
        $data['urutan'] = $dt->urutan;
        $content = 'menus/create';
        $this->_load_layout($content, $data);
    }

    public function delete() {
        if (!is_authorized('menus', 'delete'))
        access_denied();
//
        $result = new stdClass();
        $result->res = '0';
        $menu_id = $this->input->post('menu_id');
        $this->db->trans_start();

        $query = "select * from menu where menu_id=$menu_id";
        //echo $query;
        $fr = $this->db->query($query);
        $page_id = $fr->row()->page_id;
        
        if ($page_id == NULL or $page_id == '') {
            $query2 = "select * from menu where parent_menu_id=" . $menu_id . "";
            $fr2 = $this->db->query($query2);
            $cekHaveData = $fr2->num_rows();

            if ($cekHaveData > 0) {
                $result->mesg = "Menu ini sudah punya anak";
                $result->res = '0';
                $this->result($result);
            } else {

                $this->menus->delete($menu_id);
                $result->res = '1';
                $this->result($result);
            
            }
        } else {

            $queryx = "select * from privilege_page where page_id=" . $page_id . "";
            $f2r = $this->db->query($queryx);
            $cekHaveData = $f2r->num_rows();
            if ($cekHaveData > 0) {
                $result->mesg = "Menu sedang dipakai";
                $result->res = '0';
                $this->result($result);
            } else {
                
                
                $this->menus->delete($menu_id);
                $result->res = '1';
                $this->result($result);
            

            }
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
        } else {
            //if everything went right, commit the data to the database
            $this->db->trans_commit();
            $this->result($result);
        }
    }

    
    public function myRules() {


       $u ='';
	   if( $this->action_id)
	      $u=($this->_isCreate) ? 'required|trim|callback_check':'required|trim|callback_check2'; 
        
		return $this->_fields = array(
            array(
                'field' => 'menu',
                'label' => 'Nama menu',
                'rules' =>  ($this->_isCreate) ? 'trim|callback_checkmenu':'trim|callback_checkmenu2'
            ),
             array(
                'field' => 'action_id',
                'label' => 'Action',
                'rules' => $u 
            ), 

             array(
                'field' => 'urutan',
                'label' => 'Urutan',
                'rules' => 'required|trim'
            )
        );
    }

    public function checkmenu($s) {
    
        $q = "Select * from menu where menu_name='$s'";
        $fg = $this->db->query($q);
        $fgs = $fg->num_rows();
        if ($fgs > 0) {
           
           $this->form_validation->set_message('checkmenu', 'Nama Menu '.$s.' sudah ada , silahkan edit data Anda!');
           return false;
         }  else {
            return true;
        }  
    }  


    public function checkmenu2($s) {
        $menu_id = $this->menu_id;
        $q = "Select * from menu where menu_name='$s' and menu_id<>$menu_id";
        $fg = $this->db->query($q);
        $fgs = $fg->num_rows();
        //$fgs = $fg->num_rows();
        if ($fgs > 0) {
           
           $this->form_validation->set_message('checkmenu2', 'Nama Menu '.$s.' sudah ada , silahkan edit data Anda!');
           return false;
         }  else {
            return true;
        }  
    }    
    public function check($id) {
        $page_id = $this->page_id;
        $q = "Select * from menu where action_id=$id and page_id=$page_id";
        $fg = $this->db->query($q);
        $fgs = $fg->num_rows();
        if ($fgs > 0) {
            $action = $this->db->query("select * from actions where action_id=$id")->row()->action;
            $this->form_validation->set_message('check', '%s ' . $action . ' duplicat action dan page ada yang sama (sudah ada), silahkan edit data Anda!');
            return false;
        } else {
            return true;
        }
    }

     public function check2($id) {
        $page_id = $this->page_id;
			
		$menu_id = $this->menu_id;
		$menu = $this->menu;
		$q2= "";
		if($id=='' and $page_id!='')
		{
			$q2 = "Select * from menu where menu_name='$menu' and page_id=$page_id";
		}
		else if($id!='' and $page_id=='')
		{
			$q2 = "Select * from menu where menu_name='$menu' and action_id=$id";
		}
		else if($id=='' and $page_id=='')
		{
			$q2 = "Select * from menu where menu_name='$menu' ";
		}
        else
        {			
		    $q2 = "Select * from menu where action_id=$id and page_id=$page_id and menu_id<>$menu_id";
		//    $q2 = "Select * from menu where menu_name='$menu'  and menu_id<>$menu_id";
		}
		//echo $q2;
		//exit;
		
		$fg2 = $this->db->query($q2);
	    $fgs2 = $fg2->num_rows();
		if ($fgs2 == 0) {
			
			return true;
		}
        else
        {			
			//$q = "Select * from privilege_action where action_id=$id and page_id=$page_id";
			$q = "Select * from menu where action_id=$id and page_id=$page_id";
			$fg = $this->db->query($q);
			$fgs = $fg->num_rows();
			if ($fgs > 0) {
				$action = $this->db->query("select * from actions where action_id=$id")->row()->action;
				$this->form_validation->set_message('check2', '%s ' . $action . 'duplicat action dan page ada yang sama (sudah ada), silahkan edit data Anda!');
				return false;
			} else {
				return true;
			}
		}	
    } 
    

}
