<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
       
        $this->load->model("Dashboard_model","dash"); 
       
    }

    public function index()
    {
     
		//if (!is_authorized('dashboard', 'index'))
        //    access_denied();

        $data['title'] = 'Tracking | Telkomsel';
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Dashboard' => siteAdminUrl('dashboard'));

        $data['message'] = $this->session->flashdata('message');
        $data['cls_home']      = '';
        $data['cls_dashboard'] = 'class="active"';
		$data['cls_activity_progress'] = '';
		$data['cls_reporting'] = '';
		$data['cls_log_activity'] = '';

        $content = 'dashboard/content';
        $this->_load_layout($content, $data); 
		
        
    }
    // owner program / section
	// group net
	
	public function summary()
	{
			$hasil = $this->db->query("SELECT DISTINCT directorate from activity_progress")->result_array(); 
			 
			$return=''; 
			$nilai_terbesar = $this->dash->nilaiMaxRowSummary();
			
			// echo json_encode($hasil); die();

			foreach($hasil As $row)
			{
				//   $g = round($row["per"],2);  
    
				  $return .= '<div class="pricing-plans plans-2">
				  <div class="plan-container">
				  <table class="table table-condensed" width="100%" border="0">
				  <tbody>
				  <tr>
				  <td colspan="4" bgcolor="#000">
				  <center><font color="#FFFFFF" size="+1">'.$row["directorate"].'</font></center>
				  </td>
				  </tr>
				  ';

				  $has = $this->db->query("SELECT a.directorate, a.`group`,
					COALESCE(COUNT(*), 0) AS jumlah_activity,
					COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
					FROM activity_progress a
						LEFT JOIN 
						( 
							SELECT directorate, `group`, COUNT(*) AS jumlah_activity_done FROM activity_progress
							WHERE progress = 'Closed'
							GROUP BY directorate, `group` 
						) b ON a.`group` = b.`group` 	
					WHERE a.directorate ='".$row['directorate']."'  
					GROUP BY a.directorate, a.`group`
					")->result_array();
				  
				   $c = 0;			  
				   foreach($has as $row)
				   {

					   $jumlah_activity =  $row["jumlah_activity"];
					   $jumlah_activity_done =  $row["jumlah_activity_done"];
					   $persentase = round(($jumlah_activity_done / $jumlah_activity) * 100, 2);

					   $return .= '<tr>
											<td width="90" bgcolor="#FFD966">'.$row["jumlah_activity_done"].' / '.$row["jumlah_activity"].'</td>
											<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row["group"].'</font></td>
											<td width="55" bgcolor="#A9D18E" style><center>'.$persentase.' %</center></td> 
								  </tr>';		
						
					$c++;			
				   } 
				    if($c < $nilai_terbesar)
				   {
						   $g = $nilai_terbesar - $c;
						   for($i=0; $i<$g; $i++)
						   {
							   $return .= '<tr class="success"> 
												<td width="90" bgcolor="#FFD966">&nbsp;</td>
												<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">&nbsp;</font></td>
												<td width="55" bgcolor="#A9D18E">&nbsp;</td>
										   </tr>';	
						   }	   
				   } 
				   
				   $return .='</tbody>
				   </table>
				   </div> 
				   </div> 
				   ';
			}
			echo $return;		
				 
   }
   
   public function summary_regional()
   {
	 	  $hasil = $this->db->query("select * from
								   (
									  select DISTINCT a.regional
									  from activity_progress a
									  where regional
									  like '%[R%' 
									  group by a.regional
								   ) ac		
								  
								  ")->result_array();	// ".filterBygroup2()."		  
							  
		
		$nilai_terbesar = $this->dash->nilaiMaxSummaryregional();
		$return='';
		foreach($hasil As $row)
		{
			  $regional = $row["regional"];
		      
			  //$sr = strtoupper(substr($row["regional"],0,3));
		      //if( $sr ==  "NEP" )
			  // {
			  //	  $regi = $row["regional"];
			  //}
              //else
              //{
				  $regi = $row["regional"]." ".array_regional()[$row["regional"]];
			  //}				  
			  $return .= '<div class="pricing-plans plans-2">
							<div class="plan-container">
								<table class="table table-condensed" width="100%" border="0"">
									<tbody>
										<tr> 
											<td colspan="4" bgcolor="#000">
											<center><font color="#FFFFFF" size="+1">'.$regi.'</font></center>
											</td> 
										</tr>			  
										<tr>
											<td colspan="2" bgcolor="#d9d9d9">
												<div align="center"><b>PLAN</b></div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_week_plan_regional($row["regional"]).'</div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_total_up_week_plan_regional($row["regional"]).'</div> 
											</td>
											<td colspan="2" bgcolor="#bebebe">	   
												<div align="center"><b>ACTUAL</b></div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_week_actual_regional($row["regional"]).'</div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_total_up_week_actual_regional($row["regional"]).'</div> 
											</td>
										</tr> 
										<tr>
											<td colspan="4" bgcolor="#595959">  
											'.$this->dash->total_delta().'
											</td> 
										</tr>  
										';

			  $wher =  "regional"; 
			  	  
			  $has = $this->db->query("
									   select DISTINCT  a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
									   COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
									   from activity_progress a 
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress where ".$wher." ='".$row['regional']."'  
												  group by activity) b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and ".$wher." ='".$row['regional']."'
												  group by activity) c on c.activity=a.activity
									   where b.jumlah_activity != '0'				  
									  ")->
									  result_array();

			   $c =0;
			   foreach($has as $row)
			   { 
				   $return .= '<tr>
										<td width="90" bgcolor="#FFD966">'.$row["jumlah_activity_done"].' / '.$row["jumlah_activity"].'</td>
										<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row["activity"].'</font></td>
										<td width="55" bgcolor="#A9D18E">'.round($row["per"],2).' % </td>
							  </tr>';		
					
					$c++;		
			   }  
               
			   if($c < $nilai_terbesar)
			   {
					   $g = $nilai_terbesar - $c;
					   for($i=0; $i<$g; $i++) 
					   {
						   $return .= '<tr class="success"> 
											<td width="90" bgcolor="#FFD966">&nbsp;</td>
											<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">&nbsp;</font></td>
											<td width="55" bgcolor="#A9D18E">&nbsp;</td>
									   </tr>';	
					   }	   
			   }

						$return .='
						<tr>
							<td colspan="4" bgcolor="#595959">  
							<center><font color="#FFFFFF" style = "font-size: 14px">'.$this->dash->total_activity($regional).'</font></center>
							</td> 
						</tr>   
						</tbody>
						</table>
						</div> 
						</div> 
						'; 
		
		}
		
		$return.=''; 
		
		echo $return;	
   }

   public function summary_regional_noqm()
   {
	 	$hasil = $this->db->query("SELECT DISTINCT owner_program as regional
		 FROM activity_progress
		 WHERE owner_program LIKE 'NOQM%' OR owner_program LIKE 'NEP%'")->result_array();	   
		
		$nilai_terbesar = $this->dash->nilaiMaxSummaryregionalNOQM();

		// echo json_encode($nilai_terbesar); 

		$return=''; 
		foreach($hasil As $row)
		{
			$regional = $row["regional"];

			$return .= '<div class="pricing-plans plans-2">
							<div class="plan-container">
								<table class="table table-condensed" width="100%" border="0"">
									<tbody>
										<tr> 
											<td colspan="4" bgcolor="#000">
											<center><font color="#FFFFFF" size="+1">'.$regional.'</font></center>
											</td> 
										</tr>			  
										<tr>
											<td colspan="2" bgcolor="#d9d9d9"> 
												<div align="center"><b>PLAN</b></div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_week_plan_regional_noqm($row["regional"]).'</div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_total_up_week_plan_regional_noqm($row["regional"]).'</div> 
											</td>
											<td colspan="2" bgcolor="#bebebe">	   
												<div align="center"><b>ACTUAL</b></div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_week_actual_regional_noqm($row["regional"]).'</div>
												<div align="center" style = "font-size: 11px">'.$this->dash->target_total_up_week_actual_regional_noqm($row["regional"]).'</div> 
											</td>
										</tr> 
										<tr>
											<td colspan="4" bgcolor="#595959">  
											'.$this->dash->total_delta_noqm().'
											</td> 
										</tr>  
										';
			  	  
			  $has = $this->db->query("select DISTINCT a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
									   COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
									   from activity_progress a 
									   left join ( select count('activity') as jumlah_activity,activity 
												  from activity_progress where owner_program ='".$row['regional']."'  
												  group by activity) b on b.activity=a.activity
									   left join ( select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where (
												  progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and owner_program ='".$row['regional']."' 
												  group by activity) c on c.activity=a.activity
									   where b.jumlah_activity != '0'				  
									  ")->
									  result_array();

			   $c =0;
			   foreach($has as $row)
			   { 
				   $return .= '<tr>
										<td width="90" bgcolor="#FFD966">'.$row["jumlah_activity_done"].' / '.$row["jumlah_activity"].'</td>
										<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row["activity"].'</font></td>
										<td width="55" bgcolor="#A9D18E">'.round($row["per"],2).' % </td>
							  </tr>';		
					
					$c++;		
			   }  
               
			   if($c < $nilai_terbesar)
			   {
					   $g = $nilai_terbesar - $c;
					   for($i=0; $i<$g; $i++) 
					   {
						   $return .= '<tr class="success"> 
											<td width="90" bgcolor="#FFD966">&nbsp;</td>
											<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">&nbsp;</font></td>
											<td width="55" bgcolor="#A9D18E">&nbsp;</td>
									   </tr>';	
					   }	   
			   }

						$return .='
						<tr>
							<td colspan="4" bgcolor="#595959">  
							<center><font color="#FFFFFF" style = "font-size: 14px">'.$this->dash->total_activity_noqm($regional).'</font></center>
							</td> 
						</tr>   
						</tbody>
						</table>
						</div> 
						</div> 
						'; 
		
		}
		
		$return.=''; 
		
		echo $return;	
   }

   public function summary_area()
   {
	
	$nilai_terbesar = $this->db->query("SELECT MAX(b.total) AS total FROM (
		SELECT a.`area`, sum(a.total) AS total
		FROM 
		(
		SELECT regional, COUNT(DISTINCT activity) AS total,
		CASE WHEN regional IN ('[R01]','[R02]','[R10]') THEN 'Area1'
			 WHEN regional IN ('[R03]','[R04]') THEN 'Area2'
			 WHEN regional IN ('[R05]','[R06]','[R07]') THEN 'Area3'
			 WHEN regional IN ('[R08]','[R09]','[R11]') THEN 'Area4'
		
		END AS `area`
		FROM activity_progress
		WHERE regional NOT LIKE 'Area%' AND regional NOT LIKE '[HQ]'
		GROUP BY regional
		) a
		GROUP BY a.`area`
		) b	")->result_array()[0]['total'];  
		$return='';

	 	$hasil = $this->db->query("select distinct regional from activity_progress where regional like '%[R%' order by regional ASC")->result_array();		  

		//seleksi regional
		$area1 = []; $area2 = []; $area3 = []; $area4 = [];
		foreach($hasil as $item) {
			//area 1
			if($item["regional"]=='[R01]' || $item["regional"]=='[R02]' || $item["regional"]=='[R10]') {
				array_push($area1, "'".$item["regional"]."'"); 
			}
			//area 2 
			if($item["regional"]=='[R03]' || $item["regional"]=='[R04]') {
				array_push($area2, "'".$item["regional"]."'"); 
			}
			//area 3
			if($item["regional"]=='[R05]' || $item["regional"]=='[R06]' || $item["regional"]=='[R07]') {
				array_push($area3, "'".$item["regional"]."'"); 
			}
			//area 4
			if($item["regional"]=='[R08]' || $item["regional"]=='[R09]' || $item["regional"]=='[R11]') {
				array_push($area4, "'".$item["regional"]."'"); 
			}
		}

		$area = [
			'Area1', 'Area2', 'Area3', 'Area4'
		];

		foreach($area As $row)
		{
			 if($row == 'Area1') {
				$area_parameter = $area1;
				$regional_terkait = implode(',',$area_parameter); 
			 } else if ($row == 'Area2') {
				$area_parameter = $area2;
				$regional_terkait = implode(',',$area_parameter); 
			} else if ($row == 'Area3') {
				$area_parameter = $area3;
				$regional_terkait = implode(',',$area_parameter); 
			 } else if ($row == 'Area4') {
				$area_parameter = $area4;
				$regional_terkait = implode(',',$area_parameter);  
			 } 
						  
			  $return .= '<div class="pricing-plans plans-2" style="margin=2px">
								<div class="plan-container">
									<table class="table table-condensed" width="100%" border="0">
										<tbody>
											<tr>   
												<td colspan="4" bgcolor="#000"> 
												<center><font color="#FFFFFF" size="+1">'.$row.'</font></center>
												</td>
											</tr>			   
											<tr>	
												<td colspan="2" bgcolor="#d9d9d9">
													<div align="center"><b>PLAN</b></div>
													<div align="center" style = "font-size: 11px">'.$this->dash->target_week_plan_area($regional_terkait).'</div>
													<div align="center" style = "font-size: 11px">'.$this->dash->target_total_up_week_plan_area($regional_terkait).'</div> 
												</td>
												<td colspan="2" bgcolor="#bebebe">	   
													<div align="center"><b>ACTUAL</b></div>
													<div align="center" style = "font-size: 11px">'.$this->dash->target_week_actual_area($regional_terkait).'</div>
													<div align="center" style = "font-size: 11px">'.$this->dash->target_total_up_week_actual_area($regional_terkait).'</div> 
												</td>
											</tr>
											<tr>
												<td colspan="4" bgcolor="#595959"> 
												'.$this->dash->total_delta_area().'
												</td> 
											</tr>  
											';
			  $has = $this->db->query("
									   select DISTINCT a.activity,
									   COALESCE(b.jumlah_activity, 0) As jumlah_activity,
									   COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
									   COALESCE((COALESCE(c.jumlah_activity_done, 0)/COALESCE(b.jumlah_activity, 0)),0) * 100 As per
									   from activity_progress a 
									   left join (
										   		  select count('activity') as jumlah_activity,activity 
												  from activity_progress where regional IN (".$regional_terkait.")  
												  group by activity
												  ) b on b.activity=a.activity
									   left join (
										   		  select count('activity') as jumlah_activity_done,activity 
												  from activity_progress where ( progress='Closed' or progress='CLOSED' or progress='closed' ) 
												  and regional IN (".$regional_terkait.") 
												  group by activity 
												  ) c on c.activity=a.activity
									   where b.jumlah_activity != '0'				  
									  ")-> 
									  result_array();

				// echo json_encode($has); die();

			   $c =0;
			   foreach($has as $items)
			   {
				   $return .= '<tr>
									<td width="90" bgcolor="#FFD966">'.$items["jumlah_activity_done"].' / '.$items["jumlah_activity"].'</td>
									<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$items["activity"].'</font></td>
									<td width="55" bgcolor="#A9D18E">'.round($items["per"],2).' % </td> 
							  </tr>';		
					
					$c++;		
			   }  
		 

			   if($c < $nilai_terbesar)
			   { 
					   $g = $nilai_terbesar - $c;
					   for($i=0; $i<$g; $i++) 
					   {
						   $return .= '<tr class="success"> 
											<td width="90" bgcolor="#FFD966">&nbsp;</td>
											<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">&nbsp;</font></td>
											<td width="55" bgcolor="#A9D18E">&nbsp;</td>
									   </tr>';	
					   }	   
			   }

			   $return .='
			   <tr>
			   <td colspan="4" bgcolor="#595959">  
			   <center><font color="#FFFFFF" style = "font-size: 14px">'.$this->dash->total_activity_area($regional_terkait).'</font></center>
			   </td> 
			   </tr>   
			   </tbody>
			   </table>
			   </div>  
			   </div> 
			   ';
		
		}
		
		$return.=''; 
		
		echo $return;	
   }

	public function progress_tracking_week() 
	{
	$date = date('Y-m-d');
	$date_last_week	= date('Y-m-d', strtotime("-1 week"));
	$last_week		= $this->db->query("select week from date_week where stardate<='$date_last_week' and enddate>='$date_last_week' order by week asc")->result_array()[0];

	$get_last_date = $this->db->query("select updated_at from activity_progress_speed_week where updated_at <= '$date' order by updated_at desc limit 1")->result_array(); 

	$date_last = $get_last_date[0]['updated_at'];

	$total_plans = $this->db->query("select * from activity_progress_speed_week where updated_at = '$date_last' order by week asc")->result_array();		

	$weeks = []; $target_up_to_plan = []; $target_up_to_actual = [];
	array_push($weeks,'W41');
	array_push($target_up_to_plan, "0");
	array_push($target_up_to_actual, "0"); 	

	foreach($total_plans as $item) { 
		array_push($weeks, $item['week']);			
		array_push($target_up_to_plan, $item['target_plan_up_to']);
		if($item['week'] <= $last_week['week']) {
			array_push($target_up_to_actual, $item['target_actual_up_to']);
		}		
	} 

	$data['weeks'] = $weeks;
	$data['target_up_to_plan'] = array_map('intval', $target_up_to_plan);	
	$data['target_up_to_actual'] = array_map('intval', $target_up_to_actual);		  

	// echo json_encode($data); die();

	$this->load->view('dashboard/progress_tracking_week', $data);

	 }

	//Untuk Chart Progress Week Regional
   public function store_progress_tracking_week_regional()
   {
	$week = $this->db->query("select distinct target_week from activity_progress where target_week like 'W%' order by target_week ASC")->result_array();
	$date = date('Y-m-d');
	$week_now = $this->db->query("select week from date_week where stardate <= '$date' order by week asc")->result_array();

	// echo print_r($week);

	//Nasional
	$total_plans = [];
	foreach($week as $item) {
		$total_plan = $this->dash->total_plan_week_nasional($item["target_week"]);
		array_push($total_plans, $total_plan);
	}

	foreach($total_plans as $item) { 
		$data = array(
			'updated_at' => date('Y-m-d'),
			'week' => $item['week'],
			'target_plan_up_to' => $item['target_up_to_plan'], 
			'target_actual_up_to' => $item['target_up_to_actual']
		);
	$this->db->insert('activity_progress_speed_week', $data);
	}
	
	//Regional
	$group = $this->db->query("select distinct owner_program from activity_progress")->result_array();
	 
	foreach($group as $item) {
		$group = $item['owner_program']; 

		$total_plans_regional = [];
		foreach($week as $item) {
			$total_plan_regional = $this->dash->total_plan_week_regional($item["target_week"], $group);
			array_push($total_plans_regional, $total_plan_regional); 
		}
		
		foreach($total_plans_regional as $item) {
		$data_regional = array(
			'updated_at' => date('Y-m-d'),
			'week' => $item['week'], 
			'owner_program' => $item['owner_program'], 
			'target_plan_up_to' => $item['target_up_to_plan'],
			'target_actual_up_to' => $item['target_up_to_actual'] 
		);
		$this->db->insert('activity_progress_speed_week_owner', $data_regional);
		}	
	}
		// echo json_encode($week_now); die();
	} 

	public function progress_tracking_week_regional()
	{
	$owner = $this->session->groupName;

	$date = date('Y-m-d');
	$date_last_week	= date('Y-m-d', strtotime("-1 week"));
	$last_week		= $this->db->query("select week from date_week where stardate<='$date_last_week' and enddate>='$date_last_week' order by week asc")->result_array()[0];

	if($owner == 'IT HQ') {
		 
	$get_last_date = $this->db->query("select updated_at from activity_progress_speed_week_owner where updated_at <= '$date' and owner_program IN ('IT CHARGING SERVICES','IT FULFILLMENT SERVICES','IT INFRASTRUCTURE MANAGEMENT','IT NEW BUSINESS SERVICES', 'IT SQM', 'IT UX Enablement') order by updated_at desc limit 1")->result_array(); 
	$date_last = $get_last_date[0]['updated_at'];
	$total_plans = $this->db->query("SELECT a.updated_at as updated_at, 'IT HQ' AS owner_program, a.`week`, SUM(a.target_plan_up_to) as target_plan_up_to, SUM(a.target_actual_up_to) as target_actual_up_to FROM
	(select * from activity_progress_speed_week_owner where updated_at = '$date_last' and owner_program IN ('IT CHARGING SERVICES','IT FULFILLMENT SERVICES','IT INFRASTRUCTURE MANAGEMENT','IT NEW BUSINESS SERVICES', 'IT SQM', 'IT UX Enablement') order by week ASC) a
	GROUP BY a.updated_at, a.`week`")->result_array();	

	} else {

	$get_last_date = $this->db->query("select updated_at from activity_progress_speed_week_owner where updated_at <= '$date' and owner_program = '$owner' order by updated_at desc limit 1")->result_array(); 
	$date_last = $get_last_date[0]['updated_at']; 
	$total_plans = $this->db->query("select * from activity_progress_speed_week_owner where updated_at = '$date_last' and owner_program = '$owner' order by week asc")->result_array();		

	} 

	$weeks = []; $target_up_to_plan = []; $target_up_to_actual = [];
	array_push($weeks,'W41'); 
	array_push($target_up_to_plan, "0");
	array_push($target_up_to_actual, "0"); 

	foreach($total_plans as $item) {  
		array_push($weeks, $item['week']);			
		array_push($target_up_to_plan, $item['target_plan_up_to']);
		if($item['week'] <= $last_week['week']) {
			array_push($target_up_to_actual, $item['target_actual_up_to']);
		}		
	}

	$data['weeks'] = $weeks;
	$data['target_up_to_plan'] = array_map('intval', $target_up_to_plan);	
	$data['target_up_to_actual'] = array_map('intval', $target_up_to_actual);		  

	$this->load->view('dashboard/progress_tracking_week_regional', $data);

	 }
 
//    public function summary_regional_nep_combat()
//    {
// 	   $hasil_nep = $this->db->query("select 
// 							    a.regional,
// 		                        COALESCE(b.jumlah_target, 0) As jumlah_target,
// 							    COALESCE(c.jumlah_target_close, 0) As jumlah_target_close,
// 							    COALESCE((COALESCE(c.jumlah_target_close, 0)/COALESCE(b.jumlah_target, 0)),0) * 100 As per
// 								from
// 								(	
// 									  select * from
// 									   (
// 										  select DISTINCT a.regional
// 										  from activity_progress a
// 										  where regional
// 										  like '%[R%'
// 										  group by a.regional
// 									   ) ac	
// 									)a
								   
// 								left join ( select count('regional') as jumlah_target_close,regional 
// 												  from activity_progress  where progress in ('Closed','CLOSED','closed')
// 												  and   owner_program like '%nep%'
// 												  group by regional) c on c.regional=a.regional		   
								   
// 								left join ( select count('regional') as jumlah_target,regional 
// 												  from activity_progress   where   owner_program like '%nep%'
// 												  group by regional) b on b.regional=a.regional	")->result_array();
												  
     

// 	 $hasil_combat = $this->db->query("select re.name_regional,re.regional as regional2,b.regional,
// 			 COALESCE(b.jumlah_target,0) as jumlah_target,
// 			 COALESCE(b.jumlah_done,0) as jumlah_close,
//              COALESCE((COALESCE(b.jumlah_done, 0)/COALESCE(b.jumlah_target, 0)),0) * 100 As per

			 
// 			 from
// 		     (  
// 			 select 'Sumbagut' as name_regional,  '[R01]' as regional
// 			 union
// 			 select 'Sumbagsel' as name_regional, '[R02]' as regional
// 			 union
// 			 select 'Jabotabek' as name_regional, '[R03]' as regional
// 			 union
// 			 select 'Jabar' as name_regional, '[R04]' as regional
// 			 union
// 			 select 'Jateng' as name_regional, '[R05]' as regional
// 			 union
// 			 select 'Jatim' as name_regional, '[R06]' as regional
// 			 union
// 			 select 'Bali Nusra' as name_regional, '[R07]' as regional
// 			 union
// 			 select 'Kalimantan' as name_regional, '[R08]' as regional
// 			 union
// 			 select 'Sulawesi' as name_regional, '[R09]' as regional
// 			 union
// 			 select 'Sumbagteng' as name_regional, '[R10]' as regional
// 			 union
// 			 select 'PUMA' as name_regional, '[R11]' as regional 
//           )re
//           left join
//           (
// 			 				  select a.regional,
// 		                          count('a.regional') As jumlah_target,
// 								  COALESCE(b.jumlah_done,0) as jumlah_done  
// 								  from 
// 								  activity_progress a
// 								  left join ( select count('regional') As jumlah_done,regional
// 													from activity_progress 
// 													where
// 													 progress in ('Closed','CLOSED','closed') AND 
// 													(owner_program like '%combat%' or 
// 													`group` like '%combat%') 
// 													group by regional
// 									) as b on b.regional=a.regional
// 									where (a.owner_program like '%combat%' or  a.group like '%combat%') 
// 								 group by a.regional
// 			 )b on b.regional=re.regional")->result_array();		
     
// 	  // NEP		 
//       $return = '<div class="pricing-plans plans-2">
// 								<div class="plan-container">
// 									<table class="table table-condensed" width="100%" border="0">
//               <tbody>
// 			  <tr>
//               <td colspan="4" bgcolor="#000">
//               <center><font color="#FFFFFF" size="+1">NEP</font></center>
//               </td>
//               </tr>
// 			  <tr>
//               <td colspan="2" bgcolor="#d9d9d9">
//                 <div align="center">'.$this->dash->target_up_to_week_summary_NEP().'</div>
//                 <div align="center">Total Done : <strong>'.$this->dash->_summary_NEP_total()['jumlah_activity_done'].'</strong></div>  
//                 <div align="center">Target Total : <strong>'.$this->dash->_summary_NEP_total()['target_total'].'</strong></div>
//               </td>
//               <td colspan="2" bgcolor="#bebebe">
//                 <div align="center">'.$this->dash->target_up_to_week_summary_NEP_ach().'</div>
//                 <div align="center">Ach Overall : <strong>'.round($this->dash->_summary_NEP_total()['per'],2).' %</strong></div>
//               </td>
//               </tr>';  
// 				 foreach($hasil_nep as $row)
// 			     {
// 									$return.= '<tr>
// 										   <td width="90" bgcolor="#FFD966">'.$row["jumlah_target_close"].' / '.$row["jumlah_target"].'</td>
// 										   <td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row["regional"].' '.array_regional()[$row["regional"]].'</font></td>
// 										   <td width="55" bgcolor="#A9D18E">'.round($row["per"],2).' % </td>
// 								    </tr>';
// 				 }	        
									   
// 	   $return.='</tbody>
// 			   </table>
// 			   </div> 
// 			   </div>';	
			
// 	   // COMBAT	  
// 	   $return.='<div class="pricing-plans plans-2">
// 				 <div class="plan-container">
// 				 <table class="table table-condensed" width="100%" border="0">
//                  <tbody>
// 			     <tr>
//                  <td colspan="4" bgcolor="#000">
//                  <center><font color="#FFFFFF" size="+1">COMBAT</font></center>
// 				  </td>
// 				  </tr>
// 				  <tr>
// 				  <td colspan="2" bgcolor="#d9d9d9">
// 					<div align="center">'.$this->dash->target_up_to_week_summary_COMBAT().'</div>
// 					<div align="center">Total Done : <strong>'.$this->dash->_summary_COMBAT_total()['jumlah_activity_done'].'</div>  
// 					<div align="center">Target Total : <strong>'.$this->dash->_summary_COMBAT_total()['target_total'].'</div>
// 				  </td>
// 				  <td colspan="2" bgcolor="#bebebe">
// 					<div align="center">'.$this->dash->target_up_to_week_summary_COMBAT_ach().'</div>
// 					<div align="center">Ach Overall : <strong>'.round($this->dash->_summary_COMBAT_total()['per'],2).' %</strong></div>
// 				  </td>
// 				  </tr>';		

	  
// 				 foreach($hasil_combat as $row)
// 			     {
// 					$return.= '<tr>
// 						   <td width="90" bgcolor="#FFD966">'.$row["jumlah_close"].' / '.$row["jumlah_target"].'</td>
// 						   <td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row["regional2"].' '.$row["name_regional"].'</font></td>
// 						   <td width="55" bgcolor="#A9D18E">'.round($row["per"],2).' % </td>
// 					</tr>';
// 				 }	 						   
									   
// 	  $return.= '</tbody>
// 			   </table>
// 			   </div> 
// 			   </div>';			   
// 	  echo  $return;							  
//    }

   public function progress_tracking_regional()
   {
	    //$grp = $this->session->userdata('groupName');
		//if($grp == "ADMIN")
		//{
	     	 $adm = $this->dash->progres_tracking_regional_query_admin();
			 echo json_encode($adm); die();  
		//}
	    //else
		//{
		//	$adm = $this->dash->progres_tracking_regional_query_region();
		//	echo json_encode($adm); 
		//}
   }
   
   public function progress_tracking_nep()
   {
	   
		
		$adm = $this->dash->progres_tracking_nep_admin();
	    echo json_encode($adm);  
		
   }
   public function progress_tracking_combat()
   {
	  
		$adm = $this->dash->progres_tracking_combat_admin();
	    echo json_encode($adm);  
		
   }
   
   public function progress_tracking_nasional()
   {
	   
			 // $rs = $this->db->query("  select 
			  // distinct a.owner_program, 
			  // COALESCE(b.jumlah_activity,0)  as jumlah_target,
			  // COALESCE(c.jumlah_activity_done,0)  as jumlah_done 
			  // from activity_progress a
			  // left join ( select count(*) as jumlah_activity,owner_program 
						  // from activity_progress  
						  // group by owner_program ) b on b.owner_program=a.owner_program
			  // left join ( select count(*) as jumlah_activity_done,owner_program
									// from activity_progress where (progress='Closed' or progress='CLOSED' or progress='closed' ) 
									 // group by owner_program) c on c.owner_program=a.owner_program
									 
			  // where a.regional  <> '[HQ]' and a.owner_program like '%nep%'				 
			  // order by a.owner_program ")->result_array();
			  
			   $rs = $this->db->query("      
			                             select DISTINCT a.group,
										 COALESCE(c.jumlah_activity_done, 0) As jumlah_done,
										 COALESCE(b.jumlah_activity, 0) As jumlah_target,
										
										 COALESCE((COALESCE(c.jumlah_activity_done, 0)/b.jumlah_activity),0) * 100 As per 
										 from activity_progress a 
										 left join ( select count(*) as jumlah_activity,`group` 
																  from activity_progress  
																  group by `group` ) b on b.group=a.group
										 left join ( select count(*) as jumlah_activity_done,`group` 
														from activity_progress where progress in ('closed','Closed','CLOSED')
										  			  group by `group`) c on c.group=a.group
										  where a.regional  <> '[HQ]'	
										
										  ")->result_array(); 
	  
	  
	  /*
			  
			  $hasil = $this->db->query("      
			                             select DISTINCT a.group,
										 COALESCE(c.jumlah_activity_done, 0) As jumlah_activity_done,
										 COALESCE(b.jumlah_activity, 0) As target_total,
										
										 COALESCE((COALESCE(c.jumlah_activity_done, 0)/b.jumlah_activity),0) * 100 As per 
										 from activity_progress a 
										 left join ( select count(*) as jumlah_activity,`group` 
																  from activity_progress  
																  group by `group` ) b on b.group=a.group
										 left join ( select count(*) as jumlah_activity_done,`group` 
														from activity_progress where progress in ('closed','Closed','CLOSED')
										  			  group by `group`) c on c.group=a.group
										  where a.regional  <> '[HQ]'	
										
										  ")->result_array(); // ".filterBygroup1()."	
		
		$return=''; // 
		$nilai_terbesar = $this->dash->cariJumlahrowsummaryExceGroup()-1;
		$arr = [];
		foreach($hasil As $row)
		{
			  
			  $jumlah_2 = $row["jumlah_activity_done"];
			  $jumlah = $row["target_total"];
			  $jumlahper = $row["per"];
			  $return .= '<div class="pricing-plans plans-3">
			  <div class="plan-container">
			  <table class="table" width="100%" border="0">
              <tbody>
			  <tr>
              <td colspan="4" bgcolor="#595959"> 
              <center><font color="#FFFFFF" size="+1">'.$row["group"].'</font></center>
              </td>
              </tr>
			  <tr>
              <td colspan="2" bgcolor="#FFCC00">
                '.$jumlah_2.' / '.$jumlah.'
              </td>
              <td colspan="2" bgcolor="#70AD47"> 
                '.round($jumlahper,2).' %
              </td>
              </tr>';
			 	  
			  
			  */
		
	 
			 $r =array();
			 $rgroup =array();
			 $target =array();
			 $done =array();
			 $lineset =array();
			 $r2 = 0;
			 foreach($rs as $row)
			 {
			  $per = ( $row['jumlah_done'] / $row['jumlah_target'] ) * 100;
			  //$per2 = round($per,2);
			  //array_push($r,array("owner_program"=>$row['owner_program'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
			  $per2 = round($per,2);
			  //array_push($r,array("group"=>$row['group'],"target"=>$row['jumlah_target'],"persen"=>$per2,"done"=>$row['jumlah_done']));	
			  $rgroup[$r2]['label'] = $row['group'];
			  $target[$r2]['value'] = $row['jumlah_target'];
			  $done[$r2]['value']	= $row['jumlah_done'];
			  $lineset[$r2]['value']= $per2;
			 $r2++;
			 }
	   
	   //print_r($rgroup);
	   
	   
	   $ret = [
			"chart"=> [
			"caption"=> "National Progress Tracking",
			"subcaption"=> "2019",
			//"xaxisname"=> "Years",
			//"yaxisname"=> "Total",
			//"pyaxisname"=> "Units Sold",
			//"formatnumberscale"=> "1",
			//"plottooltext" => '<b>$dataValue</b> apps were available on <b>$seriesName</b> in $label',
			"plottooltext" => '<b>$dataValue</b>    $seriesName on $label',
			//"syaxisname"=> "% of total ",
		    "snumbersuffix"=> "%",
			"showvalues"=> "0",
		    "syaxismaxvalue"=> "100",
			"theme"=>"fusion",
			"drawcrossline"=> "1",
			"divlinealpha"=> "20"
		  ],
		  "categories"=> [
			[
			  "category"=> $rgroup 
			]
		  ],
		    "dataset"=> [
    [
      "dataset"=> [
        [
          "seriesname"=> "Target",
          "data"=> $target /* [
						[
						  "value"=> $r[array_search('NEP Area 01', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('NEP Area 02', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('NEP Area 03', array_column($r, 'owner_program'))]['target']
						],
						[
						  "value"=> $r[array_search('NEP Area 04', array_column($r, 'owner_program'))]['target']
						],
						
          ] */
        ],
        
      ]
    ],
    [
      "dataset"=> [
        [
          "seriesname"=> "Done",
          "data"=> $done /* [
                [
				  "value"=> $r[array_search('NEP Area 01', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('NEP Area 02', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('NEP Area 03', array_column($r, 'owner_program'))]['done']
				],
				[
				  "value"=> $r[array_search('NEP Area 04', array_column($r, 'owner_program'))]['done']
				],
				
          ] */
        ],
        
      ]
    ]
  ],
		
		    "lineset"=> [
			[
			  "seriesname"=> "on progress %",
			  "plottooltext"=> 'Total $label is <b>$dataValue</b> ',
			  "showvalues"=> "1",
			  "data"=>$lineset /* [
					[
					  "value"=> $r[array_search('NEP Area 01', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('NEP Area 02', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('NEP Area 03', array_column($r, 'owner_program'))]['persen']
					],
					[
					  "value"=> $r[array_search('NEP Area 04', array_column($r, 'owner_program'))]['persen']
					],
				] */
			]
		  ] 
		 
		];

		
		echo json_encode($ret,true);  
   }	   
   public function progress_tracking_nasional__()
   {
	    $r = $this->dash->progres_tracking_nasional_query();
		$ct = [];
		$vl_r = [];
		$vl_d = [];
		$vl_op = [];
		foreach($r as $row)
		{
		           
		           array_push($ct, array("label"=>$row['section']));
		           array_push($vl_r, array("value"=>$row['remaining']));
		           array_push($vl_d, array("value"=>$row['jumlah_done']));
		           //$semua = $row['remaining'] + $row['jumlah_done'];
		           //$on_progress = round(($row['jumlah_done']/$semua) * 100,2);
		           $on_progress = round( $row['on_progress'] , 2);
				   array_push($vl_op, array("value"=>$on_progress));
		}
	    
	   $ret = [
			"chart" => [
			"showvalues" => "0",
			"caption" => "National Progress Tracking",
			//"subcaption" => "Week 33",
			//"numbersuffix" => "K",
			//"yaxisname" => "Calories Burnt",
			//"syaxisname" => "Share of market",
			"labelDisplay" => "rotate",
			"slantLabel" => "1",
		    "syaxismaxvalue" => "115",
			"plottooltext" => '<b>$value</b> calories burnt through $seriesName on $label',
			"snumbersuffix" => "%",
			"drawcustomlegendicon" => "0",
			//"rotatelabels" => "1",
			"drawcrossline" =>"1",
	        "divlinealpha" => "20",
			"theme" => "fusion" 
			
		  ],
		  "categories"=> [
		  [
		    "category" => $ct
		  ]
		  ],
		  "dataset"=> [
			[
			  "seriesname"=> "Done",
			  "data"=> $vl_d
			],
			[
			  "seriesname"=> "Remaining",
			  "data"=> $vl_r
			],
			[
			 "seriesname"=> "On Progress",
			 "showvalues"=> "1",
			 "parentyaxis" => "S",  
			 "plottooltext" => 'Goal was to lose <b>$value</b> calories on $label',
			 "renderas"=> "line",
			 "data"=> $vl_op
			]
		],
		
		 
		  
		];

     echo json_encode($ret,true);  
	   
   }
   public function progress_trackking_nasional_per_week()
   {
				  $ret='<table class="table table-dark" style="margin-top:20px;">
					    <thead class="thead-dark">
						<tr>
						<th width="85" colspan="3" bgcolor="#0099FF">Owner Program </th>
						<th width="33" bgcolor="#0099FF"></th>';
						$kapr = $this->dash->progres_tracking_nasional_query_week();
						$jumlah_colom_week = 0;
						foreach($kapr as $r) 
						{
							$ret.='<th width="34" bgcolor="#0099FF" style="text-align: center;">'.$r["target_week"].' </th>';
						$jumlah_colom_week= $jumlah_colom_week + 1;
						}
					    $ret.='</tr></thead>';
					    $ret.='<tbody>';
						$kapr2 = $this->dash->progres_tracking_nasional_query_section();

						foreach($kapr2 as $r)
						{
							    $sec = $r["section"];
								$ret.='<tr>
									<td colspan="3" rowspan="2">'.$sec.'  </td>
									<td>Plan </td>';
									$kapr3 = $this->dash->progress_tracking_nasional_qiery_ach_target($sec);
							  		$ret_c='';
									foreach($kapr3 as $r)
									{
										$ret.='<td style="text-align: center;">'.$r["target"].'</td>';
										$ret_c.='<td style="text-align: center;">'.$r["target_done"].'</td>'; 
									}	
								 $ret.='</tr>
								  <tr style="background-color:#93e493">
									<td>Act </td>';
									$ret.=$ret_c; 
								  $ret.='</tr>'; 
						}
						
					 
				  $ret.='</tbody>';
				  $ret.='</table>';
	   
	   echo $ret;	
   }
   function summary_by_group()
   { 

		$nilai_terbesar = $this->dash->cariJumlahrowsummaryExceGroup();
		
		$hasil = $this->db->query("SELECT DISTINCT section from activity_progress ORDER BY section ASC")->result_array();

		$return='';
	 	
		foreach($hasil As $row)
		{
			  $return .= '<div class="pricing-plans plans-2">
			  <div class="plan-container">
			  <table class="table" width="100%" border="0">
              <tbody>
			  <tr>
              <td colspan="4" bgcolor="#595959"> 
              <center><font color="#FFFFFF" size="+1">'.$row["section"].'</font></center>
              </td>
              </tr>'; 
			  
              $has = $this->db->query("
			  SELECT a.section, a.activity, 
			  COALESCE(COUNT(*), 0) AS jumlah_activity,
			  COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
			  FROM activity_progress a
				LEFT JOIN  
				(
					SELECT section, activity, COUNT(*) AS jumlah_activity_done FROM activity_progress
					WHERE progress = 'Closed'
					AND section = '".$row['section']."'
					GROUP BY section, activity
				) b ON a.activity = b.activity
			  WHERE a.section = '".$row['section']."' 
			  GROUP BY a.section, a.activity
							  ")->result_array();
			  
			   $c= 0;
			   $total_activity = 0;
			   $total_activity_done = 0;
			   foreach($has as $row) 
			   {

				$jumlah_activity =  $row["jumlah_activity"];
				$jumlah_activity_done =  $row["jumlah_activity_done"];

				$total_activity += $row["jumlah_activity"];
				$total_activity_done += $row["jumlah_activity_done"];

				$persentase = round(($jumlah_activity_done / $jumlah_activity) * 100, 2);

				   $return .= '<tr class="success"> 
						<td width="90" bgcolor="#FFD966">'.$row["jumlah_activity_done"].' / '.$row["jumlah_activity"].'</td>
						<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row["activity"].'</font></td>
						<td width="55" bgcolor="#A9D18E"><center>'.$persentase.' %</center></td>
				   </tr>';	 						
				   $c++;  
			   }  

			   if($c < $nilai_terbesar)
			   {
					   $g = $nilai_terbesar - $c;
					   for($i=0; $i<$g; $i++)
					   {
						   $return .= '<tr class="success"> 
											<td width="90" bgcolor="#FFD966">&nbsp;</td>
											<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">&nbsp;</font></td>
											<td width="55" bgcolor="#A9D18E">&nbsp;</td>
									   </tr>
									   ';	
					   }	   
			   }
				  
			   
			   $total_persentase = round(( $total_activity_done/$total_activity ) * 100, 2);

			   $return .='
			   <tr>
			   <td colspan="2" bgcolor="#FFCC00">
			   <center>'.$total_activity_done.' / '.$total_activity.'</center>
			   </td>
			   <td colspan="2" bgcolor="#70AD47"> 
			   <center>'.$total_persentase.' %</center>
			   </td>
			   </tr>  
			   </tbody>
			   </table>
			   </div> 
			   </div> 
			   ';
		
		}
		
		echo $return;	
   }
   function summary_by_divisi()
   {
	    $hasil = $this->db->query("SELECT a.owner_program,
									COALESCE(COUNT(*), 0) AS jumlah_activity,
									COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
									FROM activity_progress a
									LEFT JOIN 
									(
										SELECT owner_program, COUNT(*) AS jumlah_activity_done FROM activity_progress
										WHERE progress = 'Closed'
										GROUP BY owner_program
									) b ON a.owner_program = b.owner_program
									GROUP BY a.owner_program
									ORDER BY a.owner_program ASC
								  ")->result_array(); 	
	   
	   $rt = '<center>
					<table  width="100%" class="table">
					  <thead>
					    <tr>
							<td colspan="4" bgcolor="#595959">
							 <font color="#FFFFFF" style="font-size:1.875em;"><center>Owner Program</center></font> 
							</td>
						</tr>
					  <t/head> 
					  <tbody>';
					  
					  $total_activity = 0;
					  $total_activity_done = 0;

					  foreach($hasil As $row){
						$persentase = round(($row['jumlah_activity_done']/$row['jumlah_activity']) * 100, 2);
						$total_activity += $row['jumlah_activity'];
						$total_activity_done += $row['jumlah_activity_done'];

						   $rt.='<tr>';
						   $rt.= '<td width="70" bgcolor="#FFD966"><center>'.$row['jumlah_activity_done'].' / '.$row['jumlah_activity'].'</center></td>';
						   $rt.= '<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row['owner_program'].'</font></td>';
						   $rt.= '<td width="55" bgcolor="#A9D18E"><center>'.$persentase.' %</center></td>';
						   $rt.='</tr>';
						  
					  }
					
				$rt.='
				<tr>
				<td colspan="2" bgcolor="#FFCC00"><div align="center"><font size="+2"><strong>'.$total_activity_done.'/'.$total_activity.'</strong></font></div></td>
				<td colspan="2" bgcolor="#70AD47"><div align="center"><font size="+2"><strong>'.round(($total_activity_done/$total_activity)*100,2).'%</strong></font></div></td>
			  	</tr>				
				</tbody>
				</table>
				</center>
				';
		echo $rt;		
   }	   
  
   function progress_trackking_combat_nasional()
   {
	    $has = $this->db->query("	select 
					sum(asd.jumlah_activity) as jum1,
					sum(asd.jumlah_activity_done) as jum2
					from
					(				
						  select 
						  distinct a.owner_program, 
						  COALESCE(b.jumlah_activity,0)  as jumlah_activity,
						  COALESCE(c.jumlah_activity_done,0)  as jumlah_activity_done
									
						  from activity_progress a
						  left join ( select count(*) as jumlah_activity,owner_program 
									  from activity_progress  
									  group by owner_program ) b on b.owner_program=a.owner_program
						  left join ( select count(*) as jumlah_activity_done,owner_program
												from activity_progress where (progress='Closed' or progress='CLOSED' or progress='closed' ) 
												 group by owner_program) c on c.owner_program=a.owner_program
						  ".filterBygroup1()."					 
					)asd
                 		
					")->result_array(); 	
					$jumlah_semua =  $has[0]['jum1'];
					$jumlah_semua2 = $has[0]['jum2'];
					$jumlah_per =    ($has[0]['jum2']/$has[0]['jum1']) * 100;
					$hasil = $this->dash->combat_query();
					$rt = '<center>
					<table  width="100%" class="table">
					  <thead>
					    <tr>
							<td colspan="4" bgcolor="#595959">
							 <font color="#FFFFFF" style="font-size:1.875em;"><center>COMBAT</center></font> 
							</td>
						</tr>
					  <t/head>
					  <tbody>'; 
                      $rt .= $this->dash->target_up_to_week_combat_tr(); 
					  foreach($hasil As $row){
						   $d = ($row['jumlah_done']/($row['jumlah_target']==0 ? 1 : $row['jumlah_target'])) * 100;
						   $per = round($d,2);
						   $rt.='<tr>';
						   $rt.= '<td width="70" bgcolor="#FFD966"><center>'.$row['jumlah_done'].' / '.$row['jumlah_target'].'</center></td>';
						   $rt.= '<td colspan="2" bgcolor="#595959"><font color="#FFFFFF">'.$row['name_regional'].'</font></td>';
						   $rt.= '<td width="55" bgcolor="#A9D18E"><center>'.$per.' %</center></td>';
						   $rt.='</tr>';
						  
					  }
					
	            $rt.='</tbody>
				</table>
				</center>
				';
		echo $rt;		
   }	   
   
}
