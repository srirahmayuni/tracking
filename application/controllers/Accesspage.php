<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accesspage extends MY_Controller {

    protected $_fields; 

    public function __construct() {
        parent::__construct();
        $this->load->model('Accesspage_model', 'akespage');
		$this->load->model('Page_model', 'pages');
        $this->load->model('Groups_model', 'groups'); 
		$this->load->library(array('form_validation'));
    }

  
	public function index() {
        if (!is_authorized('accesspage', 'index'))
            access_denied();

        $data['title'] = title().' | Hak Akses Halaman';
        $data['message'] = $this->session->flashdata('message');
        $content = 'accesspage/content';
        $this->_load_layout($content, $data);
    }
	 public function data()
    {
        if (!is_authorized('accesspage', 'index'))
            access_denied();
        
              
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            0 => 'g.name', 
            1 => 'pg.controller',
            2 => 'pa.allow'
         ); 

                
      
        $defaultOrder =  'pa.created_date desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->akespage->getSql();
      
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {
            $no = $requestData['start'] + 1;
        }           
        
        foreach($query[0] As $row)
        {
             $nestedData=array(); 
             $nestedData[] = $no;
             $nestedData[] = $row['groupname'];
             $nestedData[] = $row['pagename'];    
             $nestedData[] = $row['allow'];
             $aksi = '<a  class="btn btn-sm btn-info" href="'.base_url().folderback().'/accesspage/update/'.$row['id'].'"> <i class="glyphicon glyphicon-pencil"></i>Edit</a>';
             
		     //$aksi2.= '<a class="btn btn-sm btn-danger" onClick="Delete(' . $row['id'] . ')"><i class="glyphicon glyphicon-trash">
			 //									</i>Delete</a>';
			 $aksi2 = '<a  class="btn btn-sm btn-danger"   href="'.base_url().folderback().'/accesspage/delete/'.$row['id'].'"><i class="glyphicon glyphicon-trash"></i>Delete</a>';
             $nestedData[] = $aksi.' '.$aksi2;   
             $data[] = $nestedData;
             $no++;
        }
        $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
        
    }
	
	 public function create() {
       if (!is_authorized('accesspage', 'create'))
            access_denied();

        $privilege_page_id = null;
        $group_id = null;
        $page_id = null;
        $allow = null;
		
        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Hak Akses Page' => siteAdminUrl('accessaction'),
            'Tambah Haks Akes Page' => siteAdminUrl('accesspage/create'));

        if ($this->input->post('Crate')) {
            $this->_isCreate = true;

            $group_id = $this->input->post('group_id');
            $page_id = $this->input->post('page_id');
            $allow = $this->input->post('allow');
            $this->group_id = $group_id;
            $this->page_id = $page_id ;
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();

                $ssss = array(
                    'group_id' => $group_id,
                    'page_id' => $page_id,
                    'allow' => $allow
                );
                $ss = $this->akespage->insert($ssss);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Hak Akses gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data Hak Akses baru berhasil di buat!');
                    redirect(folderBack().'/accesspage');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Tambah Hak Akses page';
        $data['err'] = $err;

        $data['action'] = $this->_isCreate;
        $data['privilege_page_id'] = $privilege_page_id;
        $data['group_id'] = $group_id;
        $data['page_id'] = $page_id;
        $data['allow'] = $allow;
       
        $dataGroups = $this->groups->get_dropdown_listR();
		$data['dropdownGroups'] = $dataGroups;
		$data['dropdownHalaman'] = $this->pages->ComboHalaman();
       
        
		$content = 'accesspage/create';
        $this->_load_layout($content, $data);
    }
	
	 public function update() {
        if (!is_authorized('accesspage', 'update'))
            access_denied();

        $group_id = null;
        $page_id = null;
       
        $allow = null;
		
		$privilege_page_id = $this->uri->segment(4);
        $err = false;
        $this->_isCreate = false;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Hak Akses Page' => siteAdminUrl('accessaction'),
            'Edit Haks Akes Page' => siteAdminUrl('accesspage/update/'.$privilege_page_id.''));

        if ($this->input->post('Crate')) {
            $this->_isCreate = false;

            $privilege_page_id = $this->input->post('privilege_page_id'); 
            $group_id = $this->input->post('group_id');
            $page_id = $this->input->post('page_id');
            $allow = $this->input->post('allow');
           
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();

                $ssss = array(
                    'group_id' => $group_id,
                    'page_id' => $page_id,
                    'allow' => $allow
                );
                $sss = $this->akespage->update($ssss, $privilege_page_id);
                
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data  gagal di update, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message','Data baru berhasil di update!');
                    redirect(folderBack().'/accesspage');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Update Hak Akses Page';
        $data['err'] = $err;
        $dt = $this->akespage->GetAccesspageByID($privilege_page_id);
		//print_r($dt);
        $data['action'] = $this->_isCreate;
        $data['group_id'] = $dt->group_id;
        $data['page_id'] = $dt->page_id;
        
        $data['allow'] = $dt->allow;
        $data['privilege_page_id'] = $privilege_page_id;
        $dataGroups = $this->groups->get_dropdown_listR();
		$data['dropdownGroups'] = $dataGroups;
		$data['dropdownHalaman'] = $this->pages->ComboHalaman();
       
		$content = 'accesspage/create';
        $this->_load_layout($content, $data);
    }
	
	public function delete() {
       if (!is_authorized('accesspage', 'delete'))
            access_denied();
		$privilege_page_id = $this->uri->segment(4);
		$r = $this->akespage->cekDelete($privilege_page_id);
		
		if($r)
		{
			$this->session->set_flashdata('message', 'Maaf Data Tidak bisa dihapus!');
		    redirect(folderBack().'/accesspage/index');
		}
        else
		{
			$this->akespage->delete($privilege_page_id);
		    $this->session->set_flashdata('message', 'Data berhasil dihapus!');
		    redirect(folderBack().'/accesspage/index');
		}	
			
		
		
		
	}

    public function myRules() {
        return $this->_fields = array(
            array(
                'field' => 'group_id',
                'label' => 'Group Name',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'allow',
                'label' => 'Allow',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'page_id',
                'label' => 'Halaman',
                'rules' => 'required|trim|callback_check'
            )
			
        );
    }
	
	public function check($id) {
        $group_id = $this->group_id;
		
		if($id  and  $group_id)
		{	
		
			$q = "Select * from privilege_page where page_id=$id and group_id=$group_id ";
			$fg = $this->db->query($q);
			$fgs = $fg->num_rows();
			if ($fgs > 0) {
				$this->form_validation->set_message('check', ' duplicat row Group Dan Page  sudah ada , silahkan edit data Anda!');
				return false;
			} else {
				return true;
			}
		}
		else
			return true;
    }
}


