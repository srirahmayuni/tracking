<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class log_activity extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->session->userdata('userid');
        //$this->session->userdata('name');
        //$this->session->userdata('level');

        //$this->_is_logged_in();
    }

    public function index()
    {
      
		 //if (!is_authorized('log_activity', 'index'))
        //    access_denied();

        $data['title'] = 'Log Activity  | Telkomsel';
        $data['breadcrumb'] = array('Beranda' =>  base_url() .'/home',
            'Master' => '#',
            'Log Activity ' => siteAdminUrl('log_activity'));

        $data['message'] = $this->session->flashdata('message');
        
		$data['cls_home']      = '';
        $data['cls_dashboard'] = '';
		$data['cls_activity_progress'] = '';
		$data['cls_log_activity'] = 'class="active"';
		$data['cls_reporting'] = ''; 
        
        $content = 'log_activity/content';
        $this->_load_layout($content, $data);
		
        
    }
   
    public function data()
    {
        $this->load->model('Activitylog_model','activitylog');
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        
		$columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            //0 => 'a.name',
            0 => 'a.shift',
            1 => 'a.tanggal',
            2 => 'a.activity',
            3 => 'a.event'
	     ); 
   
        $defaultOrder =  'a.id_log_activity desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->activitylog->getSql($requestData);
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {    
             
            $no = $requestData['start'] + 1;
        }
		foreach($query[0] As $row)
        {
             $nestedData=array(); 
             //$nestedData[] = '';
			 //$nestedData[] = $row['id_log_activity'];
			 $nestedData[] = $no;
             $nestedData[] = $row['nama'];  
             $nestedData[] = $row['shift'];    
             $nestedData[] = $row['tanggal'];
             $nestedData[] = $row['activity']; 
             $nestedData[] = $row['event']; 
		     //$aksi = '<a  class="btn btn-sm btn-info "  href="'.base_url().'activity_progress/update/'.$row['id'].'"> <i class="glyphicon"></i>Change Status</a>';
		     
			 if($this->session->userdata('group_id')==2)
		     {
				 $aksi ="";
				 $aksi2 ="";
			 }
             else
             {				 
			 
				$aksi = '<a  class="btn btn-sm btn-info"  onclick="edit('.$row['id_log_activity'].')"> <i class="glyphicon"></i>Edit</a>';
				$aksi2 = '<a  class="btn btn-sm  btn-danger"  onclick="del('.$row['id_log_activity'].')"> <i class="glyphicon"></i>Delete</a>';
             
			 }
			 
			 $nestedData[] =$aksi." ".$aksi2;
			 $data[] = $nestedData;
             $no++;
        }
	    $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
		
	}
    
	/* public function detail_log()
	{
		$this->load->model('Activitylog_model','activitylog');
		$result = array();
        $result['res'] = '1';
        $id = $this->input->post('id');
        //$d = $this->activitylog->getSql2($id);
		$table = '<table class="table ">
					<thead>
						<tr>
							<th>No</th>
							<th>Nomor BA</th>
							<th>Tanggal BA</th>
							<th>Service Transation Code</th>
							<th>Request Status Name</th>
							<th>Action</th>
						</tr>
					</thead><tbody>';
		$no = 0;
		foreach($d as $row)
		{  
			     // $no = $no + 1; 
				 // $dis = ($row->DETAIL_REQUEST_STATUS_CODE=='APPROVED_BY_TNPPE') ? "disabled" : "";
				 // $dis2 = ($row->DETAIL_REQUEST_STATUS_CODE=='REJECTED_BY_TNPPE') ? "disabled" : "";
				 // $det1 = '<a  class="btn btn-sm btn-info bg-color-green '.$dis.'" onClick="approv_anak('.$row->DETAIL_REQUEST_ID . ')""> <i class="glyphicon"></i>Approved</a>';
				 // $det2 = '<a  class="btn btn-sm btn-info bg-color-red '.$dis2.'" onClick="reject_anak('.$row->DETAIL_REQUEST_ID . ')""> <i class="glyphicon"></i>Rejected</a>';
			     // $det =$det1.' '. $det2; 
				 // $table.="<tr>
				 // <td style='width:10px;'>".$no."</td>
				 // <td>".$row->NOMOR_BA."</td>
				 // <td>".$row->TANGGAL_BA."</td>
				 // <td>".$row->SERVICE_TRANSACTION_CODE."</td>
				 // <td>".$row->DETAIL_REQUEST_STATUS_NAME."</td>
				 // <td>".$det."</td>
				 // </tr>";
		}
		$table.= '</tbody>
				</table> ';
		$result['data'] = $table;
	    echo json_encode($result);
	}
	 */
	function updatedata()
    {
		$this->load->model('Activitylog_model','activitylog');
        $result = array();
        $result['res'] = '0';
       
        $Activity = $this->input->post('Activity');
        $shift = $this->input->post('shift');
        $tgl = $this->input->post('tgl');
        $event = $this->input->post('event');
        $nama = $this->input->post('nama');
        $id = $this->input->post('idlog');
		
		$this->db->trans_start();
        
		 $datac = array(    
		   'shift' => $shift,
		   'event' => $event,
		   'tanggal' => $tgl,
		   'activity' => $Activity,
		   'nama'  => $nama
		 );
		
		 $this->activitylog->update2($datac, $id); 
		
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $result;
           
		}
		echo json_encode($result); 
	}	
	 
	function insertdata()
	{
		$this->load->model('Activitylog_model','activitylog');
        $result = array();
        $result['res'] = '0';
        $data = $this->input->post('data');
        $Activity = $this->input->post('Activity');
        $shift = $this->input->post('shift');
        $event = $this->input->post('event');
        $tgl = $this->input->post('tgl');
		$data  = json_decode($data, true);
		
         $this->db->trans_start();
         //$insert_id = $this->db->insert_id();
		 $fal = "";
		 foreach($data AS $in=>$f)
		 {
			 
			 $fal.=$f['nama'].", ";
			 //$datad = array(    
			 // 'id_log_activity' => $insert_id,
			 // 'nama' => $f['nama'],
			 // 'note' => ''
			 //);
			 //$this->activitylog->insert3($datad);
		 }
		
		 $datac = array(    
			   'shift' => $shift,
			   'event' => $event,
			   'tanggal' => $tgl,
			   'activity' => $Activity,
			   'nama' => $fal
		 );
		$f = $this->activitylog->insert2($datac);
		
		//echo json_encode($f);
		
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $result;
           
		}
		echo json_encode($result); 
	}
	
	public function del()
	{
			$ss = $this->uri->segment(3);
			$this->db->delete('log_activity', array('id_log_activity' => $ss));
			$this->session->set_flashdata('message', 'Data  berhasil dihapus!');
			redirect('log_activity');
	}
	
	public function editData()
	{
		
		$this->load->model('Activitylog_model','activitylog');
        $result = array();
        $result['res'] = '0';
        $id = $this->input->post('id');
        
		
        $this->db->trans_start();
        
		 
		$ss =  $this->activitylog->GetByID($id);
		 
		
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
             $this->db->trans_commit();
			 $result['res'] = '1';
             $result['data'] = $ss;
           
		}
		echo json_encode($result); 
	}

	function exportData()
	{
	     $this->load->model('Activitylog_model','activitylog');
	     $data['data'] = $this->activitylog->getDataLogKehadiran();
         $this->load->view('excel/excelLogKehadiran', $data); 
	}
   

}
