<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Klien extends MY_Controller {

    protected $id_klien;
   
    protected $_isCreate;
    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
	    $this->load->helper('thumb');
    }

  
	public function index() {
        if (!is_authorized('klien', 'index'))
            access_denied();

        $data['title'] = title().' | Klien';
        $data['message'] = $this->session->flashdata('message');
		
        $content = 'klien/content';
        $this->_load_layout($content, $data);
    }
	 public function data()
    {
        if (!is_authorized('klien', 'index'))
            access_denied();
        $this->load->model('Klien_model','klien');
              
        $this->load->library('querydata');
        $requestData= $_REQUEST;
        $columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
            0 => 'a.nama_klien',
            1 => 'a.link',
            2 => 'a.gbr_klien',
            3 => 'a.keterangan',
            4 => 'a.aktif'
           
         ); 
 
        $defaultOrder =  'created_date desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
        $sql = $this->klien->getSql();
    
        $query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
        $data = array();
        if($requestData['start']==0)
        {
            $no =1;
        }
        else
        {
            $no = $requestData['start'] + 1;
        }           
        
        foreach($query[0] As $row)
        {
             $nestedData=array(); 
             $nestedData[] = 	 $no;
             $nestedData[] =  	 $row['nama_klien'];
             $nestedData[] =     $row['link'];
            
             $nestedData[] =     '<center><img src='.base_url().'assets/images/img_klien/kecil_'.$row["gbr_klien"].' width=50></center>';  
			 $nestedData[] =  	 $row['keterangan'];
			 $nestedData[] =  	 $row['aktif'];
			 $aksi = '<a  class="btn btn-sm btn-info" href="'.base_url_admin().'/klien/update/'.$row['id_klien'].'"> <i class="glyphicon glyphicon-pencil"></i>Edit</a>';
		     $aksi2 = '<a  class="btn btn-sm btn-danger"   href="'.base_url_admin().'/klien/delete/'.$row['id_klien'].'"><i class="glyphicon glyphicon-trash"></i>Delete</a>';
             $nestedData[] = $aksi.' '.$aksi2;   
             $data[] = $nestedData;
             $no++;
        }
        $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $query[1] ),  
                    "recordsFiltered" => intval( $query[2] ), 
                    "data"            => $data
                    );
        echo json_encode($json_data);  
        
    }
	
	  public function create() {
       if (!is_authorized('klien', 'create'))
            access_denied();
        $this->load->model('Klien_model','klien');
        $id_klien = null;
        $nama_klien = null;
        $gbr_klien = null;
        $keterangan = null;
        $aktif = null;
        $link = null;
       
       
       
        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Klien' => siteAdminUrl('klien'),
            'Tambah klien' => siteAdminUrl('klien/create'));

        if ($this->input->post('Create')) {
            $this->_isCreate = true;
            $nama_klien = $this->input->post('nama_klien');
            $keterangan = $this->input->post('keterangan');
            $link = $this->input->post('link');
            $aktif = $this->input->post('aktif');
            $this->form_validation->set_rules($this->myRules());
			//if (empty($_FILES['gambar']['name']))
			//{
			//	$this->form_validation->set_rules('gambar', 'Logo', 'required');
			//}
			
			
			
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
                
				$arr = array();
                $lokasi_file    = $_FILES['gambar']['tmp_name'];
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['gambar']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadKlien($nama_file_unik,$_FILES);
					$arr = array(
					   'gbr_klien' => $nama_file_unik
					 );
					
				}	
				$ssss1 = array(
                     'nama_klien' => $nama_klien,
                     'link' => $link,
                     'keterangan' => $keterangan,
                     'aktif' => $aktif
                   
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->klien->insert($ssss);
				$this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect(folderBack().'/klien');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Tambah Data Klien';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
        $data['id_klien'] = $id_klien;
        $data['gambar'] = $gbr_klien;

        $data['link'] = $link;
        $data['nama_klien'] = $nama_klien;
        $data['aktif'] = $aktif;
        $data['keterangan'] = $keterangan;
		$content = 'klien/create';
        $this->_load_layout($content, $data);
    }
	
	 public function update() {
       if (!is_authorized('klien', 'update'))
            access_denied();
        $this->load->model('Klien_model','klien');   
        //$this->load->helper('thumb');   
        
        $id_klien = null;
        $nama_klien = null;
        $keterangan = null;
        $aktif = null;
        $link = null;
       
       
		$id_klien=$this->uri->segment(4);
        $err = false;
        $this->_isCreate = false;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Klien' => siteAdminUrl('klien'),
            'Edit Klien' => siteAdminUrl('klien/update/'.$id_klien));

        if ($this->input->post('Create')) {
            $this->_isCreate = false;

            $id_klien = $this->input->post('id_klien');
            $nama_klien = $this->input->post('nama_klien');
            $keterangan = $this->input->post('keterangan');
            $aktif = $this->input->post('aktif');
            $link = $this->input->post('link');
            
          
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();
				
				$arr = array();
                $lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				if (!empty($lokasi_file)){
					$nama_file      = $_FILES['gambar']['name'];
					$acak           = rand(1,99);
					$nama_file_unik = $acak.$nama_file; 
					UploadKlien($nama_file_unik,$_FILES);
					
					$i      = $id_klien;
					$tabel  = 'klien';
					$arr = array(
					   'gbr_klien' => $nama_file_unik
					 );
					removeGambarKlien($i,$tabel);
				}	
				
				$ssss1 = array(
                    'nama_klien' => $nama_klien,
                    'link' => $link,
                    'keterangan' => $keterangan,
                    'aktif' => $aktif
                    
                );
				$ssss = array_merge($ssss1,$arr);
                $ss = $this->klien->update($ssss,$id_klien);
				
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Data baru berhasil di buat!');
                    redirect(folderBack().'/klien');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Edit Klien';
        $data['err'] = $err;
        $data['action'] = $this->_isCreate;
		$ssssa = $this->klien->getById($id_klien==null ?  $this->input->post('id_klien'):$id_klien);
        $data['id_klien'] =  $ssssa->id_klien;
        $data['nama_klien'] =  $ssssa->nama_klien;
        $data['keterangan'] =  $ssssa->keterangan;
        $data['link'] =  $ssssa->link;
        $data['aktif'] =  $ssssa->aktif;
        $data['gambar'] = $ssssa->gbr_klien;
		$content = 'klien/create';
        $this->_load_layout($content, $data);
    }
	
	
	
	public function delete() {
       if (!is_authorized('klien', 'delete'))
		access_denied();
		$this->load->model('Klien_model','klien');
		$ss = $this->uri->segment(4);
		$r = $this->klien->delete($ss);
		removeGambarKlien($ss,"klien");
		$this->session->set_flashdata('message', 'Data  berhasil dihapus!');
	    redirect(folderBack().'/klien/index');
					
	}
    public function myRules() {
        return $this->_fields = array(
            array(
                'field' => 'nama_klien',
                'label' => 'Nama',
                'rules' => 'required|trim' 
            )
			
        );
    }
}


