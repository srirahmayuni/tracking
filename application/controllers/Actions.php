<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Actions extends MY_Controller {

    protected $_fields; 
    public function __construct() {
        parent::__construct();

        $this->load->model('Aksi_model', 'actions');
        $this->load->library(array('form_validation', 'pagination'));
    }

    public function index() {
        if (!is_authorized('actions', 'index'))
            access_denied();

        $data['title'] = title().' | Aksi';
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Aksi' => siteAdminUrl('actions'));

        $data['message'] = $this->session->flashdata('message');

        $content = 'actions/content';
        $this->_load_layout($content, $data);
    }

    public function getData() {
        if (!is_authorized('actions', 'index'))
            access_denied();
        $table = 'actions';
        $primaryKey = 'action_id';
        $columns = array(
            array('db' => 'action_id', 'dt' => 0, 'field' => 'action_id'), //nomor
            array('db' => 'action_id', 'dt' => 1, 'field' => 'action_id'),
            array('db' => 'action', 'dt' => 2, 'field' => 'action')
        );
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        require( 'ssp.php' );
        $joinQuery = "";
        //$extraWhere = "`u`.`salary` >= 90000";        
        $extraWhere = "";
        $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere);
        $start = $_GET['start'];
        $res = array();
        $i = $start;
        foreach ($result['data'] as $sss) {
            $crd = '<a class="btn btn-sm btn-info" href=' . base_url() . 'admin12345/actions/update/' . $sss[1] . '>
			           <i class="glyphicon glyphicon-pencil"></i>Edit</a> ';
            $crd.= '<a class="btn btn-sm btn-danger" onClick="Delete(' . $sss[1] . ',\'' . $sss[2] . '\')"><i class="glyphicon glyphicon-trash">
												</i>Delete</a>';
            array_push($res, array('0' => $i + 1, '1' => $sss[2], '2' => $crd));
            $i++;
        }
        $result['data'] = $res;
        echo json_encode($result);
    }

    public function create() {
        if (!is_authorized('actions', 'create'))
            access_denied();

        $action_id = null;
        $aksi = null;

        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Aksi' => siteAdminUrl('users'),
            'Tambah Aksi' => siteAdminUrl('actions/create'));

        if ($this->input->post('Create')) {


            // jika dia bukan superadmin, cek table keterkaitanya (reseller/ coordinator)
            $action_id = NULL;
            if ($this->session->userdata('action_id'))
                $action_id = $this->session->userdata('action_id');
            $this->_isCreate = true;
            $aksi = $this->input->get_post('aksi');


            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();

                $ssss = array(
                    'action' => $aksi
                );

                $users_id = $this->actions->insert($ssss);


                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Halaman gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Halaman dengan nama ' . $aksi . ' berhasil di buat!');
                    redirect('admin12345/actions');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = 'CariTruk | Tambah Aksi';
        $data['err'] = $err;

        $data['action'] = $this->_isCreate;
        $data['action_id'] = $action_id;
        $data['aksi'] = $aksi;


        $content = 'actions/create';
        $this->_load_layout($content, $data);
    }

    public function update() {
        if (!is_authorized('actions', 'update'))
            access_denied();

        $aksi = null;
        //$dataUsers = new stdClass();
        $this->_isCreate = false;
        $err = false;
        $action_id = $this->uri->segment(4);

        if ($this->input->post('Create')) {

            $this->_isCreate = false;
            $name = $this->input->get_post('aksi');
            $action_id = $this->input->get_post('action_id');
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $updateDataUsers2 = array(
                    'action' => $name,
                );
                $this->db->trans_start();
                $updateUsers = $this->actions->update($updateDataUsers2, $action_id);

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Group gagal di update, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();

                    $this->session->set_flashdata('message', 'Group dengan nama ' . $name . ' berhasil di update!');
                    redirect('admin12345/actions');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }
        // getData
        $dataUsers = $this->actions->getName($action_id);

        //print_r($dataUsers );
        $adminGroupFirstName = $dataUsers;

        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Aksi' => siteAdminUrl('actions'),
            'Update Aksi ' . $adminGroupFirstName => siteAdminUrl('actions/update/' . $action_id));


        $data['action_id'] = $action_id;
        $data['aksi'] = $adminGroupFirstName;
        $data['action'] = $this->_isCreate;
        $data['err'] = $err;
        $data['title'] = 'CariTruk | Update Aksi';

        $content = 'actions/create';
        $this->_load_layout($content, $data);
    }

    public function delete() {
        if (!is_authorized('actions', 'delete'))
            access_denied();
        $result = new stdClass();
        $result->response = FALSE;
        $action_id = $this->input->post('action_id');

        //if($action_id)
        //$action_id = $this->actions->getGroupName($action_id);
        $query = "select * from menu where action_id=" . $action_id . "";
        $fr = $this->db->query($query);

        $query2 = "select * from privilege_action where action_id=" . $action_id . "";
        $fr2 = $this->db->query($query2);

        $cekHaveData = $fr->num_rows();
        $cekHaveData2 = $fr2->num_rows();
        //$cekHaveData = $this->whois->loud($action_id);
        if (($cekHaveData + $cekHaveData2) == 0) {
            $this->db->trans_start();
            $this->actions->delete($action_id);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                //if something went wrong, rollback everything
                $this->db->trans_rollback();
            } else {
                //if everything went right, commit the data to the database
                $this->db->trans_commit();
                $result->response = TRUE;
                $this->result($result);
            }
        } else
            $this->result($result);
    }

    public function myRules() {

        return $this->_fields = array(
            array(
                'field' => 'aksi',
                'label' => 'Nama',
                'rules' => 'required|trim')
        );
    }

}
