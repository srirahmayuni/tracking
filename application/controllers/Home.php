<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	 
    public function __construct() {
        parent::__construct();
        $this->load->helper('fungsidate');
        $this->load->helper('download');
        $this->load->model('Galeri_model','gal');
        
    }

    public function index() {
         //if (!is_authorized('home', 'index'))
         //   access_denied();
		 $this->load->model('Home_model','home');
		
         $data['title'] = title().'| Beranda';
         $data['po'] = $this->home->_countStatistik();
         $data['cls_home']      = 'class="active"';
         $data['cls_dashboard'] = '';	
		 $data['cls_activity_progress'] = '';
		 $data['cls_reporting'] = '';
		 $data['cls_log_activity'] = '';
         $data['breadcrumb'] = array('Beranda' => base_url() .'/home');
		 $q = "select a.* from tbl_files a where  a.publish='YA'";
		 $q2 = "select a.* from tbl_ebook a where  a.publish='YA'";
		 $data['gallery'] = $this->gal->getDataFront();
		 $dd = $this->db->query($q)->result();
		 $data['data'] = $dd;
         
		 
		 $dd2 = $this->db->query($q2)->result();
		 $data['dataku'] = $dd2; 

		 $content = 'home/content2';
         $this->_load_layout($content, $data);
      
    }
	function unduh(){
		$id=$this->uri->segment(3);
		$get_db = $this->db->query("SELECT file_id,file_judul,file_deskripsi,DATE_FORMAT(created_date,'%d/%m/%Y') AS tanggal,file_download,file_data FROM tbl_files WHERE file_id='$id'");
		$q=$get_db->row_array();
		$file=$q['file_data'];
		$path='./asset/files/'.$file;
		$data =  file_get_contents($path);
		$name = $file;
		force_download($name, $data); 
		redirect('home'); 
		
   }
   function unduh2(){
		$id=$this->uri->segment(3);
		$get_db = $this->db->query("SELECT ebook_id,file_judul,file_deskripsi,DATE_FORMAT(created_date,'%d/%m/%Y') AS tanggal,file_download,file_data FROM tbl_ebook WHERE ebook_id='$id'");
		$q=$get_db->row_array();
		$file=$q['file_data'];
		$path='./asset/ebook/'.$file;
		$data =  file_get_contents($path);
		$name = $file;
		force_download($name, $data); 
		redirect('home'); 
	}
}
