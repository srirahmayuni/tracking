<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {

    protected $_fields; // andy add

    public function __construct() {
        parent::__construct();


        $this->load->model('Page_model', 'pages');
        $this->load->library(array('form_validation', 'pagination'));
    }

    public function index() {
        if (!is_authorized('pages', 'index'))
            access_denied();

        $data['title'] = title().' | Halaman';
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Halaman' => siteAdminUrl('pages'));

        $data['message'] = $this->session->flashdata('message');

        $content = 'pages/content';
        $this->_load_layout($content, $data);
    }

    public function getData() {
        if (!is_authorized('pages', 'index'))
            access_denied();
        $table = 'pages';
        $primaryKey = 'page_id';
        $columns = array(
            array('db' => 'page_id', 'dt' => 0, 'field' => 'page_id'), //nomor
            array('db' => 'page_id', 'dt' => 1, 'field' => 'page_id'),
            array('db' => 'controller', 'dt' => 2, 'field' => 'controller')
        );
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        require( 'ssp.php' );
        $joinQuery = "";
        //$extraWhere = "`u`.`salary` >= 90000";        
        $extraWhere = "";
        $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere);
        $start = $_GET['start'];
        $res = array();
        $i = $start;
        foreach ($result['data'] as $sss) {
            $crd = '<a class="btn btn-sm btn-info" href=' . base_url_admin() . '/pages/update/' . $sss[1] . '>
			           <i class="glyphicon glyphicon-pencil"></i>Edit</a> ';
            $crd.= '<a class="btn btn-sm btn-danger" onClick="Delete(' . $sss[1] . ',\'' . $sss[2] . '\')"><i class="glyphicon glyphicon-trash">
												</i>Delete</a>';
            array_push($res, array('0' => $i + 1, '1' => $sss[2], '2' => $crd));
            $i++;
        }
        $result['data'] = $res;
        echo json_encode($result);
    }

    public function create() {
        if (!is_authorized('pages', 'create'))
            access_denied();

        $page_id = null;
        $page = null;

        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Halaman' => siteAdminUrl('users'),
            'Tambah Halaman' => siteAdminUrl('pages/create'));

        if ($this->input->post('Create')) {
            
            $page_id = NULL;
            if ($this->session->userdata('page_id'))
                $page_id = $this->session->userdata('page_id');
            $this->_isCreate = true;
            $page = $this->input->get_post('page');


            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();

                $ssss = array(
                    'controller' => $page
                );

                $users_id = $this->pages->insert($ssss);


                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Halaman gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Halaman dengan nama ' . strtoupper($page) . ' berhasil di buat!');
                    redirect(folderBack().'/pages');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = title().' | Tambah Halaman';
        $data['err'] = $err;

        $data['action'] = $this->_isCreate;
        $data['page_id'] = $page_id;
        $data['page'] = $page;


        $content = 'pages/create';
        $this->_load_layout($content, $data);
    }

    public function update() {
        if (!is_authorized('pages', 'update'))
            access_denied();

        $adminGroupFirstName = null;
        //$dataUsers = new stdClass();
        $this->_isCreate = false;
        $err = false;
        $page_id = $this->uri->segment(4);

        if ($this->input->post('Create')) {

            $this->_isCreate = false;
            $name = $this->input->get_post('page');
            $page_id = $this->input->get_post('page_id');
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $updateDataUsers2 = array(
                    'controller' => $name,
                );
                $this->db->trans_start();
                $updateUsers = $this->pages->update($updateDataUsers2, $page_id);

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Group gagal di update, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();

                    $this->session->set_flashdata('message', 'Group dengan nama ' . strtoupper($name) . ' berhasil di update!');
                    redirect(folderBack().'/pages');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }
        // getData
        $dataUsers = $this->pages->getName($page_id);
        //print_r($dataUsers );
        $adminGroupFirstName = $dataUsers;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Halaman' => siteAdminUrl('pages'),
            'Update Halaman ' . $adminGroupFirstName => siteAdminUrl('pages/update/' . $page_id));


        $data['page_id'] = $page_id;
        $data['page'] = $adminGroupFirstName;
        $data['action'] = $this->_isCreate;
        $data['err'] = $err;
        $data['title'] = 'CariTruk | Update Halaman';

        $content = 'pages/create';
        $this->_load_layout($content, $data);
    }

    public function delete() {
        if (!is_authorized('pages', 'delete'))
            access_denied();
        $result = new stdClass();
        $result->response = FALSE;
        $page_id = $this->input->post('page_id');

        $query = "select * from menu where page_id=" . $page_id;
        $fr = $this->db->query($query);
        $cekHaveData = $fr->num_rows();


        $query2 = "select * from privilege_page where page_id=" . $page_id;
        $fr2 = $this->db->query($query2);
        $cekHaveData2 = $fr2->num_rows();

        $query3 = "select * from privilege_action where page_id=" . $page_id;
        $fr3 = $this->db->query($query3);
        $cekHaveData3 = $fr3->num_rows();


        //$cekHaveData = $this->whois->loud($page_id);
        if (($cekHaveData + $cekHaveData2 + $cekHaveData3) == 0) {
            $this->db->trans_start();
            $this->pages->delete($page_id);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                //if something went wrong, rollback everything
                $this->db->trans_rollback();
            } else {
                //if everything went right, commit the data to the database
                $this->db->trans_commit();
                $result->response = TRUE;
                $this->result($result);
            }
        } else
            $this->result($result);
    }

    public function myRules() {

        return $this->_fields = array(
            array(
                'field' => 'page',
                'label' => 'Nama',
                'rules' => 'required|trim')
        );
    }

}
