<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends MY_Controller {

    protected $_fields; 

    public function __construct() {
        parent::__construct();

        $this->load->model('Groups_model', 'groups');
        $this->load->library(array('form_validation', 'pagination'));
    }

    public function index() {
        if (!is_authorized('groups', 'index'))
            access_denied();

        $data['title'] = title().' | Group';
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Groups' => siteAdminUrl('groups'));
      
        $data['message'] = $this->session->flashdata('message');

        $content = 'groups/content';
        $this->_load_layout($content, $data);
    }

    public function getData() {
        if (!is_authorized('groups', 'index'))
            access_denied();
        $table = 'groups';
        $primaryKey = 'group_id';
        $columns = array(
            array('db' => 'group_id', 'dt' => 0, 'field' => 'group_id'), //nomor
            array('db' => 'group_id', 'dt' => 1, 'field' => 'group_id'),
            array('db' => 'name', 'dt' => 2, 'field' => 'name')
        );
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        require( 'ssp.php' );
        $joinQuery = "";
        //$extraWhere = "`u`.`salary` >= 90000";        
        $extraWhere = "";
        $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere);
        $start = $_GET['start'];
        $res = array();
        $i = $start;
        foreach ($result['data'] as $sss) {
			$crd = '<a class="btn btn-sm btn-info" onClick="privilege(' . $sss[1] . ',\'' . $sss[2] . '\')"><i class="glyphicon"></i>Privilege</a> ';
            $crd.= '<a class="btn btn-sm btn-info" href=' . base_url() . 'set/groups/update/' . $sss[1] . '>
			           <i class="glyphicon glyphicon-pencil"></i>Edit</a> ';
            $crd.= '<a class="btn btn-sm btn-danger" onClick="Delete(' . $sss[1] . ',\'' . $sss[2] . '\')"><i class="glyphicon glyphicon-trash">
												</i>Delete</a>';
            array_push($res, array('0' => $i + 1, '1' => $sss[2], '2' => $crd));
            $i++;
        }
        $result['data'] = $res;
        echo json_encode($result);
    }
    
	public function  dataHalaman()
	{
		$this->load->model('Page_model','page');
		$this->load->library('querydata');
		$requestData= $_REQUEST;
		$columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
			0 => 'zz.controller', 
			1 => 'zz.name',
			2 => 'zz.allow'
		 ); 
	
		$defaultOrder =  'zz.allow desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
		//$sql = $this->page->getSql('', $_REQUEST['status'], $coordinator_id);
		$sql = $this->page->getSql($_REQUEST); 
		$groupId = $_REQUEST['gr_id'];
		$query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
		$data = array();
		if($requestData['start']==0)
		{
			$no =1;
		}
        else
        {
			$no = $requestData['start'] + 1;
		}			
			
		foreach($query[0] As $row)
		{
			 $nestedData=array(); 
			 $nestedData[] = $no;
			 $nestedData[] = $row['page_id'];
			 $nestedData[] = $row['controller'];
			 $c = ($row['allow'] == 1) ? "checked" : "";			 
			 $dev=' <div class="checkbox" >
		<label><input '.$c.'  type="checkbox" id="ac'.$row['page_id'].'"  onclick="comboChange(\''.$row['privilege_page_id'].'\',\''.$row['page_id'].'\',\''.$groupId.'\')"> 
                        </label><span id="load'.$row['page_id'].'"></span>
					</div>'; 
			 $aksi= '<a class="btn btn-sm btn-danger" onClick="privAksi(\''.$row['page_id'].'\',\''.$groupId.'\')"><i class="glyphicon">
												</i>Privilege Actions</a>';	
			 $nestedData[] = $dev;	
			 $nestedData[] = $aksi;
		     $data[] = $nestedData;
			$no++;
		}
		
		
		$json_data = array(
					"draw"            => intval( $requestData['draw'] ),   
					"recordsTotal"    => intval( $query[1] ),  
					"recordsFiltered" => intval( $query[2] ), 
					"data"            => $data
					);

		echo json_encode($json_data);  // send data as json format
	}
	public function  dataActions()
	{
		$this->load->model('Aksi_model','aksi');
		$this->load->library('querydata');
		$requestData= $_REQUEST;
		$columnOrderBy = array( //sesuiakan dengan select di query dan urutan di datatable
			0 => 'zz.action', 
			1 => 'zz.allow'
		 ); 
	
		$defaultOrder =  'zz.allow desc'; /* bila tidak di isi maka default order mengacu kepada array index 0 $columnOrderBy  */
		$sql = $this->aksi->getSql($_REQUEST); 
		$groupId = $_REQUEST['gr_id'];
		$id_page = $_REQUEST['id_page'];
		$query =  $this->querydata->data($requestData,$sql,$columnOrderBy,$defaultOrder);
		$data = array();
		if($requestData['start']==0)
		{
			$no =1;
		}
        else
        {
			$no = $requestData['start'] + 1;
		}			
		
		foreach($query[0] As $row)
		{
			 $nestedData=array(); 
			 $nestedData[] = $no;
			 $nestedData[] = $row['action_id'];
			 $nestedData[] = $row['action'];
			 $c = ($row['allow'] == 1) ? "checked" : "";			 
			 $dev=' <div class="checkbox" >
		<label><input '.$c.'  type="checkbox" id="ac'.$row['action_id'].'"  onclick="comboChange2(\''.$row['privilege_action_id'].'\',\''.$id_page.'\',\''.$groupId.'\',\''.$row['action_id'].'\')"> 
                        </label><span id="load'.$row['action_id'].'"></span>
					</div>'; 
			 //$aksi= '<a class="btn btn-sm btn-danger" onClick="privAksi(\''.$row['action_id'].'\',\''.$groupId.'\')"><i class="glyphicon">
			 //								</i>Privilege Actions</a>';	
			 $nestedData[] = $dev;	
			 //$nestedData[] = $aksi;
		     $data[] = $nestedData;
			$no++;
		}
		
		
		$json_data = array(
					"draw"            => intval( $requestData['draw'] ),   
					"recordsTotal"    => intval( $query[1] ),  
					"recordsFiltered" => intval( $query[2] ), 
					"data"            => $data
					);

		echo json_encode($json_data);  // send data as json format
	}
	
    public function create() {
        if (!is_authorized('groups', 'create'))
            access_denied();

        $group_id = null;
        $adminGroupFirstName = null;

        $err = false;
        $this->_isCreate = true;
        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Group' => siteAdminUrl('users'),
            'Tambah Group' => siteAdminUrl('groups/create'));

        if ($this->input->post('adminGroupCreate')) {
            $reseller_id = NULL;
            $coordinator_id = NULL;
            if ($this->session->userdata('reseller_id'))
                $reseller_id = $this->session->userdata('reseller_id');

            if ($this->session->userdata('coordinator_id'))
                $coordinator_id = $this->session->userdata('coordinator_id');

            // jika dia bukan superadmin, cek table keterkaitanya (reseller/ coordinator)
            $group_id = NULL;
            if ($this->session->userdata('group_id'))
                $group_id = $this->session->userdata('group_id');
            $this->_isCreate = true;
            $adminGroupFirstName = $this->input->get_post('adminGroupFirstName');


            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $this->db->trans_start();

                $dataUsers = array(
                    'name' => strtoupper($adminGroupFirstName)
                );

                $users_id = $this->groups->insert($dataUsers);


                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Group gagal di simpan, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();
                    $this->session->set_flashdata('message', 'Group dengan nama ' . strtoupper($adminGroupFirstName) . ' berhasil di buat!');
                    redirect('admin12345/groups');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }

        $data['title'] = 'CariTruk | Tambah Group';
        $data['err'] = $err;

        $data['action'] = $this->_isCreate;
        $data['group_id'] = $group_id;
        $data['adminGroupFirstName'] = $adminGroupFirstName;


        $content = 'groups/create';
        $this->_load_layout($content, $data);
    }

    public function update() {
        if (!is_authorized('groups', 'update'))
            access_denied();

        $adminGroupFirstName = null;
        //$dataUsers = new stdClass();
        $this->_isCreate = false;
        $err = false;
        $group_id = $this->uri->segment(4);

        if ($this->input->post('adminGroupCreate')) {

            $this->_isCreate = false;
            $name = $this->input->get_post('adminGroupFirstName');
            $group_id = $this->input->get_post('group_id');
            $this->form_validation->set_rules($this->myRules());
            if ($this->form_validation->run() !== false) {
                $updateDataUsers2 = array(
                    'name' => strtoupper($name),
                );
                $this->db->trans_start();
                $updateUsers = $this->groups->update($updateDataUsers2, $group_id);

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    //if something went wrong, rollback everything
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message', 'Data Group gagal di update, silahkan coba kembali!');
                } else {
                    //if everything went right, commit the data to the database
                    $this->db->trans_commit();

                    $this->session->set_flashdata('message', 'Group dengan nama ' . strtoupper($name) . ' berhasil di update!');
                    redirect('admin12345/groups');
                }
            } else {
                $err = true;
                $data['pesan_error'] = $this->form_validation->error_array();
            }
        }
        // getData
        $dataUsers = $this->groups->getGroupName($group_id);

        //print_r($dataUsers );
        $adminGroupFirstName = $dataUsers;

        $data['breadcrumb'] = array('Beranda' => siteAdminUrl(),
            'Master' => '#',
            'Group' => siteAdminUrl('groups'),
            'Update Group ' . $adminGroupFirstName => siteAdminUrl('groups/update/' . $group_id));


        $data['group_id'] = $group_id;
        $data['adminGroupFirstName'] = $adminGroupFirstName;
        $data['action'] = $this->_isCreate;
        $data['err'] = $err;
        $data['title'] = 'CariTruk | Update Group';

        $content = 'groups/create';
        $this->_load_layout($content, $data);
    }

    public function delete() {
        if (!is_authorized('groups', 'delete'))
            access_denied();
        $result = new stdClass();
        $result->response = FALSE;
        $group_id = $this->input->post('group_id');

        //if($group_id)
        //$group_id = $this->groups->getGroupName($group_id);
        $query = "select * from users where group_id=" . $group_id;
        $fr = $this->db->query($query);
        $cekHaveData = $fr->num_rows();


        $query2 = "select * from privilege_page where group_id=" . $group_id;
        $fr2 = $this->db->query($query2);
        $cekHaveData2 = $fr2->num_rows();

        $query3 = "select * from privilege_action where group_id=" . $group_id;
        $fr3 = $this->db->query($query3);
        $cekHaveData3 = $fr3->num_rows();



        if (($cekHaveData + $cekHaveData2 + $cekHaveData3) == 0) {
            $this->db->trans_start();
            $this->groups->delete($group_id);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                //if something went wrong, rollback everything
                $this->db->trans_rollback();
            } else {
                //if everything went right, commit the data to the database
                $this->db->trans_commit();
                $result->response = TRUE;
                $this->result($result);
            }
        } else
            $this->result($result);
    }

    public function myRules() {

        return $this->_fields = array(
            array(
                'field' => 'adminGroupFirstName',
                'label' => 'Nama',
                'rules' => 'required|trim')
        );
    }
    function privilege() {
        if (!is_authorized('groups', 'index'))
            access_denied();
        
		$this->load->model('Accesspage_model', 'akespage');
		
		$pr_id = $this->input->post('pr_id');
        $p_id = $this->input->post('p_id');
        $g_id = $this->input->post('g_id');
        $allow = $this->input->post('allow');
		if(empty($pr_id)) //insert
		{	
			$in = '1';
			$ssss = array(
				'group_id' => $g_id,
				'page_id' => $p_id,
				'allow' => $allow
			);
			$sss = $this->akespage->insert($ssss);
		}
		else
		{
			$in = '0';
			$ssss = array(
				//'group_id' => $g_id,
				//'page_id' => $p_id,
				'allow' => $allow
			);
			 $sss = $this->akespage->update($ssss, $pr_id);
		}	
     
        if ($sss) {
			
			if($in == '1')
				 $data['psn'] = "Berhasil Diaktifkan";
			else 
               	  $data['psn'] = "Berhasil non aktifkan";		 	
			 
            $data['st'] = '1';
           
            echo json_encode($data);
        } else {
            $data['st'] = '0';
            $data['psn'] = "Gagal";
            echo json_encode($data);
        } 
    }
	function privilege2() {
        if (!is_authorized('groups', 'index'))
            access_denied();
		$this->load->model('Accessaction_model', 'wa');
		$pr_id = $this->input->post('pr_id');
        $p_id = $this->input->post('p_id');
        $g_id = $this->input->post('g_id');
        $allow = $this->input->post('allow');
        $action_id = $this->input->post('action_id');
		if(empty($pr_id)) //insert
		{	
			$in = '1';
			$ssss = array(
				'group_id' => $g_id,
				'page_id' => $p_id,
				'allow' => $allow,
				'action_id' => $action_id
			);
			$sss = $this->wa->insert($ssss);
		}
		else
		{
			$in = '0';
			$ssss = array(
				//'group_id' => $g_id,
				//'page_id' => $p_id,
				'allow' => $allow
			);
			 $sss = $this->wa->update($ssss, $pr_id);
		}	
        if ($sss) {
			
			if($in == '1')
				 $data['psn'] = "Berhasil Diaktifkan";
			else 
               	  $data['psn'] = "Berhasil non aktifkan";		 	
			 
            $data['st'] = '1';
           
            echo json_encode($data);
        } else {
            $data['st'] = '0';
            $data['psn'] = "Gagal";
            echo json_encode($data);
        } 
    }
    function createPage()
	{
		   if (!is_authorized('pages', 'create'))
            access_denied();
		      
              $this->load->model('Page_model', 'pages');
              $page = $this->input->post('page');
			  
			  $d=$this->db->query("select * from pages where controller='$page'");
			  $ss = $d->num_rows();
              if($ss > 0)
              {
				    $data['st'] = '0';
					$data['psn'] = "Nama Page sudah ada !! silahkan rubah";
					echo json_encode($data);
			  }
			  else
			  {	  
				    $ssss = array(
							'controller' => $page
						);
		 
					   $id = $this->pages->insert($ssss);
					   if ($id) {
					    $data['psn'] = "Berhasil Ditambahkan"; 
						$data['st'] = '1';
					   
						echo json_encode($data);
					} else {
						$data['st'] = '0';
						$data['psn'] = "Gagal ditambahkan";
						echo json_encode($data);
					}
			  }	
    }
	function createAction()
	{
		   //if (!is_authorized('actions', 'create'))
           // access_denied();
		      
              $this->load->model('Aksi_model', 'actions');
              $action = $this->input->post('action');
			  
			  $d=$this->db->query("select * from actions where action='$action'");
			  $ss = $d->num_rows();
              if($ss > 0)
              {
				    $data['st'] = '0';
					$data['psn'] = "Nama Action sudah ada !! silahkan rubah";
					echo json_encode($data);
			  }
			  else
			  {	  
				    $ssss = array(
							'action' => $action
						);
		 
					   $id = $this->actions->insert($ssss);
					   if ($id) {
					    $data['psn'] = "Berhasil Ditambahkan"; 
						$data['st'] = '1';
					   
						echo json_encode($data);
					} else {
						$data['st'] = '0';
						$data['psn'] = "Gagal ditambahkan";
						echo json_encode($data);
					}
			  }	
    }
	function updatePage()
	{
		   if (!is_authorized('pages', 'update'))
            access_denied();
		      
              $this->load->model('Page_model', 'pages');
              $page = $this->input->post('page');
              $id_page = $this->input->post('id_page');
			  
			  
			  $d=$this->db->query("select * from pages where controller='$page' and page_id<>'$id_page'");
			  $ss = $d->num_rows();
              if($ss > 0)
			  {
						$data['st'] = '0';
						$data['psn'] = "Nama Page sudah ada !! silahkan rubah";
						echo json_encode($data);
			  }
              else
              {				  
			  		    $ssss = array(
							'controller' => $page
						);
					    $id = $this->pages->update($ssss,$id_page);
					    if ($id) {
						$data['psn'] = "Berhasil diubah"; 
						$data['st'] = '1';
					   
						echo json_encode($data);
					   } else {
						$data['st'] = '0';
						$data['psn'] = "Gagal diubah";
						echo json_encode($data);
					   }
			  }
	}
	function updateAction()
	{
		  // if (!is_authorized('actions', 'update'))
         //   access_denied();
		      
			  $this->load->model('Aksi_model', 'actions');
              $action = $this->input->post('action');
              $action_id = $this->input->post('action_id');
			  
			  
			  $d=$this->db->query("select * from actions where action='$action' and action_id<>'$action_id'");
			  $ss = $d->num_rows();
              if($ss > 0)
			  {
						$data['st'] = '0';
						$data['psn'] = "Nama Action sudah ada !! silahkan rubah";
						echo json_encode($data);
			  }
              else
              {				  
			  		    $ssss = array(
							'action' => $action
						);
					    $id = $this->actions->update($ssss,$action_id);
					    if ($id) {
						$data['psn'] = "Berhasil diubah"; 
						$data['st'] = '1';
					   
						echo json_encode($data);
					   } else {
						$data['st'] = '0';
						$data['psn'] = "Gagal diubah";
						echo json_encode($data);
					   }
			  }
	}
	function deletePage()
	{
		 if (!is_authorized('pages', 'delete'))
            access_denied();
		      
              $this->load->model('Page_model', 'pages');
              $id_page = $this->input->post('id_page');
			  $d=$this->db->query("select * from privilege_page where  page_id='$id_page'");
			  $ss = $d->num_rows();
              if($ss > 0)
			  {
						$data['st'] = '0';
						$data['psn'] = "Page ini tak dapat dihapus";
						echo json_encode($data);
			  }
              else
              {			
					    $id = $this->pages->delete($id_page);
						$data['psn'] = "Berhasil dihapus"; 
						$data['st'] = '1';
						echo json_encode($data);
					  
			  }
	}
	function deleteAction()
	{
			//if (!is_authorized('actions', 'delete'))
			//	access_denied();
		      
              $this->load->model('Aksi_model', 'actions');
              $id_action = $this->input->post('id_action'); 
			  $d=$this->db->query("select * from privilege_action where  action_id='$id_action'");
			  $ss = $d->num_rows();
              if($ss > 0)
			  {
						$data['st'] = '0';
						$data['psn'] = "Page ini tak dapat dihapus";
						echo json_encode($data);
			  }
              else
              {			
					    $id = $this->actions->delete($id_action);
						$data['psn'] = "Berhasil dihapus"; 
						$data['st'] = '1';
						echo json_encode($data);
					  
			  }
	}
}
