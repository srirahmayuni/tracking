<!DOCTYPE html>
<html lang="en">
 
<head>
    <meta charset="utf-8">
    <title>Login - TELKOMSEL SIAGA</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
	<link href="<?php echo base_url()?>asset/temp/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>asset/temp/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

	<link href="<?php echo base_url()?>asset/temp/css/font-awesome.css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
		
	<link href="<?php echo base_url()?>asset/temp/css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url()?>asset/temp/css/pages/signin.css" rel="stylesheet" type="text/css">
	
	<!-- #FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url('asset/temp/img/favicon.ico'); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('asset/temp/img/favicon.ico'); ?>" type="image/x-icon">
   <style>
  .brand>img {
    height: 40px;
	width : 100px;
    margin-top: -10px;
    margin-left: 5px;
  }
   </style>
</head>

<body>
<div class="navbar navbar-fixed-top">
	<div>
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<!-- <a class="brand" href="index.html">
				<img src="<?php echo base_url()?>asset/logo.png"  />				
			</a>		 -->
			<div class="nav-collapse">
<!-- 
				<ul class="nav pull-left">
					<div class="mh-100" style="width: 175px; height: 75px;">
					<img style="height:100%; width:175px;" src="<?php echo base_url()?>asset/logo.png" />
					</div>
				</ul> -->
  
				<ul class="nav pull-right">
					<!--<li class="">						
						<a href="signup.html" class="">
							Don't have an account?
						</a>
					</li>
					<li class="">						
						<a href="index.html" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
					</li>-->
				</ul>
			</div><!--/.nav-collapse -->	
		</div> <!-- /container -->
	  </div> <!-- /navbar-inner -->
    </div> <!-- /navbar -->
	<?php echo $body; ?>
	<script src="<?php echo base_url()?>asset/temp/js/jquery-1.7.2.min.js"></script>
	<script src="<?php echo base_url()?>asset/temp/js/bootstrap.js"></script>
	<script src="<?php echo base_url()?>asset/temp/js/signin.js"></script>
</body>
</html>
