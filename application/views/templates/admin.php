<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Dashboard - Telkomsel Siaga</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<link href="<?php echo base_url()?>asset/temp/css/bootstrap.min.css" rel="stylesheet"> 
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
-->
<link href="<?php echo base_url()?>asset/temp/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="<?php echo base_url()?>asset/temp/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url()?>asset/temp/css/style.css" rel="stylesheet">

<link href="<?php echo base_url()?>asset/temp/css/pages/dashboard.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- Placed at the end of the document so the pages load faster --> 
<!--
<script src="<?php echo base_url()?>asset/temp/js/jquery-3.3.1.js"></script> 
-->
<!-- #FAVICONS -->
<link rel="shortcut icon" href="<?php echo base_url('asset/temp/img/favicon.ico'); ?>" type="image/x-icon">
<link rel="icon" href="<?php echo base_url('asset/temp/img/favicon.ico'); ?>" type="image/x-icon">

<script src="<?php echo base_url()?>asset/temp/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo base_url()?>asset/temp/js/excanvas.min.js"></script> 
<script src="<?php echo base_url()?>asset/temp/js/chart.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url()?>asset/temp/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url()?>asset/temp/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="<?php echo base_url()?>asset/temp/js/base.js"></script> 	


<link href="<?php echo base_url()?>asset/temp/css/jquery.dataTables.min.css" rel="stylesheet">

<!--
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
-->
<style>
 tfoot {
    display: table-header-group;
}




.brand>img {
    height: 40px;
	width : 100px;
    margin-top: -10px;
    margin-left: 5px;
  }

</style>



<script src="<?php echo base_url()?>asset/temp/js/jquery.dataTables.min.js"></script>
<!--
<script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script> 
	-->
	
</head>
<body> 
<div class="navbar navbar-fixed-top">

<div class="navbar-inner">

    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a>

        <div class="nav-collapse"> 

        <ul class="nav pull-left">
          <div class="mh-100" style="width: 120px; height: 65px;">
            <img style="height:100%; width:120px;" src="<?php echo base_url()?>asset/logo.png" />
          </div>
        </ul>

        <ul class="nav pull-right" style="padding-top:15px">
          <?php 
			   if ($this->session->userdata('group_id')==1 or $this->session->userdata('group_id')==2)
			   {	   
			  ?>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> <?php echo $this->session->userdata('first_name'); ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url()?>Users/index">User Manajemen</a></li>
              <li><a href="<?php echo base_url()?>document/index">Document</a></li>
              <li><a href="<?php echo base_url()?>ebook/index">Ebook</a></li>  
              <li><a href="<?php echo base_url()?>gallery/index">Gallery Slide</a></li>
            </ul>
          </li>
		   <?php  
			   }
			   else
			   {	   
			  ?>
			  
		    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> <?php echo $this->session->userdata('first_name'); ?> </a>
            </li>	  
		    
             <?php  
			   }
			     
			  ?>
			  
			  <li class="dropdown"><a href="<?php echo base_url()?>myauth/logout" ><i
                            class="icon-signout"></i> Logout</a></li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container"> 
      <ul class="mainnav">  
        <li <?php echo $cls_home;?>><a href="<?php echo base_url()?>home"><i class="icon-home"></i><span>Home</span> </a> </li>

        <?php if( $this->session->userdata("group_id") == "1" or $this->session->userdata("group_id") == "2") { ?>        
          <li <?php echo $cls_dashboard;?>><a href="<?php echo base_url()?>dashboard"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
          <li <?php echo $cls_activity_progress;?>><a href="<?php echo base_url()?>activity_progress"><i class="icon-list-alt"></i><span>Activity Progress</span> </a> </li>
          <li <?php echo $cls_reporting;?>><a href="<?php echo base_url()?>reporting"><i class="icon-list-alt"></i><span>Reporting</span> </a></li>
          <li <?php echo $cls_log_activity;?>><a href="<?php echo base_url()?>log_activity"><i class="icon-list"></i><span>Log Activity</span> </a></li>
        <?php } ?>
		<?php 
		    $gr = $this->session->userdata("group_id");
		    $in_ar = array(3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49);
			if (in_array($gr, $in_ar)) 
			{
		?>
		    <li <?php echo $cls_dashboard;?>><a href="<?php echo base_url()?>dashboard"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li <?php echo $cls_activity_progress;?>><a href="<?php echo base_url()?>activity_progress"><i class="icon-list-alt"></i><span>Activity Progress</span> </a> </li>
        <li <?php echo $cls_reporting;?>><a href="<?php echo base_url()?>reporting"><i class="icon-list-alt"></i><span>Reporting</span> </a></li>
        <?php } ?> 
      </ul> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>

<!---- -->
<?php echo $body; ?>

<div class="extra">
  <div class="extra-inner">
   
    <!-- /container --> 
  </div>
  <!-- /extra-inner --> 
</div>

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2019 <a href="#">Telkomsel Siaga</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<!-- Le javascript
================================================== --> 

<script>     

        var lineChartData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
				{
				    fillColor: "rgba(220,220,220,0.5)",
				    strokeColor: "rgba(220,220,220,1)",
				    pointColor: "rgba(220,220,220,1)",
				    pointStrokeColor: "#fff",
				    data: [65, 59, 90, 81, 56, 55, 40]
				},
				{
				    fillColor: "rgba(151,187,205,0.5)",
				    strokeColor: "rgba(151,187,205,1)",
				    pointColor: "rgba(151,187,205,1)",
				    pointStrokeColor: "#fff",
				    data: [28, 48, 40, 19, 96, 27, 100]
				}
			]

        }

        var myLine = new Chart(document.getElementById("area-chart").getContext("2d")).Line(lineChartData);


        var barChartData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
				{
				    fillColor: "rgba(220,220,220,0.5)",
				    strokeColor: "rgba(220,220,220,1)",
				    data: [65, 59, 90, 81, 56, 55, 40]
				},
				{
				    fillColor: "rgba(151,187,205,0.5)",
				    strokeColor: "rgba(151,187,205,1)",
				    data: [28, 48, 40, 19, 96, 27, 100]
				}
			]

        }    

        $(document).ready(function() {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var calendar = $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          selectable: true,
          selectHelper: true,
          select: function(start, end, allDay) {
            var title = prompt('Event Title:');
            if (title) {
              calendar.fullCalendar('renderEvent',
                {
                  title: title,
                  start: start,
                  end: end,
                  allDay: allDay
                },
                true // make the event "stick"
              );
            }
            calendar.fullCalendar('unselect');
          },
          editable: true,
          events: [
            {
              title: 'All Day Event',
              start: new Date(y, m, 1)
            },
            {
              title: 'Long Event',
              start: new Date(y, m, d+5),
              end: new Date(y, m, d+7)
            },
            {
              id: 999,
              title: 'Repeating Event',
              start: new Date(y, m, d-3, 16, 0),
              allDay: false
            },
            {
              id: 999,
              title: 'Repeating Event',
              start: new Date(y, m, d+4, 16, 0),
              allDay: false
            },
            {
              title: 'Meeting',
              start: new Date(y, m, d, 10, 30),
              allDay: false
            },
            {
              title: 'Lunch',
              start: new Date(y, m, d, 12, 0),
              end: new Date(y, m, d, 14, 0),
              allDay: false
            },
            {
              title: 'Birthday Party',
              start: new Date(y, m, d+1, 19, 0),
              end: new Date(y, m, d+1, 22, 30),
              allDay: false
            },
            {
              title: 'EGrappler.com',
              start: new Date(y, m, 28),
              end: new Date(y, m, 29),
              url: 'http://EGrappler.com/'
            }
          ]
        });
      });
    </script><!-- /Calendar -->
</body>
</html>

