<!DOCTYPE html>
<?php
$a_menu = null;
isset($active_menu) ? $a_menu = $active_menu : $a_menu = '';
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $title; ?></title>

        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>/assets/ico/icon.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/ico/favicon.ico">

        <!-- CSS Global -->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/owl-carousel2/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/owl-carousel2/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/animate/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/swiper/css/swiper.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="<?php echo base_url(); ?>assets/css/theme.css" rel="stylesheet">

        <!-- Head Libs -->
        <script src="<?php echo base_url(); ?>assets/plugins/modernizr.custom.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <!--[if lt IE 9]>
        <script src="assets/plugins/iesupport/html5shiv.js"></script>
        <script src="assets/plugins/iesupport/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="home" class="wide">
        <!-- PRELOADER -->
        <div id="preloader">
            <div id="preloader-status">
                <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>
                </div>
                <div id="preloader-title">Loading</div>
            </div>
        </div>
        <!-- /PRELOADER -->

        <!-- WRAPPER -->
        <div class="wrapper">

            <!-- HEADER -->
            <header class="header fixed">
                <div class="header-wrapper">
                    <div class="container">

                        <!-- Logo -->
                        <div class="logo">
                            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Cari-Truk-new.png" alt="caritruk.com"/></a>
                        </div>
                        <!-- /Logo -->

                        <!-- Mobile menu toggle button -->
                        <a href="#" class="menu-toggle btn ripple-effect btn-theme-transparent"><i class="fa fa-bars"></i></a>
                        <!-- /Mobile menu toggle button -->

                        <!-- Navigation -->
                        <nav class="navigation closed clearfix">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <!-- navigation menu -->
                                    <a href="#" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>

                                    <ul class="nav sf-menu">
                                        <li class="<?php echo ($a_menu == 'home') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>">Beranda</a></li>
                                        <?php
                                        if (!$this->session->userdata('logged_in')) {
                                            ?>		
                                            <li class="<?php echo ($a_menu == 'login') ? 'active' : ''; ?>"><a href="<?php echo site_url('login'); ?>">Login/Register</a></li>
                                            <?php
                                        }
                                        ?>
                                        <li class="<?php echo ($a_menu == 'partner') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>">Partner</a></li>
                                        <!-- jika keranjang terisi, tampilkan-->
                                        <li class="">
                                            <a href="<?php echo site_url('shopping') ?>" title="Pesanan Anda">
                                                <span class="label label-success" style="margin:-5px 20px 10px -5px;position: fixed;z-index: 1;">
                                                    <?php echo count($this->cart->contents()); ?>
                                                </span> 
                                                <img src="<?php echo base_url(); ?>assets/img/white-truck-48.png" alt="Keranjang belanja" style="
                                                     /* Safari, Chrome */
                                                     -webkit-transform: rotate(0deg);
                                                     /* Mozilla */
                                                     -moz-transform: rotate(0deg);
                                                     /* Opera */
                                                     -o-transform: rotate(0deg);
                                                     /* KDE Browser */
                                                     -khtml-transform: rotate(0deg);
                                                     /* IE9 */
                                                     -ms-transform:rotate(0deg);
                                                     margin:-5px 40px 0px 5px;
                                                     height:25px;"
                                                     >
                                            </a>
                                        </li>
                                        <!-- Informasi login -->
                                        <?php
                                        if ($this->session->userdata('logged_in')) {
                                            $first_name = $this->session->userdata('first_name');
                                            ?>
                                            <!--li class="active">
                                                    <a href="<?php echo site_url('login/logout') ?>">
                                                    <span class="glyphicon glyphicon-user"></span> <?php echo $id; ?>
                                                    </a>
                                            </li-->
                                            <li class="active">
                                                <a href="#">
                                                    <span class="glyphicon glyphicon-user"></span> <?php echo $first_name; ?>
                                                </a>
                                                <ul>
                                                    <li><a href="<?php echo site_url('profile/index'); ?>">Profile</a></li>
                                                    <li><a href="<?php echo site_url('order/index'); ?>">Orders</a></li>
                                                    <li><a href="<?php echo site_url('login/logout') ?>">Logout</a></li>
                                                </ul>
                                            </li>

                                            <?php
                                        }
                                        ?>
                                        <!-- Informasi login -->
                                        <li>	
                                            <!--
                                                    margin:10px 5px 15px 20px;
                                                                    top margin is 10px
                                                                    right margin is 5px
                                                                    bottom margin is 15px
                                                                    left margin is 20px
                                            -->
                                            <section class="form-group text text-inline" style="margin-top:12px;margin-bottom:4px;margin-left:40px;">
                                                <?php echo form_open(site_url('shipment/ShipmentSearchMap'), array('method' => 'get', 'autocomplete' => 'off')); ?>
                                                <input title="Name is required" data-toggle="tooltip" style="margin-right:-62px;width: 53%; display: inline; height:32px;"
                                                       class="form-control" type="text" name="shipmentNoShipment" placeholder="ID Shipment" value="<?php echo isset($isianNoShipment) ? $isianNoShipment : '' ?>">
                                                <button type="submit" style="display: inline; height:32px;" name="shipmentBtnTrack" value="Track" class="btn btn-submit btn-theme pull-right btn-theme-dark">Track</button>
                                                <?php echo form_close(); ?>
                                            </section>
                                        </li>

                                        <!--div class="input-group" style="margin:8px 40px -100px 0px;width: 29%;position:fixed">
<input type="text" class="form-control"  placeholder="Search for...">
<span class="input-group-btn">
<button class="btn btn-default" type="button">Go!</button>
</span>
</div-->
                                    </ul>

                                    <!-- /navigation menu -->
                                </div>
                            </div>
                            <!-- Add Scroll Bar -->
                            <div class="swiper-scrollbar"></div>
                        </nav>
                        <!-- /Navigation -->

                    </div>
                </div>

            </header>
            <!-- /HEADER -->

            <!-- CONTENT AREA -->
            <?php echo $body; ?>
            <!-- CONTENT AREA -->
            <!-- FOOTER -->
            <footer class="footer">
                <div class="footer-widgets">
                    <div class="container">
                        <div class="row">

                            <div class="col-md-3">
                                <div class="widget">
                                    <h4 class="widget-title">Tentang Kami</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <ul class="social-icons">
                                        <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget">
                                    <h4 class="widget-title">Berlangganan</h4>
                                    <p>Dapatkan promo langsung</p>
                                    <form action="#">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Masukan email"/>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-theme">Langganan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget widget-categories">
                                    <h4 class="widget-title">Informasi</h4>
                                    <ul>
                                        <li><a style="color:#fff"  href="#">About Us</a></li>
                                        <li><a style="color:#fff"  href="#">Delivery Information</a></li>
                                        <li><a style="color:#fff"  href="#">Contact Us</a></li>
                                        <li><a style="color:#fff"  href="#">Terms and Conditions</a></li>
                                        <li><a style="color:#fff"  href="#">Private Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget widget-tag-cloud">
                                    <h4 class="widget-title">Partner Pembayaran</h4>
                                    <ul>
                                        <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/Visa.png" style="width:64px;"></a></li>
                                        <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/Mastercard.png" style="width:64px;"></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="footer-meta">
                    <div class="container">
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="copyright">&copy; 2015 Rent It — An One Page Rental Car Theme made with passion by jThemes Studio</div>
                            </div>

                        </div>
                    </div>
                </div>
            </footer>
            <!-- /FOOTER -->


            <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>

        </div>
        <!-- /WRAPPER -->

        <!-- JS Global -->
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/superfish/js/superfish.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/owl-carousel2/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery.sticky.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery.easing.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery.smoothscroll.min.js"></script>
        <!--<script src="assets/plugins/smooth-scrollbar.min.js"></script>-->
        <!--<script src="assets/plugins/wow/wow.min.js"></script>-->
        <script>
            // WOW - animated content
            //new WOW().init();
        </script>
        <script src="<?php echo base_url(); ?>assets/plugins/swiper/js/swiper.jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/datetimepicker/js/moment-with-locales.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

        <!-- JS Page Level -->
        <script src="<?php echo base_url(); ?>assets/js/theme-ajax-mail.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/theme.js"></script>

    </body>
</html>