<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script> 
<script src="<?php echo base_url()?>tracking/asset/temp/js/dataTables.fixedHeader.min.js"></script> 
<script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script> 
<link href="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
<!--datetimepicker-->
<!--CSS ADD-->
<!--datetimepicker-->
<script src="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/build/jquery.datetimepicker.full.js"></script>

<style>
th, td {
    xwhite-space: nowrap;
	text-align:center;
}


.modal {
  text-align: center;
}

@media screen and (min-width: 900px) { 
  .modal:before {
    display: inline-block;
    vertical-align: middle;
    content: " ";
    height: 100%;
  }
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}


</style>


<div id="myModal_edit" class="modal hide fade modal-dialog modal-lg" _style="width:900px;" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Edit Activity</h3>
  </div>
  <div class="modal-body">
  <p class="overflow-auto">
	<form id="editAct">
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Owner program</label>
				<div class="controls">
				<input type="hidden" class="form-control span3"  id="id_activity_progress"  name="id_activity_progress" placeholder="Owner Program" />
				<input type="text" class="form-control span3"  id="owner_program"  name="owner_program" placeholder="Owner Program" />
				</div>		
			</div>  
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Group</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="group"  name="group" placeholder="Group" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Section </label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="section"  name="section" placeholder="Section" />
				</div>		
			</div> 
	     </div>
		 
	</div>	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Activity </label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="activity"  name="activity" placeholder="Activity" />
				</div>		
			</div> 
	     </div>
		  <div class="span3">
			<div class="control-group">											
				<label class="control-label">Detail activity</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="detail_activity"  name="detail_activity" placeholder="Detail Activity" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Regional</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="regional"  name="regional" placeholder="Regional" />
				</div>		
			</div> 
	     </div>
	</div>	

	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Supporting Program</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="	supporting_program"  name="	supporting_program" placeholder="Supporting program" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Nama poi</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="nama_poi"  name="nama_poi" placeholder="Nama poi" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Divisi in charge</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="divisi_in_charge"  name="divisi_in_charge" placeholder="Divisi in charge" />
				</div>		
			</div> 
	     </div>
		 
	</div>	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">PIC</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="pic"  name="pic" placeholder="PIC" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Pic support</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="pic_support"  name="pic_support" placeholder="Pic support" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Target week</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="target_week"  name="target_week" placeholder="Target week" />
				</div>		
			</div> 
	     </div>
	</div>	
	
	
	
	
	
	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Progress</label>
				<div class="controls">
				<!-- <input type="text" class="form-control span3"  id="progress"  name="progress" placeholder="progress" /> -->
					
				<div class="controls">
					 <select class="form-control select2"  name = "progress" id ="progress">
						<option value="">  -- Select -- </option>
						<option value="Open">  Open </option>
						<option value="Closed"> Closed  </option>
					 </select> 	
																 
				</div>	
					
											
				
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Finish week</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="finish_week"  name="finish_week" placeholder="Finish week" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Finish Date</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="finish_date"  name="finish_date" placeholder="Finish Date" />
				</div>		
			</div> 
	     </div>
	</div>	
	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Millestone progress</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="millestone_progress"  name="millestone_progress" placeholder="Millestone progress" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Blocking point</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="blocking_point"  name="blocking_point" placeholder="Blocking point" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Class Category</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="class_category"  name="class_category" placeholder="Class category" />
				</div>		
			</div> 
	     </div>
	</div>	
	<div class="row">	
	 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Keterangan</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="keterangan"  name="keterangan" placeholder="Keterangan" />
				</div>		
			</div> 
	     </div>
	</div>	
	</form>
  </p>	  
  </div>
  <div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary" id="save_change2">Save </button>
  </div>
</div>




<div class="container">
	<div class="row">
		<div class="span12">
				
				<div class="error-actions">
					
					    <a href="#" class="btn btn-info btn-lg" onclick="export1();">
							<span class="glyphicon glyphicon-export"></span> Export	
						</a>
						<?php
						if ($this->session->userdata('group_id')==1)
			            {
						?>
						<a href="#" class="btn btn-info btn-lg" onclick="export2();">
							<span class="glyphicon glyphicon-export"></span> Report Log 
						</a>



						<a href="#myModal" role="button" class="btn btn-info btn-lg" data-toggle="modal">BC Actvity Progress</a>
							
						<!-- Modal -->
						<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h3 id="myModalLabel">INFORMATION</h3>
							</div>
							<div class="modal-body" id="myInput">

							<?php 

							$date_now = date('Y-m-d');
							$date_now_his = date('d F Y');
							$date_now_hiss = date('H:i');

							$query_week = $this->db->query("SELECT week from date_week where stardate <= '$date_now' and enddate >= '$date_now'")->result_array();

							$query_dir = $this->db->query("SELECT a.directorate,
										COALESCE(COUNT(*), 0) AS jumlah_activity,
										COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
										FROM activity_progress a
											LEFT JOIN 
											( 
												SELECT directorate, COUNT(*) AS jumlah_activity_done FROM activity_progress
												WHERE progress = 'Closed'
												GROUP BY directorate
											) b ON a.directorate = b.directorate 	 
										GROUP BY a.directorate ORDER BY a.directorate DESC")->result_array();	

							$query_network = $this->db->query("SELECT a.owner_program,
							COALESCE(COUNT(*), 0) AS jumlah_activity,
							COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
							FROM activity_progress a	
								LEFT JOIN  
								( 
									SELECT owner_program, COUNT(*) AS jumlah_activity_done FROM activity_progress
									WHERE progress = 'Closed'
									GROUP BY owner_program
								) b ON a.owner_program = b.owner_program  
							WHERE a.owner_program LIKE 'NEP %' OR a.owner_program LIKE 'NOQM %'
							GROUP BY a.owner_program
							ORDER BY a.owner_program ASC")->result_array();

							$query_network2 = $this->db->query("SELECT a.owner_program,
							COALESCE(COUNT(*), 0) AS jumlah_activity,
							COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
							FROM activity_progress a	
								LEFT JOIN  
								( 
									SELECT owner_program, COUNT(*) AS jumlah_activity_done FROM activity_progress
									WHERE progress = 'Closed'
									GROUP BY owner_program
								) b ON a.owner_program = b.owner_program  
							WHERE a.owner_program NOT LIKE 'IT %' AND a.owner_program NOT LIKE 'Internal %'
							GROUP BY a.owner_program
							ORDER BY a.owner_program ASC")->result_array();

							$query_section = $this->db->query("SELECT a.section,
										COALESCE(COUNT(*), 0) AS jumlah_activity,
										COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
										FROM activity_progress a
											LEFT JOIN  
											( 
												SELECT section, COUNT(*) AS jumlah_activity_done FROM activity_progress
												WHERE progress = 'Closed'
												GROUP BY section
											) b ON a.section = b.section
										WHERE a.section NOT LIKE 'Corporate%' AND a.section NOT LIKE 'Information%'
										GROUP BY a.section")->result_array();

							$query_section2 = $this->db->query("SELECT a.section,
							COALESCE(COUNT(*), 0) AS jumlah_activity,
							COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
							FROM activity_progress a
								LEFT JOIN  
								( 
									SELECT section, COUNT(*) AS jumlah_activity_done FROM activity_progress
									WHERE progress = 'Closed'
									GROUP BY section
								) b ON a.section = b.section    
							GROUP BY a.section")->result_array();

							$overall = $this->db->query("SELECT SUM(jumlah_activity) AS total_activity, SUM(jumlah_activity_done) AS total_activity_done 
							FROM (
							SELECT COALESCE(COUNT(*), 0) AS jumlah_activity,
							COALESCE(b.jumlah_activity_done, 0) As jumlah_activity_done
							FROM activity_progress a
								LEFT JOIN 
								( 
									SELECT directorate, COUNT(*) AS jumlah_activity_done FROM activity_progress
									WHERE progress = 'Closed'
									GROUP BY directorate
								) b ON a.directorate = b.directorate 	 
							GROUP BY a.directorate ORDER BY a.directorate DESC ) c")->result_array();
							
							echo '<center><b><h4>Broadcast Format 1</h4></b></center><br>Dear Pak Iskri, Pak Pratig dan Para SL,<br>berikut progress activity RAFI 2020
							Week '.$query_week[0]['week'].' : <br>
							Tanggal '.$date_now_his. ' Jam '.$date_now_hiss.' WIB<br>'; 
					
							echo '<br>Overall / '.$overall[0]['total_activity'].'/'.$overall[0]['total_activity_done'].'/'.round((($overall[0]['total_activity_done']/$overall[0]['total_activity'])*100),2).'%<br><br>'; 

							$total_activity = 0;
							$total_activity_done = 0; 
							 
							$directorate_list = ['Network','Corporate Secretary','Information Technology','Sales','Marketing'];

							$directorate = [];
							foreach($query_dir as $item) {
								array_push($directorate, $item['directorate']);
							}
							
							$result_diff = array_diff($directorate_list, $directorate); 

							foreach($result_diff as $item) {
								array_push($query_dir, ['directorate' => $item, 'jumlah_activity'=>0,'jumlah_activity_done'=>0]);
							} 
 
							$average = 0;
							foreach($query_dir as $item) {
								$total_activity += $item['jumlah_activity'];
								$total_activity_done += $item['jumlah_activity_done'];
 
								$directorate_name = $item['directorate'];
								if($item['directorate'] == 'Information Technology') {
									$directorate_name = 'IT';
								} else if($item['directorate'] == 'Corporate Secretary') {
									$directorate_name = 'Corcom'; 
								}

								if($item['jumlah_activity_done'] != 0 && $item['jumlah_activity'] != 0) {
									$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
									echo $directorate_name.' : '.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].'/'.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
								} else {
									$average += 0;
									echo $directorate_name.' : '.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].' / 0%<br>';										
								} 
 
							}	
							
							echo '<br>*Network Activities*<br>Divisi/Total Activity/Done/%Done<br>';
							$total_activity_network = 0;
							$total_activity_network_done = 0; 
							$average = 0;  
							foreach($query_network as $item) {
								$total_activity_network += $item['jumlah_activity'];
								$total_activity_network_done += $item['jumlah_activity_done'];
								$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);

								if (strpos($item['owner_program'], 'NOQM Region 0') !== false) {
									$owner_program = str_replace("NOQM Region 0","R",$item['owner_program']);

								} else if (strpos($item['owner_program'], 'NOQM Region 10') !== false) {
									$owner_program = str_replace("NOQM Region ","R",$item['owner_program']);
							
								} else if (strpos($item['owner_program'], 'NOQM Region 11') !== false) {
									$owner_program = str_replace("NOQM Region ","R",$item['owner_program']);
							
								} else if (strpos($item['owner_program'], 'NEP Area ') !== false) {
									$owner_program = str_replace("NEP Area ","NEP_A",$item['owner_program']); 
							
								} else { 
									$owner_program = $item['owner_program'];
								}

								echo $owner_program.'/'.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].'/'.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
							} 
					
							echo '<br><b>*Activities based on network domain:*</b><br><br>';
							$total_activity_section = 0;
							$total_activity_section_done = 0;  
							$average = 0; 
							foreach($query_section as $item) {
								$total_activity_section += $item['jumlah_activity'];
								$total_activity_section_done += $item['jumlah_activity_done'];
								$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
								echo $item['section'].'/'.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].'/'.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
							}
							echo '<br>-TC RAFI 2020-<br><br>';

							echo '<br><br><center><b><h4>Boardcast Format 2</h4></b></center><br>
							RAFI 2020 Daily Activity Report<br>
							Week '.$query_week[0]['week'].'<br>
							Tanggal '.date('Y-m-d H:i').' wib<br><br>
							Format : [Direktorat/Total Activity/Done/%Done]<br><br> 
							'; 
							$average = 0;
							foreach($query_dir as $item) {
								$total_activity += $item['jumlah_activity'];
								$total_activity_done += $item['jumlah_activity_done'];
 
								$directorate_name = $item['directorate'];  

								if($item['jumlah_activity_done'] != 0 && $item['jumlah_activity'] != 0) {
									$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
									echo $directorate_name.'/'.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].'/'.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
								} else { 
									$average += 0;
									echo $directorate_name.'/'.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].'/0%<br>';										
								} 
							}
							echo '<br>Overall / '.$overall[0]['total_activity'].'/'.$overall[0]['total_activity_done'].'/'.round((($overall[0]['total_activity_done']/$overall[0]['total_activity'])*100),2).'%<br>'; 
 							
							echo '<br><b>*Network Activities*</b><br><br>';
							$total_activity_network = 0;
							$total_activity_network_done = 0; 
							$average = 0;  
							foreach($query_network2 as $item) {
								$total_activity_network += $item['jumlah_activity'];
								$total_activity_network_done += $item['jumlah_activity_done'];
								$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
 
								$owner_program = $item['owner_program'];  

								echo $owner_program.'/'.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].'/'.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
							} 
							
							echo '<br><b>*Activities Based On Domain*</b><br><br>';
							$total_activity_section = 0;
							$total_activity_section_done = 0;   
							$average = 0; 
							foreach($query_section2 as $item) {
								$total_activity_section += $item['jumlah_activity'];
								$total_activity_section_done += $item['jumlah_activity_done'];
								$average += round(($item['jumlah_activity_done'] / $item['jumlah_activity']) * 100, 2);
								echo $item['section'].'/'.$item['jumlah_activity'].'/'.$item['jumlah_activity_done'].'/'.round((($item['jumlah_activity_done']/$item['jumlah_activity'])*100),2).'%<br>';	
							} 
							echo '<br>-TC RAFI 2020-<br><br>';
							?> 

							</div>
							<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							<!-- <button class="btn btn-primary"  onclick="myFunction()">Save changes</button> -->
							</div>
						</div> 

						
						<?php
						}
						?>
						<a href="#" class="btn btn-info btn-lg" onclick="refresh();">
							Refresh		
						</a>
					
				</div> <!-- /error-actions -->
		</div> <!-- /span12 -->
	</div> <!-- /row -->
</div> <!-- /container -->
<br>


<div class="main">
  <div class="main-inner">
    <div class="container">
		<!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Data Activity Progress</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style="padding :10px;">
              <table id="example"  class="table table-striped table-bordered "  >
                <thead>
                  <tr> 
					<th> Action</th>
					<th> Progress</th>
					<th> Target Week</th>
                    <th> Activity</th>
					<th > Detail Activity</th>
					<th> Directorate</th>
					<th> Group</th>                     
                    <th> Owner Program</th>
                    <th> Section</th>
                    <th > Regional</th>
                    <th > Supporting Program</th>
                    <th> Nama POI</th>
                    <th> Divisi in charge</th>
                    <th> PIC</th>
                    <th> PIC Support</th>
                                      
                  </tr>
                </thead>
				
				
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
	</div>	
  </div>	
</div>	

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Change Progress</h3>
  </div>
  <div class="modal-body">
	   <p>
	   <div class="row">	
			<div class="col-md-12 ">
			   
				<form id="edit-profile" class="form-horizontal">
 
				<div class="control-group">				
					<label class="control-label">Select</label>
					<input type="hidden" id="kaprok_hidden">
					<div class="controls">
					<label class="radio inline">
					<input type="radio"  name="radiobtns" value="open" > Open
					</label>
					<label class="radio inline">
					<input type="radio" name="radiobtns"  value="Closed"> Closed
					</label>
					</div>			
				</div> 	
				<!-- <div class="control-group">											
					<label class="control-label">Millestone %</label>
					<div class="controls">
					<input type="text" class="form-control"  id="Millestone"  name="Millestone" placeholder="Millestone Progress" />
					</div>		
				</div>  -->
				</form>						
			</div>
		</div>
		</p>
  </div>
  <div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary" id="save_change">Save changes</button>
  </div>
</div>


<script>

$("#save_change").click(function(){
		   
		   AS = $("#kaprok_hidden").val();
		   mille = $("#Millestone").val();
		   
		   r = $('input[name=radiobtns]:checked').val();
		   approv2(AS,r,mille);
		   //$('#example').DataTable().ajax.reload();
	  });
$("#save_change2").click(function(){
		   
		   id_activity_progress = $("#id_activity_progress").val();
		  
		   update(id_activity_progress);
		   //$('#example').DataTable().ajax.reload();
	  });
$(document).ready(function() {
			
			
			 $('#finish_date').datetimepicker({
				//format: "Y-m-d h:m:s"
				format: "Y-m-d h:m:s"
				//format: "d-m-Y h:m:s"
		     }); 
			
			 $('#Millestone').on('input', function (event) { 
				this.value = this.value.replace(/[^0-9]/g, '');
			 });
			  $('#millestone_progress').on('input', function (event) { 
				this.value = this.value.replace(/[^0-9]/g, '');
			 });

			 $('#example thead tr').clone(true).appendTo( '#example thead' );
			 $('#example thead tr:eq(1) th').each( function (i) {
			 var title = $(this).text();
			 if( i == '0' )
			 {
					$(this).html( "" );
					//df = '<input style="width: 40px;"  type="text" placeholder="Search '+title+'" />';
					//$(this).html( df );
			 }
			 else
			 {
					df = '<input style="width: 80px;"  type="text" placeholder="'+title+'" />';						
					$(this).html( df );
			  }						
			});
			
				var otable = $('#example').removeAttr('width').DataTable({
				//"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				//		"t"+
				//		"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				"autoWidth" : false,
				//"oLanguage": {
				//	"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				//},
				// bAutoWidth: false, 
				  
				 //"fixedColumns": true,
				 "scrollY": 600,
                 "scrollX": true,
				 "processing": true,
				 "serverSide": true, 
				 "aoColumnDefs": [
						//{"aTargets" : [0],  "sClass":  "details-control"}, 
						//{ "bVisible": false, "aTargets": [ 1 ] },
						  {"aTargets": [0], "bSortable": false }, 
						  {"aTargets": [1], "bSortable": false,  },
						  {"aTargets": [2], "bSortable": false,   },
						  {"aTargets": [3], "bSortable": false,   },
						  {"aTargets": [4], "bSortable": false,   },
						  {"aTargets": [5], "bSortable": false,   },
						  {"aTargets": [6], "bSortable": false,   },
						  {"aTargets": [7], "bSortable": false,   },
						  {"aTargets": [8], "bSortable": false,   },
						  {"aTargets": [9], "bSortable": false,   },
						  {"aTargets": [10], "bSortable": false,   },
						  {"aTargets": [11], "bSortable": false,   },
						  {"aTargets": [12], "bSortable": false,   },
						  {"aTargets": [13], "bSortable": false,   },
						  {"aTargets": [14], "bSortable": false,   }
						 ],
			      select: {
						style:    'os',
						selector: 'td:first-child'
					},
				//order: [[ 1, 'asc' ]]			  
						 
				"ajax":{
					url :"<?php echo base_url(); ?>activity_progress/data", 
					type: "post",
					"data": function ( d ) {
								//d.active = $('#selec_filter').val();
							    //d.awal = $('#PROGRAM_START_S').val();
						        //d.akhir = $('#PROGRAM_END_S').val();
							    //d.filter = $('#hiden_text_search').val();
					},
					"dataSrc": function ( json ) {
						
						return json.data;
					},  
					error: function(){  // error handling
						
					},
				},	
				

				
			

		});	
				 otable.columns().every(function () {
					var table = this;
					$('input', this.header()).on('keyup change', function () {
						if (table.search() !== this.value) {
							   table.search(this.value).draw();
						}
					});
				});
				
			
	
} );

function approv(id,pr)
{
	$('#kaprok_hidden').val("");
	$('#Millestone').val("");
	$('#kaprok_hidden').val(id);
	if(pr=="open")
	{
		$( "input[value='open']" ).prop( "checked", true );
	}
    
	else if(pr=="Closed" || pr=="closed" || pr=="CLOSED"  )
    {
		$( "input[value='Closed']" ).prop( "checked", true );
	}
    else
    {
		$( "input[value='open']" ).prop( "checked", true );
	}		
	
	$('#myModal').modal({
		keyboard: true,
		backdrop: "static",
		show:true
	});  
   
}

function update(id)
{
	var input_data = new Object();

        input_data.id = id;
        input_data.data = $("#editAct").serializeArray();
        
				jQuery.ajax(
                {
                    url: "<?php echo base_url()?>activity_progress/update",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {

                        if (data.res == 0 )
                        {
                            alert("Gagal Merubah Progress");
                        } 
						else
                        {
                            alert("Merubah Progress Sukses");
						    $('#myModal_edit').modal('hide');
							$('#example').DataTable().ajax.reload();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}

function approv2(id,status,mille)
{
        var input_data = new Object();

        input_data.id = id;
        input_data.status = status;
        input_data.mille = mille;
        jQuery.ajax(
                {
                    url: "<?php echo base_url()?>activity_progress/detail_approv",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {

                        if (data.res == 0 )
                        {
                            // tidak bisa dihapus
                            alert("Gagal Merubah Progress");


                        } 
						else
                        {
                            alert("Merubah Progress Sukses");
						    $('#myModal').modal('hide');
							$('#example').DataTable().ajax.reload();
                           
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}

function edit(id)
{
		
		
		var input_data = new Object();
        input_data.id = id;
        
		jQuery.ajax(
		{
			url: "<?php echo base_url()?>activity_progress/editData",
			type: "POST",
			data: input_data,
			dataType: "json",
			success: function (data)
			{ 

				if ( data.res == 0 )
				{
					// tidak bisa dihapus
					alert("Simpan gagal ");
				} 
				else
				{
				    
					    $('#id_activity_progress').val(data.data.id_activity_progress);
					    $('#owner_program').val(data.data.owner_program);
					    $('#group').val(data.data.group);
					    $('#section').val(data.data.section);
					    $('#activity').val(data.data.activity);
					    $('#detail_activity').val(data.data.detail_activity);
					    $('#regional').val(data.data.regional);
					    $('#supporting_program').val(data.data.supporting_program);
					    $('#nama_poi').val(data.data.nama_poi);
					    $('#divisi_in_charge').val(data.data.divisi_in_charge);
					    $('#pic').val(data.data.pic);
					    $('#pic_support').val(data.data.pic_support);
					    $('#target_week').val(data.data.target_week);
					    $('#progress').val(data.data.progress);
					    $('#finish_week').val(data.data.finish_week);
					    $('#finish_date').val(data.data.finish_date);
					    $('#millestone_progress').val(data.data.millestone_progress);
					    $('#blocking_point').val(data.data.blocking_point);
					    $('#class_category').val(data.data.class_category);
					    $('#keterangan').val(data.data.keterangan);
						
						 $('#myModal_edit').modal({
						keyboard: true,
						backdrop: "static",
						show:true
					 });  
				}

			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus);
			}
		});
		
		
		
		
			
}
	 
function export1()
{
	window.open("<?php echo site_url('activity_progress/export_excel'); ?>");
}
function export2()
{
	window.open("<?php echo site_url('activity_progress/export_excel2'); ?>");
}
function bc_activity()
{
	window.open("<?php echo site_url('Activity_progress/bc_activity'); ?>");
} 
function refresh()
{
	 $('input').val("");	
	 $('#example').DataTable().ajax.reload();
	 location.reload();
}

</script>
			