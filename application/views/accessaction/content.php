
<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>	
</div>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">    
                    <?php
                    if (is_authorized('accessaction', 'create')) {
                        ?>
                        <button class="btn btn-primary" onClick="createPrivAksi()">Tambah</button>
                        <br>
                        <hr>
                        <br>
                        <?php
                    }
                    ?>	
                    <div class="col-xs-12 table-responsive">
                        <br />
                        <table id="table_aksesAction" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Group</th>
                                    <th>Page</th>
                                    <th>Action</th> 
                                    <th>Allow</th>
                                    <th style="width:170px;">Action</th> 
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>  
                    </div>	
                </div>
            </div>
           
        </div><!-- /.box-body -->

    </div>
</section>
<!-- /.content -->
<script type="text/javascript">
    var table;
    $(document).ready(function ()
    {
       
         var t1 = $('#table_aksesAction').DataTable({
		
		   "processing": true,
		   "serverSide": true, 
		     aoColumnDefs: [
						 
						{"aTargets": [5], "bSortable": false }
						],
			"ajax":{
				url :"<?php echo base_url_admin(); ?>/accessaction/data",
				type: "post"
			}
			
			
		});

    });

    function createPrivAksi()
    {
       
        url = "<?php echo site_url(folderBack().'/accessaction/create'); ?>";
        $(location).attr("href", url);
    }

    function Delete(users_id, first_name)
    {
        var result = confirm("Yakin hapus menus " + first_name + "?");
        if (result) {
            //Logic to delete the item
            Delete2(users_id, first_name);
        }
    }
    function Delete2(users_id, first_name)
    {
        var input_data = new Object();

        input_data.menu_id = users_id;
        jQuery.ajax(
                {
                    url: "<?php echo site_url(folderBack().'/menus/delete'); ?>",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {

                        if (data.response == false)
                        {
                            // tidak bisa dihapus
                            alert("Menu " + first_name + " tidak bisa di hapus!");


                        } else
                        {
                            alert("Menu " + first_name + " telah di hapus");
                            location.reload();

                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
    }

    function Update(id)
    {
        url = "<?php echo site_url(folderBack().'/menus/update'); ?>/" + id;
        $(location).attr("href", url);
    }
</script>
