<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'gallery/create';
} else {
    $urlnya = 'gallery/update';
}
?>

<div class="box-header with-border">
            
           
</div><!-- /.box-header -->


<div class="main">
  <div class="main-inner">
    <div class="container">
	 	
		<!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Form Data Gambar</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style="padding :10px;">
		
		<div class="content clearfix">
		<?php
         echo form_open_multipart(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
         echo form_hidden('id_gallery', $id_gallery);
        ?>
        <div class="box-body">
		
			<?php
            if ($has_err) {//echo $err;
               //echo '<div class="row">
				//			<div class="col-md-12">
					echo			'<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>';
				//			</div>
				//	  </div>';
            }
            ?>     
		
            <fieldset>
          
					<div class="control-group">
                        <label for="JudulGallery">Judul Gallery *</label>
						<div class="controls">
							 <input type="text"  class="form-control span6" placeholder="Nama"  name="judul_gallery" id="judul_gallery"  value="<?php echo set_value('judul_gallery', $judul_gallery) ?>">
              
						</div>
                    </div>
                    <div class="control-group">
                         <label class="control-label" for="keterangan_gambar">Keterangan </label>
                       	 <input type="text" class="form-control span6" placeholder="Nama"  name="keterangan" id="keterangan"  value="<?php echo set_value('keterangan', $keterangan) ?>">
              
                    </div>
					<div class="col-md-9">
						<div class="form-group">
							<label for="Favicon">Gambar  *</label>
							  <input id="gambar" type="file" name="gambar" accept="image/*" onChange="validate(this)" >
							<p class="help-block">Jpeg/jpg format.</p> 
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						<div class="widget-user-image">  <a href="#" id="pop1">
						  <img id="imageresource1" class="img-circle" height="60"  width="60" src="<?php echo base_url() ?>asset/images/img_galeri/<?php echo ($gambar == "") ? 'no-image-icon-6.png' : $gambar?>" alt="attachment image">
						  </a>
						</div>  
						</div>
					</div>
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="control-group">
                        
                        <?php
							  if ($aktif=='YA')
							  {
							   echo "
							   <p class=inline-small-label> 
							   <label for=field4>Publish</label>
							   <input type=radio name='aktif' value='YA' checked>Ya 
							   <input type=radio name='aktif' value='TIDAK'>Tidak
							   </p>";
							   }
																  
							   else
							   {
								   echo "
								   <p class=inline-small-label> 
								   <label for=field4>Publish</label>
								   <input type=radio name='aktif' value='YA'>Ya 
								   <input type=radio name='aktif' value='TIDAK' checked>Tidak
								   </p>";
							   }
									
						
						?>
                    </div>	
                    
                </div><!-- /.col -->

			  
			</fieldset>
			<div class="box-footer">
                <!--button type="submit" class="btn btn-primary pull-right" name="adminOrdersCreate" id="adminOrdersCreate" >Simpan</button-->
                Isian dengan tanda * adalah wajib.<br />
                <input type='reset' class="btn btn-primary pull-right" value="Reset">
                <input type="submit" class="btn btn-primary pull-left" name="Create" id="Create" value="Simpan">
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
       </div>
            <!-- /widget-content --> 
		</div>	
	</div>	
  </div>	
</div>	



<!-- /.content -->
<div class="modal fade" id="imagemodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Slide</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
                <img src="" id="imagepreview1" style="width: auto; height: auto; " >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

 $(document).ready(function () {
		
					
	$("#pop1").on("click", function () {
		$('#imagepreview1').attr('src', $('#imageresource1').attr('src'));
		$('#imagemodal1').modal('show');
	});
	
	 //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
 });


function validate(k) {

        size = k.files[0].size; 
		var ext = k.value.split(".");
		ext = ext[ext.length - 1].toLowerCase();
		var arrayExtensions = ["jpg", "jpeg","ico","png"];

		if (arrayExtensions.lastIndexOf(ext) == -1) {
			alert("Hanya boleh JPG atau JPEG.");
			$(k).val("");
		}

		if (size > 5242880)
		{
			alert("File tidak boleh melebihi 5 MB");
			$(k).val("");

		}
	}
</script>