<div class="login-box">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }

    if ($messageForgotSucc) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $messageForgotSucc;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>
    <div class="login-logo">
        &nbsp;
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <div class="login-logo">
            <img src="<?php echo base_url(); ?>assets/<?php echo $this->config->item('asset_folder_frontend')?>/img/logo-main.png" alt="logo">
        </div>
        <p class="login-box-msg">Lupa Password</p>
        <?php echo form_open('admin/myauth/forgot', array('class' => 'form-login')); ?>
        <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="adminMyauthForgotEmail" id="adminMyauthForgotEmail" />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>

        <div class="row">
            <div class="col-xs-8">
                <!--div class="checkbox icheck">
                  <label>
                    <input type="checkbox"> Remember Me
                  </label>
                </div-->
            </div><!-- /.col -->
            <div class="col-xs-4">
                <input type="submit" class="btn btn-primary btn-block btn-flat" name="adminMyauthForgot" id="adminMyauthForgot" value="Kirim">
            </div><!-- /.col -->
        </div>
        </form>

        <!--div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div--><!-- /.social-auth-links -->
        <br />
        <a href="<?php echo site_url('admin'); ?>">Cancel</a>
        <a href="<?php echo site_url() ?>" class="text-center" style="padding-left:62%">Beranda</a>
        <!--a href="register.html" class="text-center">Register a new membership</a-->

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->