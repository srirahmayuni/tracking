<!--<div class="row" >	
   <div class="span12" style=" 
    position:absolute;
    margin: auto;
    top: 50px;
    bottom: 0;
    left: 0;
    right: 0;">
	 
			<div class="control-group">
				<div class="controls">
					 <div class="alert">
					  <button type="button" class="close" data-dismiss="alert">×</button>
					  <h4>Warning!</h4>
					  Best check yo self, you're not...
					</div>
				</div> 
			 </div>
		
    </div>
</div>
 -->
 
 <p>
				<?php
					/* if ($message) {
						echo '<div class="row span12">
							<div class="col-md-12" >
							<div class="alert alert-danger alert-dismissible" role="alert">
							<span class="sr-only">Error:</span>
							<button type="button" class="close" data-dismiss="alert">
							×</button>';
						echo $message;
						echo '</div>
							</div>
							</div>';
					}
					if ($messageResetSuccess) {

						echo '<div class="row span12">
							<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
							<span class="sr-only">Error:</span>
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
						echo $messageResetSuccess;
						echo '</div>
							</div>
							</div>';
					} */
					?>
				</p>
							

<div class="account-container" style="margin:15%">
	
	<div class="content clearfix">
		
	<form action="<?php echo site_url('myauth/authentication'); ?>" method="post" class="form-login" id="login-form" >
		
			<h1>Login</h1>		
			
			<div class="login-fields">

			<h4 style="padding-top:-5px; margin-bottom:10px">Dashboard Activity RAFI 2020</h4>
			<!-- <h4 style="padding-top:-5px; margin-bottom:10px">Mohon maaf atas ketidaknyamanan saat ini, sedang maintance perbaikan data activity, silakan login beberapa menit lagi</h4> -->

				<div class="field">
					<label for="username">Email</label>
                    <input type="text" autocomplete="off" class="login username-field" placeholder="Email atau ID" name="adminLoginEmail" id="adminLoginEmail" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
                    <input type="password" class="login password-field" placeholder="Password" name="adminLoginPassword" id="adminLoginPassword" />
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					<label class="choice" for="Field">Keep me signed in</label>
				</span>
									
				<button class="button btn btn-success btn-large">Sign In</button>
				
			</div> <!-- .actions -->
			
			<?php
			
			 if ($message) 
			 {
				 
				?>
				 <div style="margin-top:80px;">
				
						<div class="controls">
							 <div class="alert">
							  <button type="button" class="close" data-dismiss="alert">×</button>
							  <h4>Warning!</h4>
							 <?php echo $message;?>
							</div>
						</div> 
				
				
				</div> 
			<?php 
			 }
			 //if ($messageResetSuccess) 
			 //{
			?>
				<!-- <div style="margin-top:10px;">
				
						<div class="controls">
							 <div class="alert">
							  <button type="button" class="close" data-dismiss="alert">×</button>
							  <h4>Warning!</h4>
							 <?php //echo $messageResetSuccess;?>
							</div>
						</div> 
				
				
				</div> -->
			
			<?php
			 //}
			?>
		</form>	
	
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->

<div class="login-extra">
	<!-- <a href="#">Reset Password</a> -->
</div> <!-- /login-extra -->
	
<style>
/* customizable snowflake styling */
.snowflake {
  color: #fff;
  font-size: 1em;
  font-family: Arial, sans-serif;
  text-shadow: 0 0 5px #000;
}

@-webkit-keyframes snowflakes-fall{0%{top:-10%}100%{top:100%}}@-webkit-keyframes snowflakes-shake{0%,100%{-webkit-transform:translateX(0);transform:translateX(0)}50%{-webkit-transform:translateX(80px);transform:translateX(80px)}}@keyframes snowflakes-fall{0%{top:-10%}100%{top:100%}}@keyframes snowflakes-shake{0%,100%{transform:translateX(0)}50%{transform:translateX(80px)}}.snowflake{position:fixed;top:-10%;z-index:9999;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:default;-webkit-animation-name:snowflakes-fall,snowflakes-shake;-webkit-animation-duration:10s,3s;-webkit-animation-timing-function:linear,ease-in-out;-webkit-animation-iteration-count:infinite,infinite;-webkit-animation-play-state:running,running;animation-name:snowflakes-fall,snowflakes-shake;animation-duration:10s,3s;animation-timing-function:linear,ease-in-out;animation-iteration-count:infinite,infinite;animation-play-state:running,running}.snowflake:nth-of-type(0){left:1%;-webkit-animation-delay:0s,0s;animation-delay:0s,0s}.snowflake:nth-of-type(1){left:10%;-webkit-animation-delay:1s,1s;animation-delay:1s,1s}.snowflake:nth-of-type(2){left:20%;-webkit-animation-delay:6s,.5s;animation-delay:6s,.5s}.snowflake:nth-of-type(3){left:30%;-webkit-animation-delay:4s,2s;animation-delay:4s,2s}.snowflake:nth-of-type(4){left:40%;-webkit-animation-delay:2s,2s;animation-delay:2s,2s}.snowflake:nth-of-type(5){left:50%;-webkit-animation-delay:8s,3s;animation-delay:8s,3s}.snowflake:nth-of-type(6){left:60%;-webkit-animation-delay:6s,2s;animation-delay:6s,2s}.snowflake:nth-of-type(7){left:70%;-webkit-animation-delay:2.5s,1s;animation-delay:2.5s,1s}.snowflake:nth-of-type(8){left:80%;-webkit-animation-delay:1s,0s;animation-delay:1s,0s}.snowflake:nth-of-type(9){left:90%;-webkit-animation-delay:3s,1.5s;animation-delay:3s,1.5s}.snowflake:nth-of-type(10){left:25%;-webkit-animation-delay:2s,0s;animation-delay:2s,0s}.snowflake:nth-of-type(11){left:65%;-webkit-animation-delay:4s,2.5s;animation-delay:4s,2.5s}
</style>

<div class="snowflakes" aria-hidden="true">
  <div class="snowflake">
  ❅
  </div> 
  <div class="snowflake">
  ❆
  </div>
  <div class="snowflake">
  ❅
  </div>
  <div class="snowflake">
  ❆
  </div>
  <div class="snowflake">
  ❅
  </div>
  <div class="snowflake">
  ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
</div>
