<div class="login-box">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    if ($messageResetErr) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $messageResetErr;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>
    <div class="login-logo">
        <a href="#"><b>CariTruk</b></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Reset Password</p>
        <?php
        echo form_open('admin/myauth/resetPassword', array('class' => 'form-login'));
        echo form_hidden('token', $token);
        ?>

        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password"  name="adminMyauthResetPassword" id="adminMyauthResetPassword">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Ulangi Password" name="adminMyauthResetPassword2" id="adminMyauthResetPassword2" />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label>
                <input type="checkbox" name="adminMyauthResetPasswordShowPass" id="adminMyauthResetPasswordShowPass" /> Tampilkan password
            </label>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-8">
                <!--div class="checkbox icheck">
                  <label>
                    <input type="checkbox"> Remember Me
                  </label>
                </div-->
            </div><!-- /.col -->
            <div class="col-xs-4">
                <input type="submit" class="btn btn-primary btn-block btn-flat" name="adminMyauthResetPasswordReset" id="adminMyauthResetPasswordReset" value="Simpan">
            </div><!-- /.col -->
        </div>
        </form>

        <!--div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div--><!-- /.social-auth-links -->

        <a href="<?php echo site_url('admin'); ?>">Login</a><br>
        <!--a href="register.html" class="text-center">Register a new membership</a-->

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>	
<script>
    $("#adminMyauthResetPasswordShowPass").on("click", function () {
        if (this.checked) {
            //Do stuff
            $('#adminMyauthResetPassword').prop('type', 'text');
            $('#adminMyauthResetPassword2').prop('type', 'text');
        } else
        {
            $('#adminMyauthResetPassword').prop('type', 'password');
            $('#adminMyauthResetPassword2').prop('type', 'password');
        }

    });
</script>	