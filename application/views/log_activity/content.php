<link rel="stylesheet" href="<?php echo base_url()?>assets/temp/plugins/iCheck/all.css">
<script src="<?php echo base_url()?>asset/temp/plugins/iCheck/icheck.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>asset/temp/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<!--<script src="<?php echo base_url()?>assets/admin/dist/js/app.min.js"></script>-->

<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url()?>asset/temp/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>


<link href="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
<!--datetimepicker-->
<!--CSS ADD-->
<!--datetimepicker-->
<script src="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/build/jquery.datetimepicker.full.js"></script>
<div class="container">
	<div class="row">
		<div class="span12">
				
				<div class="error-actions">
					<?php 
					if($this->session->userdata('group_id')==1)
					{	
					?>
					<a href="#myModal_" role="button" class="btn btn-large btn-primary" data-toggle="modal">
						&nbsp;
						Insert Kehadiran					
					</a>
					<a href="#" class="btn btn-success btn-large" onclick="exportdata();">
							<span class="glyphicon glyphicon-export"></span> Export	Data
						</a>
					<?php 
					}	 

					?>
					
				</div> <!-- /error-actions -->
		</div> <!-- /span12 -->
	</div> <!-- /row -->
</div> <!-- /container -->
<br>
<div class="main">
  <div class="main-inner">
    <div class="container">
	 	<?php
		if ($message) { 
		?>
		<div class="control-group">
		<label class="control-label"></label>
			<div class="controls">
			 <div class="alert lert alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Warning!</strong> <?php  echo $message; ?>
			</div>
			</div>
		</div>
		 <?php
		 }
		 ?>
		 
		<!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Log Activity</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style="padding :10px;">
              <table id="example2" class="table table-striped table-bordered" width="100%">
                <thead>
                   <tr>
                    <!--<th align="center"> Detail  </th>
                    <th>  </th> -->
                    <th> No </th>
                    <th width="100"> Name</th>
                    <th> Shift</th>
                    <th> Tanggal</th>
                    <th> Activity</th>
                    <th> Event</th>
                    <th> More</th>
                  </tr>
                </thead>
                <tbody>
				  
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
	</div>	
  </div>	
</div>	
<style>
	fieldset { 
  display: block;
  margin-left: 2px;
  margin-right: 2px;
  padding-top: 0.35em;
  padding-bottom: 0.625em;
  padding-left: 0.75em;
  padding-right: 0.75em;
  border: 2px groove (internal value);
  
}
legend {
	  margin-left: 20px;
	  display: block;
}
td.details-control {
    background: url('<?php echo base_url()?>asset/temp/img/plus.png') no-repeat center center;
    cursor: pointer;
}
tr.details td.details-control {
    background: url('<?php echo base_url()?>asset/temp/img/minus.png') no-repeat center center;
} 

</style> 
<!-- Modal -->
<div id="myModal_" class="modal hide fade modal-dialog modal-lg" style="width:750px;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Form Log Activity</h3>
  </div>
  <div class="modal-body">
	   <p>
	   <div class="row">	 
			<div class="col-md-12 ">
			   <form  class="form-horizontal" id="form_log_activty">
										<div class="control-group">											
											<label class="control-label" for="lastname">Jumlah PIC Stanby</label>
											<div class="controls">
											     <input type="number" id="pic_s" min="1" name="input_pic" data-bind="value:replyNumber" />
												 <input type="button" class="btn" id="gen_form" value="Generate Form" /> 
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<fieldset id="s_k" style="display:none;"> 
                                            <legend >Input Log Activity</legend>
                                            <div id="s_k2"></div>											
                                            <div class="control-group">										
												<label class="control-label" for="tgl">Tanggal</label>
												<div class="controls">
													 <input type="text" class="form-control" id="tgl" name="tgl" placeholder="Tanggal Masukan" />
													 
												</div>			 
											</div>
											<div class="control-group">											
												<label class="control-label" for="shift">Shift</label>
												<div class="controls">
													 <select class="form-control select2"  name = "shift" id ="shift">
														<!-- <option value="Shift 1 (00.00 - 08.00)">  Shift 1 (00.00 - 08.00) </option>
														<option value="Shift 2 (08.00 - 16.00)"> Shift 2 (08.00 - 16.00)  </option>
														<option value="Shift 3 (16.00 - 24.00)"> Shift 3 (16.00 - 24.00)  </option> -->
														 <option value="Shift 1">Shift 1</option>
														<option value="Shift 2">Shift 2</option>
														<option value="Shift 3">Shift 3</option>
													 </select> 								 
												</div>			
											</div> 
											<div class="control-group">											
												<label class="control-label" for="Event">Event</label>
												<div class="controls">
													 <select class="form-control select2"  name = "event" id ="event">
													   
													   <?php 
													   if($this->session->userdata('group_id') == 36 )
													   {
														  echo '<option value="Rafi 2019">  Rafi 2019 </option>';
													   }
                                                       else
                                                       {
														   
													   ?>
													 
														<!-- <option value="Rafi 2019">  Rafi 2019 </option> -->
														<option value="Naru 2019"> Naru 2019  </option>
														<!-- <option value="Pemilu 2019"> Pemilu 2019  </option> -->
														
														<?php
													    }
														?>
													 </select> 	
																								 
												</div>			
											</div> 
			 
											<div class="control-group">											
												<label class="control-label" for="Activity">Activity</label>
												<div class="controls">
													 <textarea cols="7"  id="Activity" name="Activity"></textarea>
												</div> 			
											</div>		
											
										</fieldset>	
			


				</form>						
			</div>
		</div>
		</p>
  </div>
  <div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary" id="save_change">Save </button>
  </div>
</div>



<div id="myModal_edit" class="modal hide fade modal-dialog modal-lg" style="width:750px;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Edit Log Activity</h3>
  </div>
  <div class="modal-body">
	   <p>
	   <div class="row">	
			<div class="col-md-12 ">
			   <form  class="form-horizontal" id="form_log_activty_edit">
										
										
                                            
											<div class="control-group">											
												<label class="control-label" for="nama">Nama</label>
												<div class="controls">
													 <textarea cols="3"  id="nama2" name="nama2"></textarea>
												</div> 			
											 </div>		 
											<div class="control-group">										
												<label class="control-label" for="tgl">Tanggal</label>
												<div class="controls">
													 <input type="text" class="form-control" id="tgl2" name="tgl2" placeholder="Tanggal Masukan" />
													 <input type="hidden" class="form-control" id="idlog" name="idlog" />
													 
												</div>			
											</div>
											<div class="control-group">											
												<label class="control-label" for="shift">Shift</label>
												<div class="controls">
													 <select class="form-control select2"  name = "shift" id ="shift">
														<option value="Shift 1 (00.00 - 08.00)">  Shift 1 (00.00 - 08.00) </option>
														<option value="Shift 2 (08.00 - 16.00)"> Shift 2 (08.00 - 16.00)  </option>
														<option value="Shift 3 (16.00 - 24.00)"> Shift 3 (16.00 - 24.00)  </option>
													 </select> 	
																								 
												</div>			
											</div> 
											<div class="control-group">											
												<label class="control-label" for="Event">Event</label>
												<div class="controls">
													 <select class="form-control select2"  name = "event2" id ="event2">
													   <?php 
													   if($this->session->userdata('group_id') == 36 )
													   {
														  echo '<option value="Rafi 2019">  Rafi 2019 </option>';
													   }
                                                       else
                                                       {
														   
													   ?>
													 
														<option value="Rafi 2019">  Rafi 2019 </option>
														<option value="Naru 2019"> Naru 2019  </option>
														<option value="Pemilu 2019"> Pemilu 2019  </option>
														
														<?php
													    }
														?>
													 </select> 	
																								 
												</div>			
											</div> 
			  							    <div class="control-group">											
												<label class="control-label" for="Activity">Activity</label>
												<div class="controls">
													 <textarea cols="4"  id="Activity2" name="Activity2"></textarea>
												</div> 			
											 </div>		
											
										
			


				</form>						
			</div>
		</div>
		</p>
  </div>
  <div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary" id="save_change2">Save </button>
  </div>
</div>


<script>

		var editor1=''; //.setData('your data');
		var editor2=''; //setData('your data');

$(document).ready(function() {
	
	 //$('#example2 tfoot th').each( function () { 
	 //	 var title = $(this).text();
	 //	 $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	 //} );
	 
		 $('#myModal_').on('hidden.bs.modal', function () {
			
			 $('#s_k').hide();
			 $('.textnama').remove();
			 $('#form_log_activty')[0].reset();
		 });
		 $('#myModal_edit').on('hidden.bs.modal', function () {
			 //$('#s_k2').html("");
			  $('#s_k').hide();
			  $('.textnama').remove();
			 $('#form_log_activty_edit')[0].reset();
		 });
	     $('#tgl').datetimepicker({
				//format: "Y-m-d h:m:s"
				format: "Y-m-d h:m:s"
				//format: "d-m-Y h:m:s"
		  }); 
	 
	       CKEDITOR.replace('Activity');
	       CKEDITOR.replace('Activity2');
	       CKEDITOR.replace('nama2');
		  
		   editor = CKEDITOR.instances['Activity'];
		   editor1 = CKEDITOR.instances['Activity2'];
		   editor2 = CKEDITOR.instances['nama2'];
		
		  $(".textarea").wysihtml5(); 	
		   
	 
	$('#gen_form').click(function(){
		$('.textnama').remove();
		
		vl = $('#pic_s').val();
		Sd = '';
		for(var i=1;i<=vl;i++)
		{	
		
			Sd += '<div class="control-group textnama" >'+											
				  '<label class="control-label" for="tgl">Name '+i+'</label>'+
				  '<div class="controls">'+
					 '<input type="text" class="form-control" name="nama['+i+']" placeholder="Nama" />'+
				'</div>'+ 				
			  '</div>'; 

		}
		
		$('#s_k').show();
		$('#s_k2').append(Sd);
		
	});
	
	
	var t12 = $('#example2').DataTable({
				 //"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				 //		"t"+
				 //		"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				 "autoWidth" : true,
				 //"oLanguage": {
				 //	"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				 //},
				 "scrollY": 500,
                 "scrollX": true,
				 "processing": true,
				 "serverSide": true, 
				 "aoColumnDefs": [
						//{"aTargets" : [0],  "sClass":  "details-control"}, 
						//{ "bVisible": false, "aTargets": [ 1 ] },
						{"aTargets": [6], "bSortable": false }
						
						],

				"ajax":{
					url :"<?php echo base_url(); ?>log_activity/data", 
					type: "post",
					"data": function ( d ) {
								//d.active = $('#selec_filter').val();
							    //d.awal = $('#PROGRAM_START_S').val();
						       // d.akhir = $('#PROGRAM_END_S').val();
					},
					"dataSrc": function ( json ) {
						
						return json.data;
					},  
					error: function(){  // error handling
						
					},
			},		
			

		});	
        
		
	  $('#save_change').click(function(){
		  
		  inp();
		  
	  });
	  
	  $('#save_change2').click(function(){
		  
		  update();
		  
	  });
	  
	  
	 //});
	
	
});
function formatDetail( d ) {
	var input_data = new Object();
	input_data.id = d.data()[1];
	jQuery.ajax(
			{
				url: "<?php echo base_url()?>log_activity/detail_log",
				type: "POST",
				data: input_data,
				dataType: "json",
				beforeSend: function() {
					
				},
				success: function (data)
				{
						d.child( data.data ).show();

				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert(textStatus);
				}
			}); 
}
function inp()
{
        vl = $('#pic_s').val();
	    var dataku = [];
		//var inputDetail = new Object();
		//var no = 1;
		for(var i=1;i<=vl;i++)
		{	
			var input = new Object();
			ssd =  $("input[name='nama["+i+"]']").val();
			input.nama = ssd;
			dataku.push(input);   
		}
		var input_data = new Object();
        input_data.data = JSON.stringify(dataku);
        input_data.Activity   = editor.getData();//$('#Activity').val();
        input_data.shift = $('#shift').val();
        input_data.event = $('#event').val();
        input_data.tgl = $('#tgl').val();
      //editor2.setData(data.data.nama);
					 //   editor1.setData(data.data.activity);
        jQuery.ajax(
                {
                    url: "<?php echo base_url()?>log_activity/insertdata",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    { 

                        if (data.response == false || data.response == 0 )
                        {
                            // tidak bisa dihapus
                            alert("Save gagal ");
                        } 
						else
                        {
                           alert("Data berhasil di masukan");
						   //$('#s_k2').html("");
						   $('#myModal_').modal('hide');
						   $('#Activity').val('');
						   $('#form_log_activty')[0].reset();
						   $('#example2').DataTable().ajax.reload();
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}

function update()
{
       
		var input_data = new Object();
        input_data.Activity   = editor1.getData();
        input_data.shift = $('#shift2').val();
        input_data.event = $('#event2').val();
        input_data.tgl = $('#tgl2').val();
        input_data.nama = editor2.getData();
        input_data.idlog = $('#idlog').val();
      
        jQuery.ajax(
                {
                    url: "<?php echo base_url()?>log_activity/updatedata",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    { 

                        if (data.res == 0 )
                        {
                            // tidak bisa dihapus
                            alert("Save gagal ");
                        } 
						else
                        {
                           alert("Data berhasil di ubah");
						   // $('#s_k2').html("");
						   $('#myModal_edit').modal('hide');
						   //$('#Activity').val('');
						   //$('#Activity').val('');
						   $('#form_log_activty_edit')[0].reset();
						   $('#example2').DataTable().ajax.reload();
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}



function del(id)
{
        var result = confirm("Yakin hapus pengguna Data ini ");
        if (result) {
            //Logic to delete the item
            //Delete2(id);
			url = "<?php echo base_url()?>log_activity/del/"+id;
            $(location).attr("href", url);
			
        }
}
    


function edit(id)
{
		
		
		var input_data = new Object();
        input_data.id = id;
        
		jQuery.ajax(
		{
			url: "<?php echo base_url()?>log_activity/editData",
			type: "POST",
			data: input_data,
			dataType: "json",
			success: function (data)
			{ 

				if ( data.res == 0 )
				{
					// tidak bisa dihapus
					alert("Save gagal ");
				} 
				else
				{
				    
					    $('#tgl2').val(data.data.tanggal);
					    $('#shift2').val(data.data.shift);
					    $('#idlog').val(data.data.id_log_activity);
						editor2.setData(data.data.nama);
					    editor1.setData(data.data.activity);
					 $('#myModal_edit').modal({
						keyboard: true,
						backdrop: "static",
						show:true
					 });  
				}

			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus);
			}
		});	
			
}

function exportdata()
{
	window.open("<?php echo site_url('log_activity/exportData'); ?>");
}

</script>
			