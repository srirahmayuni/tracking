<div style="margin-top:20px" id="chart_week"></div>  

<script>
var weeks = <?php echo json_encode($weeks); ?>;    
var target_up_to_plan = <?php echo json_encode($target_up_to_plan); ?>;
var target_up_to_actual = <?php echo json_encode($target_up_to_actual); ?>;
    
console.log(weeks);
console.log(target_up_to_plan);
console.log(target_up_to_actual);

Highcharts.chart('chart_week', {
 
title: {
    text: 'kurva-S' 
},

subtitle: {
    text: '( Per Week )'
}, 

yAxis: {
    title: { 
        text: 'Total'
    }
},
legend: {
    enabled: true,
    itemStyle: {
        "color": "#000", 
        "font-weight": 400, 
        "font-family": 
        "Lato, Sans-serif", 
        "font-size": "0.7rem",  
        "text-transform": "uppercase"}
    },

credits: {
    enabled: false
},

xAxis: {
            categories: weeks
       },

series: [{
    color: '#FF0000',
    name: 'Target Plan Up To',
    data: target_up_to_plan
}, {
    color: '#0383d9',
    name: 'Target Actual Up To',
    data: target_up_to_actual
}
],

responsive: {
    rules: [{
        condition: {
            maxWidth: 500
        },
        chartOptions: {
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            }
        }
    }]
}

});
</script>