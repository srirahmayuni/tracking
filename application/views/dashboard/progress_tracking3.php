	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
	<script type="text/javascript">
		FusionCharts.ready(function(){
			var chartObj = new FusionCharts({
				//type: 'stackedcolumn3dline', 
				//type: 'stackedcolumn3dlinedy', 
				type: 'msstackedcolumn2dlinedy', 
				// type: 'mscolumn2d',
				renderAt: 'chart-container-tracking-nasional',
				width: '100%',
				height: '400',
				dataFormat: 'jsonurl',
				dataSource: '<?php echo base_url()?>dashboard/progress_tracking_nasional'
				
				
				});
			chartObj.render();
		});
</script>
	
<div id="chart-container-tracking-nasional"></div>
	