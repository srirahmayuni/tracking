	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
	<script type="text/javascript">
	  FusionCharts.ready(function(){
					var chartObjx = new FusionCharts({
									//type: 'mscolumn2d',
									"type": "msstackedcolumn2dlinedy",
									"renderAt": "chart-container-network",
									"width": "100%",
									"height": "400",
									//"dataFormat": "json",
									dataFormat: 'jsonurl', 
									dataSource: '<?php echo base_url()?>reporting/progress_tracking_regional_network'
					});
					chartObjx.render();
		});
		 
	

       //$('text:contains("FusionCharts Trial")').html("");  
  /* FusionCharts.ready(function(){   
	      var chartObjs = new FusionCharts({
   type: "msstackedcolumn2dlinedy",
   renderAt: 'chart-container',
   width: "100%",
   height: "100%",
   dataFormat: "json",
   dataSource: {
  "chart": {
    "caption": "Market Share of Korean Automobile Manufacturers in US",
    "subcaption": "2011 - 2016",
    "pyaxisname": "Units Sold",
    "syaxisname": "% of total market share",
    "snumbersuffix": "%",
    "syaxismaxvalue": "25",
    "theme": "fusion",
    "showvalues": "0",
    "drawcrossline": "1",
    "divlinealpha": "20"
  },
  "categories": [
    {
      "category": [
        {
          "label": "2011"
        },
        {
          "label": "2012"
        },
        {
          "label": "2013"
        },
        {
          "label": "2014"
        },
        {
          "label": "2015"
        },
        {
          "label": "2016"
        }
      ]
    }
  ],
  "dataset": [
    {
      "dataset": [
        {
          "seriesname": "Honda City",
          "data": [
            {
              "value": "997281"
            },
            {
              "value": "1063599"
            },
            {
              "value": "1063964"
            },
            {
              "value": "1152123"
            },
            {
              "value": "1289128"
            },
            {
              "value": "1394972"
            }
          ]
        },
        
      ]
    },
    {
      "dataset": [
        {
          "seriesname": "Hyundai Verna",
          "data": [
            {
              "value": "373709"
            },
            {
              "value": "391276"
            },
            {
              "value": "380002"
            },
            {
              "value": "411456"
            },
            {
              "value": "476001"
            },
            {
              "value": "500537"
            }
          ]
        },
        
      ]
    }
  ],
  "lineset": [
    {
      "seriesname": "Market Share %",
      "plottooltext": "Total market share of Korean cars in $label is <b>$dataValue</b> in US",
      "showvalues": "0",
      "data": [
        {
          "value": "17.74"
        },
        {
          "value": "19.23"
        },
        {
          "value": "15.43"
        },
        {
          "value": "12.34"
        },
        {
          "value": "15.34"
        },
        {
          "value": "21.17"
        }
      ]
    }
  ]
}
});
	      chartObjs.render();

	   });  */
		
</script>
	
<div id="chart-container-network"></div>
	