<link href="<?php echo base_url()?>asset/temp/css/pages/plans.css" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="main">

	<div class="main-inner">
	    <div class="container">
	      <div class="row">
	      	<div class="span12">     
	      		<div class="widget ">
	      			<div class="widget-header">
	      				<i class="icon-user"></i> 
	      				<h3>Executive Summary</h3>
	  				</div> <!-- /widget-header -->
					<div class="widget-content">
						<div class="tabbable"> 
						<ul class="nav nav-tabs">
						  <li class="active"> 
						    <a href="#formcontrols" data-toggle="tab" style = "font-size: 12px">Summary By Directorate</a>
						  </li>
						  <li><a href="#jscontrolsarea" data-toggle="tab" style = "font-size: 12px">Summary By Area</a></li>
						  <li><a href="#jscontrols" data-toggle="tab" style = "font-size: 12px">Summary By Region + HQ</a></li>
						  <li><a href="#jscontrolsNOQM" data-toggle="tab" style = "font-size: 12px">Summary By NEP NOQM</a></li> 
						  <li><a href="#jscontrols3" data-toggle="tab" style = "font-size: 12px">Summary By Group</a></li>
						  <li><a href="#jscontrols4" data-toggle="tab" style = "font-size: 12px">Summary By Owner Program</a></li>
						  <li><a href="#jscontrols2" data-toggle="tab" style = "font-size: 12px">Activity Progress Tracking</a></li>   
						</ul>

						<br>
						 						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
							      	<fieldset id="summary">	
								    </fieldset> 
								</div> 

								<div class="tab-pane" id="jscontrols">
									<fieldset>
									 <div id="summary_regional"></div> 
								    </fieldset> 
									<!-- <fieldset>
									 <div id="summary_regional2"></div>
								    </fieldset>  -->
								</div>

								<div class="tab-pane" id="jscontrolsNOQM">
									<fieldset>
									 <div id="summary_regional_noqm"></div> 
								    </fieldset> 
									<!-- <fieldset>
									 <div id="summary_regional2"></div>
								    </fieldset>  -->
								</div>


								<div class="tab-pane" id="jscontrolsarea">
									<fieldset>
									 <div id="summary_area"></div> 
								    </fieldset> 
								</div> 
								<div class="tab-pane" id="jscontrols2">

									<h4 style="margin-left:70px;">Updated at <?php echo date('d M Y H:i:s') ?></h4>

									<?php $this->load->view('dashboard/progress_tracking2'); //tracking regional?>
									<fieldset> 
									 <div id="progress_tracking_week"></div> 
									</fieldset>  
									<?php $this->load->view('dashboard/progress_by_section_national');?>    
								</div>
								<div class="tab-pane" id="jscontrols3">
									<fieldset id="summary_by_group">
								    </fieldset> 
								</div>
								<div class="tab-pane" id="jscontrols4">
									<fieldset id="summary_by_divisi">
								    </fieldset> 
								</div>
								
							</div>
						</div>
					</div> <!-- /widget-content -->
				 </div> <!-- /widget -->
		      </div> <!-- /span8 -->
	       </div> <!-- /row -->
	    </div> <!-- /container -->
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
<script>    

function load()
{
	//$(".preload").fadeOut(2000, function() {
   //     $(".main").fadeIn(1000);        
    //});
}

</script>    

<script src="https://code.highcharts.com/highcharts.js"></script>
<?php $this->load->view('dashboard/dashboard_js'); ?>    