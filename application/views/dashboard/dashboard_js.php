<script>
$(document).ready(function() {
	 jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/summary",
            type: "POST",
    		beforeSend: function(){
			},
	        success: function (data)
            {
                $('#summary').append(data);		
    		},
			complete:function(data){
				$(".preload").show();
			},
	        error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        });

	  jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/summary_regional",
            type: "POST",
            success: function (data)
            {
                $('#summary_regional').append(data);		
               
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        });

        jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/summary_regional_noqm",
            type: "POST",
            success: function (data)
            {
                $('#summary_regional_noqm').append(data);		
               
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        });

        jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/summary_area",
            type: "POST",
            success: function (data)
            {
                $('#summary_area').append(data);		
               
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        });

        jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/progress_tracking_week",
            type: "POST",
            success: function (data)
            {
                $('#progress_tracking_week').append(data);		
                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        });

        jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/progress_tracking_week_regional",
            type: "POST",
            success: function (data)
            {
                $('#progress_tracking_week_regional').append(data);		
               
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        });        

		// jQuery.ajax({
        //     url: "<?php echo base_url()?>dashboard/summary_regional_nep_combat",
        //     type: "POST",
        //     success: function (data)
        //     {
        //         $('#summary_regional2').append(data);		
        //     },
        //     error: function (jqXHR, textStatus, errorThrown)
        //     {
        //         alert(textStatus);
        //     }
        // });
		
		
		jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/progress_trackking_nasional_per_week",
            type: "POST",
            success: function (data)
            {
                $('#table_per_week_nasional').append(data);		
           },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        }); 
		//combat 
		/* jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/progress_trackking_combat_nasional",
            type: "POST",
            success: function (data)
            {
                $('#table_combat_grid').append(data);		
           },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        });  */
		
		
		//summary_by_group
		jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/summary_by_group",
            type: "POST",
            success: function (data)
            {
                $('#summary_by_group').append(data);		
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        }); 
		//summary_by_divisi
		jQuery.ajax({
            url: "<?php echo base_url()?>dashboard/summary_by_divisi",
            type: "POST",
            //data: { id  : 3},
            //dataType: "json",
            success: function (data)
            {
                $('#summary_by_divisi').append(data);		
                //console.log(data);
			},
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus);
            }
        }); 

});
</script>