<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'users/create';
    $checkbox = "checked";
    $pass = '*';
} else {
    $urlnya = 'users/update';

    ($adminUsersActive) ? $checkbox = "checked" : $checkbox = "";

    $pass = '';
}
?>

<div class="box-header with-border">
            
           
</div><!-- /.box-header -->
<div class="main">
  <div class="main-inner">
    <div class="container">
	 	
		<!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Form Data User</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style="padding :10px;">
		
		<div class="content clearfix">
		<?php
        echo form_open(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
        echo form_hidden('adminUsersUsers_id', $adminUsersUsers_id);
        ?>
        <div class="box-body">
		
			<?php
            if ($has_err) {//echo $err;
               //echo '<div class="row">
				//			<div class="col-md-12">
					echo			'<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>';
				//			</div>
				//	  </div>';
            }
            ?>
		
		
            <fieldset>
          
					<div class="control-group">
                        <label class="control-label" for="adminUsersFirstName">Nama *</label>
						<div class="controls">
							<input type="text" class="form-control span6" placeholder="Nama"  name="adminUsersFirstName" id="adminUsersFirstName"  value="<?php echo set_value('adminUsersFirstName', $adminUsersFirstName) ?>">
						</div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="adminUsersMobile"><?php echo (($isNumber) ? "ID User" : "Username"); ?> *</label>
                        <input type="text" class="span6" name="adminUsersMobile" id="adminUsersMobile" placeholder="<?php echo (($isNumber) ? "ID User" : "Username"); ?>" value="<?php echo set_value('adminUsersMobile', $adminUsersMobile) ?>">
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="adminUsersEmail">Email *</label>
                        <input type="email" class="span6" name="adminUsersEmail" id="adminUsersEmail" placeholder="email" value="<?php echo set_value('adminUsersEmail', $adminUsersEmail) ?>">
                    </div>
                    <div class="control-group">
                            <!--input type="text" class="form-control" name="adminCoordinatorName" id="adminCoordinatorName" placeholder="Nama" value="<?php //echo set_value('adminCoordinatorName')   ?>"-->
                        <div class="col-md-6">
                            <label class="control-label" for="adminUsersPassword">Password <?php echo $pass ?></label>
                            <input type="password" class="span6" placeholder="Password"  name="adminUsersPassword" id="adminUsersPassword"  value="<?php echo set_value('adminUsersPassword') ?>">
                        </div><!-- /.col-lg-6 -->
                        <div class="col-md-6">
                            <label class="control-label" for="adminUsersPassword2">Ulangi Password <?php echo $pass ?></label>
                            <input type="password" class="span6" placeholder="Ulangi Password"  name="adminUsersPassword2" id="adminUsersPassword2" value="<?php echo set_value('adminUsersPassword2') ?>">
                        </div><!-- /.col-lg-6 -->
                    </div>
                    <br />&nbsp;
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="adminUsersShowPass" id="adminUsersShowPass" /> Tampilkan password
                            </label>
                        </div>
                    </div>

                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="control-group">
                        <label class="control-label" for="adminUsersGroup">Group *</label>
                        <select class="select2 span6" style="width: 50%;" name = "adminUsersGroup" id ="adminUsersGroup">
                            <?php
                            echo '<option value="">Pilih Group</option>';
                            foreach ($dropdownGroups as $row) {
                                if (isset($adminUsersGroup) && $row->group_id == $adminUsersGroup)
                                    echo '<option value="' . $row->group_id . '" selected="selected" >' . $row->name . '</option>';
                                else
                                    echo '<option value="' . $row->group_id . '">' . $row->name . '</option>';
                            }
                            ?>
                        </select>
                    </div>	
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" style="float:left;" name="adminUsersActive" id="adminUsersActive" <?php echo $checkbox ?>> Aktif
                        </label>
                    </div> 
                </div><!-- /.col -->

			  
			</fieldset>
			<div class="box-footer">
                <!--button type="submit" class="btn btn-primary pull-right" name="adminOrdersCreate" id="adminOrdersCreate" >Simpan</button-->
                Isian dengan tanda * adalah wajib.<br />
                <input type="submit" class="btn btn-primary pull-right" name="adminUsersCreate" id="adminUsersCreate" value="Simpan">
                <!--input class="btn btn-primary btn-block btn-flat" type="submit" value="Blast" name="bidAdd"-->
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
       </div>
            <!-- /widget-content --> 
		</div>	
	</div>	
  </div>	
</div>	

<?php
$this->load->view('users/js/create_js');
?>
			