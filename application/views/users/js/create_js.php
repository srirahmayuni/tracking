<script>
    $("#adminUsersShowPass").on("click", function () {
        if (this.checked) {
            //Do stuff
            $('#adminUsersPassword').prop('type', 'text');
            $('#adminUsersPassword2').prop('type', 'text');
        } else
        {
            $('#adminUsersPassword').prop('type', 'password');
            $('#adminUsersPassword2').prop('type', 'password');
        }

    });
</script>