
<div class="main">
  <div class="main-inner">
    <div class="container">
	 	<?php
		if ($message) { 
		?>
		<div class="control-group">
		<label class="control-label"></label>
			<div class="controls">
			 <div class="alert lert alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Warning!</strong> <?php  echo $message; ?>
			</div>
			</div>
		</div>
		 <?php
		 }
		 ?>
		 <button class="btn btn-primary" onClick="createUsers()">Tambah User</button>
	 	 <br>
		 <hr>
		<!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Data Users</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style="padding :10px;">
              <table id="example2" class="table table-striped table-bordered" width="100%">
                <thead>
                   <tr>
										<th>No</th>
                                        <th>ID/Username</th>
                                        <th>Group</th>
                                        <th>Nama Depan</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th style="width:200px;">Action</th>
                  </tr>
                </thead>
                <tbody>
				  
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
	</div>	
  </div>	
</div>	


<script>
$(document).ready(function() {
	var t12 = $('#example2').DataTable({
				 //"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				 //		"t"+
				 //		"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				 "autoWidth" : true,
				 //"oLanguage": {
				 //	"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				 //},
				 "scrollY": 200,
                 "scrollX": true,
				 "processing": true,
				 "serverSide": true, 
				 "aoColumnDefs": [
						//{"aTargets" : [0],  "sClass":  "details-control"}, 
						//{ "bVisible": false, "aTargets": [ 1 ] },
						{"aTargets": [6], "bSortable": false }
						
						],

				"ajax":{
					url :"<?php echo base_url(); ?>users/data", 
					type: "post",
					"data": function ( d ) {
								//d.active = $('#selec_filter').val();
							    //d.awal = $('#PROGRAM_START_S').val();
						       // d.akhir = $('#PROGRAM_END_S').val();
					},
					"dataSrc": function ( json ) {
						
						return json.data;
					},  
					error: function(){  // error handling
						
					},
			},		
			

		});	
});

function createUsers()
{
	url = "<?php echo site_url('users/create'); ?>";
	$(location).attr("href", url);
}
function adminUsersDelete(users_id, first_name)
{
        var result = confirm("Yakin hapus pengguna " + first_name + "?");
        if (result) {
            //Logic to delete the item
            adminUsersDelete2(users_id, first_name);
        }
}
function adminUsersDelete2(users_id, first_name)
{
	var input_data = new Object();

	input_data.users_id = users_id;
	jQuery.ajax(
			{
				url: "<?php echo site_url('admin/users/delete'); ?>",
				type: "POST",
				data: input_data,
				dataType: "json",
				success: function (data)
				{
					//jquery_unblockui();

					if (data.response == false)
					{
						// tidak bisa dihapus
						alert("Pengguna " + first_name + " tidak bisa di hapus!");


					} else
					{
						//bisa dihapus
						alert("Pengguna " + first_name + " telah di hapus");
						// url = "<?php echo site_url('admin/coordinator/index'); ?>";
						// $( location ).attr("href", url);
						location.reload();

					}

				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					//jquery_unblockui();
					alert(textStatus);
				}
			});
}

function adminUsersUpdate(users_id)
{
	url = "<?php echo site_url('users/update'); ?>/" + users_id;
	$(location).attr("href", url);
}
</script>
			