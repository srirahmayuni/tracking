<?php
$has_recordMaster = isset($data) && is_array($data) && count($data);
?>

<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>	
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title"></h1>
                </div><!-- /.box-header -->

                <div class="box-body">    
                   
                    
                    <div class="col-xs-12 table-responsive">
                        <br />
                        <table id="berita" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:20px;">No</th>
                                    <th>Logo</th>
                                    <th style="width:200px;">Action</th> 
                                </tr>
                            </thead>

                        </table>  
                    </div>	
                  
                </div>
            </div>
           
        </div><!-- /.box-body -->

    </div>
</section>
<style>
table.dataTable >thead th  {
    text-align: center;
    background: #9ffB;
}
table.dataTable >tbody td {
    text-align: center;
    background: #99BCDB;
}

table.dataTable > tbody tr.odd td { 
    background: #EBF2F8
;
}
</style>
<!-- /.content -->
<script type="text/javascript">
    var t;

    $(document).ready(function ()
    {
        t = $('#berita').DataTable({
             "processing": true,
             "serverSide": true, 
             aoColumnDefs: [
                         
                        {"aTargets": [2], "bSortable": false }
                        ],
            "ajax":{
                url :"<?php echo base_url_admin(); ?>/logowebsite/data",
                type: "post"
            }

        });
		
		

    });

    function create()
    {
        //alert('create');
        url = "<?php echo site_url(''.folderBack().'/pesanmasuk/create'); ?>";
        $(location).attr("href", url);
    }

    function Delete(users_id, first_name)
    {
        var result = confirm("Yakin hapus pengguna " + first_name + "?");
        if (result) {
           
        }
    }
</script>