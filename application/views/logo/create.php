<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'logowebsite/create';
} else {
    $urlnya = 'logowebsite/update';
}
?>
 
 <!-- Main content -->
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <?php
            if ($has_err) {//echo $err;
                echo '<div class="row">
							<div class="col-md-12">
								<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>
							</div>
					  </div>';
            }
            ?>
           
        </div><!-- /.box-header -->
        <?php
        echo form_open_multipart(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
        echo form_hidden('id_logo', $id_logo);
        ?>
        

        <div class="panel-body">
            <div class="row">
                
				<div class="col-md-9">
						<div class="form-group">
							<label for="Gambar">Gambar  *</label>
							  <input id="fupload" type="file" name="fupload" accept="image/*" onChange="validate(this)" >
							<p class="help-block">Jpeg/jpg format  Ukuran gambar maksimal lebar 260px.</p> 
						</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					<div class="widget-user-image">  <a href="#" id="pop1">
					  <img id="imageresource1" class="img-circle" height="60"  width="60" src="<?php echo base_url() ?>assets/images/logo/<?php echo ($gambar == "") ? 'noimage.png' : $gambar?>" alt="attachment image">
					  </a>
					</div>  
					</div>
				</div>
			</div>
            </div><!-- /.row -->
            <div class="box-footer">
                Isian dengan tanda * adalah wajib.<br /><br />
				<input type='reset' class="btn btn-primary pull-right" value="Reset">
                <input type="submit" class="btn btn-primary pull-left" name="Create" id="Create" value="Simpan">
            </div>
        </div>
        <!-- /.box-body -->
        <?php echo form_close(); ?>
     
    </div>
</section>

<!-- /.content -->
<div class="modal fade" id="imagemodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Favicon</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
                <img src="" id="imagepreview1" style="width: 200px; height: 200px; " >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- /.content -->
<script>
 $(document).ready(function () {
   $("#pop1").on("click", function () {
		$('#imagepreview1').attr('src', $('#imageresource1').attr('src'));
		$('#imagemodal1').modal('show');
	});
	
 });
</script>