<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    } 
    ?>	
</div>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">    
                    <?php
                    if (is_authorized('album', 'create')) {
                        ?>
                        <a class="btn btn-success btn-flat" href="<?php echo base_url().'artikel/create'?>"><span class="fa fa-plus"></span> Post Tulisan</a>
          
                        <br>
                        <hr>
                        <br>
                        <?php
                    }
                    ?>	
                    <div class="col-xs-12 table-responsive">
                        <br />
						
						<div class="panel panel-default">
						
						<div class="panel-heading">
                         Atikel
                        </div>
						
						  <div class="panel-body">
							<table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
      					<th>Gambar</th>
      					<th>Judul</th>
      					<th>Tanggal</th>
      					<th>Author</th>
      					<th>Baca</th>
      					<th>Publish</th>
                    
                        <th style="text-align:right;">Aksi</th>
                </tr>
                </thead>
                <tbody>
          				<?php
          					$no=0;
          					foreach ($data->result_array() as $i) :
          					   $no++;
          					   $tulisan_id=$i['tulisan_id'];
          					   $tulisan_judul=$i['tulisan_judul'];
          					   $tulisan_isi=$i['tulisan_isi'];
          					   $tulisan_tanggal=$i['tanggal'];
          					   $tulisan_author=$i['tulisan_author'];
          					   $tulisan_gambar=$i['tulisan_gambar'];
          					   $tulisan_views=$i['tulisan_views'];
							   $kategori_id=$i['tulisan_kategori_id'];
                               $kategori_nama=$i['tulisan_kategori_nama'];
                               $publish=$i['publish'];
                       
                    ?>
                <tr>
                  <td><img src="<?php echo base_url().'assets/images/artikel/'.$tulisan_gambar;?>" style="width:90px;"></td>
                  <td><?php echo $tulisan_judul;?></td>
                  
        				  <td><?php echo $tulisan_tanggal;?></td>
        				  <td><?php echo $tulisan_author;?></td>
        				  <td><?php echo $tulisan_views;?></td>
        				  <td><?php echo $publish;?></td>
        				  
                  <td style="text-align:right;">
                        <a class="btn" href="<?php echo base_url().'artikel/update/'.$tulisan_id;?>"><span class="fa fa-pencil"></span></a>
                        <a class="btn" href="#" onclick="deletex('<?php echo $tulisan_id;?>')"><span class="fa fa-trash"></span></a>
                  </td>
                </tr>
				<?php endforeach;?>
                </tbody>
              </table>
							
						  </div>	
						</div>	
                    </div>	
                </div>
            </div>
           
        </div><!-- /.box-body -->

    </div>
</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $('#example1').DataTable();
} );

	function deletex(id)
    {
        var result = confirm("Yakin akan menghapus data ini ?");
        if (result) {
            url = "<?php echo site_url('artikel/delete/');?>"+id;
			$(location).attr("href", url);
	   }
    }
</script>