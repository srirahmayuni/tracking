<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/plugins/iCheck/all.css">
<script src="<?php echo base_url()?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<!--<script src="<?php echo base_url()?>assets/admin/dist/js/app.min.js"></script>-->

<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url()?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>


<link href="<?php echo base_url(); ?>assets/admin/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
<!--datetimepicker-->
<!--CSS ADD-->
<!--datetimepicker-->
<script src="<?php echo base_url(); ?>assets/admin/plugins/datetimepicker/build/jquery.datetimepicker.full.js"></script>


<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'artikel/create';
} else {
    $urlnya = 'artikel/update';
}
?>
 <!-- Main content -->
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <?php
            if ($has_err) {//echo $err;
                echo '<div class="row">
							<div class="col-md-12">
								<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>
							</div>
					  </div>';
            }
            ?>
           
        </div><!-- /.box-header -->
        <?php
        echo form_open_multipart(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
        echo form_hidden('tulisan_id', $tulisan_id);
        ?>
        

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="adminUsersFirstName">Judul Artikel *</label>
                        <input type="text" class="form-control" placeholder="Judul"  name="tulisan_judul" id="tulisan_judul"  value="<?php echo set_value('tulisan_judul', $tulisan_judul) ?>">
                    </div>
                </div><!-- /.col -->
				
				<div class="col-md-6">
                    <div class="form-group">
                        <label for="adminUsersFirstName">Penulis *</label>
                        <input type="text" class="form-control" placeholder="Nama Penulis"  name="tulisan_author" id="tulisan_author"  value="<?php echo set_value('tulisan_author', $tulisan_author) ?>">
                    </div>
                </div><!-- /.col -->
				<div class="col-md-6">
				   
					<div class="form-group">
						<label for="tanggal">Tanggal input *</label>
						<input type="text" class="form-control" value="<?php echo set_value('tulisan_tanggal',$tulisan_tanggal); ?>" name="tulisan_tanggal" id="tulisan_tanggal" placeholder=""  />
						 
					</div>
											
                    
                </div><!-- /.col -->
				
				
				<div class="col-md-9">
						<div class="form-group">
							<label for="Favicon">Gambar Artikel *</label>
							  <input id="gambar" type="file" name="tulisan_gambar" accept="image/*" onChange="validate(this)" >
							<p class="help-block">Jpeg/jpg format.</p> 
						</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					<div class="widget-user-image">  <a href="#" id="pop1">
					  <img id="imageresource1" class="img-circle" height="60"  width="60" src="<?php echo base_url() ?>assets/images/artikel/<?php echo ($gambar == "") ? 'noimage.png' : $gambar?>" alt="attachment image">
					  </a>
					</div>  
					</div>
				</div>
				<div class="col-md-12">
                    <div class="form-group">
                        <label for="keterangan_gambar">Isi *</label>
						<textarea class="form-control" required id="ckeditor" name='tulisan_isi'  rows="2" placeholder=" ..."><?php echo $tulisan_isi; ?></textarea>
                       
					</div>
                </div><!-- /.col -->
				<div class="col-md-12">
                    <div class="form-group">
                        <?php
							  if ($aktif=='YA')
							  {
							   echo "
							   <p class=inline-small-label> 
							   <label for=field4>Publish</label>
							   <input type=radio name='aktif' value='YA' checked>Ya 
							   <input type=radio name='aktif' value='TIDAK'>Tidak
							   </p>";
							   }
																  
							   else
							   {
								   echo "
								   <p class=inline-small-label> 
								   <label for=field4>Publish</label>
								   <input type=radio name='aktif' value='YA'>Ya 
								   <input type=radio name='aktif' value='TIDAK' checked>Tidak
								   </p>";
							   }
									
						
						?>
						
                    </div>
                </div><!-- /.col -->
				
				
            </div><!-- /.row -->
            <div class="box-footer">
               
                Isian dengan tanda * adalah wajib.<br /><br />
				<input type='reset' class="btn btn-primary pull-right" value="Reset">
                <input type="submit" class="btn btn-primary pull-left" name="Create" id="Create" value="Simpan">
				
            </div>
        </div>
       
        <!-- /.box-body -->
        <?php echo form_close(); ?>
        <div class="box-footer">

        </div>
    </div>
</section>
<!-- /.content -->

<!-- /.content -->
<div class="modal fade" id="imagemodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Favicon</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
                <img src="" id="imagepreview1" style="width: auto; height: auto; " >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

  //$(document).ready(function(){
	//$('#tulisan_tanggal').datepicker();		
  
   // $( "#tulisan_tanggal" ).datepicker({
     //  dateFormat: 'dd-mm-yy'
    // });
  //});

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
	
	 $('#tulisan_tanggal').datetimepicker({
			//format: "Y-m-d h:m:s"
			format: "Y-m-d h:m:s"
			//format: "d-m-Y h:m:s"
	 }); 
  
	
    CKEDITOR.replace('ckeditor');
	
	
   
	
  });
</script>


<script>

 $(document).ready(function () {
		
					
	$("#pop1").on("click", function () {
		$('#imagepreview1').attr('src', $('#imageresource1').attr('src'));
		$('#imagemodal1').modal('show');
	});
	
	 //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
 });


function validate(k) {

        size = k.files[0].size; 
		var ext = k.value.split(".");
		ext = ext[ext.length - 1].toLowerCase();
		var arrayExtensions = ["jpg", "jpeg","ico","png"];

		if (arrayExtensions.lastIndexOf(ext) == -1) {
			alert("Hanya boleh JPG atau JPEG.");
			$(k).val("");
		}

		if (size > 5242880)
		{
			alert("File tidak boleh melebihi 5 MB");
			$(k).val("");

		}
	}
</script>