<?php
$has_recordMaster = isset($data) && is_array($data) && count($data);
?>

<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>	
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">    
                    <?php
                    if (is_authorized('groups', 'create')) {
                        ?>
                        <button class="btn btn-primary" onClick="createGroups()">Tambah</button>
                        <br>
                        <hr>
                        <br>
                        <?php
                    }
                    ?>	
                    
                    <div class="col-xs-12 table-responsive">
                        <br />
                        <table id="table_adminGroups" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th style="width:200px;">Action</th> 
                                </tr>
                            </thead>

                        </table>  
                    </div>	
                   
                </div>
            </div>
            
        </div><!-- /.box-body -->

    </div>
	<div class="row">
        <div class="col-xs-12" id="kaprok" style="display:none;">
		<a class="btn btn-sm btn-danger" onClick="hide()"><i class="glyphicon">
												</i>Hide  <span id="TitleK"></span> </a>
		
		   <div class="box">
		    <br>
			   
		    
			<div class="box-body">
			<div class="col-xs-6 table-responsive">
			           <h4 class="page-header"><strong>Privilege Page</strong>
					   <button id="pop3" style="float:right;" class="btn btn-flat  btn-danger"  >Delete Page</button>
					   <button id="pop2" style="float:right;" class="btn btn-flat  btn-primary"  >Update Page</button>
					   <button id="pop1" style="float:right;" class="btn btn-flat"  >Add Page</button>
					   </h4> 
                        <br />
                      
                        <table id="halaman" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>id_page</th>
                                    <th>Name</th>
                                    <th>Aktifkan</th>
                                    <th style="width:80px;">Action</th> 
                                </tr>
                            </thead>
                        </table>  
            </div>
            <div class="col-xs-6 table-responsive" style="display:none;" id="kanantabel">
                          <h4 class="page-header"><strong>Privilege Action</strong>
						  <button id="pop33" style="float:right;" class="btn btn-flat  btn-danger"  >Delete Action</button>
						  <button id="pop22" style="float:right;" class="btn btn-flat  btn-primary"  >Update Action</button>
					      <button id="pop11" style="float:right;" class="btn btn-flat"  >Add Action</button>
						  </h4>
                          <br />
                        <table id="actions"  class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>id_actions</th>
                                    <th>Name</th>
                                    <th>Aktifkan</th>
                                   
                                </tr>
                            </thead>
                        </table>  
            </div>				
            </div>	
         </div>
        </div>
     </div>
</section>
<div class="modal fade" id="imagemodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Form page/controller</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
				<form id="addpage">
				<div class="panel panel-default">
				<div class="panel-heading">
					&nbsp;&nbsp;
				</div>

				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="adminUsersFirstName">Nama *</label>
								<input type="text" class="form-control" placeholder="Nama"  name="page" id="page"  />
								<input type="hidden" name="page_id" id="page_id"  />
							</div>
						</div><!-- /.col -->

					</div><!-- /.row -->
					<div class="box-footer">
						Isian dengan tanda * adalah wajib.<br />
						<input type="submit" class="btn btn-primary pull-right" name="Create" id="Create" value="Simpan"><span id="Create2"></span>
					</div>
				</div>
				</div>
				<!-- /.box-body -->
				<?php echo form_close(); ?>
			   
			   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="imagemodal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel2">Form Actions</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
				<form id="addaction">
				<div class="panel panel-default">
				<div class="panel-heading">
					&nbsp;&nbsp;
				</div>

				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="adminUsersFirstName">Nama *</label>
								<input type="text" class="form-control" placeholder="Nama"  name="action" id="action"  />
								<input type="hidden" name="action_id" id="action_id"  />
							</div>
						</div><!-- /.col -->

					</div><!-- /.row -->
					<div class="box-footer">
						Isian dengan tanda * adalah wajib.<br />
						<input type="submit" class="btn btn-primary pull-right" name="Create2" id="Create2" value="Simpan"><span id="loadact"></span>
					</div>
				</div>
				</div>
				<!-- /.box-body -->
				<?php echo form_close(); ?>
			   
			   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name ="gr_id" id="gr_id">
<input type="hidden" name ="id_page" id="id_page">
<input type="hidden" name ="temp_page_id_halaman" id="temp_page_id_halaman">
<input type="hidden" name ="temp_act_id" id="temp_act_id">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<!-- /.content --> 
<script type="text/javascript">
    var t1;
    var t2;
    var row;

    $(document).ready(function ()
    {
        $("#addpage").submit(function(){
		
		   addController();
			
			return false;
		});
		 $("#addaction").submit(function(){
		
		   addAction();
			
			return false;
		});
		
		$("#pop1").on("click", function () {
			$('#page').val("");
		    $('#page_id').val("");
			$('#imagemodal1').modal('show');
	    });
		$("#pop11").on("click", function () {
			$('#action').val("");
		    $('#action_id').val("");
			$('#imagemodal2').modal('show');
	    });
		$("#pop2").on("click", function () {
			
			if($('#temp_page_id_halaman').val())
		    {
			     $('#page').val(row.data()[2]);
				 $('#page_id').val(row.data()[1]);
				 $('#imagemodal1').modal('show');
				 t1.$('tr.selected').removeClass('selected');
				 $('#temp_page_id_halaman').val('');//tamp pegaid remov
			}
            else
			{	
               alert("Please select page ");	 
			   t1.$('tr.selected').removeClass('selected');
			   $('#temp_page_id_halaman').val('');//tamp pegaid remov
			 
			}
	    });
		$("#pop22").on("click", function () {
			
			if($('#temp_act_id').val())
		    {
			     $('#action').val(row.data()[2]);
				 $('#action_id').val(row.data()[1]);
				 $('#imagemodal2').modal('show');
				 t2.$('tr.selected').removeClass('selected');
				 $('#temp_act_id').val('');
			}
            else
			{	
               alert("Please select page ");	 
			   t2.$('tr.selected').removeClass('selected');
			   $('#temp_act_id').val('');
			}
	    });
		$("#pop3").on("click", function () { //delete
			
			if($('#temp_page_id_halaman').val())
		    {
			     
				 $('#page_id').val(row.data()[1]);
				 deletePage($('#temp_page_id_halaman').val());
				 t1.$('tr.selected').removeClass('selected');
				 $('#temp_page_id_halaman').val('');//tamp pegaid remov
			}
            else
			{	
                 alert("Please select page ");	 
			     t1.$('tr.selected').removeClass('selected');
			     $('#temp_page_id_halaman').val('');//tamp pegaid remov
			 
			}
	    });
		$("#pop33").on("click", function () { //delete
			
			if($('#temp_act_id').val())
		    {
			     
				 $('#page_id').val(row.data()[1]);
				 deletAction($('#temp_act_id').val()); 
				 t2.$('tr.selected').removeClass('selected');
				 $('#temp_act_id').val('');//tamp pegaid remov
			}
            else
			{	
               alert("Please select page ");	 
			   t2.$('tr.selected').removeClass('selected');
			   $('#temp_act_id').val('');//tamp pegaid remov
			 
			}
	    });
		table = $('#table_adminGroups').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url_admin(); ?>/groups/getData",
            "order": [[1, "desc"]]
        });
		
		
		 var t1 = $('#halaman').DataTable({
		   language: {  
				processing: "<img src='<?php echo base_url()?>assets/ico/loading.gif')}}'> Loading..."
		   },
		   "processing": true,
		   "serverSide": true, 
		     aoColumnDefs: [
						{ "bVisible": false, "aTargets": [ 1 ] },
						{"aTargets": [3], "bSortable": false }
						],
				"ajax":{
					url :"<?php echo base_url(); ?>set/groups/dataHalaman",
					type: "post",
					"data": function ( d ) {
						
						d.gr_id = $('#gr_id').val();
					},
					"dataSrc": function ( json ) {
					return json.data;
				  }
			}
		});
		 var t2 = $('#actions').DataTable({
		   language: {  
				processing: "<img src='<?php echo base_url()?>assets/ico/loading.gif')}}'> Loading..."
		   },
		   "processing": true,
		   "serverSide": true, 
		     aoColumnDefs: [
						{ "bVisible": false, "aTargets": [ 1 ] },
						{"aTargets": [2], "bSortable": false }
						],
				"ajax":{
					url :"<?php echo base_url(); ?>set/groups/dataActions",
					type: "post",
					"data": function ( d ) {
						
						d.gr_id = $('#gr_id').val();
						d.id_page = $('#id_page').val();
					},
					"dataSrc": function ( json ) {
					return json.data;
				  }
			}
		});
		$('#halaman tbody').on( 'click', 'tr', function () {
					if ( $(this).hasClass('selected') ) {
						$(this).removeClass('selected');
					}
					else {
						 t1.$('tr.selected').removeClass('selected');
						 $(this).addClass('selected');
						 var tr = $(this).closest('tr'); 
					     row = t1.row( tr );
						 
						 $('#temp_page_id_halaman').val(row.data()[1]);//tamp peg aid
					}
		  });
         $('#actions tbody').on( 'click', 'tr', function () {
					if ( $(this).hasClass('selected') ) {
						$(this).removeClass('selected');
					}
					else {
						 t2.$('tr.selected').removeClass('selected');
						 $(this).addClass('selected');
						 var tr = $(this).closest('tr'); 
					     row = t2.row( tr );
						 $('#temp_act_id').val(row.data()[1]);//tamp act id
					}
		  });			  
    });
    function createGroups()
    {
        url = "<?php echo site_url('set/groups/create'); ?>";
        $(location).attr("href", url);
    }
	function hide()
	{
		 $('#kaprok').hide();
		 $('#gr_id').val('');
	}
	function privilege(id,nama)
	{
	    $('#kanantabel').css( 'display', 'none' );
		$('#kaprok').show();
	    $('#TitleK').html('Groups '+nama);
	    $('#gr_id').val(id);
		$('#halaman').DataTable().ajax.reload();
	}
    function Delete(users_id, first_name)
    {
        var result = confirm("Yakin hapus pengguna " + first_name + "?");
        if (result) {
            //Logic to delete the item
            Delete2(users_id, first_name);
        }
    }
    function Delete2(users_id, first_name)
    {
        var input_data = new Object();

        input_data.group_id = users_id;
        jQuery.ajax(
                {
                    url: "<?php echo site_url('set/groups/delete'); ?>",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {
                        if (data.response == false)
                        {
                            alert("Group " + first_name + " tidak bisa di hapus!");
                        } else
                        {
                            alert("Group " + first_name + " telah di hapus");
                            location.reload();

                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
    }

    function adminUsersUpdate(users_id)
    {
        url = "<?php echo site_url('set/groups/update'); ?>/" + users_id;
        $(location).attr("href", url);
    }
	
	function comboChange(id_priv, page_id,group_id)
    {
     	id = "ac" + page_id;
        if ($('#' + id).is(":checked"))
        {
            $.ajax({
                url: '<?php echo base_url_admin(); ?>/groups/privilege',
                cache: false,
                dataType: 'json',
                type: 'post',
				cache: false,
				beforeSend: function() {
					$('#load'+page_id).html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
				},
                data: {pr_id:id_priv,p_id:page_id,g_id:group_id,allow:1},
                success: function (msg) {
                    $('#load'+page_id).html(" <i>"+msg['psn']+"</i> ");
					setInterval(function(){  $('#load'+page_id).html("");  }, 6000);
					$('#halaman').DataTable().ajax.reload();
                    

                }

            });
        }
        else
        {
			$.ajax({
                 url: '<?php echo base_url_admin(); ?>/groups/privilege',
                    cache: false,
                    dataType: 'json',
                    type: 'post',
                    data: {pr_id:id_priv,p_id:page_id,g_id:group_id,allow:0},
                    success: function (msg) {
					    $('#load'+page_id).html(" <i>"+msg['psn']+"</i> ");
					    setInterval(function(){  $('#load'+page_id).html("");  }, 6000);
					    $('#halaman').DataTable().ajax.reload();
                    }

                });
		}			
		
    }
	function comboChange2(id_priv, page_id,group_id,action_id)
    {
     	id = "ac" + action_id;
        if ($('#' + id).is(":checked"))
        {
            $.ajax({
                url: '<?php echo base_url_admin(); ?>/groups/privilege2',
                cache: false,
                dataType: 'json',
                type: 'post',
				cache: false,
				beforeSend: function() {
					$('#load'+action_id).html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
				},
                data: {pr_id:id_priv,p_id:page_id,g_id:group_id,allow:1,action_id:action_id},
                success: function (msg) {
                    $('#load'+action_id).html(" <i>"+msg['psn']+"</i> ");
					setInterval(function(){  $('#load'+action_id).html("");  }, 6000);
					$('#actions').DataTable().ajax.reload();
                }
            });
        }
        else
        {
			$.ajax({
                 url: '<?php echo base_url_admin(); ?>/groups/privilege2',
                    cache: false,
                    dataType: 'json',
                    type: 'post',
                    data: {pr_id:id_priv,p_id:page_id,g_id:group_id,allow:0,action_id:action_id},
                    success: function (msg) {
					    $('#load'+action_id).html(" <i>"+msg['psn']+"</i> ");
					    setInterval(function(){  $('#load'+action_id).html("");  }, 6000);
					    $('#actions').DataTable().ajax.reload();
                    }

                });
		}
    }
	function privAksi(id,dd)//pageid-group id 
	{
		  $('#id_page').val(id);
		  $('#kanantabel').css( 'display', 'block' );
		  $('#actions').DataTable().ajax.reload();
	}
	function addController()
	{
		 if($("#page_id").val()) //update
		 {
			 
			 if($("#page").val())
				 {	 
				 
					 $.ajax({
							url: '<?php echo base_url_admin(); ?>/groups/updatePage',
							cache: false,
							dataType: 'json',
							type: 'post',
							cache: false,
							beforeSend: function() {
								$('#Create2').html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
							},
							data: {id_page:$("#page_id").val(),page:$("#page").val()},
							success: function (msg) {
								$('#Create2').html(" <i>"+msg['psn']+"</i> ");
								setInterval(function(){  $('#Create2').html("");  }, 6000);
								$('#halaman').DataTable().ajax.reload();
								

							}

						});
				 }
				 else
				 {
					 alert("Nama kosong");
				 }	
		 }
         else
         {			 
				 if($("#page").val())
				 {	 
				 
					 $.ajax({
							url: '<?php echo base_url_admin(); ?>/groups/createPage',
							cache: false,
							dataType: 'json',
							type: 'post',
							cache: false,
							beforeSend: function() {
								$('#Create2').html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
							},
							data: {page:$("#page").val()},
							success: function (msg) {
								$('#Create2').html(" <i>"+msg['psn']+"</i> ");
								setInterval(function(){  $('#Create2').html("");  }, 6000);
								$('#halaman').DataTable().ajax.reload();
							}

						});
				 }
				 else
				 {
					 alert("Nama kosong");
				 }	
		 }		 
	}
	function addAction()
	{
		 if($("#action_id").val()) //update
		 {
			 
			 if($("#action").val())
				 {	 
				 
					 $.ajax({
							url: '<?php echo base_url_admin(); ?>/groups/updateAction',
							cache: false,
							dataType: 'json',
							type: 'post',
							cache: false,
							beforeSend: function() {
								$('#loadact').html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
							},
							data: {action_id:$("#action_id").val(),action:$("#action").val()},
							success: function (msg) {
								$('#loadact').html(" <i>"+msg['psn']+"</i> ");
								setInterval(function(){  $('#loadact').html("");  }, 6000);
								$('#actions').DataTable().ajax.reload();
							}

						});
				 }
				 else
				 {
					 alert("Nama kosong");
				 }	
		 }
         else
         {			 
				 if($("#action").val())
				 {	 
					 $.ajax({
							url: '<?php echo base_url_admin(); ?>/groups/createAction',
							cache: false,
							dataType: 'json',
							type: 'post',
							cache: false,
							beforeSend: function() {
								$('#loadact').html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
							},
							data: {action:$("#action").val()},
							success: function (msg) {
								$('#loadact').html(" <i>"+msg['psn']+"</i> ");
								setInterval(function(){  $('#loadact').html("");  }, 6000);
								$('#actions').DataTable().ajax.reload();
							}

						});
				 }
				 else
				 {
					 alert("Nama kosong");
				 }	
		 }		 
	}
	function deletePage(dataId)
	{
		
			 $.ajax({
					url: '<?php echo base_url_admin(); ?>/groups/deletePage',
					cache: false,
					dataType: 'json',
					type: 'post',
					cache: false,
					beforeSend: function() {
						//$('#Create2').html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
					},
					data: {id_page:dataId},
					success: function (msg) {
						
						if (msg['st'] == '0')
                        {
                            alert(msg['psn']);
							$('#halaman').DataTable().ajax.reload();
                        }
						else
						{
							 alert(msg['psn']);
							 $('#halaman').DataTable().ajax.reload();
						}

					}

				});
	}
	function deletAction(dataId)
	{
		
		var x = confirm("Are you sure you want to delete?");
		  if (x)
		  {
			  $.ajax({
					url: '<?php echo base_url_admin(); ?>/groups/deleteAction',
					cache: false,
					dataType: 'json',
					type: 'post',
					cache: false,
					beforeSend: function() {
						//$('#Create2').html("<img src='<?php echo base_url();?>assets/admin/ajax-loader.gif' />");
					},
					data: {id_action:dataId},
					success: function (msg) {
						
						if (msg['st'] == '0')
                        {
                            alert(msg['psn']);
							$('#actions').DataTable().ajax.reload();
                        }
						else
						{
							 alert(msg['psn']);
							 $('#actions').DataTable().ajax.reload();
						}

					}

				});
			  
		  }	  
		  else
			return false;
		
		
			 
	}
</script>