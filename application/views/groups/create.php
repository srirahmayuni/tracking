<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'groups/create';
    //$checkbox ="checked";
    //$pass = '*';
} else {
    $urlnya = 'groups/update';

    //($adminUsersActive) ? $checkbox ="checked" : $checkbox ="";
    //$pass = '';
}
?>
<!-- Main content -->
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <?php
            if ($has_err) {//echo $err;
                echo '<div class="row">
							<div class="col-md-12">
								<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>
							</div>
					  </div>';
            }
            ?>
        </div><!-- /.box-header -->
        <?php
        echo form_open(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
        echo form_hidden('group_id', $group_id);
        ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            &nbsp;&nbsp;
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="adminUsersFirstName">Nama *</label>
                        <input type="text" class="form-control" placeholder="Nama"  name="adminGroupFirstName" id="adminGroupFirstName"  value="<?php echo set_value('adminGroupFirstName', $adminGroupFirstName) ?>">
                    </div>
                </div><!-- /.col -->

            </div><!-- /.row -->
            <div class="box-footer">
                <!--button type="submit" class="btn btn-primary pull-right" name="adminOrdersCreate" id="adminOrdersCreate" >Simpan</button-->
                Isian dengan tanda * adalah wajib.<br />
                <input type="submit" class="btn btn-primary pull-right" name="adminGroupCreate" id="adminGroupCreate" value="Simpan">
                <!--input class="btn btn-primary btn-block btn-flat" type="submit" value="Blast" name="bidAdd"-->
            </div>
        </div><!-- /.box-body -->
        </div>
        <?php echo form_close(); ?>
        <div class="box-footer">

        </div>
    </div>
</section>
<!-- /.content -->
<?php
//$this->load->view('admin/users/js/create_js');
?>
