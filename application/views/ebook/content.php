


<div class="container">
	<div class="row">
		<div class="span12">
				
				<div class="error-actions">
					<?php 
					if($this->session->userdata('group_id')!=2)
					{	
					?>
						<button class="btn btn-primary" onClick="create()">Tambah</button>
                        <hr>
                    <?php 
					}	
					?>
					
				</div> <!-- /error-actions -->
		</div> <!-- /span12 -->
	</div> <!-- /row -->
</div> <!-- /container -->
<br>
<div class="main">
  <div class="main-inner">
    <div class="container">
	 	<?php
		if ($message) { 
		?>
		<div class="control-group">
		<label class="control-label"></label>
			<div class="controls">
			 <div class="alert lert alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Warning!</strong> <?php  echo $message; ?>
			</div>
			</div>
		</div>
		 <?php
		 }
		 ?>
		 
		<!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Ebook</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style="padding :10px;">
              <table id="table_al2" class="table table-striped table-bordered" width="100%">
               <thead>
									<tr>
										<th>No</th>
										<th>Judul File</th>
										<th>Link</th>
										
									
										<th style="width:170px;">Action</th> 
									</tr>
								</thead>
                <tbody>
				  
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
	</div>	
  </div>	
</div>	



<!-- /.content -->
<script type="text/javascript">
    
    jQuery(document).ready(function ()
        { 
         var t1 = $('#table_al2').DataTable({
          
            "processing": true,
            "serverSide": true, 
             aoColumnDefs: [
                         
                        {"aTargets": [3], "bSortable": false }
                        ],
            "ajax":{
                url :"<?php echo base_url(); ?>/ebook/data",
                type: "post"
            }
            
            
        });

    });

    function create()
    {
       
        url = "<?php echo base_url()?>ebook/create";
        $(location).attr("href", url);
    }
	
	function deleted(id)
    {
        var result = confirm("Yakin akan menghapus data ini ?");
        if (result) {
            url = "<?php echo base_url()?>ebook/delete";
            $(location).attr("href", url+"/"+id);
            
        }
    }
   
   
</script>
