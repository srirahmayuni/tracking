<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'ebook/create';
} else {
    $urlnya = 'ebook/update';
}
?>

<div class="box-header with-border">
</div><!-- /.box-header -->


<div class="main">
  <div class="main-inner">
    <div class="container">
	 	
		<!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Form Data User</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style="padding :10px;">
		
		<div class="content clearfix">
		<?php
         echo form_open_multipart(base_url().$urlnya, array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
         echo form_hidden('ebook_id', $ebook_id);
        ?>
        <div class="box-body">
		
			<?php
            if ($has_err) {//echo $err;
               //echo '<div class="row">
				//			<div class="col-md-12">
					echo			'<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>';
				//			</div>
				//	  </div>';
            }
            ?>
		
		
            <fieldset>
			
					<div class="control-group">
                                                <label for="adminUsersFirstName">Nama File*</label>

						<div class="controls">
                        <input type="text" class="form-control" placeholder="Nama"  name="file_judul" id="file_judul"  value="<?php echo set_value('file_judul', $file_judul) ?>">
						</div>
                    </div>
					
					<div class="control-group">
                                                <label for="Favicon">File Upload*</label>

						<div class="controls">
                        <input id="file_data" type="file" name="file_data" accept="image/*" onChange="validate(this)" >
							<p class="help-block">Doc/PDF.</p> 
						</div>
                    </div>
					
						<div class="control-group">
					<div class="widget-user-image">  <a href="#" id="pop1">
					  <img id="imageresource1" class="img-circle" height="60"  width="60" src="<?php echo base_url() ?>asset/ebook/<?php echo ($file_data == "") ? 'noimage.png' : $file_data?>" alt="attachment image">
					   <span> <?php echo $file_data?> </span>
					  </a>
					</div>  
					</div>
						
						
			
			
          
					<div class="panel-body">
           
            <div class="box-footer">
               
                Isian dengan tanda * adalah wajib.<br /><br />
				<input type='reset' class="btn btn-primary pull-right" value="Reset">
                <input type="submit" class="btn btn-primary pull-left" name="Create" id="Create" value="Simpan">
				
            </div>
			</div>
         
			</fieldset>
		
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
       </div>
            <!-- /widget-content --> 
		</div>	
	</div>	
  </div>	
</div>	

<!-- /.content -->
<div class="modal fade" id="imagemodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Favicon</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
                <img src="" id="imagepreview1" style="width: auto; height: auto; " >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

 $(document).ready(function () {
		
	$("#pop1").on("click", function () {
		$('#imagepreview1').attr('src', $('#imageresource1').attr('src'));
		$('#imagemodal1').modal('show');
	});
	
	 //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
 });


function validate(k) {

        size = k.files[0].size; 
		var ext = k.value.split(".");
		ext = ext[ext.length - 1].toLowerCase();
		var arrayExtensions = ["docx","pdf","doc","xls","xlsx","jpg", "jpeg","png"];

		if (arrayExtensions.lastIndexOf(ext) == -1) {
			alert("docx,pdf,doc,xls,xlsx,jpg,jpeg,png"");
			$(k).val("");
		}

		if (size > 5242880)
		{
			alert("File tidak boleh melebihi 5 MB");
			$(k).val("");

		}
	}
</script>