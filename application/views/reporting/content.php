<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script> 
<script src="<?php echo base_url()?>tracking/asset/temp/js/dataTables.fixedHeader.min.js"></script> 
<script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script> 
<link href="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
<!--datetimepicker-->
<!--CSS ADD-->
<!--datetimepicker-->
<script src="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/build/jquery.datetimepicker.full.js"></script>

<style>
th, td {
    xwhite-space: nowrap;
	text-align:center;
}


.modal {
  text-align: center;
}

@media screen and (min-width: 900px) { 
  .modal:before {
    display: inline-block;
    vertical-align: middle;
    content: " ";
    height: 100%;
  }
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}


</style>

<div id="myModal_edit" class="modal hide fade modal-dialog modal-lg" _style="width:900px;" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Edit Activity</h3>
  </div>
  <div class="modal-body">
  <p class="overflow-auto">
	<form id="editAct">
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Owner program</label>
				<div class="controls">
				<input type="hidden" class="form-control span3"  id="id_activity_progress"  name="id_activity_progress" placeholder="Owner Program" />
				<input type="text" class="form-control span3"  id="owner_program"  name="owner_program" placeholder="Owner Program" />
				</div>		
			</div>  
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Group</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="group"  name="group" placeholder="Group" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Section </label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="section"  name="section" placeholder="Section" />
				</div>		
			</div> 
	     </div>
		 
	</div>	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Activity </label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="activity"  name="activity" placeholder="Activity" />
				</div>		
			</div> 
	     </div>
		  <div class="span3">
			<div class="control-group">											
				<label class="control-label">Detail activity</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="detail_activity"  name="detail_activity" placeholder="Detail Activity" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Regional</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="regional"  name="regional" placeholder="Regional" />
				</div>		
			</div> 
	     </div>
	</div>	

	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Supporting Program</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="	supporting_program"  name="	supporting_program" placeholder="Supporting program" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Nama poi</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="nama_poi"  name="nama_poi" placeholder="Nama poi" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Divisi in charge</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="divisi_in_charge"  name="divisi_in_charge" placeholder="Divisi in charge" />
				</div>		
			</div> 
	     </div>
		 
	</div>	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">PIC</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="pic"  name="pic" placeholder="PIC" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Pic support</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="pic_support"  name="pic_support" placeholder="Pic support" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Target week</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="target_week"  name="target_week" placeholder="Target week" />
				</div>		
			</div> 
	     </div>
	</div>	
	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Progress</label>
				<div class="controls">
				<!-- <input type="text" class="form-control span3"  id="progress"  name="progress" placeholder="progress" /> -->
					
				<div class="controls">
					 <select class="form-control select2"  name = "progress" id ="progress">
						<option value="">  -- Select -- </option>
						<option value="Open">  Open </option>
						<option value="Closed"> Closed  </option>
					 </select> 												 
				</div>	
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Finish week</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="finish_week"  name="finish_week" placeholder="Finish week" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Finish Date</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="finish_date"  name="finish_date" placeholder="Finish Date" />
				</div>		
			</div> 
	     </div>
	</div>	
	<div class="row">
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Millestone progress</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="millestone_progress"  name="millestone_progress" placeholder="Millestone progress" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Blocking point</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="blocking_point"  name="blocking_point" placeholder="Blocking point" />
				</div>		
			</div> 
	     </div>
		 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Class Category</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="class_category"  name="class_category" placeholder="Class category" />
				</div>		
			</div> 
	     </div>
	</div>	
	<div class="row">	
	 <div class="span3">
			<div class="control-group">											
				<label class="control-label">Keterangan</label>
				<div class="controls">
				<input type="text" class="form-control span3"  id="keterangan"  name="keterangan" placeholder="Keterangan" />
				</div>		
			</div> 
	     </div>
	</div>	
	</form>
  </p>	  
  </div>
  <div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary" id="save_change2">Save </button>
  </div>
</div>

<div class="main">
  <div class="main-inner">
    <div class="container">


	<div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
            	<h3>Progress Tracking</h3>
            </div> 
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
				<?php $this->load->view('dashboard/progress_tracking_regional'); //tracking regional?>    
                </div>
                <!-- /widget-content --> 
                
              </div> 
            </div>
          </div>
          <!-- /widget -->
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> kurva-S</h3>
            </div> 
            <!-- /widget-header -->
            <div class="widget-content"> 
				<fieldset> 
				<div id="progress_tracking_week_regional"></div> 
				</fieldset>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
          
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Report Activity</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">

			<table id="example"  class="table table-striped table-bordered "  >
                <thead>
                  <tr>  
                    <th> Owner Program</th>
					<th > Detail Activity</th>
					<th> Progress</th>
                    <th> Target Week</th> 
                  </tr>
                </thead>
			
                <tbody>
                </tbody>
              </table>

			<a href="#" class="btn btn-info btn-lg" style="float:right;" onclick="export_reporting();">
				<span class="glyphicon glyphicon-export"></span> Export	
			</a>
 
            </div>
            <!-- /widget-content --> 
          </div>

          <!-- /widget -->
          

        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 

  </div>	
</div>	

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="myModalLabel">Change Progress</h3>
  </div>
  <div class="modal-body">
	   <p>
	   <div class="row">	
			<div class="col-md-12 ">
			   <form id="edit-profile" class="form-horizontal">
 
				<div class="control-group">				
					<label class="control-label">Select</label>
					<input type="hidden" id="kaprok_hidden">
					<div class="controls">
					<label class="radio inline">
					<input type="radio"  name="radiobtns" value="open" > Open
					</label>
					<label class="radio inline">
					<input type="radio" name="radiobtns"  value="Closed"> Closed
					</label>
					</div>			
				</div> 		
				<!-- <div class="control-group">											
					<label class="control-label">Millestone %</label>
					<div class="controls">
					<input type="text" class="form-control"  id="Millestone"  name="Millestone" placeholder="Millestone Progress" />
					</div>		
				</div>  -->
				</form>						
			</div>
		</div>
		</p>
  </div>
  <div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary" id="save_change">Save changes</button>
  </div>
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<?php $this->load->view('dashboard/dashboard_js'); ?>  

<script>

$("#save_change").click(function(){
		   
		   AS = $("#kaprok_hidden").val();
		   mille = $("#Millestone").val();
		   
		   r = $('input[name=radiobtns]:checked').val();
		   approv2(AS,r,mille);
		   //$('#example').DataTable().ajax.reload();
	  });
$("#save_change2").click(function(){
		   
		   id_activity_progress = $("#id_activity_progress").val();
		  
		   update(id_activity_progress);
		   //$('#example').DataTable().ajax.reload();
	  });
$(document).ready(function() {
			
			
			 $('#finish_date').datetimepicker({
				//format: "Y-m-d h:m:s"
				format: "Y-m-d h:m:s"
				//format: "d-m-Y h:m:s"
		     }); 
			
			 $('#Millestone').on('input', function (event) { 
				this.value = this.value.replace(/[^0-9]/g, '');
			 });
			  $('#millestone_progress').on('input', function (event) { 
				this.value = this.value.replace(/[^0-9]/g, '');
			 });

			 $('#example thead tr').clone(true).appendTo( '#example thead' ); 
			 $('#example thead tr:eq(1) th').each( function (i) {
			 var title = $(this).text();
			
					df = '<input style="width: 80px;"  type="text" placeholder="'+title+'" />';			
					$(this).html( df );
			  			
			});
			
				var otable = $('#example').removeAttr('width').DataTable({
				//"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				//		"t"+
				//		"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				"autoWidth" : false,
				//"oLanguage": {
				//	"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				//},
				// bAutoWidth: false, 
				  
				 //"fixedColumns": true,
				 "scrollY": 630,
                 "scrollX": true,
				 "processing": true,
				 "serverSide": true, 
				 "aoColumnDefs": [
						//{"aTargets" : [0],  "sClass":  "details-control"}, 
						//{ "bVisible": false, "aTargets": [ 1 ] },
						  {"aTargets": [0], "bSortable": false }, 
						  {"aTargets": [1], "bSortable": false,  },
						  {"aTargets": [2], "bSortable": false,   },
						  {"aTargets": [3], "bSortable": false,   }
						 ],
			      select: {
						style:    'os',
						selector: 'td:first-child'
					}, 
				//order: [[ 1, 'asc' ]]			 
						 
				"ajax":{
					url :"<?php echo base_url(); ?>reporting/data", 
					type: "post",
					"data": function ( d ) {
								//d.active = $('#selec_filter').val();
							    //d.awal = $('#PROGRAM_START_S').val();
						        //d.akhir = $('#PROGRAM_END_S').val();
							    //d.filter = $('#hiden_text_search').val();
					},
					"dataSrc": function ( json ) {
						
						return json.data;
					},  
					error: function(){  // error handling
						
					},
				},	
				

				
			

		});	
				 otable.columns().every(function () {
					var table = this;
					$('input', this.header()).on('keyup change', function () {
						if (table.search() !== this.value) {
							   table.search(this.value).draw();
						}
					});
				});
				
			
	
} );

function approv(id,pr)
{
	$('#kaprok_hidden').val("");
	$('#Millestone').val("");
	$('#kaprok_hidden').val(id);
	if(pr=="open")
	{
		$( "input[value='open']" ).prop( "checked", true );
	}
    
	else if(pr=="Closed" || pr=="closed" || pr=="CLOSED"  )
    {
		$( "input[value='Closed']" ).prop( "checked", true );
	}
    else
    {
		$( "input[value='open']" ).prop( "checked", true );
	}		
	
	$('#myModal').modal({
		keyboard: true,
		backdrop: "static",
		show:true
	});  
   
}

function update(id)
{
	var input_data = new Object();

        input_data.id = id;
        input_data.data = $("#editAct").serializeArray();
        
				jQuery.ajax(
                {
                    url: "<?php echo base_url()?>activity_progress/update",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {

                        if (data.res == 0 )
                        {
                            alert("Gagal Merubah Progress");
                        } 
						else
                        {
                            alert("Merubah Progress Sukses");
						    $('#myModal_edit').modal('hide');
							$('#example').DataTable().ajax.reload();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}

function approv2(id,status,mille)
{
        var input_data = new Object();

        input_data.id = id;
        input_data.status = status;
        input_data.mille = mille;
        jQuery.ajax(
                {
                    url: "<?php echo base_url()?>activity_progress/detail_approv",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {

                        if (data.res == 0 )
                        {
                            // tidak bisa dihapus
                            alert("Gagal Merubah Progress");


                        } 
						else
                        {
                            alert("Merubah Progress Sukses");
						    $('#myModal').modal('hide');
							$('#example').DataTable().ajax.reload();
                           
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}

function edit(id)
{
		
		
		var input_data = new Object();
        input_data.id = id;
        
		jQuery.ajax(
		{
			url: "<?php echo base_url()?>activity_progress/editData",
			type: "POST",
			data: input_data,
			dataType: "json",
			success: function (data)
			{ 

				if ( data.res == 0 )
				{
					// tidak bisa dihapus
					alert("Simpan gagal ");
				} 
				else
				{
				    
					    $('#id_activity_progress').val(data.data.id_activity_progress);
					    $('#owner_program').val(data.data.owner_program);
					    $('#group').val(data.data.group);
					    $('#section').val(data.data.section);
					    $('#activity').val(data.data.activity);
					    $('#detail_activity').val(data.data.detail_activity);
					    $('#regional').val(data.data.regional);
					    $('#supporting_program').val(data.data.supporting_program);
					    $('#nama_poi').val(data.data.nama_poi);
					    $('#divisi_in_charge').val(data.data.divisi_in_charge);
					    $('#pic').val(data.data.pic);
					    $('#pic_support').val(data.data.pic_support);
					    $('#target_week').val(data.data.target_week);
					    $('#progress').val(data.data.progress);
					    $('#finish_week').val(data.data.finish_week);
					    $('#finish_date').val(data.data.finish_date);
					    $('#millestone_progress').val(data.data.millestone_progress);
					    $('#blocking_point').val(data.data.blocking_point);
					    $('#class_category').val(data.data.class_category);
					    $('#keterangan').val(data.data.keterangan);
						
						 $('#myModal_edit').modal({
						keyboard: true,
						backdrop: "static",
						show:true
					 });  
				}

			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus);
			}
		});			
}
	 
function export_reporting()
{
	window.open("<?php echo site_url('reporting/export_reporting'); ?>");
}

function refresh()
{
	 $('input').val("");	
	 $('#example').DataTable().ajax.reload();
	 location.reload();
}
</script>
			