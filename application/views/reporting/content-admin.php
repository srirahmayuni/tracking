<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script> 
<script src="<?php echo base_url()?>tracking/asset/temp/js/dataTables.fixedHeader.min.js"></script> 
<script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script> 
<link href="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
<!--datetimepicker-->
<!--CSS ADD-->
<!--datetimepicker-->
<script src="<?php echo base_url(); ?>asset/temp/plugins/datetimepicker/build/jquery.datetimepicker.full.js"></script>

<style>
th, td {
    xwhite-space: nowrap;
	text-align:center;
}


.modal {
  text-align: center;
}

@media screen and (min-width: 900px) { 
  .modal:before {
    display: inline-block;
    vertical-align: middle;
    content: " ";
    height: 100%;
  }
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}


</style>

<div class="main">
  <div class="main-inner">
    <div class="container">


	<div class="row">
        <div class="span6">


          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>Progress Tracking</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
				<?php $this->load->view('dashboard/progress_tracking_reporting'); //tracking regional?>    
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
		  <!-- /widget -->

		  <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>Progress Tracking Network</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
				<?php $this->load->view('dashboard/progress_tracking_reporting_network'); //tracking regional?>    
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
		  <!-- /widget -->

		  <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>Progress Tracking Sales</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
				<?php $this->load->view('dashboard/progress_tracking_reporting_sales'); //tracking regional?>    
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
		  <!-- /widget -->

		  <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>Progress Tracking Information Technology</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
				<?php $this->load->view('dashboard/progress_tracking_reporting_it'); //tracking regional?>    
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
		  <!-- /widget -->
		  

        </div>
		<!-- /span6 -->
		
        <div class="span6">

		<div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> kurva-S</h3>
            </div> 
            <!-- /widget-header -->     
            <div class="widget-content"> 
				<fieldset> 
				<div id="progress_tracking_week"></div> 
				</fieldset>
            </div>
            <!-- /widget-content --> 
          </div>
		
		<div class="widget widget-table action-table">
			<div class="widget-header"> <i class="icon-th-list"></i>
				<h3>Summary</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">

				<table class="table table-striped table-bordered" style="padding:10px;">
					<thead>
						<tr>
							<th style="background:red;color:azure;text-align: center;width:30%">Activities [Closed / Total]</th> 
							<th style="background:red;color:azure;text-align: center;width:50%">Directorate</th>
							<th style="background:red;color:azure;text-align: center;width:20%">Achievement</th>
						</tr>
					</thead> 
					<tbody>
						<?php
						foreach($directorates as $item) {
						?>
						<tr>
							<td style="text-align: center;"><?php echo $item['jumlah_activity_done']?> / <?php echo $item['jumlah_activity']?></td>
							<td style="text-align: center;"><?php echo $item['directorate']?></td> 
							<td style="text-align: center;"><?php echo $total = round((($item['jumlah_activity_done']/$item['jumlah_activity']) * 100),2) ?> %</td>
						</tr>
						<?php
						} ?>
					</tbody>
				</table>

				<table class="table table-striped table-bordered" style="padding:10px;">
					<thead>
						<tr>
							<th style="background:#f16a1b;color:azure;text-align: center;width:30%">Activities [Closed / Total]</th> 
							<th style="background:#f16a1b;color:azure;text-align: center;width:50%">Divisi</th>
							<th style="background:#f16a1b;color:azure;text-align: center;width:20%">Achievement</th>
						</tr>
					</thead>
					<tbody>
					<?php
						foreach($owners as $item) {
						?>
						<tr>
							<td style="text-align: center;"><?php echo $item['jumlah_activity_done']?> / <?php echo $item['jumlah_activity']?></td>
							<td style="text-align: center;"><?php echo $item['owner_program']?></td> 
							<td style="text-align: center;"><?php echo $total = round((($item['jumlah_activity_done']/$item['jumlah_activity']) * 100),2) ?> %</td>
						</tr>
						<?php
						} ?>
					</tbody>
				</table>

				<table class="table table-striped table-bordered" style="padding:10px;">
					<thead>
						<tr>
							<th style="background:green;color:azure;text-align: center">Activities [Closed / Total]</th> 
							<th style="background:green;color:azure;text-align: center">Domain</th>
							<th style="background:green;color:azure;text-align: center">Achievement</th>
						</tr>
					</thead>
					<tbody>
					<?php
						foreach($sections as $item) {
						?>
						<tr>
							<td style="text-align: center;"><?php echo $item['jumlah_activity_done']?> / <?php echo $item['jumlah_activity']?></td>
							<td style="text-align: center;"><?php echo $item['section']?></td> 
							<td style="text-align: center;"><?php echo $total = round((($item['jumlah_activity_done']/$item['jumlah_activity']) * 100),2) ?> %</td>
						</tr>
						<?php
						} ?>
					</tbody>
				</table>
			</div>
			<!-- /widget-content -->
		</div>


		<!-- <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Report Activity</h3>
            </div>
            <div class="widget-content">

			<table id="example"  class="table table-striped table-bordered "  >
                <thead>
                  <tr>  
                    <th> Owner Program</th>
					<th > Detail Activity</th>
					<th> Progress</th>
                    <th> Target Week</th> 
                  </tr>
                </thead>
			
                <tbody>
                </tbody>
              </table>

			<a href="#" class="btn btn-info btn-lg" style="float:right;" onclick="export_reporting();">
				<span class="glyphicon glyphicon-export"></span> Export	
			</a>
 
            </div>
          </div> -->

          <!-- /widget -->
          

        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 

  </div>	
</div>	


<script src="https://code.highcharts.com/highcharts.js"></script>
<?php $this->load->view('dashboard/dashboard_js'); ?>  

<script>

$("#save_change").click(function(){
		   
		   AS = $("#kaprok_hidden").val();
		   mille = $("#Millestone").val();
		   
		   r = $('input[name=radiobtns]:checked').val();
		   approv2(AS,r,mille);
		   //$('#example').DataTable().ajax.reload();
	  });
$("#save_change2").click(function(){
		   
		   id_activity_progress = $("#id_activity_progress").val();
		  
		   update(id_activity_progress);
		   //$('#example').DataTable().ajax.reload();
	  });
$(document).ready(function() {
			
			
			 $('#finish_date').datetimepicker({
				//format: "Y-m-d h:m:s"
				format: "Y-m-d h:m:s"
				//format: "d-m-Y h:m:s"
		     }); 
			
			 $('#Millestone').on('input', function (event) { 
				this.value = this.value.replace(/[^0-9]/g, '');
			 });
			  $('#millestone_progress').on('input', function (event) { 
				this.value = this.value.replace(/[^0-9]/g, '');
			 });

			 $('#example thead tr').clone(true).appendTo( '#example thead' ); 
			 $('#example thead tr:eq(1) th').each( function (i) {
			 var title = $(this).text();
			
					df = '<input style="width: 80px;"  type="text" placeholder="'+title+'" />';			
					$(this).html( df );
			  			
			});
			
				var otable = $('#example').removeAttr('width').DataTable({
				//"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				//		"t"+
				//		"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				"autoWidth" : false,
				//"oLanguage": {
				//	"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				//},
				// bAutoWidth: false, 
				  
				 //"fixedColumns": true,
				 "scrollY": 630,
                 "scrollX": true,
				 "processing": true,
				 "serverSide": true, 
				 "aoColumnDefs": [
						//{"aTargets" : [0],  "sClass":  "details-control"}, 
						//{ "bVisible": false, "aTargets": [ 1 ] },
						  {"aTargets": [0], "bSortable": false }, 
						  {"aTargets": [1], "bSortable": false,  },
						  {"aTargets": [2], "bSortable": false,   },
						  {"aTargets": [3], "bSortable": false,   }
						 ],
			      select: {
						style:    'os',
						selector: 'td:first-child'
					}, 
				//order: [[ 1, 'asc' ]]			 
						 
				"ajax":{
					url :"<?php echo base_url(); ?>reporting/data", 
					type: "post",
					"data": function ( d ) {
								//d.active = $('#selec_filter').val();
							    //d.awal = $('#PROGRAM_START_S').val();
						        //d.akhir = $('#PROGRAM_END_S').val();
							    //d.filter = $('#hiden_text_search').val();
					},
					"dataSrc": function ( json ) {
						
						return json.data;
					},  
					error: function(){  // error handling
						
					},
				},	
				

				
			

		});	
				 otable.columns().every(function () {
					var table = this;
					$('input', this.header()).on('keyup change', function () {
						if (table.search() !== this.value) {
							   table.search(this.value).draw();
						}
					});
				});
				
			
	
} );

function approv(id,pr)
{
	$('#kaprok_hidden').val("");
	$('#Millestone').val("");
	$('#kaprok_hidden').val(id);
	if(pr=="open")
	{
		$( "input[value='open']" ).prop( "checked", true );
	}
    
	else if(pr=="Closed" || pr=="closed" || pr=="CLOSED"  )
    {
		$( "input[value='Closed']" ).prop( "checked", true );
	}
    else
    {
		$( "input[value='open']" ).prop( "checked", true );
	}		
	
	$('#myModal').modal({
		keyboard: true,
		backdrop: "static",
		show:true
	});  
   
}

function update(id)
{
	var input_data = new Object();

        input_data.id = id;
        input_data.data = $("#editAct").serializeArray();
        
				jQuery.ajax(
                {
                    url: "<?php echo base_url()?>activity_progress/update",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {

                        if (data.res == 0 )
                        {
                            alert("Gagal Merubah Progress");
                        } 
						else
                        {
                            alert("Merubah Progress Sukses");
						    $('#myModal_edit').modal('hide');
							$('#example').DataTable().ajax.reload();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}

function approv2(id,status,mille)
{
        var input_data = new Object();

        input_data.id = id;
        input_data.status = status;
        input_data.mille = mille;
        jQuery.ajax(
                {
                    url: "<?php echo base_url()?>activity_progress/detail_approv",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {

                        if (data.res == 0 )
                        {
                            // tidak bisa dihapus
                            alert("Gagal Merubah Progress");


                        } 
						else
                        {
                            alert("Merubah Progress Sukses");
						    $('#myModal').modal('hide');
							$('#example').DataTable().ajax.reload();
                           
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
}

function edit(id)
{
		
		
		var input_data = new Object();
        input_data.id = id;
        
		jQuery.ajax(
		{
			url: "<?php echo base_url()?>activity_progress/editData",
			type: "POST",
			data: input_data,
			dataType: "json",
			success: function (data)
			{ 

				if ( data.res == 0 )
				{
					// tidak bisa dihapus
					alert("Simpan gagal ");
				} 
				else
				{
				    
					    $('#id_activity_progress').val(data.data.id_activity_progress);
					    $('#owner_program').val(data.data.owner_program);
					    $('#group').val(data.data.group);
					    $('#section').val(data.data.section);
					    $('#activity').val(data.data.activity);
					    $('#detail_activity').val(data.data.detail_activity);
					    $('#regional').val(data.data.regional);
					    $('#supporting_program').val(data.data.supporting_program);
					    $('#nama_poi').val(data.data.nama_poi);
					    $('#divisi_in_charge').val(data.data.divisi_in_charge);
					    $('#pic').val(data.data.pic);
					    $('#pic_support').val(data.data.pic_support);
					    $('#target_week').val(data.data.target_week);
					    $('#progress').val(data.data.progress);
					    $('#finish_week').val(data.data.finish_week);
					    $('#finish_date').val(data.data.finish_date);
					    $('#millestone_progress').val(data.data.millestone_progress);
					    $('#blocking_point').val(data.data.blocking_point);
					    $('#class_category').val(data.data.class_category);
					    $('#keterangan').val(data.data.keterangan);
						
						 $('#myModal_edit').modal({
						keyboard: true,
						backdrop: "static",
						show:true
					 });  
				}

			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus);
			}
		});			
}
	 
function export_reporting()
{
	window.open("<?php echo site_url('reporting/export_reporting'); ?>");
}

function refresh()
{
	 $('input').val("");	
	 $('#example').DataTable().ajax.reload();
	 location.reload();
}
</script>
			