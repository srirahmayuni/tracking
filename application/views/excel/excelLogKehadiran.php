<?php
date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
header("Content-type: application/xls");
header("Content-Type: application/download"); 
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=Log_Attendance_Schedule_Naru_2019_Updated_at_" . date('d_F_Y') . ".xls");

$date_now = date('Y-m-d H:i:s');
$title_date = date("d F Y H:i:s", strtotime($date_now));
 
echo "Reports of Attendance Schedule | " . $title_date . "<br><br>";
echo "<table border='1' cellpadding='0' cellspacing='0'>
			<tr> 
				<th>No</th>	
				<th>Name</th>						
				<th>Shift</th>			
				<th>Standby Date</th> 
				<th>Activity</th>
				<th>Event</th>
				<th>Created at</th> 
			</tr>";
$i = 1; 
foreach ($data as $loc): 
    echo "<tr>" .
	"<td valign=top>" . $i . "</td>
				<td valign=top>" . $loc->nama . "</td>	
				<td valign=top>" . $loc->shift . "</td>	
				<td valign=top>'" . date("d F Y H:i:s", strtotime($loc->tanggal)) . "</td>
				<td style='width:50%' valign=top>" . $loc->activity . "</td> 
				<td valign=top>" . $loc->event . "</td>
				<td valign=top>'" . date("d F Y H:i:s", strtotime($loc->created_date)) . "</td> 	 	
		  </tr>"; 
    $i++;  
endforeach;
echo "</table>";
?>