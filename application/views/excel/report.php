<?php

date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
header("Content-type: application/xls");
header("Content-Type: application/download");
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=Reporting_Activity " . date('Y-m-d H:i:s') . ".xls");

$tgl = date('Y-m-d H:i:s'); 

echo "Reports of  Activity Progress per " . $tgl . "<br><br>";
echo "<table border='1' cellpadding='0' cellspacing='0'>
			<tr>
				<th>No</th>
				<th>Directorate</th>
				<th>Group</th>
				<th>Owner Program</th>
				<th>Section</th>
				<th>Activity</th>
				<th>Activity Detail</th>
				<th>Regional</th>
				<th>Supporting Program</th>
				<th>Nama POI</th>
				<th>Divisi In Charge</th>
				<th>PIC</th>
				<th>PIC Support</th>
				<th>Target Week</th>
				<th>Progress</th> 
			</tr>";
$i = 1;
foreach ($data as $loc):
    echo "<tr>" .
	"<td valign=top>" . $i . "</td>
				<td valign=top>" . $loc->directorate . "</td>	
				<td valign=top>" . $loc->group . "</td>
				<td valign=top>" . $loc->owner_program . "</td>
				<td valign=top>" . $loc->section . "</td>
				<td valign=top>" . $loc->activity . "</td>
				<td valign=top>" . $loc->detail_activity. "</td>
				<td valign=top>" . $loc->regional . "</td>
				<td valign=top align=left>" . $loc->supporting_program . "</td>
				<td valign=top>" . $loc->nama_poi . "</td>
				<td valign=top>" . $loc->divisi_in_charge . "</td>
				<td valign=top>" . $loc->pic. "</td> 
				<td valign=top>" . $loc->pic_support . "</td>	
				<td valign=top>" . $loc->target_week . "</td>	
				<td valign=top>" . $loc->progress . "</td>	 
			</tr>";
    $i++;
endforeach;
echo "</table>";
?>