<?php

date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
header("Content-type: application/xls");
header("Content-Type: application/download");
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=LOG_Activity_Progress " . date('Y-m-d H:i:s') . ".xls");

$tgl = date('Y-m-d H:i:s');


echo "Reports of  Log user for Activity Progress per " . $tgl . "<br><br>";
echo "<table border='1' cellpadding='0' cellspacing='0'>
			<tr>
				<th>No</th>
				<th>Detail Activity</th>
				
				<th>Activity</th>
				<th>User Activity</th>
				<th>Date</th>
			</tr>";
$i = 1;
foreach ($data as $loc):
    echo "<tr>" .
    "<td valign=top>" . $i . "</td>
				<td valign=top>" . $loc->detail_activity. "</td>
				
				<td valign=top>" . $loc->status . "</td>	
				<td valign=top>" . $loc->user . "</td>	
				<td valign=top>" . $loc->created_date . "</td>	
			</tr>";
    $i++;
endforeach;
echo "</table>";
?>