<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'accesspage/create';
} else {
    $urlnya = 'accesspage/update';
}
?>
<!-- Main content -->
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <?php
            if ($has_err) { //echo 'SSS'.$err;
                echo '<div class="row">
							<div class="col-md-12">
								<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>
							</div>
					  </div>';
            }
            ?>
        </div><!-- /.box-header -->
        <?php
        echo form_open(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
        echo form_hidden('privilege_page_id', $privilege_page_id);
       
        ?>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="adminUsersGroup">Group *</label>
                        <select class="form-control select2" style="width: 100%;" name = "group_id" id ="group_id">
                            <?php
                            echo '<option value="">Pilih Group</option>';
                            foreach ($dropdownGroups as $row) {
                                if ($row->group_id == $group_id)
                                    echo '<option value="' . $row->group_id . '" selected="selected" >' . $row->name . '</option>';
                                else
                                    echo '<option value="' . $row->group_id . '">' . $row->name . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="halaman">Halaman *</label>
                        <select class="form-control select2" style="width: 100%;" name = "page_id" id ="page_id">
                            <?php
                            echo '<option value="">Pilih Halaman</option>';
                            foreach ($dropdownHalaman as $row) {
                                if (isset($page_id) && $row['page_id'] == $page_id)
                                    echo '<option value="' . $row['page_id'] . '" selected="selected" >' . $row['controller'] . '</option>';
                                else
                                    echo '<option value="' . $row['page_id'] . '">' . $row['controller'] . '</option>';
                            }
                            ?>
                        </select>

                    </div>
                   
                    <br />&nbsp;


                </div><!-- /.col -->
                <div class="col-md-6">
                     <div class="form-group">
                        <label for="allow">Allow </label>
                        <select class="form-control select2" style="width: 100%;" name="allow" id ="allow">
							<?php 
							if($allow==0)
							{
								echo '<option value="0" selected>0</option><option value="1" >1</option>';
								
							}
							else if($allow==1)
							{
								echo '<option value="1" selected>1</option><option value="0" >0</option>';
								
							}
							else
							{
								echo '<option value="1" >1</option><option value="0">0</option>';
							} 
							
							?>
						</select>
						
						
                    </div>
                    <div class="form-group">
                        
                    </div>						
                    

                </div>
            </div>
        </div><!-- /.row -->
        <div class="box-footer">
            <!--button type="submit" class="btn btn-primary pull-right" name="adminOrdersCreate" id="adminOrdersCreate" >Simpan</button-->
            Isian dengan tanda * adalah wajib.<br />
            <input type="submit" class="btn btn-primary pull-right" name="Crate" id="Crate" value="Simpan">
            <!--input class="btn btn-primary btn-block btn-flat" type="submit" value="Blast" name="bidAdd"-->
        </div>
    </div><!-- /.box-body -->
    <?php echo form_close(); ?>

</div>
</section>
<!-- /.content -->
<script>
 //Initialize Select2 Elements
  $(function () {
    $(".select2").select2();
  });
</script>
