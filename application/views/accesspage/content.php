
<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>	
</div>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">    
                    <?php
                    if (is_authorized('accesspage', 'create')) {
                        ?>
                        <button class="btn btn-primary" onClick="createPrivPage()">Tambah</button>
                        <br>
                        <hr>
                        <br>
                        <?php
                    }
                    ?>	
                    <div class="col-xs-12 table-responsive">
                        <br />
						
						<div class="panel panel-default">
						
						<div class="panel-heading">
                            DataTables Advanced Tables
                        </div>
						
						<div class="panel-body">
							<table id="table_aksespage" width="100%"  class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Group</th>
										<th>Page</th>
										<th>Allow</th>
										<th style="width:170px;">Action</th> 
									</tr>
								</thead>
								<tbody>
								</tbody>

							</table>  
						  </div>	
						</div>	
                    </div>	
                </div>
            </div>
           
        </div><!-- /.box-body -->

    </div>
</section>
<!-- /.content -->
<script type="text/javascript">
    
    jQuery(document).ready(function ()
        { 
                
      
         var t1 = $('#table_aksespage').DataTable({
          
           "processing": true,
           "serverSide": true, 
             aoColumnDefs: [
                         
                        {"aTargets": [4], "bSortable": false }
                        ],
            "ajax":{
                url :"<?php echo base_url_admin(); ?>/accesspage/data",
                type: "post"
            }
            
            
        });

    });

    function createPrivPage()
    {
       
        url = "<?php echo site_url(folderBack().'/accesspage/create'); ?>";
        $(location).attr("href", url);
    }

   
</script>
