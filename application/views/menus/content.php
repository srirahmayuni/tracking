<?php
//$has_recordMaster	= isset($data) && is_array($data) && count($data);
?>
<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>	
</div>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">    
                    <?php
                    if (is_authorized('menus', 'create')) {
                        ?>
                        <button class="btn btn-primary" onClick="createMenu()">Tambah</button>
                        <br>
                        <hr>
                        <br>
                        <?php
                    }
                    ?>	
                    <div class="col-xs-12 table-responsive">
                        <br />
                        <table id="table_adminMenus" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Menu</th>
                                    <th>Parent</th>
                                    <th>Action</th> 
                                    <th>Page</th>
                                    <th>Urutan</th>
                                    <th style="width:200px;">Action</th> 
                                </tr>
                            </thead>

                        </table>  
                    </div>	
                </div>
            </div>
           
        </div><!-- /.box-body -->

    </div>
</section>
<!-- /.content -->
<script type="text/javascript">
    var table;
    $(document).ready(function ()
    {
        t = $('#table_adminMenus').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url_admin(); ?>/menus/getData"
                    //"order": [[ 1, "desc" ]]


        });

    });

    function createMenu()
    {
        url = "<?php echo site_url(''.folderBack().'/menus/create'); ?>";
        $(location).attr("href", url);
    }

    function Delete(id, name)
    {
        var result = confirm("Yakin hapus menus " + name + "?");
        if (result) {
            //Logic to delete the item
            Delete2(id, name);
        }
    }
    function Delete2(id, name)
    {
        var input_data = new Object();

        input_data.menu_id = id;
        jQuery.ajax(
                {
                    url: "<?php echo site_url(''.folderBack().'/menus/delete'); ?>",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {
                        if (data.res == '0')
                        {
                          	alert("Menu " + name + " tidak bisa di hapus, \n"+data.mesg+"");
                        } else
                        {
                            alert("Menu " + name + " telah di hapus");
                            location.reload();

                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
    }

    function Update(id)
    {
        url = "<?php echo site_url(''.folderBack().'/menus/update'); ?>/" + id;
        $(location).attr("href", url);
    }
</script>
