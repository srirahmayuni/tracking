<?php
$has_err = isset($err) && !empty($err);
//if ($action) {
//    $urlnya = 'profile/create';
//} else {
    $urlnya = 'profile/index';
//}
?>
<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/plugins/iCheck/all.css">
<script src="<?php echo base_url()?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<!--<script src="<?php echo base_url()?>assets/admin/dist/js/app.min.js"></script>-->

<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url()?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

 
<!-- Main content -->
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <?php
            if ($has_err) { //echo 'SSS'.$err;
                echo '<div class="row">
							<div class="col-md-12">
								<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>
							</div>
					  </div>';
            }
            ?>
        </div><!-- /.box-header -->
        <?php
          echo form_open_multipart(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
          echo form_hidden('id_identitas', $id_identitas);

        ?>
        <div class="box-body">
            <div class="row">
                
				
				<div class="col-md-12">
						  <div class="box box-info">
							<div class="box-header">
							  <h3 class="box-title">Profile perusahaan *
								
							  </h3>
							  <!-- tools box -->
							  <div class="pull-right box-tools">
								<button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								  <i class="fa fa-minus"></i></button>
								
							  </div>
							  <!-- /. tools -->
							</div>
							<!-- /.box-header -->
							<div class="box-body pad">
									<textarea  id="profile_perusahaan" name="profile_perusahaan" rows="10" cols="80"><?php echo $dataId->profile_perusahaan; ?>
									</textarea>
							 
							</div>
						  </div>
						  <!-- /.box -->
				</div>
            </div>
        </div><!-- /.row -->
        <div class="box-footer">
          
            Isian dengan tanda * adalah wajib.<br />
            <input type="submit" class="btn btn-primary pull-right" name="Simpan" id="Simpan" value="Simpan">
            
        </div>
    </div><!-- /.box-body -->
    <?php echo form_close(); ?>

</div>
</section>

<!-- /.content -->

<script>
$(document).ready(function () {
   
	    CKEDITOR.replace('profile_perusahaan');
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5(); 				
	
	
});

</script>
<?php 
//$this->load->view('set/tarif/adminOrdersNumber_js');
 ?>
