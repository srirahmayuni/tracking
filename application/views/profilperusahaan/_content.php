
<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>	
</div>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">    
                    <?php
                    if (is_authorized('ansu', 'create')) {
                        ?>
                        <button class="btn btn-primary" onClick="create()">Tambah</button>
                        <br>
                        <hr>
                        <br>
                        <?php
                    }
                    ?>	
                    <div class="col-xs-12 table-responsive">
                        <br />
                        <table id="table_as" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Asuransi</th>
                                    <th>Ket.</th>
                                    <th>Logo</th>
                                    <th>Aktif</th>
                                    <th style="width:140px;">Action</th> 
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>  
                    </div>	
                </div>
            </div>
           
        </div><!-- /.box-body -->

    </div>
</section>
<!-- /.content -->
<script type="text/javascript">
    var table;
    $(document).ready(function ()
    {
       
         var t1 = $('#table_as').DataTable({
		
		   "processing": true,
		   "serverSide": true, 
		     aoColumnDefs: [
						 
						{"aTargets": [5], "bSortable": false }
						],
			"ajax":{
				url :"<?php echo base_url_admin(); ?>/ansu/data",
				type: "post"
			}
			
			
		});

    });

    function create()
    {
       
        url = "<?php echo site_url(folderBack().'/ansu/create'); ?>";
        $(location).attr("href", url);
    }

    function Delete(id,nama)
    {
        var result = confirm("Yakin hapus  "+nama+"");
        if (result) {
            //Logic to delete the item
            url = "<?php echo site_url(folderBack().'/ansu/delete'); ?>/" + id;
			 $(location).attr("href", url);
        }
    }
   
</script>
