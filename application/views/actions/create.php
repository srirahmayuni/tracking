<?php
$has_err = isset($err) && !empty($err);
if ($action) {
    $urlnya = 'actions/create';
} else {
    $urlnya = 'actions/update';
}
?>
<!-- Main content -->
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <?php
            if ($has_err) {//echo $err;
                echo '<div class="row">
							<div class="col-md-12">
								<div class="alert alert-danger alert-dismissible" role="alert">
									<span class="sr-only">Error:</span>
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                foreach ($pesan_error as $error):
                    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
                    echo $error;
                    echo "<br />";
                endforeach;
                echo '          </div>
							</div>
					  </div>';
            }
            ?>
            <h3 class="box-title">Aksi</h3>
        </div><!-- /.box-header -->
        <?php
        echo form_open(siteAdminUrl($urlnya), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
        echo form_hidden('action_id', $action_id);
        ?>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="adminUsersFirstName">Nama *</label>
                        <input type="text" class="form-control" placeholder="Nama"  name="aksi" id="aksi"  value="<?php echo set_value('aksi', $aksi) ?>">
                    </div>
                </div><!-- /.col -->

            </div><!-- /.row -->
            <div class="box-footer">
                <!--button type="submit" class="btn btn-primary pull-right" name="adminOrdersCreate" id="adminOrdersCreate" >Simpan</button-->
                Isian dengan tanda * adalah wajib.<br />
                <input type="submit" class="btn btn-primary pull-right" name="Create" id="Create" value="Simpan">
                <!--input class="btn btn-primary btn-block btn-flat" type="submit" value="Blast" name="bidAdd"-->
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
        <div class="box-footer">

        </div>
    </div>
</section>
<!-- /.content -->
<?php
//$this->load->view('admin/users/js/create_js');
?>
