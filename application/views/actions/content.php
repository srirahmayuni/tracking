<?php
$has_recordMaster = isset($data) && is_array($data) && count($data);
?>

<div class="box-body">
    <?php
    if ($message) {

        echo '<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible" role="alert">
								<span class="sr-only">Error:</span>
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $message;
        //echo '<br />';
        echo '      </div>
						</div>
				  </div>';
    }
    ?>	
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title">Aksi</h1>
                </div><!-- /.box-header -->

                <div class="box-body">    
                    <?php
                    if (is_authorized('actions', 'create')) {
                        ?>
                        <button class="btn btn-primary" onClick="create()">Tambah</button>
                        <br>
                        <hr>
                        <br>
                        <?php
                    }
                    ?>	
                   
                    <div class="col-xs-12 table-responsive">
                        <br />
                        <table id="table_pages" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:20px;">No</th>
                                    <th>Name</th>
                                    <th style="width:200px;">Action</th> 
                                </tr>
                            </thead>

                        </table>  
                    </div>	
                    
                </div>
            </div>
            
        </div><!-- /.box-body -->

    </div>
</section>
<!-- /.content -->
<script type="text/javascript">
    var table;

    $(document).ready(function ()
    {
        t = $('#table_pages').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url_admin(); ?>/actions/getData",
            "order": [[1, "asc"]]


        });

    });

    function create()
    {
        //alert('create');
        url = "<?php echo site_url(folderBack().'/actions/create'); ?>";
        $(location).attr("href", url);
    }

    function Delete(users_id, first_name)
    {
        var result = confirm("Yakin hapus pengguna " + first_name + "?");
        if (result) {
            //Logic to delete the item
            Delete2(users_id, first_name);
        }
    }
    function Delete2(users_id, first_name)
    {
        var input_data = new Object();

        input_data.action_id = users_id;
        jQuery.ajax(
                {
                    url: "<?php echo site_url(folderBack().'/actions/delete'); ?>",
                    type: "POST",
                    data: input_data,
                    dataType: "json",
                    success: function (data)
                    {
                        if (data.response == false)
                        {
                            alert("Aksi " + first_name + " tidak bisa di hapus!");
                        } else
                        {
                            alert("Aksi " + first_name + " telah di hapus");
                            location.reload();

                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                });
    }

    function adminUsersUpdate(users_id)
    {
        url = "<?php echo site_url(folderBack().'/pages/update'); ?>/" + users_id;
        $(location).attr("href", url);
    }
</script>