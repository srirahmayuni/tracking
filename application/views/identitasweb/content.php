<?php
//$has_records	= isset($dropdownTranstypes) && is_array($dropdownTranstypes) && count($dropdownTranstypes);
?>
<!-- Content Header (Page header) -->
<!--section class="content-header">
  <h1>
        &nbsp;
  </h1>
</section-->

<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Profile Website</h3>
        </div>
        <?php 
        echo form_open_multipart(site_url(folderBack().'/identitasweb/index'), array('method' => 'post', 'autocomplete' => 'off', 'class' => 'form-delivery'));
        echo form_hidden('id_identitas', $dataId->id_identitas);
        ?>
        <div class="box-body">
            <?php
            if ($messageProf) {

                echo '<div class="row">
									<div class="col-md-12">
										<div class="alert alert-danger alert-dismissible" role="alert">
											<span class="sr-only">Error:</span>
											<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                echo $messageProf;
               
                echo '      </div>
									</div>
							  </div>';
            }
            ?>
            <?php
            if ($messageProfSucc) {

                echo '<div class="row">
									<div class="col-md-12">
										<div class="alert alert-success alert-dismissible" role="alert">
											<span class="sr-only">Error:</span>
											<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                echo $messageProfSucc;
              
                echo '      </div>
									</div>
							  </div>';
            }
            ?>
            <div class="row"> 
                <div class="col-md-12">
				
				    <div class="row">
					    <div class="col-md-9">
						<div class="form-group">
							<label for="Favicon">Logo Website *</label>
							  <input id="logo_website" type="file" name="logo_website" accept="image/*" onChange="validate(this)" >
							<p class="help-block">Jpeg/jpg format.</p> 
						</div>
						</div>
						<div class="col-md-3">
						<div class="form-group">
							<div class="widget-user-image">  <a href="#" id="pop2">
							  <img id="imageresource2" class="img-circle" height="60"  width="60" src="<?php echo base_url() ?>assets/images/logo/<?php echo ($dataId->logo_website == "") ? 'noimage.png' : $dataId->logo_website ?>" alt="attachment image">
							  </a>
							</div>  
						</div>
						</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
						    <div class="form-group">
                            <label for="adminResellerName">Nama Website *</label>
                            <input type="text" class="form-control" name="namawebsite" id="namawebsite" placeholder="Nama" value="<?php echo $dataId->nama_website ?>">
							</div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
							<label for="email">Email *</label>
                             <input type="text" class="form-control" name="email" id="email" placeholder="email"  value="<?php echo $dataId->email?>" ><i>Bila lebih dari satu pisahkan dengan tanda (,) koma</i>
                            </div>
                        </div>
                    </div>
					<div class="row">
					    <div class="col-md-9">
						<div class="form-group">
							<label for="Favicon">Favicon *</label>
							  <input id="favicon" type="file" name="favicon" accept="image/*" onChange="validate(this)" >
							<p class="help-block">Jpeg/jpg format.</p> 
						</div>
						</div>
						<div class="col-md-3">
						<div class="form-group">
							<div class="widget-user-image">  <a href="#" id="pop1">
							  <img id="imageresource1" class="img-circle" height="60"  width="60" src="<?php echo base_url() ?>assets/<?php echo ($dataId->favicon == "") ? 'noimage.png' : $dataId->favicon ?>" alt="attachment image">
							  </a>
							</div>  
						</div>
						</div>
                    </div>
					<div class="form-group">
                        <label for="Url">Url *</label>
                        <input type="text" class="form-control" name="url" id="url" placeholder="URL" value="<?php echo $dataId->url ?>">
                    </div>
                    <div class="form-group">
                        <label for="Facebook">Facebook </label>
                        <input type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook" value="<?php echo $dataId->facebook ?>">
                    </div>
					<div class="row">
					    <div class="col-md-6">
						<div class="form-group">
                        <label for="Rekening">Rekening Bank</label>
                        <!-- Trigger the modal with a button -->
						<input type="text" class="form-control" name="rekening" id="rekening" placeholder="Rekening" value="<?php echo $dataId->rekening?>">
						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group">
                        <label for="atasnma">Atas Nama</label>
                        <!-- Trigger the modal with a button -->
						<input type="text" class="form-control" name="atasnama" id="atasnama" placeholder="AN" value="<?php echo $dataId->atasnama?>">
						</div>
						</div>
                    </div>
					
					
                   
					<div class="form-group">
                        <label for="Rekening">Nama Bank </label>
                        <!-- Trigger the modal with a button -->
						<input type="text" class="form-control" name="bank" id="bank" placeholder="nama bank" value="<?php echo $dataId->bank?>">
                    </div>
					<div class="row">
					    <div class="col-md-9">
						<div class="form-group">
							<label for="Favicon">Logo Bank *</label>
							  <input id="logo_bank" type="file" name="logo_bank" accept="image/*" onChange="validate(this)" >
							<p class="help-block">Jpeg/jpg format.</p> 
						</div>
						</div>
						<div class="col-md-3">
						<div class="form-group">
							<div class="widget-user-image">  <a href="#" id="pop1">
							  <img id="imageresource1" class="img-circle" height="60"  width="60" src="<?php echo base_url() ?>assets/images/bank/<?php echo ($dataId->logo_bank == "") ? 'noimage.png' : $dataId->logo_bank ?>" alt="attachment image">
							  </a>
							</div>  
						</div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="Rekening">No Telp * </label>
                        <!-- Trigger the modal with a button -->
						<input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="no_telp" value="<?php echo $dataId->no_telp?>"><i>Bila lebih dari satu pisahkan dengan tanda (,) koma</i>
                    </div>		
					<div class="form-group">
                        <label for="alamat">Alamat * </label>
                        <!-- Trigger the modal with a button -->
						<textarea class="form-control" name="alamat" id="alamat"><?php echo $dataId->alamat?></textarea>
					</div>						
                    <div class="form-group">
                        <label for="meta_deskripsi">Meta Deskripsi </label>
                        <textarea class="form-control" rows="3" placeholder="Meta deskripsi" name="meta_deskripsi" id="meta_deskripsi"><?php echo $dataId->meta_deskripsi?></textarea>
                    </div>
					<div class="form-group">
                        <label for="meta_keyword">Meta keyword </label>
                        <textarea class="form-control" rows="3" placeholder="Meta deskripsi" name="meta_keyword" id="meta_keyword"><?php echo $dataId->meta_keyword?></textarea>
                    </div>
					<div class="form-group">
                        <label for="Rekening">Maps </label>
                        <!-- Trigger the modal with a button -->
						<input type="text" class="form-control" name="maps" id="maps" placeholder="Rekening" value="<?php echo $dataId->maps?>">
                    </div>
					<div class="form-group">
                        <label for="Rekening">Footer *</label>
                        <!-- Trigger the modal with a button -->
						<input type="text" class="form-control" name="footer" id="footer" placeholder="footer" value="<?php echo $dataId->footer?>">
                    </div>
                </div>	
            </div>
            <div class="box-footer">
                Isian dengan tanda * adalah wajib.<br />
                <input type="submit" class="btn btn-primary pull-right" name="update" id="update" value="Simpan">
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="box-footer">
        </div>
    </div>
</section>

<!-- /.content -->
<div class="modal fade" id="imagemodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Favicon</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
                <img src="" id="imagepreview1" style="width: auto; height: auto; " >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- /.content -->
<div class="modal fade" id="imagemodal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Favicon</h4>
            </div>
            <div class="modal-body" style="overflow : auto;">
                <img src="" id="imagepreview2" style="width: auto; height: auto; " >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>

 $(document).ready(function () {
										
	$("#pop1").on("click", function () {
		$('#imagepreview1').attr('src', $('#imageresource1').attr('src'));
		$('#imagemodal1').modal('show');
	});
	$("#pop2").on("click", function () {
		$('#imagepreview2').attr('src', $('#imageresource2').attr('src'));
		$('#imagemodal2').modal('show');
	});
 });


function validate(k) {

        size = k.files[0].size; //858 /879394
		var ext = k.value.split(".");
		ext = ext[ext.length - 1].toLowerCase();
		var arrayExtensions = ["jpg", "jpeg","ico","png"];

		if (arrayExtensions.lastIndexOf(ext) == -1) {
			alert("Hanya boleh JPG atau JPEG.");
			$(k).val("");
		}

		if (size > 5242880)
		//if(size > 5242)
		{
			alert("File tidak boleh melebihi 5 MB");
			$(k).val("");

		}
	}
</script>