<?php

date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.

function seong($id,$tulisan_judul)
{
	$fg = str_replace(" ","-",$tulisan_judul);
	
	return $fg."-".md5($id);
}

function hari_ini()
{	
	$seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
	$hari = date("w");
	return $hari_ini = $seminggu[$hari];
}

function tgl_sekarang()
{
	return $tgl_sekarang = date("Y-m-d");
}

function tgl_skrg()
{	
  return	$tgl_skrg     = date("d");
}

function 	bln_sekarang()
{
 return $bln_sekarang = date("m");
}

function thn_sekarang()
{
   return	$thn_sekarang = date("Y");
}

function jam_sekarang()
{
	$jam_sekarang = date("H:i:s");
}

function nama_bln()
{
return $nama_bln=array(1=> "Januari", "Februari", "Maret", "April", "Mei", 
                    "Juni", "Juli", "Agustus", "September", 
                    "Oktober", "November", "Desember");
}

function tgl_indo($tgl){
			$tanggal = substr($tgl,8,2);
			$bulan = getBulan(substr($tgl,5,2));
			$tahun = substr($tgl,0,4);
			return $tanggal.' '.$bulan.' '.$tahun;		 
}
function tgl_indo2($tgl){
            $ty = date($tgl);
            $new_date = date('Y-m-d', strtotime($ty));
            $tanggal = substr($new_date,8,2);
            $bulan = getBulan(substr($new_date,5,2));
            $tahun = substr($new_date,0,4);
            return $tanggal.' '.$bulan.' '.$tahun;       
}
function tgl_to_jam($tgl){
            $ty = date($tgl); 
            $new_date = date('h:i:s', strtotime($ty));
            return $new_date;       
}   	
//2017-11-02 14:38:30
function tgl_grafik($tgl){
        $tanggal = substr($tgl,8,2);
        $bulan = getBulan(substr($tgl,5,2));
        $tahun = substr($tgl,0,4);
        return $tanggal.'_'.$bulan;       
}  

function getBulan($bln){
				switch ($bln){
					case 1: 
						return "Jan";
						break;
					case 2:
						return "Feb";
						break;
					case 3:
						return "Mar";
						break;
					case 4:
						return "Apr";
						break;
					case 5:
						return "Mei";
						break;
					case 6:
						return "Jun";
						break;
					case 7:
						return "Jul";
						break;
					case 8:
						return "Agu";
						break;
					case 9:
						return "Sep";
						break;
					case 10:
						return "Okt";
						break;
					case 11:
						return "Nov";
						break;
					case 12:
						return "Des";
						break;
				}
} 
function DateToIndo($date) {
    // fungsi atau method untuk mengubah tanggal ke format indonesia
    // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
    $BulanIndo = array("Januari", "Februari", "Maret",
        "April", "Mei", "Juni",
        "Juli", "Agustus", "September",
        "Oktober", "November", "Desember");

    $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
    $bulan = substr($date, 5, 2); // memisahkan format bulan menggunakan substring
    $tgl = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring

    $result = $tgl . " " . $BulanIndo[(int) $bulan - 1] . " " . $tahun;
    return($result);
}

//selisih hari
function JumSelisihHari($tglStart, $tglEnd) {
    $tgl1 = $tglStart;  // 1 Oktober 2009 ==> tglStart
    $tgl2 = $tglEnd;  // 10 Oktober 2009 ==> tglEnd
    // memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
    // dari tanggal pertama

    $pecah1 = explode("-", $tgl1);
    $date1 = $pecah1[2];
    $month1 = $pecah1[1];
    $year1 = $pecah1[0];

    // memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
    // dari tanggal kedua

    $pecah2 = explode("-", $tgl2);
    $date2 = $pecah2[2];
    $month2 = $pecah2[1];
    $year2 = $pecah2[0];

    // menghitung JDN dari masing-masing tanggal
    $jd1 = GregorianToJD($month1, $date1, $year1);
    $jd2 = GregorianToJD($month2, $date2, $year2);

    // hitung selisih hari kedua tanggal
    $selisih = $jd2 - $jd1;

    //echo "Selisih kedua tanggal adalah ".$selisih." hari";
    return $selisih;
}

function search_arrx($id, $typeTruk2) {
    $i = 1;
    $re = false;
    foreach ($typeTruk2 as $x) {

        if ($id == $x['transtype_id']) {
            $re = true;
            break;
        }
        $i++;
    }
    return $re;
}
function pagination($get)
{
  
	$ci = & get_instance();
    $ci->load->database();
	
  $ci->load->model("Tarif_model","tarif");
  $sql = $ci->tarif->getTarifDepanSql($get);
  $num = $ci->db->query($sql)->num_rows();
	
  $kotasal    = $get['kotaasal'];
  $kotatujuan = $get['kotatujuan'];
  $cari = !empty($get['cari']) ? $get['cari'] : ".html" ;
  //echo "kaprok ".$cari; exit;
  $p = isset($get['page']) ? $get['page'] : 0;
  $l = isset($get['limit']) ? $get['limit'] : 0;
  
  $gh = ($num/10);
  
  $p1 =  $p-1;
  $l1 =  10;//+$l;
  
  $p2 =  $p+1;
  //$l2 =  $num-10;//+$l;
  $l2 =  10;//+$l;
  
  if(!empty($get['kotaasal']) or !empty($get['kotatujuan']) )
  {	   
	  $link1 = "tarif-lengkap-".$kotasal."-".$kotatujuan."-".$p1."-".$l1."-".$cari."";	
	  $link2 = "tarif-lengkap-$kotasal-$kotatujuan-$p2-$l2-$cari";	
  }
  else
  {
	  $link1 = "tarif-lengkap-".$p1."-".$l1."-".$cari."";	
      $link2 = "tarif-lengkap-$p2-$l2-$cari";
  }
  
  $sty1 ="";
  $sty2 ="";
  
  if( $p1  < 0 )
  {
	   $sty1 = 'style="pointer-events: none;
		cursor: default;"';
  }	  
 
  //if( $p2  > 0 )
  //{
	//   $sty2 = 'style="pointer-events: none;
	//	cursor: default;"';
  //}	 
 
  $r = '<ul class="pagination pagination-lg">
                        <li><a href="'.base_url().''.$link1.'" '.$sty1.'><i class="fa fa-long-arrow-left"></i>Previous Page</a></li>
                      
                        <li><a href="'.base_url().''.$link2.'"  '.$sty2.' >Next Page<i class="fa fa-long-arrow-right"></i></a></li>
                    </ul>';
	return $r;					
}
/* function pagination($get)
{
  //tarif-lengkap.html?kotaasal=jakarta&kotatujuan=bandung&p=0&l=10&get=Cari	
  $r = '<ul class="pagination pagination-lg">
                        <li><a href="#"><i class="fa fa-long-arrow-left"></i>Previous Page</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">Next Page<i class="fa fa-long-arrow-right"></i></a></li>
                    </ul>';
	return $r;					
} */

function kirimEmail_XX($emailto,$dataEmail,$nama_pengirim)
{
	/* Kirim EMAIL */
	
	 $ci = & get_instance();
	 $ci->load->database();
	 $r = $ci->db->query("
						select
						email
						from 
						identitas
						where 
						id_identitas='1'")->row(); 
		
		  $from = $r->email;
	 
	
	$ci->load->library('sendmail');
	//$sendmail2 = $ci->sendmail->send('PastExpress | Order Baru', array("to" => emailTimtransport()), $dataEmail, 'email/order_create');
	$sendmail3 = $ci->sendmail->send('PastExpress | Order Baru', array("to" => $emailto ), $dataEmail, 'email/order_create');
	  
	
	/* Kirim Email */
					
					
}

function kirimEmail($emailto,$dataEmail,$nama_pengirim)
{
	
	
	 $ci = & get_instance();
	 $ci->load->database();
	 $r = $ci->db->query("
						select
						email
						from 
						identitas
						where 
						id_identitas='1'")->row(); 
		
		  $from = $r->email;
	 
	
	$to = "$emailto,$from";
	$subject = "Order Baru";

	$message = $dataEmail;
	
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// More headers
	//$headers .= 'From: <'.$from.'>' . "\r\n";
	$headers .= 'From: <putraartasejahtera@gmail.com>' . "\r\n";
	//$headers .= 'Cc: '.$from.'' . "\r\n";
    $headers .= 'Cc: '.$from.'' . "\r\n";
	mail($to,$subject,$message,$headers);
	
}
function kirimEmail23($emailto,$dataEmail,$nama_pengirim)
{
	
		  $ci = & get_instance();
		  $ci->load->database();
		  $r = $ci->db->query("
						select
						email
						from 
						identitas
						where 
						id_identitas='1'")->row(); 
		
		  $from = $r->email;
		
	
		$ci->load->library('email');
		
		$to = $emailto;    
		$subject = "Order Baru Pengiriman Mobil PastExpress Dari $nama_pengirim";
		$data=array();
		// $mesg = $this->load->view('template/email',$data,true);
		// or
		$txt = $dataEmail;

		$config=array(
			'charset'=>'utf-8',
			'wordwrap'=> TRUE,
			'mailtype' => 'html'
		);

		$ci->email->initialize($config);

		$ci->email->to($to);
		$ci->email->from($from, "Pengiriman Mobil: PT. PUTRA ARTA SEJAHTERA");
		$ci->email->subject($subject);
		$ci->email->message($txt);
		$mail = $ci->email->send();
	
	
}

function kirimEmail2($emailto,$dataEmail,$nama_pengirim)
{
	//ini_set( 'display_errors', 1 );   
    //error_reporting( E_ALL );    
    $ci = & get_instance();
    $ci->load->database();
	$r = $ci->db->query("
						select
						email
						from 
						identitas
						where 
						id_identitas='1'")->row(); 
	
	$from = $r->email;
	$to = $emailto;    
	/* $from = $r->email;    
    $to = $emailto;    
    $subject = "Order Baru Pengiriman Mobil PastExpress";    
    $message = $dataEmail;   
    $headers = "From:" . $from;    
    mail($to,$subject,$message, $headers);   */  
	
	
	//$to = "somebody@example.com";
    $subject = "Order Baru Pengiriman Mobil PastExpress Dari $nama_pengirim";
    $txt = $dataEmail;
    $headers = "From: ".$from."" . "\r\n" .
    "CC: ".$from."";

mail($to,$subject,$txt,$headers);
	
    //echo "Pesan email sudah terkirim.";
}
function limit_words($string, $word_limit){
                $words = explode(" ",$string);
                return implode(" ",array_splice($words,0,$word_limit));
}
                
