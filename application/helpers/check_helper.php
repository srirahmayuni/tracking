<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('need_login')) {

    function need_login() {
        //redirect("/set/myauth/index/return/" . uri_string(), 'refresh', 401);
        redirect("/myauth/index/return/" . uri_string(), 'refresh', 401);
        exit(0);
    }

}

if (!function_exists('access_denied')) {

    function access_denied() {
        //show_error('Access denied !', 403);
        //redirect('admin/Myerror/err403');

        $CI = &get_instance();

        // if ($CI->session->userdata('group_id') == '1' ||
                // $CI->session->userdata('group_id') == '2' ||
                // $CI->session->userdata('group_id') == '3' ||
                // $CI->session->userdata('group_id') == '7' ||
                // $CI->session->userdata('group_id') == '8') 
		    if ($CI->session->userdata('group_id'))
			{
            //echo "Anda masih punya login= " . $this->session->userdata('group_id');exit;
            //echo "<pre>";
            //echo var_dump($this->session->all_userdata());exit;
            redirect('/home/index');
			$authorization = $CI->session->userdata('authorization');
            /* $authorization = $CI->session->userdata('authorization');
            if ($authorization) {
                //redirect('set/home/index');
                redirect('home/index');
            } else {
                // echo "mau masuk ya";exit;
                //$CI->session->set_flashdata('message', 'Anda baru saja logout dari Frontend, silahkan login!');
                $CI->session->set_flashdata('message', 'Anda baru saja logout , silahkan login!');
                $CI->session->unset_userdata('logged_in');
                redirect('Myauth');
            } */
        } else {
            $CI->session->set_flashdata('message', 'Anda tidak berhak masuk!');
            $CI->session->unset_userdata('logged_in');
            redirect('Myauth');
        }
    }

}

if (!function_exists('is_authorized')) {

    function is_authorized($page, $action) {
        $CI = &get_instance();

        $result = FALSE;
        $authorization = $CI->session->userdata('authorization');
        if (isset($authorization[$page][$action])) {
            $result = $authorization[$page][$action] == TRUE;
        }

        return $result;
    }

}

if (!function_exists('siteAdminUrl')) {

    /**
     * Site URL
     *
     * Create a local URL based on your basepath. Segments can be passed via the
     * first parameter either as a string or an array.
     *
     * @param	string	$uri
     * @param	string	$protocol
     * @return	string
     */
    function siteAdminUrl($uri = '', $protocol = NULL) {
        //return get_instance()->config->site_url('set/' . $uri, $protocol);
        return get_instance()->config->site_url($uri, $protocol);
    }

}

if (!function_exists('base_url_admin')) {

    /**
     * Site URL
     *
     * Create a local URL based on your basepath. Segments can be passed via the
     * first parameter either as a string or an array.
     *
     * @param	string	$uri
     * @param	string	$protocol
     * @return	string
     */
     function base_url_admin($uri = '', $protocol = NULL) {
        return get_instance()->config->site_url('set');
     }

}

    
	
	function title(){
		
		return '';
		
	}
	
	
	function footerbawah(){
		 // $t = & get_instance();
         // $t->load->database();
		 // $id = 1;
		 // $sql = "select * from identitas where id_identitas='$id'";
		 // $result = $t->db->query($sql);
         // return $result->row()->footer;
		
		
	}
	function pesanBelumDibaca(){
		
	// $ci = & get_instance();
    // $ci->load->database();
	// $id = $ci->session->userdata('users_id');
	// $r = $ci->db->query("
        // select a.* 
        // from hubungi a
		// left join users on users.users_id = a.created_by
        // where  a.created_by ='$id' and a.dibaca='N'")->num_rows();
	 // return $r;	
	} 

    function getID1B() {
    // $reseller_id = NULL;

    // $ci = & get_instance();
    // $ci->load->database();

     // $numb=time();
     // $rand  = rand(1,99999);
     

     // $df = "ORD" . $rand .$numb;
	 // $r = $ci->db->query("
        // select *
        // from data_order 
		// where  ID ='$df' ")->num_rows();
	 
	 // if($r > 0)
     // {
		 // getID1B();
	 // }
     // else
     // {
         // return $df;
     // } 
 
	 
	}
	function getID2B($id) {
		//$reseller_id = NULL;
		$ci = & get_instance();
		$ci->load->database();
		$numb=time();
		$ra= rand(1,99999);
		$df = "RES".$ra.'0'.$numb;
		$r = $ci->db->query("
        select *
        from data_order_detail 
		where  no_resi ='$df' ")->num_rows();
	 
		 if($r > 0)
		 {
			 getID2B();
		 }
		 else
		 {
			 return $df;
		 } 
	}
    function anti_injection2($val) {
    $db = get_instance()->db->conn_id;
    $val = mysqli_real_escape_string($db, $val);
    return $val;
	}

	function anti_injection($data){
	 
	  $koneksi = koneksi_php_biasa();
	  $filter = mysqli_real_escape_string($koneksi, stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
	  
	  return $filter;
	
	}

    function koneksi_php_biasa()
    {
		  $ci = & get_instance();
		  $db['host'] = $ci->load->database()['default']['hostname']; //host
		  $db['user'] = $ci->load->database()['default']['username'];//username database
		  $db['pass'] = $ci->load->database()['default']['password']; //password database
		  $db['name'] = $ci->load->database()['default']['database']; //nama database
			 
		  $koneksi = mysqli_connect($db['host'], $db['user'], $db['pass'], $db['name']);
		  return $koneksi;
	}	
	
	function  filterBygroup1()
	{
		$ci = & get_instance();
		$grp = $ci->session->userdata('groupName');
		if($grp=="ADMIN")
		{
			$where = "";
		}
		else
		{
			$where = "where a.owner_program='$grp' ";
			//$where = "";
		}	
		return $where;
	}
	
	function  filterBygroup122()
	{
		$ci = & get_instance();
		$grp = $ci->session->userdata('groupName');
		if($grp=="ADMIN")
		{
			$where = "";
		}
		else
		{
			$where = "and a.owner_program='$grp' ";
			//$where = "";
		}	
		return $where;
	}
	
	
	function  filterBygroup12()
	{
		$ci = & get_instance();
		$grp = $ci->session->userdata('groupName');
		if($grp=="ADMIN")
		{
			$where = "";
		}
		else
		{
			$where = "and a.owner_program='$grp' ";
		}	
		return $where;
	}
	
	function  filterBygroup_summary()
	{
		$ci = & get_instance();
		$grp = $ci->session->userdata('groupName');
		if($grp=="ADMIN")
		{
			$where = "";
		}
		else
		{
			$where = "where a.owner_program='$grp' and a.group not in('network')";
		}	
		return $where;
	}
	
	function  filterBygroup2()
	{
		$ci = & get_instance();
		$grp = $ci->session->userdata('groupName');
		if($grp=="ADMIN")
		{
			$where = "";
		}
		else
		{
			$where = "and a.owner_program='$grp'";
		}
        return $where;		
	}
	
	function array_regional()
	{
		return array("[R01]"=>"Sumbagut",
		             "[R02]"=>"Sumbagsel",
		             "[R03]"=>"Jabotabek",
		             "[R04]"=>"Jabar",
		             "[R05]"=>"Jateng",
		             "[R06]"=>"Jatim",
		             "[R07]"=>"Bali Nusra",
		             "[R08]"=>"Kalimantan",
		             "[R09]"=>"Sulawesi",
		             "[R10]"=>"Sumbagteng",
		             "[R11]"=>"PUMA"
		             );
	}
	
	